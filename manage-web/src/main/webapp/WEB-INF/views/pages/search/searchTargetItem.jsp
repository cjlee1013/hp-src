<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%-- 검색관리 테이블 설정 공통 CSS --%>
<link rel="stylesheet" type="text/css" href="/static/css/searchCommon.css" />

<div class="com wrap-title">
    <form id="searchForm" onsubmit="return false;">
        <div class="com wrap-title has-border">
            <h2 class="title font-malgun">검색대상 상품 조회</h2>
        </div>
        <div class="com wrap-title sub">
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>검색대상 상품 조회</caption>
                    <colgroup>
                        <col width="7%">
                        <col width="59%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>점포</th>
                        <td>
                            <div class="ui form inline">
                                <select id="storeType" name="storeType" class="ui input middle left-float mg-r-5 select-style">
                                    <option value="HYPER">HYPER</option>
                                    <option value="EXP">EXP</option>
                                    <option value="DS">DS</option>
                                </select>
                                <label class="ui radio small inline"><input type="radio" name="storeSearchType"  value="DIRECT" checked><span>직접입력</span></label>
                                <label class="ui radio small inline"><input type="radio" name="storeSearchType"  value="SEARCH"><span>조회</span></label>
                                <input type="text" id="storeIds" name="storeId" value="37" class="ui input medium mg-r-5" style="width: 100px;" placeholder="점포ID">
                                <input type="text" id="schStoreNm" name="schStoreNm" class="ui input medium mg-r-5" style="width: 150px;display:none;" placeholder="점포명" readonly>
                                <button type="button" id="schStoreBtn" class="ui button medium" style="display:none;" onclick="targetItemJs.getStorePop('targetItemJs.callBack');" >조회</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="2">
                            <div class="ui form mg-l-30 vertical-button-style">
                                <button type="submit" id="schBtn" class="ui button large cyan " onclick="targetItemJs.getItemList();">검색</button><br>
                                <button type="button" id="schResetBtn" class="ui button large white mg-t-5" onclick="targetItemJs.setInit();" >초기화</button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>상품번호</th>
                        <td>
                            <div id="schInputBox" name="schInputBox" style="display: inline">
                                <input name="itemNo" type="text" class="ui input medium mg-r-5 search-input-style-small" maxlength="20">
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </form>

    <div id="no_search_result" style="display: none;">
        <div class="com wrap-title sub">
            <h3 class="title">• 조회결과가 없습니다.</h3>
        </div>
    </div>
    <div id="search_result" style="display: block;">
        <div class="com wrap-title sub">
            <h3 class="title">• 상품 기본 정보</h3>
        </div>
        <div class="topline"></div>
        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table" >
                <colgroup>
                    <col width="7%">
                    <col width="22%">
                    <col width="7%">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>상품번호</th>
                    <td id="itemNo"></td>

                    <th rowspan="4">상품이미지</th>
                    <td rowspan="4"><img id="img" src=""></td>
                </tr>
                <tr>
                    <th>상품 상태 <span style="font-weight: bold; font-size: 15px;" class="text-red">*</span></th>
                    <td id="itemStatus"></td>
                </tr>
                <tr>
                    <th>노출여부 <span style="font-weight: bold; font-size: 15px;" class="text-red">*</span></th>
                    <td id="dispYn"></td>
                </tr>
                <tr>
                    <th>온라인 취급여부 <span style="font-weight: bold; font-size: 15px;" class="text-red">*</span></th>
                    <td id="onlineYn"></td>
                </tr>
                <tr>
                    <th>상품명</th>
                    <td colspan="3" id="itemNm"></td>
                </tr>
                <tr>
                    <th>카테고리 <span style="font-weight: bold; font-size: 15px;" class="text-red">*</span></th>
                    <td colspan="3" id="category"></td>
                </tr>
                <tr id="trExpCategory">
                    <th>익스프레스 카테고리 <span style="font-weight: bold; font-size: 15px;" class="text-red">*</span></th>
                    <td colspan="3" id="expCategory"></td>
                </tr>
                <tr>
                    <th>장바구니제한</th>
                    <td colspan="3" id="cartLimitYn"></td>
                </tr>
                <tr>
                    <th>대표이미지 <span style="font-weight: bold; font-size: 15px;" class="text-red">*</span></th>
                    <td colspan="3" id="imgMainYn"></td>
                </tr>
                <tr id="trOptSelInfo">
                    <th>용도옵션 / 리스트 분할 노출</th>
                    <td colspan="3" id="optSelInfo"></td>
                </tr>
                <tr>
                    <th>성인상품</th>
                    <td colspan="3" id="adultInfo"></td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 상품 판매 정보 <span id="nowDate"></span></h3>
        </div>
        <div class="topline"></div>
        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table" >
                <colgroup>
                    <col width="7%">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>판매기간 <span style="font-weight: bold; font-size: 15px;" class="text-red">*</span></th>
                    <td id="saleDateInfo"></td>
                </tr>
                <tr>
                    <th>예약판매</th>
                    <td id="rsvInfo"></td>
                </tr>
                <tr>
                    <th>재고정보 <span style="font-weight: bold; font-size: 15px;" class="text-red">*</span></th>
                    <td id="stockInfo"></td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 점포 정보</h3>
        </div>
        <div class="topline"></div>
        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table" >
                <colgroup>
                    <col width="7%">
                    <col width="22%">
                    <col width="7%">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>점포ID (점포구분)</th>
                    <td id="storeId"></td>
                    <th>점포명</th>
                    <td id="storeNm"></td>
                </tr>
                <tr>
                    <th>판매가격 / 할인가격</th>
                    <td colspan="3" id="priceInfo"></td>
                </tr>
                <tr>
                    <th>온라인 취급상태 (PFR) <span id="pfrYnEssential" style="font-weight: bold; font-size: 15px;" class="text-red">*</span></th>
                    <td id="pfrYn"></td>
                    <th>가상점 단독판매 <span style="font-weight: bold; font-size: 15px;" class="text-red">*</span></th>
                    <td id="virtualStoreOnlyYn"></td>
                </tr>
                <tr id="trStoreInfo">
                    <th>점포 사용여부 <span style="font-weight: bold; font-size: 15px;" class="text-red">*</span></th>
                    <td id="storeUseYn"></td>
                    <th>점포 온라인여부 <span style="font-weight: bold; font-size: 15px;" class="text-red">*</span></th>
                    <td id="storeOnlineYn"></td>
                </tr>
                <tr>
                    <th>취급중지 <span style="font-weight: bold; font-size: 15px;" class="text-red">*</span></th>
                    <td colspan="3" id="stopDealInfo"></td>
                </tr>
                <tr id="trShipTypeInfo">
                    <th>배송방식 <span style="font-weight: bold; font-size: 15px;" class="text-red">*</span></th>
                    <td colspan="3" id="shipTypeInfo"></td>
                </tr>
                <tr>
                    <th>배송정책</th>
                    <td colspan="3" id="shipPolicyInfo"></td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 상품 기타 정보</h3>
        </div>
        <div class="topline"></div>
        <div class="ui wrap-table horizontal mg-t-10">
            <table class="ui table" >
                <colgroup>
                    <col width="7%">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr id="trItemGroupInfo">
                    <th>묶음상품</th>
                    <td id="itemGroupInfo"></td>
                </tr>
                <tr>
                    <th>스티커</th>
                    <td id="stickerInfo"></td>
                </tr>
                <tr>
                    <th>기획전/이벤트</th>
                    <td id="promoInfo"></td>
                </tr>
                <tr id="trEventInfo">
                    <th>행사</th>
                    <td id="eventInfo"></td>
                </tr>
                <tr id="trCrossOutYnInfo">
                    <th>심플행사 이중가격</th>
                    <td id="crossOutYn"></td>
                </tr>
                <tr id="trRelationInfo">
                    <th>연관상품</th>
                    <td id="relationInfo"></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript" src="/static/js/search/searchTargetItem.js?${fileVersion}"></script>
<script>
    $(document).ready(function() {
        CommonAjaxBlockUI.global();
        targetItemJs.setInit();
    });
</script>