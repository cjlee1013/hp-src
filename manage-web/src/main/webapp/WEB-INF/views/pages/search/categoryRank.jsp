<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<%-- 검색관리 테이블 설정 공통 CSS --%>
<link rel="stylesheet" type="text/css" href="/static/css/searchCommon.css" />
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">카테고리 랭킹 관리</h2>
    </div>
    <div class="com wrap-title sub">
        <div id="groupSearch" class="ui wrap-table horizontal mg-t-10">
            <form id="searchForm" onsubmit="return false;">
                <table class="ui table">
                    <caption>검색어</caption>
                    <colgroup>
                        <col width="7%">
                        <col width="22%">
                        <col width="7%">
                        <col width="30%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>검색어</th>
                        <td colspan="3">
                            <div class="ui form inline" >
                                <select id="schSearchType" name="schSearchType" class="ui input medium mg-r-10 select-style">
                                    <option value="KEYWORD" selected="selected">키워드</option>
                                    <option value="CATEGORYID">카테고리번호</option>
                                </select>
                                &nbsp;
                                <input id="searchKeyword" name="searchKeyword" type="text" class="ui input medium mg-r-5 search-input-style-small" maxlength="16">
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="2">
                            <div class="ui form mg-l-30 vertical-button-style">
                                <button type="submit" id="searchBtn" class="ui button large cyan">검색</button><br/>
                                <button type="button" id="excelDownloadBtn" class="ui button large dark-blue mg-t-5">엑셀다운</button><br>
                                <button type="button" id="resetFlagBtn" class="ui button large mg-t-5">동기화</button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>사용여부</th>
                        <td>
                            <select id="schUseFlag" name="schUseFlag" class="ui input medium mg-r-10 select-style">
                                <option value="" selected="selected">전체</option>
                                <option value="1">사용</option>
                                <option value="0">미사용</option>
                            </select>
                        </td>
                        <th>재설정대상</th>
                        <td>
                            <select id="schResetFlag" name="schResetFlag" class="ui input medium mg-r-10 select-style">
                                <option value="" selected="selected">전체</option>
                                <option value="1">대상</option>
                                <option value="0">대상아님</option>
                            </select>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과: <span id="dictionaryTotalCount">0</span>건</h3>
        </div>
        <div class="topline"></div>
        <div class="mg-b-20 " id="categoryRankGrid" style="height: 400px; width:100%"></div>
    </div>


    <div class="com wrap-title sub">
        <h3 class="title">• 등록/수정</h3>
    </div>
    <div class="topline"></div>

    <div class="ui wrap-table horizontal mg-t-10" style="width:100%;">
        <form id="categorySetForm" onsubmit="return false;">
            <table class="ui table">
                <caption>카테고리랭킹 등록/수정</caption>
                <colgroup>
                    <col width="7%">
                    <col width="22%">
                    <col width="7%">
                    <col width="*">
                </colgroup>
                <tbody>
                <tr>
                    <th>검색어</th>
                    <td>
                        <div class="ui form inline">
                            <input id="keyword" readonly="readonly" name="keyword" type="text" class="ui input medium mg-r-5 search-input-style-small" maxlength="15">
                            <span class="text">( <span style="color:red;" id="textCountKr_keyword">0</span> / 15자 )</span>
                        </div>
                    </td>
                    <th>점수</th>
                    <td>
                        <select class="ui input medium mg-r-5 select-style" id="boostType" name="boostType"
                                data-default="1">
                            <c:forEach var="arr" items="${scoreList}" varStatus="status">
                                <option value="${arr}">${arr}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th>카테고리 <span class="text-red">*</span></th>
                    <td colspan="3">
                        <div class="ui form inline">
                            <select class="ui input medium mg-r-5 select-style" id="searchCateCd1" name="schLcateCd"
                                    data-default="대분류"
                                    onchange="javascript:commonCategory.changeCategorySelectBox(1,'searchCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5 select-style" id="searchCateCd2" name="schMcateCd"
                                    data-default="중분류"
                                    onchange="javascript:commonCategory.changeCategorySelectBox(2,'searchCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5 select-style" id="searchCateCd3" name="schScateCd"
                                    data-default="소분류"
                                    onchange="javascript:commonCategory.changeCategorySelectBox(3,'searchCateCd');">
                            </select>
                            <select class="ui input medium mg-r-5 select-style" id="searchCateCd4" name="schDcateCd"
                                    data-default="세분류">
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>사용여부</th>
                    <td>
                        <label class="ui radio small inline"><input type="radio" name="useFlag" value="1" checked><span>사용</span></label>
                        <label class="ui radio small inline"><input type="radio" name="useFlag" value="0"><span>미사용</span></label>
                    </td>
                </tr>
                </tbody>
            </table>
        </form>
    </div>
    <br>
    <div class="topline"></div>
    <div class="mg-b-20 " id="categoryRankRelationGrid" style="height: 250px; width:100%"></div>
    <div class="ui center-button-group mg-t-15" style="width:100%;">
        <span class="inner">
            <button type="button" class="ui button xlarge cyan font-malgun" id="saveBtn">저장</button>
            <button type="button" class="ui button xlarge white font-malgun" id="modBtn">수정</button>
            <button type="button" class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>
</div>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/search/categoryRank.js?${fileVersion}"></script>
<!-- script -->
<script type="text/javascript">
    ${categoryRankGridBaseInfo}
    ${categoryRankRelationGridBaseInfo}


    $(document).ready(function() {
        categoryRankJs.init();
        categoryRankJs.event();
        categoryRankGrid.init();
        categoryRankRelationGrid.init();
        categoryRankJs.onload();
        CommonAjaxBlockUI.global();
    });
</script>