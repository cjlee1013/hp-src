<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/tiles/template/default/header.jsp"/>
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp"/>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<%-- 검색관리 테이블 설정 공통 CSS --%>
<link rel="stylesheet" type="text/css" href="/static/css/searchCommon.css" />
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/search/Chart.min.js?v=${fileVersion}"></script>

<div style="display: none">
    <input type="radio" name="siteTypeRadio" value="HOME" checked/>
</div>

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">검색증감률</h2>
    </div>

    <div class="com wrap-title sub">
        <div class="ui wrap-table horizontal mg-t-10">
            <form id="increaseRateForm" onsubmit="return false;">
                <input type="hidden" id="dateType" name="dateType" />
                <input type="hidden" id="siteType" name="siteType" value="home"/>
                <table class="ui table">
                    <caption>검색추이</caption>
                    <colgroup>
                        <col width="7%">
                        <col width="59%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>조회기간</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="startDt" name="startDt" class="ui input medium mg-r-5 date-input-style" placeholder="시작일">
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="endDt" name="endDt" class="ui input medium mg-r-5 date-input-style" placeholder="종료일">
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" id="dayBtn" class="ui button small cyan font-malgun mg-r-5" onclick="increaseRateJs.initSchDate('day');">일간</button>
                                <button type="button" id="weekBtn" class="ui button small gray-dark font-malgun mg-r-5" onclick="increaseRateJs.initSchDate('week');">주간</button>
                                <button type="button" id="monthBtn" class="ui button small gray-dark font-malgun mg-r-5" onclick="increaseRateJs.initSchDate('month');">월간</button>
                            </div>
                        </td>
                        <!-- 검색 버튼 -->
                        <td>
                            <div class="ui form mg-l-30 vertical-button-style">
                                <button type="button" id="searchBtn" class="ui button large cyan" onclick="increaseRateJs.searchBtn()">검색</button><br/>
                                <button type="button" id="searchResetBtn" class="ui button large mg-t-5" onclick="increaseRateJs.initBtn()">초기화</button><br/>
                                <button type="button" id="searchExcelBtn" class="ui button large dark-blue mg-t-5" onclick="increaseRateJs.excelBtn();">엑셀다운</button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
</div>
<div id="monthSearchStatic" style="display: none;"><br/>
    • 전월 전체 검색 횟수 : <span class="text" name="totalCount"></span><br/>
    • 전월 성공 검색 횟수 : <span class="text" name="successCount"></span>
                    <span class="text" name="successRate"></span><br/>
    • 전월 실패 검색 횟수 : <span class="text" name="failureCount"></span>
                    <span class="text" name="failureRate"></span><br/>
    • 전월 통계제외 (어뷰징,크롤링) 검색 횟수 : <span class="text" name="exceptionCount"></span>
                                    <span class="text" name="exceptionRate"></span>
</div>
<div style="width:90%;">
    <div class="chart-container" style="text-align: center;">
        <canvas id="myChart" height="10vh" width="80vw"></canvas>
    </div>
</div>
<div class="com wrap-title sub">
    <h3 class="title">• 검색결과</h3>
</div>
<div class="mg-b-20" id="increaseRateGrid" style="width: 100%; height: 200px;"></div>
<div>
    • 전일대비 증감률(%) = (현재일 - 전일) / 전일 * 100<br/>
    • 전주대비 증감률(%) = (현재주 - 전주) / 전주 * 100<br/>
    • 전월대비 증감률(%) = (현재월 - 전월) / 전월 * 100
</div>

<script type="text/javascript" src="/static/js/search/searchLogIncreaseRate.js?${fileVersion}"></script>
<script>
    ${increaseRateGridBaseInfo}
    $(document).ready(function() {

        CommonAjaxBlockUI.global();

        /** Grid Init **/
        increaseRateGrid.init();

        /** Process Init **/
        increaseRateJs.init();

        /** 사이트 타입 클릭 이벤트 **/
        $('input:radio[name=siteTypeRadio]').click(function(){
            $('#siteType').val($(this).val());
        });
    });
</script>