<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/views/pages/common/froala.jsp" />
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<%-- 검색관리 테이블 설정 공통 CSS --%>
<link rel="stylesheet" type="text/css" href="/static/css/searchCommon.css" />
<script type="text/javascript" src="/static/js/common/commonCategory.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/jslib/realGridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>


<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">사전배포 관리</h2>
    </div>


    <div class="com wrap-title sub">
        <div id="groupSearch" class="ui wrap-table horizontal mg-t-10">
            <form id="deployForm" onsubmit="return false;" onKeydown="javascript:if(event.keyCode == 13)  deploy.search();">
                <table class="ui table">
                    <caption>배포설정</caption>
                    <colgroup>
                        <col width="7%">
                        <col width="59%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>배포서비스</th>
                        <td>
                            <select id="schServiceCd" name="schServiceCd" class="ui input medium mg-r-10 select-style">
                                <option value="HOMEPLUS" selected="selected">통합검색</option>
                                <option value="ADDRESS">주소검색</option>
                                <option value="SUGGEST">자동완성</option>
                            </select>
                        </td>
                        <!-- 검색 버튼 -->
                        <td rowspan="3">
                            <div class="ui form mg-l-30 vertical-button-style">
                                <button type="button" id="dictionaryBuildBtn" class="ui button large cyan large">빌드</button><br/>
                                <button type="button" id="dictionaryDeployBtn" class="ui button large dark-blue mg-t-5">배포</button>
                            </div>
                        </td>
                    </tr>
                    <tr >
                        <th>배포대상</th>
                        <td>
                            <div class="ui form inline" >
                                <select id="schAppType" name="schAppType" class="ui input medium mg-r-10 select-style">
                                    <option value="API" selected="selected">API</option>
                                    <option value="NODE">NODE</option>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>사전유형</th>
                        <td >
                            <div class="ui form inline" >
                                <select id="deployDictType" name="deployDictType" class="ui input medium mg-r-10 select-style">
                                    <option value="MORPH" selected="selected">어휘사전</option>
                                    <option value="ISQ">검색품질사전</option>
                                </select>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="com wrap-title sub">
            <h3 class="title">• 배포리스트</h3>
        </div>

        <div class="topline"></div>
        <div class="mg-b-20 " id="deployGrid" style="height: 450px;"></div>
    </div>

    <div class="com wrap-title sub">
        <h3 class="title">• 등록/수정</h3>
    </div>
    <div class="topline"></div>

    <div class="com wrap-title sub">
        <div class="ui wrap-table horizontal mg-t-10">
            <form id="deplpySetForm" onsubmit="return false;">
                <input type="hidden" id="seqNo" name="seqNo"/>
                <table class="ui table">
                    <caption>등록/수정</caption>
                    <colgroup>
                        <col width="7%">
                        <col width="22%">
                        <col width="7%">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>서비스</th>
                        <td>
                            <select id="serviceCd" name="serviceCd" class="ui input medium mg-r-10 select-style">
                                <option value="HOMEPLUS">통합검색</option>
                                <option value="ADDRESS">주소검색</option>
                                <option value="SUGGEST">자동완성</option>
                            </select>
                        </td>
                        <th>구성</th>
                        <td>
                            <select id="appType" name="appType" class="ui input medium mg-r-10 select-style">
                                <option value="NODE">NODE</option>
                                <option value="API">API</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>내부도메인</th>
                        <td>
                            <input id="privateDomain" name="privateDomain" type="text" class="ui input medium mg-r-5 search-input-style-small" maxlength="50">
                        </td>
                        <th>사용여부</th>
                        <td>
                            <select id="useYn" name="useYn" class="ui input medium mg-r-10 select-style">
                                <option value="Y">Y</option>
                                <option value="N">N</option>
                            </select>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
    <div class="ui center-button-group mg-t-15">
        <span class="inner">
            <button class="ui button xlarge cyan font-malgun" id="setBtn">저장</button>
            <button class="ui button xlarge white font-malgun" id="resetBtn">초기화</button>
        </span>
    </div>
</div>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/search/deploy.js?${fileVersion}"></script>
<!-- script -->
<script type="text/javascript">
    ${deployGridBaseInfo}
    CommonAjaxBlockUI.global();
</script>