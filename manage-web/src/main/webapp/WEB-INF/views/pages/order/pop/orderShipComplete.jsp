<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<div class="com wrap-popup system">
    <div class="com wrap-title">
        <h2 class="title font-malgun">
            <i class="fa fa-square mg-r-5"></i> 배송완료
        </h2>
    </div>
    <div class="com wrap-title mg-t-10 mg-b-10" style="padding: 10px;">
        <div>
            <ul class="com-list-box em">
                <li>※ 배송 중 건에 대해서 배송완료가 가능합니다.</li>
            </ul>
        </div>
    </div>
    <div id="orderShipCompleteGrid" style="width: 100%; height: 230px;"></div>
    <div class="ui center-button-group">
        <span class="inner">
            <button id="saveBtn" class="ui button xlarge btn-danger font-malgun">저장</button>
            <button id="closeBtn" class="ui button xlarge font-malgun" onclick="self.close()">취소</button>
        </span>
    </div>
</div>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/orderUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/gridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/pop/orderShipping.js?v=${fileVersion}"></script>

<script>

    ${orderShipCompleteGridBaseInfo}
    const bundleNo = '${bundleNo}';
    const orderShipCompleteGrid = GridUtil.initializeGrid("orderShipCompleteGrid", orderShipCompleteGridBaseInfo);

    $(document).ready(function() {
        CommonAjaxBlockUI.global();
        orderShipComplete.init();
    });

</script>