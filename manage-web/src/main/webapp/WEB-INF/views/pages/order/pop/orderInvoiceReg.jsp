<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-popup system">
    <div class="com wrap-title">
        <h2 class="title font-malgun">
            <i class="fa fa-square mg-r-5"></i> 송장등록
        </h2>
    </div>
    <div class="com wrap-title mg-t-10 mg-b-10" style="padding: 10px;">
        <div>
            <ul class="com-list-box em">
                <li>※ 선택한 주문에 대해 택배사와 송장번호를 입력해 주세요.</li>
                <li>※ 우편과 직접배송은 송장번호 없이 처리할 수 있으며, 배송중으로 상태값이 변경됩니다.</li>
            </ul>
        </div>
    </div>
    <div class="topline"></div>
    <!-- 주문정보 상세 - 구매자정보 테이블 -->
    <div class="ui wrap-table horizontal mg-t-10">
        <form id="orderInvoiceRegForm">
            <table class="ui table" id="orderInvoiceRegTable">
                <caption>주문관리 - 송장등록</caption>
                <colgroup>
                    <col width="30%">
                    <col width="30%">
                    <col width="30%">
                </colgroup>
                <tbody>
                    <tr>
                        <td>
                            <label for="shipMethod">
                                <select id="shipMethod" name="shipMethod" class="ui input medium mg-r-10" style="width: 100%;">
                                    <c:forEach var="shipMethod" items="${shipMethodList}" varStatus="status">
                                        <option value="${shipMethod.mcCd}">${shipMethod.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </label>
                        </td>
                        <td>
                            <div id="dlvSelectArea">
                                <label for="dlvCd">
                                    <select id="dlvCd" name="dlvCd" class="ui input medium mg-r-10" style="width: 100%;">
                                        <option value="" selected>택배사 선택</option>
                                        <c:forEach var="dlvCd" items="${dlvCdList}" varStatus="status">
                                            <option value="${dlvCd.mcCd}">${dlvCd.mcNm}</option>
                                        </c:forEach>
                                    </select>
                                </label>
                            </div>
                            <div id="scheduleInputArea" style="display: none;">
                                <div class="ui form inline">
                                    <span class="text">배송예정일</span><input type="text" id="scheduleShipDt" name="scheduleShipDt" class="ui input medium mg-r-5" style="width:50%;" readonly />
                                </div>
                            </div>
                        </td>
                        <td>
                            <div id="invoiceInputArea">
                                <label for="invoiceNo">
                                    <input type="text" id="invoiceNo" name="invoiceNo" placeholder="" class="ui input medium mg-r-5" maxlength="30" style="width:100%">
                                </label>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    </div>
    <div class="topline"></div>
    <div id="orderInvoiceRegGrid" class="mg-l-5 mg-t-10 text-center" style="width: 99%; height: 230px;;"></div>
    <div class="ui center-button-group">
        <span class="inner">
            <button id="saveBtn" class="ui button xlarge btn-danger font-malgun">저장</button>
            <button id="closeBtn" class="ui button xlarge font-malgun" onclick="self.close()">취소</button>
        </span>
    </div>
</div>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/orderUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/gridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/pop/orderShipping.js?v=${fileVersion}"></script>

<script>
    ${orderInvoiceRegGridBaseInfo}
    const bundleNo = '${bundleNo}';
    const orderInvoiceRegGrid = GridUtil.initializeGrid("orderInvoiceRegGrid", orderInvoiceRegGridBaseInfo);

    $(document).ready(function() {
        CommonAjaxBlockUI.global();
        orderInvoiceReg.init();
    });

</script>