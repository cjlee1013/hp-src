<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<style type="text/css">
    .itemNo {
    border-right: 1px solid #eee;
    text-align: center;
    font-size: 14px;
    }
    .itemName1 {
    font-weight: bold;
    font-size: 14px;
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
    }
    .itemName2 {
    font-weight: bold;
    font-size: 14px;
    }
    .addItem {
    font-size: 12px;
    font-weight: bold;
    }
</style>
<div class="ui form inline horizontal mg-t-5 mg-b-5 mg-l-5">
    <span class="text pull-left" id="titleOrderInfo" style="font-size: 15px"></span>

    <label class="ui checkbox pull-right">
        <input type="checkbox" id="selectAllQty" name="selectAllQty"/>
        <span class="text mg-r-20">수량 전체선택</span>
    </label>

</div>

<div class="ui wrap-table horizontal mg-t-40 mg-b-20" style="width: 100%; height: auto; max-height: 350px; overflow:auto; border-left: 1px solid #ddd;  border-right: 1px solid #ddd;">
    <table class="ui table" id="multiShipTable">
        <caption>배송지 선택 테이블</caption>
        <colgroup>
            <col width="20%">
            <col width="*%">
        </colgroup>
        <thead>
            <tr style="border-bottom: 1px solid #ddd; height: 70px;">
                <th style="font-size: 15px; text-align: center; border-right: 1px solid #eee;">배송지선택</th>
                <td>
                    <select id="multiShipList" name="multiShipList" class="ui input medium mg-r-20" style="width: 100%;">
                        <c:forEach var="multiShipList" items="${multiShipList}" varStatus="status">
                            <option value="${multiShipList.multiBundleNo}">${multiShipList.shipDetail}</option>
                        </c:forEach>
                    </select>
                </td>
            </tr>
        </thead>
        <input type="hidden" id="oriMultiShipList" name="oriMultiShipList"/>
    </table>
</div>

<!-- 상품 정보 테이블-->
<div class="ui wrap-table horizontal" style="width: 100%; height: auto; max-height: 350px; overflow:auto; border-left: 1px solid #ddd;  border-right: 1px solid #ddd;">
    <input type="hidden" id="purchaseOrderNo" name="purchaseOrderNo" value="${purchaseOrderNo}">
    <input type="hidden" id="bundleNo" name="bundleNo" value="${bundleNo}">
    <input type="hidden" id="isMarket" name="isMarket" value="false" >

    <table class="ui table" id="itemInfoTable" style="table-layout: fixed">
        <caption>상품정보 테이블</caption>
        <colgroup>
            <col width="15%">
            <col width="55%">
            <col width="15%">
            <col width="15%">
        </colgroup>
        <div class="ui form inline">
            <thead>
            <tr style="border-bottom: 1px solid #ddd;">
                <th style="font-size: 15px; text-align: center; border-right: 1px solid #eee;">상품번호</th>
                <th style="font-size: 15px; text-align: center; border-right: 1px solid #eee;">상품정보</th>
                <th style="font-size: 15px; text-align: center; border-right: 1px solid #eee;">총 상품금액</th>
                <th style="font-size: 15px; text-align: center">신청수량</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </div>
    </table>
</div>
<!-- 상품 정보 테이블 end -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/pop/layout/multiShipOrderItemInfoPopLayout.js?v=${fileVersion}"></script>

