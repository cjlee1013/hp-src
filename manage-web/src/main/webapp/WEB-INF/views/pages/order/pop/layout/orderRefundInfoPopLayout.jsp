<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<!-- 한불예정금액 테이블 -->
<div class="ui wrap-table horizontal mg-t-30" style="border-left: 1px solid #ddd; border-right: 1px solid #ddd; width: 100%;">
     <table class="ui table" id="refundTextTable" >
        <caption>환불예정금액 테이블</caption>
        <colgroup>
            <col width="33%">
            <col width="34%">
            <col width="33%">
        </colgroup>
        <tr style="height: 120px;">
            <td colspan="2" rowspan="3" style="text-align: center; font-size:16px; border-right: 1px solid #ddd">환불예정금액 확인 버튼을 클릭하시면 <br><br> 환불 예정금액을 확인해 보실 수 있습니다.</td>
            <td style="text-align: center; font-size:16px; vertical-align:baseline; background-color: #f9f9f9">
                <span class="text" style="width: 100%">환불예정금액        - 원</span>
            </td>
        </tr>
    </table>
    <table class="ui table" id="refundMainTable" hidden>
        <caption>환불예정금액 테이블</caption>
        <colgroup>
            <col width="16.6%">
            <col width="16.6%">
            <col width="16.6%">
            <col width="16.6%">
            <col width="16.6%">
            <col width="16.6%">
        </colgroup>

        <div class="ui form inline">
            <tr>
                <th style="background-color: #FFFFFF;">
                    <span class="text mg-l-20" style="font-size: 17px;">주문금액</span>
                </th>
                <th style="background-color: #FFFFFF; text-align: right; border-right: 1px solid #ddd;">
                    <span id="purchaseAmt" name="purchaseAmt" style="width: 110px; font-size: 17px"></span>
                    <span class="text mg-r-20" style="font-size: 17px">원</span>
                </th>
                <th style="background-color: #FFFFFF;">
                    <span class="text mg-l-20" style="font-size: 17px;">차감금액</span>
                </th>
                <th style="background-color: #FFFFFF; text-align:right; border-right: 1px solid #ddd;">
                    <span id="discountAmt" name="discountAmt" style="width: 110px; font-size: 17px"></span>
                    <span id="oriDiscountAmt" hidden></span>
                    <span class="text mg-r-20" style="font-size: 17px">원</span>
                </th>
                <th style="font-size: 15px">
                    <span class="text mg-l-20" style="font-size: 17px;">환불예정금액</span>
                </th>
                <th style="text-align: right">
                    <span id="refundAmt" name="refundAmt" style="width: 110px; font-size: 17px"></span>
                    <span id="oriRefundAmt" hidden></span>
                    <span class="text mg-r-20" style="font-size: 17px">원</span>
                </th>
            </tr>
            <tr>
                <td>
                    <span class="text mg-l-20">· 상품금액</span>
                </td>
                <td style="text-align: right; border-right: 1px solid #ddd">
                    <span id="itemPrice" name="itemPrice" style="width: 90px; text-align: center"></span>
                    <span class="text mg-r-30">원</span>
                </td>
                <td>
                    <span class="text mg-l-20" >· 상품/행사할인</span>
                </td>
                <td style="text-align: right; border-right: 1px solid #ddd">
                    <span id="itemPromoDiscountAmt" name="itemPromoDiscountAmt" style="width: 90px; background:none;text-align: right"></span>
                    <span class="text mg-r-30">원</span>
                </td>
                <td style="background-color: #f9f9f9;">
                    <span class="text mg-l-20" >· 신용카드</span>
                </td>
                <td style="text-align: right; background-color: #f9f9f9;">
                    <span id="pgAmt" name="pgAmt" style="width: 90px; text-align: right"></span>
                    <span class="text mg-r-30">원</span>
                </td>
            </tr>
            <tr>
                <td style="border: 0">
                    <span class="text mg-l-20">· 배송비</span>
                </td>
                <td style="text-align: right; border: 0; border-right: 1px solid #ddd">
                    <span id="shipPrice" name="shipPrice" style="width: 90px; text-align: right"></span>
                    <span class="text mg-r-30">원</span>
                </td>
                <td style="border: 0">
                    <span class="text mg-l-20">· 카드상품할인</span>
                </td>
                <td style="text-align: right; border : 0; border-right: 1px solid #ddd" >
                    <span id="cardDiscountAmt" name="cardDiscountAmt" style="width: 90px; background:none;text-align: right"></span>
                    <span class="text mg-r-30">원</span>
                </td>
                <td style="background-color: #f9f9f9; border:none">
                    <span class="text mg-l-20" >· MHC포인트</span>
                </td>
                <td style="text-align: right; background-color: #f9f9f9; border:none">
                    <span id="mhcAmt" name="mhcAmt" style="width: 90px; text-align: right"></span>
                    <span class="text mg-r-30">P</span>
                </td>
            </tr>
            <tr>
                <td style="border: 0">
                    <span class="text mg-l-20" style="border: 0">· 도서산간 배송비</span>
                </td>
                <td style="text-align: right; border: 0; border-right: 1px solid #ddd">
                    <span id="islandShipPrice" name="islandShipPrice" style="width: 90px; text-align: right"></span>
                    <span class="text mg-r-30">원</span>
                </td>
                <td style="border: 0">
                    <span class="text mg-l-20">· 중복 쿠폰</span>
                </td>
                <td style="text-align: right; border : 0; border-right: 1px solid #ddd" >
                    <span id="addCouponDiscountAmt" name="addCouponDiscountAmt" style="width: 90px; background:none;text-align: right"></span>
                    <span class="text mg-r-30">원</span>
                </td>
                <td style="background-color: #f9f9f9; border:none">
                    <span class="text mg-l-20" >· DGV포인트</span>
                </td>
                <td style="text-align: right; background-color: #f9f9f9; border:none">
                    <span id="dgvAmt" name="dgvAmt" style="width: 90px; text-align: right"></span>
                    <span class="text mg-r-30">P</span>
                </td>
            </tr>

            <tr>
                <td colspan="2" style="border: 0; border-right: 1px solid #ddd">
                <td style="border: 0">
                    <span class="text mg-l-20">· 장바구니할인</span>
                </td>
                <td style="text-align: right; border : 0; border-right: 1px solid #ddd" >
                    <span id="cartDiscountAmt" name="cartDiscountAmt" style="width: 90px; background:none;text-align: right"></span>
                    <span class="text mg-r-30">원</span>
                </td>
                <td style="background-color: #f9f9f9; border:none">
                    <span class="text mg-l-20" >· 마일리지</span>
                </td>
                <td style="text-align: right; background-color: #f9f9f9; border:none">
                    <span id="mileageAmt" name="mileageAmt" style="width: 90px; text-align: right"></span>
                    <span class="text mg-r-30">P</span>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="border: 0; border-right: 1px solid #ddd">
                </td>
                <td style="border: 0">
                    <span class="text mg-l-20" >· 배송비할인</span>
                </td>
                <td style="text-align: right; border: 0; border-right: 1px solid #ddd">
                   <span id="shipDiscountAmt" name="shipDiscountAmt" style="width: 90px; background:none;text-align: right"></span>
                   <span class="text mg-r-30">원</span>
                </td>
                <td style="background-color: #f9f9f9; border:none">
                     <span class="text mg-l-20" >· OK캐시백</span>
                </td>
                <td style="text-align: right; background-color: #f9f9f9; border:none">
                    <span id="ocbAmt" name="ocbAmt" style="width: 90px; text-align: right"></span>
                    <span class="text mg-r-30">P</span>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="border: 0; border-right: 1px solid #ddd">
                </td>
                <td style="border: 0">
                    <c:choose>
                        <c:when test="${claimTypeName eq '취소'}">
                            <span class="text mg-l-20">· 추가배송비</span>
                        </c:when>
                        <c:otherwise>
                            <span class="text mg-l-20">· 반품배송비</span>
                        </c:otherwise>
                    </c:choose>
                </td>
                <td style="text-align: right; border: 0; border-right: 1px solid #ddd">
                    <span id="addShipPrice" name="addShipPrice" style="width: 90px; background:none;text-align: right" >0</span>
                    <span hidden id="addShipAmt" name="addShipAmt"></span>
                    <span class="text mg-r-30">원</span>
                </td>
                <th colspan="2" style=" border:none"></th>

            </tr>
            <tr id="addIslandShipPriceRow" hidden>
                <td colspan="2" style="border: 0; border-right: 1px solid #ddd">
                </td>
                <td style="border: 0">
                    <span class="text mg-l-20">· 추가도서산간 배송비</span>
                </td>
                <td style="text-align: right; border: 0; border-right: 1px solid #ddd">
                    <span id="addIslandShipPrice" name="addIslandShipPrice" style="width: 90px; background:none;text-align: right" >0</span>
                    <span hidden id="addIslandShipAmt" name="addIslandShipAmt"></span>
                    <span class="text mg-r-30">원</span>
                </td>
                <th colspan="2" style=" border:none"></th>
            </tr>
            <tr id="empDiscountAmtRow" hidden>
                <td colspan="2" style="border: 0; border-right: 1px solid #ddd">
                </td>
                <td style="border: 0">
                    <span class="text mg-l-20">· 임직원할인금액</span>
                </td>
                <td style="text-align: right; border: 0; border-right: 1px solid #ddd">
                    <span id="empDiscountAmt" name="empDiscountAmt" style="width: 90px; background:none;text-align: right" ></span>
                    <span class="text mg-r-30">원</span>
                </td>
                <th colspan="2" style=" border:none"></th>
            </tr>
        </div>
    </table>
    <input type="hidden" name="preRefundInfo" id="preRefundInfo" />
</div>
<!-- 환불예정금액 테이블 end-->
<script>

    let claimTypeName = '${claimTypeName}';

</script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/pop/layout/orderRefundInfoPopLayout.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/orderUtil.js?v=${fileVersion}"></script>

