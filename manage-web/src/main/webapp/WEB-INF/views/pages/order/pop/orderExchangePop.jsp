<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<div class="com wrap-popup system" style="height: auto; background-color: #FFFFFF; box-shadow:none!important;">
    <div class="com popup-wrap-title">
        <h5 class="title font-malgun">교환신청</h5>
    </div>
    <form id="orderCancelForm" onsubmit="return false;">
        <!-- 환불정보 Layout -->
        <c:choose>
            <c:when test="${multiShipYn eq 'Y'}">
                <jsp:include page="layout/multiShipOrderItemInfoPopLayout.jsp"></jsp:include>
            </c:when>
            <c:otherwise>
                <jsp:include page="layout/orderItemInfoPopLayout.jsp"></jsp:include>
            </c:otherwise>
        </c:choose>
        <!-- 환불정보 Layout end -->

        <!-- 교환 사유 입력 -->
        <div class="ui wrap-table horizontal" style="border-right: 1px solid #eee; border-left: 1px solid #eee; width: 100%">
            <div class="ui form inline mg-t-10 mg-l-10 mg-b-10">
                 <select id="claimReasonType" name="claimReasonType" class="ui input medium mg-r-20" style="width: 25%;">
                    <option value="">선택</option>
                    <c:forEach var="codeDto" items="${claimReasonType}" varStatus="status">
                        <c:if test="${codeDto.ref1 eq 'X' || codeDto.ref1 eq 'df'}">
                            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                        </c:if>
                    </c:forEach>
                </select>
                <input type="text" id="claimReasonDetail" name="claimReasonDetail" class="ui input medium mg-r-5" style="width: 70%;" minlength="2" maxlength="100" placeholder="교환 상세사유를 입력 (최대 100자)">
            </div>
            <jsp:include page="layout/orderDeductPopLayout.jsp"></jsp:include>
            <input type="hidden" id="oriClaimReasonType" name="oriClaimReasonType">
            <input type="hidden" id="detailReasonRequired" name="detailReasonRequired">
        </div>
        <!-- 취소/반품/교환 사유 입력 end -->

        <!-- 환불예정금액 버튼 -->
        <div class="ui center-button-group">
        <span class="inner">
            <button class="ui button large dark-blue font-malgun" id="checkRefundAmt" onclick="refundInfoLayout.checkRefundAmt()">교환배송비 확인</button>
            <input type="hidden" id="preRefundAmtReturnCode">
            <input type="hidden" id="claimType" name="claimType" value="X">
            <input type="hidden" id="multiShipYn" value="${multiShipYn}">
        </span>
        </div>
        <!-- 환불예정금액 버튼 end -->

        <!-- 교환/반품 배송정보 테이블 -->
        <jsp:include page="layout/orderDeliveryInfoPopLayout.jsp">
            <jsp:param name="claimType" value="X"/>
            <jsp:param name="claimTypeName" value="교환"/>
        </jsp:include>
        <!-- 교환/반품 배송정보 테이블 end -->

        <div class="ui center-button-group mg-b-20">
            <span class="inner">
                <button class="ui button xlarge btn-danger font-malgun" id="reqBtn" onclick="orderExchange.submit()">교환신청</button>
                <button class="ui button xlarge font-malgun" id="closeBtn" onclick="self.close()">취소</button>
            </span>
        </div>
    </form>
</div>
<script>
    var shipType = '${shipType}';
    var claimTypeName = '${claimTypeName}';
    var shipMethod;

    var claimReasonTypeList = new Array();


    <c:forEach var="codeDto" items="${claimReasonType}">
        <c:if test="${codeDto.ref1 eq 'X'}">
            claimReasonTypeList.push({
                mcCd : "${codeDto.mcCd}",
                mcNm : "${codeDto.mcNm}",
                whoReason : "${codeDto.ref2}",
                detailReasonRequired : "${codeDto.ref4}"
            });
        </c:if>
    </c:forEach>
</script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/pop/orderExchangePop.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/pop/orderCommonPop.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/pop/layout/orderRefundInfoPopLayout.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/orderUtil.js?v=${fileVersion}"></script>