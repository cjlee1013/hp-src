<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<div class="com wrap-popup system">
    <div class="com wrap-title">
        <h2 class="title font-malgun">
            <i class="fa fa-square mg-r-5"></i> 택배 배송정보 수정
        </h2>
    </div>
    <div class="com wrap-title sub">
        <div id="groupSearch" class="ui wrap-table horizontal mg-t-10">
            <form id="orderAddrChangeForm">
                <table class="ui table" id="orderInvoiceEditTable">
                    <caption>주문관리 - 택배 배송정보 수정</caption>
                    <colgroup>
                        <col width="10%">
                        <col width="10%">
                        <col width="10%">
                        <col width="10%">
                    </colgroup>
                    <div class="ui form inline">
                        <tbody>
                            <tr>
                                <th>이름 <span class="text-red">*</span></th>
                                <td>
                                    <div class="ui form inline">
                                        <input id="receiverNm" name="receiverNm" type="text" class="ui input medium mg-r-5" maxlength="20" style="width:153px;">
                                        <span class="text">( <span style="color:red;" id="textCountKr_receiverNm">0</span> / 20자 )</span>
                                    </div>
                                </td>
                                    <th>개인통관고유부호</th>
                                    <td>
                                        <label for="personalOverseaNo">
                                            <input type="text" id="personalOverseaNo" name="personalOverseaNo" class="ui input medium mg-r-5" readonly maxlength="13" style="width: 200px;">
                                        </label>
                                    </td>
                            </tr>
                            <tr>
                                <th>연락처 <span class="text-red">*</span></th>
                                <td colspan="3">
                                    <div class="ui form inline">
                                        <label>
                                            <input type="hidden" name="shipMobileNo" id="shipMobileNo" value="">
                                            <input type="text" name="shipMobileNo_1" id="shipMobileNo_1" class="ui input medium" style="width: 45px;" maxlength="4">
                                            <span class="text">-</span>
                                            <input type="text" name="shipMobileNo_2" id="shipMobileNo_2" class="ui input medium" style="width: 55px;" maxlength="4">
                                            <span class="text">-</span>
                                            <input type="text" name="shipMobileNo_3" id="shipMobileNo_3" class="ui input medium" style="width: 55px;" maxlength="4">
                                            <span class="text">( 안심번호 : <span id="safetyNm"></span> )</span>
                                        </label>
                                    </div>
                                </td>
                            </tr>
                            <th>주소<span class="text-red"> *</span></th>
                            <td colspan="3">
                                <div class="ui form inline">
                                    <input type="text" name="zipCode" id="zipCode" class="ui input medium mg-r-5" style="width:150px;" readonly><button type="button" class="ui button small" onclick="zipCodePopup('orderShipChange.setZipcode'); return false;">우편번호 찾기</button>
                                    <br>
                                    <input type="text" name="roadBaseAddr" id="roadBaseAddr" class="ui input medium mg-t-5 mg-r-5" style="width:320px;" readonly>
                                    <input type="text" name="roadDetailAddr" id="roadDetailAddr" class="ui input medium mg-t-5" style="width:200px;" maxlength="100">
                                    <span class="text">( <span style="color:red;" id="textCountKr_roadDetailAddr">0</span> / 100자 )</span>
                                </div>
                                <div style="height:20px;display: none" id="gibunAddrArea">
                                    <span style="height:10px;display:block;"></span>
                                    [ 지번 ] <span id="baseAddr"></span> <span id="detailAddr"></span>
                                </div>
                            </td>
                            </tr>
                            <tr>
                                <th>배송시 요청사항</th>
                                <td colspan="3">
                                    <div style="float:left;" class="ui form inline">
                                        <label>
                                            <select id="shipMsgCd" name="shipMsgCd" class="ui input medium mg-r-10" style="width:210px;float:left;">
                                                <option value="" selected>선택하세요</option>
                                                <c:forEach var="dlvMsgCd" items="${dlvMsgCd}" varStatus="status">
                                                <option value="${dlvMsgCd.key}">${dlvMsgCd.value}</option>
                                                </c:forEach>
                                                <input type="text" id="shipMsg" name="shipMsg" class="ui input medium" style="width: 300px;" maxlength="100">
                                                <span class="text">( <span style="color:red;" id="textCountKr_shipMsg">0</span> / <span id="textTotalCountKr_shipMsg">50</span>자 )</span>
                                        </label>
                                    </div>
                                </td>
                            </tr>
                            <tr id="auroraArea">
                                <th>공동현관 출입번호</th>
                                <td colspan="5">
                                    <div style="float:left;" class="ui form inline">
                                        <label>
                                            <select id="auroraShipMsgCd" name="auroraShipMsgCd" class="ui input medium mg-r-10" disabled style="width:210px;float:left;">
                                                <option value="" selected>선택하세요</option>
                                                <c:forEach var="auroraMsgCd" items="${auroraMsgCd}" varStatus="status">
                                                <option value="${auroraMsgCd.key}">${auroraMsgCd.value}</option>
                                                </c:forEach>
                                                <input type="text" id="auroraShipMsg" name="auroraShipMsg" class="ui input medium" style="width: 300px;" maxlength="30" disabled>
                                                <span class="text">( <span style="color:red;" id="textCountKr_auroraShipMsg">0</span> / <span id="textTotalCountKr_auroraShipMsg">30</span>자 )</span>
                                        </label>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </div>
                </table>
                <input type="hidden" id="shipType" name="shipType" />
                <input type="hidden" id="purchaseOrderNo" name="purchaseOrderNo" />
            </form>
        </div>
    </div>
    <div class="ui center-button-group">
        <span class="inner">
            <button id="saveBtn" class="ui button xlarge btn-danger font-malgun">저장</button>
            <button id="closeBtn" class="ui button xlarge font-malgun" onclick="self.close()">취소</button>
        </span>
    </div>
</div>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/orderUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/gridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/validateUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/pop/orderShipChange.js?v=${fileVersion}"></script>
<script>
    const bundleNo = ${bundleNo};
    const shipAreaType = '${shipAreaType}';
    const shipType = '${shipType}';
</script>