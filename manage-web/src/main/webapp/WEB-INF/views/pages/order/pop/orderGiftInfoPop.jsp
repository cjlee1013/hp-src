<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<div class="com wrap-popup system">
    <div class="com wrap-title">
        <h2 class="title font-malgun">
            <i class="fa fa-square mg-r-5"></i> 사은 행사정보
        </h2>
    </div>
    <div id="orderGiftInfoGrid" style="width: 100%; height: 230px;" class="ui mg-t-20"></div>
    <div class="ui center-button-group">
        <span class="inner">
            <button id="closeBtn" class="ui button xlarge font-malgun" onclick="self.close()">닫기</button>
        </span>
    </div>
</div>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/orderUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/gridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/pop/orderDiscountPop.js?v=${fileVersion}"></script>

<script>

    ${orderGiftInfoGridBaseInfo}
    const purchaseOrderNo = ${purchaseOrderNo};
    const orderGiftInfoGrid = GridUtil.initializeGrid("orderGiftInfoGrid", orderGiftInfoGridBaseInfo);

    $(document).ready(function() {
        CommonAjaxBlockUI.global();
        orderGiftInfo.init();
    });

</script>