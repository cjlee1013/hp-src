<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<style>

    .tbl-time { margin-top: 15px; background-color: #FFFFFF!important;}
    .tbl-time caption {overflow:hidden!important;;visibility:hidden!important;;position:absolute!important;;width:0px!important;;height:0px!important;;font-size:0px!important;;line-height:0px!important;;}
    .tbl-time table {
        width: 100%!important;
        border-top: 1px solid #eee!important;
        text-align: center!important;
    }
    .tbl-time thead th,
    .tbl-time tbody th {
        height: 40px!important;
        background: #f9fafb!important;
        border-bottom: 1px solid #eee!important;
        border-right: 1px solid #eee!important;
        font-size: 12px!important;
        color: #333!important;
        font-weight: 500!important;
    }
    .tbl-time tbody th,
    .tbl-time tbody td {
        position: relative!important;
        border-bottom: 1px solid #eee!important;
    }
    .tbl-time tbody td {
        font-size: 12px!important;
        border-left: 1px solid #eee!important;
        border-right: 1px solid #eee!important;
    }
    .tbl-time table tbody td.finish,
    .tbl-time table tbody td.closed {
        background-color: #f9fafb!important;
        color: #999!important;
        font-weight: bold!important;
    }
    .tbl-time tbody td.closed:last-child { border-right: 0!important;}
    .tbl-time input:checked + .lb-radio { font-weight: 500!important; }
</style>
<!-- 반품/교환 배송정보 테이블 -->
<div class="com wrap-title sub" id="deliveryInfoTitle">
    <h3 class="title">• ${param.claimTypeName}상세 정보</h3>
</div>
<div class="ui wrap-table horizontal mg-t-10" id="deliveryInfoTable">
    <table class="ui table mg-t-10" >
        <tr>
            <th style="width: 74px;">${param.claimTypeName}배송비</th>
            <td>
                <div class="ui form inline" id="returnShipAmtTag">
                    <span class="text" id="returnShipAmt" name="returnShipAmt" style="font-size: 12px; font-weight: bold; color: #f90000"></span>
                    <span class="text mg-l-5" id="returnShipAmtText" name="returnShipAmtText" style="font-size: 12px; color: #f90000"></span>
                    <span class="text mg-l-5" id="whoReasonTag" name="whoReasonTag" style="font-size: 12px;font-weight: bold; color: #f90000"></span>
                </div>

                <c:choose>
                    <c:when test="${claimTypeName eq '교환'}">
                        <div class="ui form inline mg-t-10" id="exchangeIsEncloseTag" style="display: none">
                            <span class="text ">&#8251; 교환배송비는 박스에 동봉해 주시기 바랍니다.</span>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <div class="ui select inline mg-t-10" id="isEncloseTag">
                            <span style="font-weight: bold">* 반품비용 결제방법</span>
                            <select id="isEnclose" name="isEnclose" class="ui select medium mg-r-10 mg-l-10" style="width: 15%">
                                <option value="Y">상품에 동봉함</option>
                                <option value="N">환불금에서 차감</option>
                            </select>
                            <span class="text" id="packMsg"></span>
                        </div>
                    </c:otherwise>
                </c:choose>
                <input type="hidden" id="oriIsEnclose" name="oriIsEnclose">
            </td>
        </tr>
        <tr>
            <th>수거방법</th>
            <td id="pickWayDS">
                <span>상품을 판매자에게 보내셨습니까?</span>
                <label class="ui radio inline mg-l-10">
                    <input type="radio" name="isPick" class="ui input medium" value="Y"><span>예</span>
                </label>
                <label class="ui radio inline">
                    <input type="radio" name="isPick" class="ui input medium" value="N" checked><span>아니요</span>
                </label>
                <div class="ui radio inline" id="isPickReq">
                <label class="ui radio inline mg-l-5">
                    <span>( </span><input type="radio" name="isPickReq" class="ui radio small" value="Y" checked><span>수거함</span>
                </label>
                <label class="ui radio inline">
                    <input type="radio" name="isPickReq" class="ui input small" value="N"><span>수거안함 )</span>
                </label>
                </div>

                <!-- 택배사 정보 입력 테이블 -->
                <table id="courierInfoTable" hidden class="mg-t-10">
                    <tr>
                        <th>· 택배사</th>
                        <td>
                            <div class="ui form inline">
                                <select id="dlvCd" name="dlvCd" class="ui input medium mg-r-20" style="width: 50%;">
                                    <option value="">선택</option>
                                    <c:forEach var="codeDto" items="${dlvCd}" varStatus="status">
                                    <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </td>
                        <th>· 송장번호</th>
                        <td>
                            <input type="text" id="invoiceNumber" name="invoiceNumber" class="ui input medium mg-r-5" numberonly>
                        </td>
                    </tr>
                </table>
                <!-- 택배사 정보 입력 테이블 end -->
            </td>
            <td id="pickWayTD">
                <div class="ui form inline mg-b-10">
                    <span class="text" style="font-size: 12px;">점포 : </span>
                    <span class="text" id="storeNm" name="storeNm" style="font-size: 12px;"></span>
                </div>
                <span class="text">수거방법</span>
                <label class="ui radio inline mg-l-10" id="tdRadioLabel"style="display: none">
                    <input type="radio" id="tdRadio" name="isPickReqTd" class="ui input small" value="Y"><span>자차수거</span>
                </label>
                <label class="ui radio inline" id="pickRadioLabel"style="display: none">
                    <input type="radio" id="pickRadio" name="isPickReqTd" class="ui input small" value="Y"><span>점포방문</span>
                </label>
                <label class="ui radio inline mg-l-10" id="quickRadioLabel"style="display: none">
                    <input type="radio" id="quickRadio" name="isPickReqTd" class="ui input small" value="Y"><span>퀵수거</span>
                </label>
                <label class="ui radio inline">
                    <input type="radio" id="noneRadio" name="isPickReqTd" class="ui input small" value="N"><span>수거안함</span>
                </label>

                <div class="ui form inline mg-b-5" id="pickPlace" style="display: none">
                    <span class="text" id="placeNm" name="placeNm"></span>
                </div>
                <div class="tbl-time mg-t-10" id="slotChangeArea">
                    <span class="text" style="font-weight: bold">수거시간</span>
                </div>
            </td>
        </tr>
        <tr id="pickShippingInfoRow">
            <th>${param.claimTypeName}수거지</th>
            <td>
                <!-- 상품 수거지 정보 입력 테이블 -->
                <table class="ui table">
                    <tr>
                        <th style="width: 100px;">*이름</th>
                        <td>
                            <input type="text" id="pickReceiverNm" name="pickReceiverNm" class="ui input medium mg-r-5" style="width: 30%;" minlength="2" maxlength="20" placeholder="이름을 입력해주세요" value="${param.name}">
                        </td>
                    </tr>
                    <tr>
                        <th>*연락처</th>
                        <td style="text-align: left">
                        <div class="ui form inline">
                            <input type="hidden" name="pickMobileNo" id="pickMobileNo">
                            <input type="text" name="pickMobileNo_1" id="pickMobileNo_1" class="ui input medium" style="width: 65px;" title="연락처" maxlength="3">
                            <span class="text">-</span>
                            <input type="text" name="pickMobileNo_2" id="pickMobileNo_2" class="ui input medium" style="width: 65px;" title="연락처" maxlength="4">
                            <span class="text">-</span>
                            <input type="text" name="pickMobileNo_3" id="pickMobileNo_3" class="ui input medium" style="width: 65px;" title="연락처" maxlength="4">
                        </div>
                        </td>
                    </tr>
                    <tr>
                        <th>*주소</th>
                        <td style="text-align: left">
                            <div class="ui form inline">
                            <input type="text" name="pickZipCode" id="pickZipCode" class="ui input medium mg-r-5" style="width:100px;" value="${param.zipCode}" readonly><button type="button" class="ui button small" onclick="javascript:zipCodePopup('deliveryInfoLayout.setPickZipcode'); return false;">우편번호 찾기</button>
                            <br>
                            <input type="text" name="pickRoadBaseAddr" id="pickRoadBaseAddr" class="ui input medium mg-t-5 mg-r-5" style="width:200px;" readonly>
                            <input type="text" name="pickRoadDetailAddr" id="pickRoadDetailAddr" class="ui input medium mg-t-5" style="width:240px;" maxlength="100" placeholder="상세주소를 입력해주세요" value="${param.addrDetail}">
                            <input class="text" id="pickBaseAddr" name="pickBaseAddr" hidden></input>
                            <input class="text" id="pickBaseDetailAddr" name="pickBaseDetailAddr" hidden></input>
                            </div>
                        </td>
                    </tr>
                </table>
                <!-- 상품 수거지 정보 입력 테이블 end -->
            </td>
        </tr>
        <c:if test="${param.claimType eq 'X'}">
        <tr id="exchShippingInfoRow">
            <th>${param.claimTypeName}배송지</th>
            <td>
                <!-- 상품 배송지 정보 입력 테이블 -->
                <table class="ui table">
                    <tr>
                        <th style="width: 100px;">*이름</th>
                        <td>
                            <input type="text" id="exchReceiverNm" name="exchReceiverNm" class="ui input medium mg-r-5" style="width: 30%;" minlength="2" maxlength="20" placeholder="이름을 입력해주세요" value="${param.name}">
                        </td>
                    </tr>
                    <tr>
                        <th>*연락처</th>
                        <td style="text-align: left">
                        <div class="ui form inline">
                            <input type="hidden" name="exchMobileNo" id="exchMobileNo">
                            <input type="text" name="exchMobileNo_1" id="exchMobileNo_1" class="ui input medium" style="width: 65px;" title="연락처" maxlength="3">
                            <span class="text">-</span>
                            <input type="text" name="exchMobileNo_2" id="exchMobileNo_2" class="ui input medium" style="width: 65px;" title="연락처" maxlength="4">
                            <span class="text">-</span>
                            <input type="text" name="exchMobileNo_3" id="exchMobileNo_3" class="ui input medium" style="width: 65px;" title="연락처" maxlength="4">
                        </div>
                        </td>
                    </tr>
                    <tr>
                        <th>*주소</th>
                        <td style="text-align: left">
                            <div class="ui form inline">
                            <input type="text" name="exchZipCode" id="exchZipCode" class="ui input medium mg-r-5" style="width:100px;" value="${param.zipCode}" readonly><button type="button" class="ui button small" onclick="javascript:zipCodePopup('deliveryInfoLayout.setExchZipcode'); return false;">우편번호 찾기</button>
                            <br>
                            <input type="text" name="exchRoadBaseAddr" id="exchRoadBaseAddr" class="ui input medium mg-t-5 mg-r-5" style="width:200px;" readonly>
                            <input type="text" name="exchRoadDetailAddr" id="exchRoadDetailAddr" class="ui input medium mg-t-5" style="width:240px;" maxlength="100" placeholder="상세주소를 입력해주세요" value="${param.addrDetail}">
                            <input class="text" id="exchBaseAddr" name="exchBaseAddr" hidden></input>
                            <input class="text" id="exchBaseAddrDetail" name="exchBaseAddrDetail" hidden></input>
                            </div>
                        </td>
                    </tr>
                </table>
                <!-- 상품 배송지 정보 입력 테이블 end -->
                </div>
            </td>
        </tr>
        </c:if>
        <tr>
            <th rowspan="3">사진첨부</th>
            <td>
                <div class="com wrap-title sub" id="imgBtnView1" hidden>
                    <span id="fileName1" name="fileName1" style="font-size: 14px;"></span>
                    <span id="uploadFileName1" name="uploadFileName1" hidden></span>
                    <button type="button" class="ui button small delete-thumb deleteImgBtn text-size-sm mg-r-60" style="float: right" onclick="claimImg.deleteImg(1);">삭제</button>
                </div>
                <div class="com wrap-title sub" id="imgBtnUpload1">
                    <span class="pull-left mg-r-10">
                        <button type="button" class="ui button medium" id="imgBtn1" name="imgBtn1" onclick="claimImg.clickFile(1);">사진첨부1</button>
                    </span>
                    <h2 class="mg-t-5">이미지 파일(GIF, PNG, JPG)을 기준으로 최대 5MB이하</h2>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="com wrap-title sub" id="imgBtnView2" hidden>
                    <span id="fileName2" name="fileName2" style="font-size: 14px;"></span>
                    <span id="uploadFileName2" name="uploadFileName2" hidden></span>
                    <button type="button" class="ui button small delete-thumb deleteImgBtn text-size-sm mg-r-60" style="float: right" onclick="claimImg.deleteImg(2);">삭제</button>
                </div>
                <div class="com wrap-title sub" id="imgBtnUpload2">
                    <span class="pull-left mg-r-10">
                        <button type="button" class="ui button medium" id="imgBtn2" name="imgBtn2" onclick="claimImg.clickFile(2);">사진첨부2</button>
                    </span>
                    <h2 class="mg-t-5">이미지 파일(GIF, PNG, JPG)을 기준으로 최대 5MB이하</h2>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="com wrap-title sub" id="imgBtnView3" hidden>
                    <span id="fileName3" name="fileName3" style="font-size: 14px;"></span>
                    <span id="uploadFileName3" name="uploadFileName3" hidden></span>
                    <button type="button" class="ui button small delete-thumb deleteImgBtn text-size-sm mg-r-60" style="float: right" onclick="claimImg.deleteImg(3);">삭제</button>
                </div>
                <div class="com wrap-title sub" id="imgBtnUpload3">
                    <span class="pull-left mg-r-10">
                        <button type="button" class="ui button medium" id="imgBtn3" name="imgBtn" onclick="claimImg.clickFile(3);">사진첨부3</button>
                    </span>
                    <h2 class="mg-t-5">이미지 파일(GIF, PNG, JPG)을 기준으로 최대 5MB이하</h2>
                </div>
            </td>
        </tr>
    <input type="hidden" id="deliveryYn" name="deliveryYn">
    </table>
</div>
<!-- 반품 상세정보 테이블 end -->

<input type="file" id="imgFile1" name="fileArr" multiple style="display: none;"/>
<input type="file" id="imgFile2" name="fileArr" multiple style="display: none;"/>
<input type="file" id="imgFile3" name="fileArr" multiple style="display: none;"/>



<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/pop/layout/orderDeliveryInfoPopLayout.js?v=${fileVersion}"></script>
