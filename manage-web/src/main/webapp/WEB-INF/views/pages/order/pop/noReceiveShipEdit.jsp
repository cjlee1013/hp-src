<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<div class="com wrap-popup system">
    <div class="com wrap-title">
        <h2 class="title font-malgun">
            <i class="fa fa-square mg-r-5"></i> 미수취 신고
        </h2>
    </div>
    <div class="topline mg-t-10"></div>
    <!-- 주문정보 상세 - 구매자정보 테이블 -->
    <div class="ui wrap-table horizontal">
        <form id="noRcvProcessModifyForm">
            <table class="ui table" id="noRcvProcessModifyTable">
                <caption>주문관리 - 미수취 신고 철회/철회요청</caption>
                <colgroup>
                    <col width="25%">
                    <col width="30%">
                    <col width="25%">
                    <col width="30%">
                </colgroup>
                <tbody>
                    <tr>
                        <th>신고일시</th>
                        <td><span id="noRcvDeclrDt"></span></td>
                        <th>등록자</th>
                        <td><span id="noRcvRegId"></span></td>
                    </tr>
                    <tr>
                        <th>신고사유</th>
                        <td colspan="3"><span id="noRcvDeclrType"></span></td>
                    </tr>
                    <tr>
                        <th>상세사유</th>
                        <td colspan="3"><span id="noRcvDetailReason"></span></td>
                    </tr>
                    <tr>
                        <th rowspan="2">미수취 신고 처리 <span class="text text-red"> *</span></th>
                        <td colspan="3">
                            <label for="noRcvProcessType">
                                <select id="noRcvProcessType" name="noRcvProcessType" class="ui input medium mg-r-10" style="width: 100%;">
                                    <option value="" selected>선택하세요</option>
                                    <c:forEach var="noRcvProcessType" items="${noRcvProcessTypeList}" varStatus="status">
                                        <option value="${noRcvProcessType.key}">${noRcvProcessType.value}</option>
                                    </c:forEach>
                                </select>
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" id="noRcvProcessCntntArea" style="display: none;">
                            <div class="ui form inline">
                                <label for="noRcvProcessCntnt">
                                    <textarea id="noRcvProcessCntnt" name="noRcvProcessCntnt" class="ui input" style="width: 100%; height: 80px;" maxlength="200"></textarea>
                                </label>
                                <br>
                                <span class="text pull-right">( <span style="color:red;" id="textCountKr_noRcvProcessCntnt">0</span> / 200자 )</span>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    </div>
    <div class="topline"></div>
    <div class="ui center-button-group">
        <span class="inner">
            <button id="saveBtn" class="ui button xlarge btn-danger font-malgun">철회</button>
            <button id="closeBtn" class="ui button xlarge font-malgun" onclick="self.close()">취소</button>
        </span>
    </div>
</div>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/orderUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/gridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/validateUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/pop/noReceiveShip.js?v=${fileVersion}"></script>

<script>
    const noRcvShipParam = ${noRcvShipParam};

    $(document).ready(function() {
        CommonAjaxBlockUI.global();
        noRcvShipEdit.init();
    });
</script>