<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-popup system">
    <div class="com wrap-title">
        <h2 class="title font-malgun">
            <i class="fa fa-square mg-r-5"></i> 판매업체 정보
        </h2>
    </div>
    <!-- 주문정보 상세 - 구매자정보 테이블 -->
    <div class="ui wrap-table horizontal mg-t-10">
        <form id="orderPartnerInfoForm">
            <table class="ui table" id="orderPartnerInfoTable">
                <caption>주문관리 - 판매업체정보</caption>
                <colgroup>
                    <col width="20%">
                    <col width="30%">
                    <col width="20%">
                    <col width="30%">
                </colgroup>
                <tbody>
                    <tr>
                        <th>판매업체</th>
                        <td>
                            <label class="ui text medium inline">
                                <span id="partnerNm"></span>
                            </label>
                        </td>
                        <th>판매업체ID</th>
                        <td>
                            <label class="ui text medium inline">
                                <span id="partnerId"></span>
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <th>담당자</th>
                        <td>
                            <label class="ui text medium inline">
                                <span id="mngNm"></span>
                            </label>
                        </td>
                        <th>담당자 연락처</th>
                        <td>
                            <label class="ui text medium inline">
                                <span id="mngPhone"></span>
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <th>대표번호</th>
                        <td colspan="3">
                            <label class="ui text medium inline">
                                <span id="phone1"></span>
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <th>업체출고지</th>
                        <td colspan="3">
                            <label class="ui text medium inline">
                                <span id="releaseAddr"></span>
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <th>업체회수지</th>
                        <td colspan="3">
                            <label class="ui text medium inline">
                                <span id="returnAddr"></span>
                            </label>
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    </div>
    <div class="ui center-button-group">
        <span class="inner">
            <button id="closeBtn" class="ui button xlarge font-malgun" onclick="self.close()">닫기</button>
        </span>
    </div>
</div>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/orderUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/gridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/pop/orderPartnerInfo.js?v=${fileVersion}"></script>

<script>
    const partnerId = '${partnerId}';
    const bundleNo = '${bundleNo}';
    $(document).ready(function() {
        CommonAjaxBlockUI.global();
        orderPartnerInfo.init();
    });

</script>