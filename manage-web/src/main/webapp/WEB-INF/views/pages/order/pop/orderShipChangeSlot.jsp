<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<style>

    .tbl-time { margin-top: 15px; background-color: #FFFFFF;}
    .tbl-time caption {overflow:hidden;visibility:hidden;position:absolute;width:0px;height:0px;font-size:0px;line-height:0px;}
    .tbl-time table {
        width: 100%;
        border-top: 1px solid #eee;
        text-align: center;
    }
    .tbl-time thead th,
    .tbl-time tbody th {
        height: 40px;
        background: #f9fafb;
        border-bottom: 1px solid #eee;
        border-right: 1px solid #eee;
        font-size: 12px;
        color: #333;
        font-weight: 500;
    }
    .tbl-time tbody th,
    .tbl-time tbody td {
        position: relative;
        border-bottom: 1px solid #eee;
    }
    .tbl-time tbody td {
        font-size: 12px;
        border-left: 1px solid #eee;
        border-right: 1px solid #eee;
    }
    .tbl-time table tbody td.finish,
    .tbl-time table tbody td.closed {
        background-color: #f9fafb;
        color: #999;
        font-weight: bold;
    }
    .tbl-time tbody td.closed:last-child { border-right: 0}
    .tbl-time input:checked + .lb-radio { font-weight: 500; }
</style>
<div class="com wrap-popup system">
    <div class="com wrap-title">
        <h2 class="title font-malgun">
            <i class="fa fa-square mg-r-5"></i> 배송시간 수정
        </h2>
    </div>
    <div class="com wrap-title sub">
        <div id="groupSearch" class="ui wrap-table horizontal mg-t-10">
            <form id="orderShipChangeSlotForm">
                <table class="ui table" id="orderShipChangeSlotTable">
                    <caption>주문관리 - 배송시간 수정</caption>
                    <colgroup>
                        <col width="200px">
                        <col width="150px">
                        <col width="200px">
                        <col width="150px">
                        <col width="200px">
                        <col width="150px">
                    </colgroup>
                    <div class="ui form inline">
                        <tbody>
                            <tr>
                                <th>이름</th>
                                <td><span id="receiverNm"></span></td>
                                <th>배송점포</th>
                                <td colspan="3"><span id="storeNm"></span></td>
                            </tr>
                            <tr>
                                <th>배송요청일시</th>
                                <td colspan="3">
                                    <span id="slotShipDt"></span> / <span id="slotShipStartTime"></span> ~ <span id="slotShipEndTime"></span> / 슬롯번호 : <span id="slotId"></span>
                                </td>
                                <th>애니타입 여부</th>
                                <td><span id="anytimeSlotYn"></span></td>

                            </tr>
                            <tr id="auroraArea">
                                <th>공동현관 출입번호</th>
                                <td colspan="5">
                                    <div style="float:left;" class="ui form inline">
                                        <label>
                                            <select id="auroraShipMsgCd" name="auroraShipMsgCd" class="ui input medium mg-r-10" disabled style="width:210px;float:left;">
                                                <option value="" selected>선택하세요</option>
                                                <c:forEach var="auroraMsgCd" items="${auroraMsgCd}" varStatus="status">
                                                <option value="${auroraMsgCd.key}">${auroraMsgCd.value}</option>
                                                </c:forEach>
                                                <input type="text" id="auroraShipMsg" name="auroraShipMsg" class="ui input medium" style="width: 300px;" maxlength="100" disabled>
                                        </label>
                                    </div>
                                </td>
                            </tr>
                            <tr id="pickerArea">
                                <th>방문수령</th>
                                <td><span id="placeNm"></span></td>
                                <th>락커번호</th>
                                <td><span id="lockerNo"></span></td>
                                <th>비밀번호</th>
                                <td><span id="lockerPasswd"></span></td>
                            </tr>
                        </tbody>
                    </div>
                </table>
            </form>
        </div>
    </div>
    <div class="tbl-time" id="slotChangeArea"></div>
    <div id="anytimeArea">
        <span class="text-red mg-l-10">※ 애니타임 주문입니다. 마일리지가 지급되니 변경하지 마세요.</span>
    </div>
    <div class="ui center-button-group">
        <span class="inner">
            <button id="saveBtn" class="ui button xlarge btn-danger font-malgun">저장</button>
            <button id="closeBtn" class="ui button xlarge font-malgun" onclick="self.close()">취소</button>
        </span>
    </div>
</div>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/orderUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/gridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/validateUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/pop/orderShipChangeSlot.js?v=${fileVersion}"></script>
<script>
    const parameter = ${parameter};
    $(document).ready(function() {
        CommonAjaxBlockUI.global();
        shipChangeSlot.init();
    });

</script>