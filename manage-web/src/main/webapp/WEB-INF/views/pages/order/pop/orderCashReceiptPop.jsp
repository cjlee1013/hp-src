<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-popup system">
    <div class="com wrap-title">
        <h2 class="title font-malgun">
            <i class="fa fa-square mg-r-5"></i> 현금영수증 발급내역
        </h2>
    </div>
    <!-- 주문정보 상세 - 구매자정보 테이블 -->
    <div class="ui wrap-table horizontal mg-t-20">
        <form id="orderStoreShipMemoInfoForm">
            <table class="ui table" id="orderStoreShipMemoInfoTable">
                <caption>주문관리 - 현금영수증발급내역</caption>
                <colgroup>
                    <col width="100%">
                </colgroup>
                <tbody>
                    <tr style="height: 60px;">
                        <td style="alignment: center; width: 100%; border: 1px solid black;">
                            <label class="ui text medium inline">
                                <span id="cashReceiptInfo">-</span> <p>
                            </label>
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    </div>
    <div class="ui center-button-group">
        <span class="inner">
            <button id="closeBtn" class="ui button xlarge font-malgun" onclick="self.close()">확인</button>
        </span>
    </div>
</div>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/orderUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/gridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/pop/orderCashReceiptPop.js?v=${fileVersion}"></script>

<script>
    const purchaseOrderNo = '${purchaseOrderNo}';
    $(document).ready(function() {
        CommonAjaxBlockUI.global();
        orderCashReceiptPop.init();
    });

</script>