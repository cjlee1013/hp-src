<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">주문정보 상세</h2>
    </div>
    <div  class="com wrap-title sub" >
        <h3 class="title">• 구매자정보</h3>
    </div>
    <div class="topline"></div>
    <!-- 주문정보 상세 - 구매자정보 테이블 -->
    <div class="ui wrap-table horizontal mg-t-10">
        <form id="orderDetailUserInfo">
            <table class="ui table" id="orderDetailUserInfoTable">
                <caption>주문정보 상세 - 구매자정보 검색</caption>
                <colgroup>
                    <col width="80px">
                    <col width="100px">
                    <col width="80px">
                    <col width="90px">
                    <col width="80px">
                    <col width="90px">
                    <col width="100px">
                    <col width="90px">
                    <col width="100px">
                    <col width="100px">
                    <col width="90px">
                    <col width="100px">
                </colgroup>
                    <tbody>
                        <tr>
                            <th>주문번호</th>
                            <td>
                                <label class="ui text medium inline">
                                    <span id="purchaseOrderNo"></span>
                                </label>
                            </td>
                            <th>결제상태</th>
                            <td>
                                <label class="ui text medium inline">
                                    <span id="paymentStatusNm"></span>
                                </label>
                            </td>
                            <th>주문일시</th>
                            <td>
                                <label class="ui text medium inline">
                                    <span id="orderDt"></span>
                                </label>
                            </td>
                            <th>결제일시</th>
                            <td>
                                <label class="ui text medium inline">
                                    <span id="paymentFshDt"></span>
                                </label>
                            </td>
                            <th>주문디바이스</th>
                            <td>
                                <label class="ui text medium inline">
                                    <span id="orderDevice"></span>
                                </label>
                            </td>
                            <th>마켓연동</th>
                            <td>
                                <label class="ui text medium inline">
                                    <span id="marketType"></span>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <th>회원번호</th>
                            <td>
                                <label class="ui text medium inline">
                                    <span id="userNo"></span>
                                </label>
                            </td>
                            <th>구매자</th>
                            <td>
                                <label class="ui text medium inline">
                                    <span id="buyerNm"></span>
                                </label>
                            </td>
                            <th>구매자ID</th>
                            <td>
                                <label class="ui text medium inline">
                                    <span id="buyerEmail"></span>
                                </label>
                            </td>
                            <th>구매자연락처</th>
                            <td>
                                <label class="ui text medium inline">
                                    <span id="buyerMobileNo"></span>
                                </label>
                            </td>
                            <th>합배송주문번호</th>
                            <td>
                                <label class="ui text medium inline">
                                    <span id="orgPurchaseOrderNo"></span>
                                </label>
                            </td>
                            <th>마켓주문번호</th>
                            <td>
                                <label class="ui text medium inline">
                                    <span id="marketOrderNo"></span>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <th>선물 수령인</th>
                            <td>
                                <label class="ui text medium inline">
                                    <span id="receiveGiftNm">-</span>
                                </label>
                            </td>
                            <th>수령인 연락처</th>
                            <td>
                                <label class="ui text medium inline">
                                    <span id="receiveGiftPhone">-</span>
                                </label>
                            </td>
                            <th>수락기한</th>
                            <td>
                                <label class="ui text medium inline">
                                    <span id="dsGiftAutoCancelDt">-</span>
                                </label>
                            </td>
                            <th>수락일시</th>
                            <td>
                                <label class="ui text medium inline">
                                    <span id="giftAcpDt">-</span>
                                </label>
                            </td>
                            <th>법인주문여부</th>
                            <td>
                                <label class="ui text medium inline">
                                    <span id="corpOrderYn">-</span>
                                </label>
                            </td>
                            <th>제휴채널ID</th>
                            <td>
                                <label class="ui text medium inline">
                                    <span id="affiliateCd">-</span>
                                </label>
                            </td>
                        </tr>
                    </tbody>
            </table>
            <input type="hidden" id="orderType" name="orderType">
            <input type="hidden" id="cmbnClaimYn" name="cmbnClaimYn">
        </form>
    </div>

    <div  class="com wrap-title sub" >
        <h3 class="title">• 금액정보</h3>
        <span class="pull-right" id="orderSaveArea">
            <button type="button" class="ui button medium" id="orderSaveBtn">적립내역</button>
        </span>
    </div>
    <div class="topline"></div>
    <!-- 주문정보 상세 - 금액정보 테이블 -->
    <div class="ui wrap-table horizontal mg-t-10">
        <form id="orderDetailPriceInfo">
            <table class="ui table" id="orderDetailPriceInfoTable">
                <caption>주문정보 상세 - 금액정보 검색</caption>
                <colgroup>
                    <col width="80px">
                    <col width="100px">
                    <col width="80px">
                    <col width="90px">
                    <col width="80px">
                    <col width="90px">
                    <col width="100px">
                    <col width="90px">
                    <col width="100px">
                    <col width="100px">
                    <col width="90px">
                    <col width="100px">
                </colgroup>
                <tbody>
                    <tr>
                        <th>결제금액</th>
                        <td>
                            <label class="ui text medium inline">
                                <span id="totAmt"></span>
                            </label>
                        </td>
                        <th>총 상품금액</th>
                        <td>
                            <label class="ui text medium inline">
                                <span id="totItemPrice"></span>
                            </label>
                        </td>
                        <th>총 할인금액</th>
                        <td>
                            <label class="ui text medium inline">
                                <span id="totDiscountAmt"></span>
                            </label>
                        </td>
                        <th>배송비</th>
                        <td>
                            <label class="ui text medium inline">
                                <span id="totShippingAmt"></span>
                            </label>
                        </td>
                        <th>클레임배송비</th>
                        <td>
                            <label class="ui text medium inline">
                                <span id="totClaimAmt"></span>
                            </label>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                         <th>상품할인</th>
                        <td>
                            <label class="ui text medium inline">
                                <span id="totItemDiscountAmt"></span>
                            </label>
                        </td>
                        <th>중복쿠폰</th>
                        <td>
                            <label class="ui text medium inline">
                                <span id="totAddCouponAmt"></span>
                            </label>
                        </td>
                        <th>배송비할인</th>
                        <td>
                            <label class="ui text medium inline">
                                <span id="totShipDiscountAmt"></span>
                            </label>
                        </td>
                        <th>장바구니할인</th>
                        <td>
                            <label class="ui text medium inline">
                                <span id="totCartDiscountAmt"></span>
                            </label>
                        </td>
                        <th>카드상품즉시할인</th>
                        <td>
                            <label class="ui text medium inline">
                                <span id="totCardDiscountAmt"></span>
                            </label>
                        </td>
                        <th>임직원할인</th>
                        <td>
                            <label class="ui text medium inline">
                                <span id="totEmpDiscountAmt"></span>
                            </label>
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    </div>

    <div  class="com wrap-title sub" >
        <h3 class="title">• 참고정보</h3>
        <span class="pull-right" id="cashReceiptArea">
            <button type="button" class="ui button medium" id="cashReceiptBtn">현금영수증 발급내역</button>
        </span>
    </div>
    <div class="topline"></div>
    <!-- 주문정보 상세 - 구매자정보 테이블 -->
    <div class="ui wrap-table horizontal mg-t-10">
        <form id="orderDetailReferenceInfo">
            <table class="ui table" id="orderDetailReferenceInfoTable">
                <caption>주문정보 상세 - 참고정보 검색</caption>
                <colgroup>
                    <col width="110px">
                    <col width="80px">
                    <col width="80px">
                    <col width="80px">
                    <col width="110px">
                    <col width="80px">
                    <col width="80px">
                    <col width="80px">
                    <col width="110px">
                    <col width="80px">
                    <col width="80px">
                    <col width="80px">
                </colgroup>
                <div class="ui form inline">
                    <tbody>
                        <tr>
                            <th>
                                <span>점포배송상품설명</span>
                                <div style="text-align:center;margin-top: 5px;">
                                    <button type="button" class="ui button medium" id="storeShipItemMemoBtn" disabled>등록/수정</button>
                                </div>
                            </th>
                            <td colspan="3">
                                <label class="ui text medium inline">
                                    <input type="text" id="storeShipItemMemo" name="storeShipItemMemo" class="ui input medium mg-r-5 mg-t-5" style="width:90%;" readonly/>
                                </label>
                            </td>
                            <th>
                                <span>회원메모</span>
                                <div style="margin-top: 5px;">
                                    <button type="button" class="ui button medium" id="userMemoBtn">상세보기</button>
                                </div>
                            </th>
                            <td colspan="3">
                                <label class="ui text medium inline">
                                    <input type="text" id="userMemo" name="userMemo" class="ui input medium mg-r-5 mg-t-5" style="width:90%;" readonly/>
                                </label>
                            </td>
                            <th>점포배송메시지</th>
                            <td colspan="3">
                                <label class="ui text medium inline">
                                    <input type="text" id="storeShipMsg" name="storeShipMsg" class="ui input medium mg-r-5 mg-t-5" style="width:90%;" readonly/>
                                </label>
                            </td>
                        </tr>
                    </tbody>
                </div>
            </table>
        </form>
    </div>

    <div  class="com wrap-title sub" >
        <h3 class="title">• 주문정보</h3>
    </div>
    <div class="topline"></div>
    <div class="com wrap-title horizontal mg-b-10 mg-t-10" >
        <span class="pull-left" id="claimButtonArea">
            <button type="button" class="ui button medium" id="claimAllBtn" disabled>전체주문취소</button>
            <button type="button" class="ui button medium" id="claimBtn" disabled>주문취소</button>
            <button type="button" class="ui button medium" id="claimCancelBtn" disabled>취소신청</button>
            <button type="button" class="ui button medium" id="claimReturnBtn" disabled>반품신청</button>
            <button type="button" class="ui button medium" id="claimExchangeBtn" disabled>교환신청</button>
        </span>
        <span class="pull-right" id="discountButtonArea">
            <button type="button" class="ui button medium mg-r-10" id="giftSend" style="width: 110px;" disabled>선물하기 재전송</button>
            <button type="button" class="ui button medium blue mg-r-10" id="promotion" style="width: 110px;">행사할인</button>
            <button type="button" class="ui button medium blue mg-r-10" id="coupon" style="width: 110px;">쿠폰할인</button>
            <button type="button" class="ui button medium blue mg-r-10" id="event" style="width: 110px;">사은행사</button>
        </span>
        <input type="hidden" name="orderDetailInfo" id="orderDetailInfo" />
    </div>
    <!-- 주문정보 상세 - 주문관리 리스트 RealGrid -->
    <div id="orderDetailGrid" style="width: 100%; height: 300px;"></div>

    <div id="storeShipGridArea">
        <div  class="com wrap-title sub " >
            <h3 class="title">• 점포 배송정보</h3>
        </div>
        <div class="topline"></div>
        <div class="com wrap-title horizontal mg-b-10" >
            <span class="pull-left mg-t-10" id="storeShipButtonArea">
                <button type="button" class="ui button medium" id="storeShipAddrBtn" disabled>배송정보 수정</button>
                <button type="button" class="ui button medium" id="storeSlotChangeBtn" disabled>배송시간 수정</button>
            </span>
            <input type="hidden" id="storeShipInfo" name="storeShipInfo">
        </div>
        <!-- 주문정보 상세 - 배송정보 리스트 RealGrid -->
        <div id="orderDetailStoreShipGrid" style="width: 100%; height: 140px;" class="ui mg-t-10"></div>
    </div>

    <div id="dlvShipGridArea">
        <div  class="com wrap-title sub " >
            <h3 class="title">• 택배 배송정보</h3>
        </div>
        <div class="topline"></div>
        <div class="com wrap-title horizontal mg-b-10" >
            <span class="pull-left mg-t-10" id="dlvShipButtonArea">
                <button type="button" class="ui button medium" id="dlvShipAddrBtn" disabled>배송정보 수정</button>
                <button type="button" class="ui button medium" id="dlvShipCheckBtn" disabled>주문확인</button>
                <button type="button" class="ui button medium" id="dlvInvoiceRegBtn" disabled>송장등록</button>
                <button type="button" class="ui button medium" id="dlvInvoiceEditBtn" disabled>송장수정</button>
                <button type="button" class="ui button medium" id="dlvShipCompleteBtn" disabled>배송완료</button>
                <button type="button" class="ui button medium" id="dlvNoReceiveBtn" disabled>미수취 신고</button>
                <button type="button" class="ui button medium" id="dlvMultiShipStatusBtn" disabled>배송상태변경</button>
            </span>
            <input type="hidden" name="dlvShipInfo" id="dlvShipInfo" />
        </div>
        <!-- 주문정보 상세 - 배송정보 리스트 RealGrid -->
        <div id="orderDetailDlvShipGrid" style="width: 100%; height: 140px;" class="ui mg-t-10"></div>
    </div>

    <div  class="com wrap-title sub" >
        <h3 class="title">• 클레임 정보</h3>
    </div>
    <div class="topline"></div>
    <!-- 주문정보 상세 - 클레임 리스트 RealGrid -->
    <div id="orderClaimGrid" style="width: 100%; height: 205px;" class="ui mg-t-10"></div>

    <div  class="com wrap-title sub" >
        <h3 class="title">• 대체주문 정보</h3>
    </div>
    <div class="topline"></div>
    <!-- 주문정보 상세 - 대체주문 리스트 RealGrid -->
    <div id="orderSubstitutionGrid" style="width: 100%; height: 205px;" class="ui mg-t-10"></div>

    <div  class="com wrap-title sub" >
        <h3 class="title">• 결제 정보</h3>
    </div>
    <div class="topline"></div>
    <div class="ui wrap-table horizontal mg-t-10 mg-b-10">
        <table class="ui table" id="paymentInfoTable">
            <caption>주문정보 상세 - 결제정보</caption>
            <colgroup>
                <col width="100%">
            </colgroup>
            <tbody>
                <tr>
                    <th>
                        <label class="ui text medium inline">
                            최종 결제금액 <span id="totalPaymentAmt">0</span>원
                        </label>
                    </th>
                </tr>
            </tbody>
        </table>
    </div>
    <!-- 주문정보 상세 - 점포배송정보 리스트 RealGrid -->
    <div id="orderPaymentGrid" style="width: 100%; height: 205px;" class="ui mg-t-10"></div>

    <div  class="com wrap-title sub" >
        <h3 class="title">• 히스토리</h3>
    </div>
    <div class="topline"></div>
    <!-- 주문정보 상세 - 점포배송정보 리스트 RealGrid -->
    <div id="orderHistoryGrid" style="width: 100%; height: 205px;" class="ui mg-t-10"></div>
</div>

<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/orderDetail.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/orderUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/gridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/core/jquery.validate.js?v=${fileVersion}"></script>

<script>
    ${orderDetailGridBaseInfo}
    ${orderDetailStoreShipGridBaseInfo}
    ${orderDetailDlvShipGridBaseInfo}
    ${orderClaimGridBaseInfo}
    ${orderPaymentGridBaseInfo}
    ${orderHistoryGridBaseInfo}
    ${orderSubstitutionGridBaseInfo}

    var purchaseOrderNo = ${purchaseOrderNo};
    var frontUrl = '${frontUrl}'; // 프론트 페이지 URL
    var theClub = '${theClub}'; // 프론트 페이지 URL

    var orderDetailGrid = GridUtil.initializeGrid("orderDetailGrid", orderDetailGridBaseInfo);
    var orderDetailStoreShipGrid = GridUtil.initializeGrid("orderDetailStoreShipGrid", orderDetailStoreShipGridBaseInfo);
    var orderDetailDlvShipGrid = GridUtil.initializeGrid("orderDetailDlvShipGrid", orderDetailDlvShipGridBaseInfo);
    var orderClaimGrid = GridUtil.initializeGrid("orderClaimGrid", orderClaimGridBaseInfo);
    var orderPaymentGrid = GridUtil.initializeGrid("orderPaymentGrid", orderPaymentGridBaseInfo);
    var orderHistoryGrid = GridUtil.initializeGrid("orderHistoryGrid", orderHistoryGridBaseInfo);
    var orderSubstitutionGrid = GridUtil.initializeGrid("orderSubstitutionGrid", orderSubstitutionGridBaseInfo);

</script>