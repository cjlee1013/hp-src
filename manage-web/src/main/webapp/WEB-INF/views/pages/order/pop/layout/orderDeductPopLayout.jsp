<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />


<div class="ui form inline mg-t-10 mg-l-10 mg-b-10" id="deductForm">
    <label class="ui checkbox">
        <input type="checkbox" id="piDeductPromoYn" name="piDeductPromoYn" disabled/>
        <span class="text mg-r-20">행사유지</span>
    </label>
    <label class="ui checkbox">
        <input type="checkbox" id="piDeductDiscountYn" name="piDeductDiscountYn" disabled/>
        <span class="text mg-r-20">할인유지</span>
    </label>
    <label class="ui checkbox">
        <input type="checkbox" id="piDeductShipYn" name="piDeductShipYn" disabled/>
        <span class="text mg-r-20">배송비 미차감</span>
    </label>
</div>
<input type="hidden" id="oriPiDeductPromoYn" name="oriPiDeductPromoYn"/>
<input type="hidden" id="oriPiDeductDiscountYn" name="oriPiDeductDiscountYn"/>
<input type="hidden" id="oriPiDeductShipYn" name="oriPiDeductShipYn"/>

<!-- 상품 정보 테이블 end -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/pop/layout/orderDeductPopLayout.js?v=${fileVersion}"></script>

