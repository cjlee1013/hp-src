<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/gridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/orderUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/orderMain.js?v=${fileVersion}"></script>
<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">주문 관리</h2>
    </div>
    <div  class="com wrap-title sub" >
        <div class="ui wrap-table horizontal mg-t-10">
            <form id="searchForm" name="searchForm" onsubmit="return false;">
                <table class="ui table">
                    <colgroup>
                        <col width="110px">
                        <col width="200px">
                        <col width="110px">
                        <col width="200px">
                        <col width="110px">
                        <col width="220px">
                        <col width="110px">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>조회기간</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <label for="schPeriodType">
                                    <select id="schPeriodType" name="schPeriodType" class="ui input medium mg-r-20 mg-t-5" style="width:110px;float:left;">
                                        <option value="01" selected>주문일시</option>
                                        <option value="02">결제일시</option>
                                    </select>
                                </label>
                                <input type="text" id="schStartDt" name="schStartDt" class="ui input medium mg-r-5 mg-t-5" style="width:120px;" />
                                <span class="text">&nbsp;&nbsp;~&nbsp;&nbsp;</span>
                                <input type="text" id="schEndDt" name="schEndDt" class="ui input medium mg-r-5 mg-t-5" style="width:120px;" />
                                <span class="text">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5 mg-t-5" onclick="orderMain.initSearchDate('-1h');">1시간</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5 mg-t-5" onclick="orderMain.initSearchDate('-1d');">1일전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5 mg-t-5" onclick="orderMain.initSearchDate('-30d');">1개월전</button>
                                <button type="button" class="ui button small gray-dark font-malgun mg-r-5 mg-t-5" onclick="orderMain.initSearchDate('-90d');">3개월전</button>
                            </div>
                        </td>
                        <th>마켓연동</th>
                        <td>
                            <div class="ui form inline">
                                <select id="affiliateCd" name="affiliateCd" class="ui input medium mg-r-5" style="width:100px;float:left;">
                                    <option value="ALL" selected>전체</option>
                                    <option value="naver">N마트</option>
                                    <option value="eleven">11번가</option>
                                </select>
                            </div>
                        </td>
                        <td rowspan="3">
                            <div style="text-align: center;" class="ui form">
                                <button type="button" id="searchBtn" class="ui button large cyan "><i class="fa fa-search"></i>검색</button><br/>
                                <button type="button" id="searchResetBtn" class="ui button large white mg-t-5">초기화</button><br/>
                                <button type="button" id="excelDownloadBtn"	class="ui button large dark-blue mg-t-5">엑셀다운</button><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>결제상태</th>
                        <td>
                            <div class="ui form inline">
                                <label class="ui checkbox mg-l-10">
                                    <input type="checkbox" name="paymentStatus" value="ALL"><span class="text text-guide mg-t-5">전체</span>
                                </label>
                                <label class="ui checkbox">
                                    <input type="checkbox" name="paymentStatus" value="P1"><span class="text text-guide mg-t-5">결제요청</span>
                                </label>
                                <label class="ui checkbox">
                                    <input type="checkbox" name="paymentStatus" value="P3" checked><span class="text text-guide mg-t-5">결제완료</span>
                                </label>
                                <label class="ui checkbox">
                                    <input type="checkbox" name="paymentStatus" value="P4"><span class="text text-guide mg-t-5">결제실패</span>
                                </label>
                            </div>
                        </td>
                        <th>주결제수단</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <label class="ui checkbox mg-l-10">
                                    <input type="checkbox" name="parentMethodCd" value="ALL" checked><span class="text text-guide mg-r-10 mg-t-5">전체</span>
                                </label>
                                <label class="ui checkbox">
                                    <input type="checkbox" name="parentMethodCd" value="CARD" checked><span class="text text-guide mg-r-10 mg-t-5">신용카드</span>
                                </label>
                                <label class="ui checkbox">
                                    <input type="checkbox" name="parentMethodCd" value="KAKAOPAY" checked><span class="text text-guide mg-r-10 mg-t-5">카카오카드</span>
                                </label>
                                <label class="ui checkbox">
                                    <input type="checkbox" name="parentMethodCd" value="KAKAOMONEY" checked><span class="text text-guide mg-r-10 mg-t-5">카카오머니</span>
                                </label>
                                <label class="ui checkbox">
                                    <input type="checkbox" name="parentMethodCd" value="NAVERPAY" checked><span class="text text-guide mg-r-10 mg-t-5">네이버페이</span>
                                </label>
                                <label class="ui checkbox">
                                    <input type="checkbox" name="parentMethodCd" value="PAYCO" checked><span class="text text-guide mg-r-10 mg-t-5">페이코</span>
                                </label>
                                <label class="ui checkbox">
                                    <input type="checkbox" name="parentMethodCd" value="FREE" checked><span class="text text-guide mg-r-10 mg-t-5">0원결제</span>
                                </label>
                                <label class="ui checkbox">
                                    <input type="checkbox" name="parentMethodCd" value="POINT" checked><span class="text text-guide mg-r-10 mg-t-5">100% 포인트 결제</span>
                                </label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>점포/판매업체</th>
                        <td colspan="3">
                            <div class="ui form inline">
                                <select style="width: 110px;" name="schStoreType" id="schStoreType" class="ui input medium mg-r-10" >
                                    <option value="ALL">전체</option>
                                    <c:forEach var="codeDto" items="${storeType}" varStatus="status">
                                        <c:set var="selected" value=""/>
                                        <c:if test="${codeDto.ref1 eq 'df'}">
                                            <c:set var="selected" value="selected"/>
                                        </c:if>
                                        <option value="${codeDto.mcCd}" ${selected}>${codeDto.mcNm}</option>
                                    </c:forEach>
                                </select>
                                <input type="text" id="schStoreId" name="schStoreId" class="ui input medium mg-r-10" style="width: 120px;" readonly>
                                <input type="text" id="schStoreNm" name="schStoreNm" class="ui input medium mg-r-10" style="width: 150px;" readonly>
                                <button type="button" class="ui button medium mg-r-5" id="schStoreBtn" disabled>조회</button>
                            </div>
                        </td>
                        <th>구매자</th>
                        <td style="width: 220px;">
                            <div class="ui form inline">
                                <label class="ui radio inline mg-t-5">
                                    <input type="radio" name="schNonUserYn" value="N" checked><span>회원</span>&nbsp;&nbsp;&nbsp;
                                </label>
                                <label class="ui radio inline mg-t-5">
                                    <input type="radio" name="schNonUserYn" value="Y"><span>비회원</span>
                                </label>
                                <select style="width: 110px; display: none" name="schBuyerInfo" id="schBuyerInfo" class="ui input medium mg-r-10 mg-t-5">
                                    <option value="01" selected>구매자 이름</option>
                                    <option value="02">구매자 연락처</option>
                                    <option value="03">구매자 이메일</option>
                                </select>
                                <input type="text" id="schUserSearch" name="schUserSearch" class="ui input medium mg-r-10 mg-t-5" style="width: 150px;" readonly>
                                <button type="button" class="ui button medium mg-r-5 mg-t-5" id="userSchButton" onclick="memberSearchPopup('orderMain.callbackMemberSearchPop'); return false;">조회</button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>검색조건</th>
                        <td>
                            <select id="schDetailType" name="schDetailType" class="ui input medium mg-r-5 mg-t-5" style="width:140px;float:left;">
                                <option value="PURCHASE" selected>주문번호</option>
                                <option value="MARKETORDERNO">마켓주문번호</option>
                                <option value="ORDER">상품주문번호</option>
                                <option value="SHIP">배송번호</option>
                                <option value="ITEM">상품번호</option>
                                <option value="BUYER_TEL_NO">구매자 연락처</option>
                                <option value="RECEIVER_TEL_NO">수령인 연락처</option>
                                <option value="PG_OID">pg연동 주문번호</option>
                                <option value="OMNI_PURCHASE">옴니주문번호</option>
                                <option value="SUB_ORDER">대체주문번호</option>
                            </select>
                            <div style="float:left;" class="ui form inline">
                                <input type="text" id="schDetailContents" name="schDetailContents" class="ui input medium mg-r-5 mg-t-5" style="width: 350px;">
                            </div>
                        </td>
                        <th>배송유형</th>
                        <td>
                            <div class="ui form inline">
                                <label class="ui checkbox mg-l-10">
                                    <input type="checkbox" id="schReserveDlvYn" name="schReserveShipMethod" value="RESERVE_DRCT_DLV"><span class="text text-guide mg-t-5">명절선물세트</span>
                                </label>
                                <label class="ui checkbox">
                                    <input type="checkbox" id="schReserveDrctYn" name="schReserveShipMethod" value="RESERVE_TD_DRCT"><span class="text text-guide mg-t-5">절임배추</span>
                                </label>
                                <label class="ui checkbox">
                                    <input type="checkbox" id="schReserveDrctNorYn" name="schReserveShipMethod" value="RESERVE_DRCT_NOR"><span class="text text-guide mg-t-5">일반상품</span>
                                </label>
                            </div>
                        </td>
                        <th>상품유형</th>
                        <td>
                            <div class="ui form inline">
                                <label class="ui checkbox mg-l-10">
                                    <input type="checkbox" id="schItemType" name="schItemType" value="P"><span class="text text-guide mg-t-5">업체배송상품</span>
                                </label>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과: <span id="totalCnt">0</span>건</h3>
        </div>
        <div class="mg-b-20 " id="orderManageGrid" style="height: 500px; width:100%;"></div>
    </div>
</div>
<!-- script -->


<script>
    ${orderManageGridBaseInfo}
    var frontUrl = '${frontUrl}'; // 프론트 페이지 URL
    var theClub = '${theClub}'; // 프론트 페이지 URL
    var orderManageGrid = GridUtil.initializeGrid("orderManageGrid", orderManageGridBaseInfo);
    var storeId = '${storeId}';
    var storeNm = '${storeNm}';
    var storeType = '${soStoreType}';
</script>