<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-popup system">
    <div class="com wrap-title">
        <h2 class="title font-malgun">
            <i class="fa fa-square mg-r-5"></i> 발송 지연 내역
        </h2>
    </div>
    <!-- 주문정보 상세 - 구매자정보 테이블 -->
    <div class="ui wrap-table horizontal mg-t-10">
        <form id="orderDelayShipForm">
            <table class="ui table" id="orderDelayShipTable">
                <caption>주문관리 - 발송지연내역</caption>
                <colgroup>
                    <col width="30%">
                    <col width="70%">
                </colgroup>
                <tbody>
                    <tr>
                        <th>등록일시</th>
                        <td>
                            <label class="ui text medium inline">
                                <span id="delayShipRegDt"></span>
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <th>지연사유</th>
                        <td>
                            <label class="ui text medium inline">
                                <span id="delayShipCd"></span>
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <th>2차 발송기한</th>
                        <td>
                            <label class="ui text medium inline">
                                <span id="delayShipDt"></span>
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <th>상세사유</th>
                        <td>
                            <label class="ui text medium inline">
                                <span id="delayShipMsg"></span>
                            </label>
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    </div>
    <div class="ui center-button-group">
        <span class="inner">
            <button id="closeBtn" class="ui button xlarge font-malgun" onclick="self.close()">닫기</button>
        </span>
    </div>
</div>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/orderUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/gridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/pop/orderDelayShipping.js?v=${fileVersion}"></script>

<script>
    const shipNo = ${shipNo};

    $(document).ready(function() {
        CommonAjaxBlockUI.global();
        orderDelayShipping.init();
    });

</script>