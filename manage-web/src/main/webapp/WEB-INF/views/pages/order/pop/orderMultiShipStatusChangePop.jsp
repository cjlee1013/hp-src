<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<div class="com wrap-popup system">
    <div class="com wrap-title">
        <h2 class="title font-malgun">
            <i class="fa fa-square mg-r-5"></i> 배송상태변경
        </h2>
    </div>
    <div class="topline mg-t-10"></div>
    <div class="com wrap-title mg-t-10 mg-b-10" style="padding: 10px;">
        <div>
            <ul class="com-list-box em">
                <li>※ 전체주문취소 시에만 사용하여 주세요.</li>
                <li>※ 전체 배송지의 배송상태가 신규주문으로 변경됩니다.</li>
            </ul>
        </div>
    </div>
    <div class="topline mg-t-10"></div>
    <div class="ui center-button-group">
        <span class="inner">
            <button id="changeBtn" class="ui button xlarge btn-danger font-malgun">변경</button>
            <button id="closeBtn" class="ui button xlarge font-malgun" onclick="self.close()">취소</button>
        </span>
    </div>
</div>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/orderUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/gridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/pop/orderMultiShipStatusChange.js?v=${fileVersion}"></script>

<script>
    const purchaseOrderNo = '${purchaseOrderNo}';
</script>