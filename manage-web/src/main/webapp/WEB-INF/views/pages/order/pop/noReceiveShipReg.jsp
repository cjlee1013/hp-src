<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-popup system">
    <div class="com wrap-title">
        <h2 class="title font-malgun">
            <i class="fa fa-square mg-r-5"></i> 미수취 신고
        </h2>
    </div>
    <div class="topline mg-t-10"></div>
    <!-- 주문정보 상세 - 구매자정보 테이블 -->
    <div class="ui wrap-table horizontal">
        <form id="noRcvProcessRegForm">
            <table class="ui table" id="noRcvProcessRegTable">
                <caption>주문관리 - 미수취 신고 등록</caption>
                <colgroup>
                    <col width="30%">
                    <col width="70%">
                </colgroup>
                <tbody>
                    <tr>
                        <th>신고사유 <span class="text-red">*</span></th>
                        <td>
                            <label for="noRcvDeclrType">
                                <select id="noRcvDeclrType" name="noRcvDeclrType" class="ui input medium mg-r-10" style="width: 100%;">
                                    <option value="">선택하세요</option>
                                    <c:forEach var="noRcvDeclrType" items="${noRcvDeclrTypeList}" varStatus="status">
                                        <option value="${noRcvDeclrType.key}">${noRcvDeclrType.value}</option>
                                    </c:forEach>
                                </select>
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <th>상세사유 <span class="text-red">*</span></th>
                        <td>
                            <div class="ui form inline">
                                <label for="noRcvDetailReason">
                                    <textarea id="noRcvDetailReason" name="noRcvDetailReason" class="ui input" style="width: 100%; height: 80px;" maxlength="200"></textarea>
                                </label>
                                <br>
                                <span class="text pull-right">( <span style="color:red;" id="textCountKr_noRcvDetailReason">0</span> / 200자 )</span>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    </div>
    <div class="topline"></div>
    <div class="ui center-button-group">
        <span class="inner">
            <button id="saveBtn" class="ui button xlarge btn-danger font-malgun">저장</button>
            <button id="closeBtn" class="ui button xlarge font-malgun" onclick="self.close()">취소</button>
        </span>
    </div>
</div>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/orderUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/gridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/validateUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/pop/noReceiveShip.js?v=${fileVersion}"></script>

<script>
    var noRcvShipParam = ${noRcvShipParam};
    $(document).ready(function() {
        CommonAjaxBlockUI.global();
        noRcvShipReg.init();
    });
</script>