<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<div class="com wrap-popup system" style="height: auto; background-color: #FFFFFF; box-shadow:none!important;">
    <div class="com popup-wrap-title">
        <c:choose>
            <c:when test="${orderCancelYn eq 'Y'}">
                <c:choose>
                    <c:when test="${claimPartYn eq 'N'}">
                        <h5 class="title font-malgun">전체주문취소</h5>
                    </c:when>
                    <c:otherwise>
                        <h5 class="title font-malgun">주문취소</h5>
                    </c:otherwise>
                </c:choose>
            </c:when>
            <c:otherwise>
                <h5 class="title font-malgun">취소신청</h5>
            </c:otherwise>
        </c:choose>
    </div>
    <form id="orderCancelForm" onsubmit="return false;">
        <!-- 환불정보 Layout -->
        <c:choose>
            <c:when test="${claimPartYn eq 'Y'}">
                <c:choose>
                    <c:when test="${multiShipYn eq 'Y'}">
                        <jsp:include page="layout/multiShipOrderItemInfoPopLayout.jsp"></jsp:include>
                    </c:when>
                    <c:otherwise>
                        <jsp:include page="layout/orderItemInfoPopLayout.jsp"></jsp:include>
                    </c:otherwise>
                </c:choose>
            </c:when>
            <c:otherwise>
                <jsp:include page="layout/orderItemInfoPopLayout.jsp"></jsp:include>
            </c:otherwise>
        </c:choose>
        <!-- 환불정보 Layout end -->

        <!-- 취소/반품/교환 사유 입력 -->
        <div class="ui wrap-table horizontal" style="border-right: 1px solid #eee; border-left: 1px solid #eee; width: 100%">
            <div class="ui form inline mg-t-10 mg-l-10 mg-b-10">
                <select id="claimReasonType" name="claimReasonType" class="ui input medium mg-r-20" style="width: 25%;">
                    <option value="">선택</option>
                    <c:forEach var="codeDto" items="${claimReasonType}" varStatus="status">
                        <c:if test="${codeDto.ref1 eq 'C'}">
                            <option value="${codeDto.mcCd}">${codeDto.mcNm}</option>
                        </c:if>
                    </c:forEach>
                </select>
                <input type="text" id="claimReasonDetail" name="claimReasonDetail" class="ui input medium mg-r-5" style="width: 70%;" maxlength="100" placeholder="취소 상세사유를 입력 (최대 100자)">
            </div>
            <jsp:include page="layout/orderDeductPopLayout.jsp"></jsp:include>
            <input type="hidden" id="oriClaimReasonType" name="oriClaimReasonType">
            <input type="hidden" id="detailReasonRequired" name="detailReasonRequired">
        </div>
        <!-- 취소/반품/교환 사유 입력 end -->

        <!-- 환불예정금액 버튼 -->
        <div class="ui form inline mg-t-10" id="syrupTextTag" style="display: none">
            <span class="text" style="color: #f90000; font-size: 14px">&#8251; 시럽쿠폰 사용으로 인한 부분취소/반품 접수 불가로 해당상품의 결제금액으로 마일리지 적립 요청해주시기 바랍니다.</span>
        </div>

        <div id="refundInfoArea">
            <div class="ui center-button-group">
                <span class="inner">
                    <c:choose>
                        <c:when test="${claimPartYn eq 'Y'}">
                            <button class="ui button large dark-blue font-malgun" id="checkRefundAmt" onclick="refundInfoLayout.checkRefundAmt()">환불예정금액 확인</button>
                        </c:when>
                        <c:otherwise>
                            <button class="ui button large dark-blue font-malgun" id="checkRefundAmt" onclick="refundInfoLayout.allCancelCheckRefundAmt()">환불예정금액 확인</button>
                        </c:otherwise>
                    </c:choose>

                    <input type="hidden" id="preRefundAmtReturnCode">
                    <input type="hidden" id="claimType" name="claimType" value="C">
                </span>
            </div>
            <!-- 환불예정금액 버튼 end -->

            <!-- 환불정보 Layout -->
            <jsp:include page="layout/orderRefundInfoPopLayout.jsp"></jsp:include>
            <!-- 환불정보 Layout end -->
        </div>
        </form>
        <div class="ui center-button-group">
            <span class="inner">
                <input type="hidden" id="claimPartYn" value="${claimPartYn}">
                <input type="hidden" id="orderCancelYn" value="${orderCancelYn}">
                <input type="hidden" id="multiShipYn" value="${multiShipYn}">

                <c:choose>
                    <c:when test="${orderCancelYn eq 'Y'}">
                        <button class="ui button xlarge btn-danger font-malgun" id="reqBtn" onclick="orderCancel.submit()">주문취소</button>
                    </c:when>
                    <c:otherwise>
                        <button class="ui button xlarge btn-danger font-malgun" id="reqBtn" onclick="orderCancel.submit()">취소신청</button>
                    </c:otherwise>
                </c:choose>
                <button class="ui button xlarge font-malgun" id="closeBtn" onclick="self.close()">닫기</button>
            </span>
        </div>
    </div>
</div>

<script>
    var orderCancelYn = '${orderCancelYn}';
    var claimPartYn = '${claimPartYn}';
    var claimReasonTypeList = new Array();

    <c:forEach var="codeDto" items="${claimReasonType}">
        <c:if test="${codeDto.ref1 eq 'C'}">
            claimReasonTypeList.push({
                mcCd : "${codeDto.mcCd}",
                mcNm : "${codeDto.mcNm}",
                whoReason : "${codeDto.ref2}",
                detailReasonRequired : "${codeDto.ref4}"
            });
        </c:if>
    </c:forEach>
</script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/pop/orderCancelPop.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/pop/orderCommonPop.js?v=${fileVersion}"></script>
