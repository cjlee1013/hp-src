<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />


<style type="text/css">
    .head {
        border-right: 1px solid #eee;
        text-align: center;
        font-size: 14px;
    }
    .itemName1 {
        font-weight: bold;
        font-size: 14px;
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
    }
    .itemName2 {
        font-weight: bold;
        font-size: 14px;
    }
    .addItem {
        font-size: 12px;
        font-weight: bold;
    }
</style>

<div class="com wrap-popup medium popup-zipcode" style="background-color: #FFFFFF">
    <div class="com popup-wrap-title">
        <h5 class="title font-malgun mg-b-10">수거지변경</h5>
        <label>* 반품수거지 - 상세 정보</label>
    </div>
    <div class="tabui-wrap mg-t-10">
        <!-- tab-cont -->
        <div class="tab-cont">
            <table class="ui table">
                <tr>
                    <th style="width: 100px;">*이름</th>
                    <td>
                        <input type="text" id="name" name="name" class="ui input medium mg-r-5" style="width: 40%;" minlength="2" maxlength="20" placeholder="이름을 입력해주세요" value="${param.name}">
                    </td>
                </tr>
                <tr>
                    <th>*연락처</th>
                    <tr>
                        <th>*연락처</th>
                        <td style="text-align: left">
                            <input type="hidden" name="phone" id="phone">
                            <input type="text" name="mobileNo_1" id="mobileNo_1" class="inp-base w-80 checkVal" title="연락처" maxlength="3"> -
                            <input type="text" name="mobileNo_2" id="mobileNo_2" class="inp-base w-80 checkVal" title="연락처" maxlength="4"> -
                            <input type="text" name="mobileNo_3" id="mobileNo_3" class="inp-base w-80 checkVal" title="연락처" maxlength="4">
                        </td>
                    </tr>
                </tr
                <tr>
                    <th>*주소</th>
                    <td style="text-align: left">
                        <div class="ui form inline">
                        <input type="text" name="zipCode" id="zipCode" class="ui input medium mg-r-5" style="width:100px;" value="${param.zipCode}" readonly><button type="button" class="ui button small" onclick="javascript:zipCodePopup('deliveryInfoPop.setZipcode'); return false;">우편번호 찾기</button>
                        <br>
                        <input type="text" name="addr" id="addr" class="ui input medium mg-t-5 mg-r-5" style="width:150px;" value="${param.addr}" readonly>
                        <input type="text" name="addrDetail" id="addrDetail" class="ui input medium mg-t-5" style="width:240px;" maxlength="100" placeholder="상세주소를 입력해주세요" value="${param.addrDetail}">
                        </div>
                    </td>
                </tr>
            </table>
            <!-- //bottom -->
             <div class="ui center-button-group mg-b-20">
             <span class="inner">
                 <input type="hidden" id="reqType" name="reqType" value="${param.reqType}"> <!-- 수거지/배송지 타입 코드 [1:수거지, 2:배송지] -->
                 <button class="ui button small btn-danger font-malgun" id="saveBtn" onclick="deliveryInfoPop.sendDeliveryInfo()">저장</button>
                 <button class="ui button small font-malgun" id="closeBtn" onclick="self.close()">취소</button>
             </span>
             </div>
        </div>
        <!-- //tab-cont -->
    </div>

</div>
<script>
/** * 전화번호 형식 체크 * * @param 데이터 */


var deliveryInfoPop = {



  setZipcode : function(res){
    $("#zipCode").val(res.zipCode);
    $("#addr").val(res.roadAddr);
  },
  sendDeliveryInfo : function(){

    if($('#name').val().length < 2){
      alert('받는사람 이름을 2자 이상 입력해주세요.');
      return false;
    }

    if($.jUtil.isEmpty($('#name').val())){
        alert('받는사람 이름을 입력해주세요.');
        return false;
    }

    if($.jUtil.isEmpty($('#phone').val())){
        alert('연락처를 입력해주세요.');
        return false;
    }

    if ($.jUtil.isEmpty($('#zipCode').val())) {
        alert('수거지 우편번호를 입력주세요');
        return false;
    }
    //수거지 주소
    if ($.jUtil.isEmpty($('#addr').val())) {
        alert('주소를 입력주세요');
        return false;
    }
    //수거지 상세주소
    if ($.jUtil.isEmpty($('#addrDetail').val())) {
        alert('상세주소를 입력주세요');
        return false;
    }


    var deliveryInfo = {
      "name" : $('#name').val(),
      "phone" :$('#phone').val(),
      "zipCode" : $('#zipCode').val(),
      "addr" : $('#addr').val(),
      "gibunAddr" : $('#gibunAddr').val(),
      "addrDetail" : $('#addrDetail').val(),
      "reqType" : $('#reqType').val()
    }
    opener.setAddrCallBack(deliveryInfo);
    self.close();
  }
}



$(function(){
    $('#phone').on('keydown', function(e){
       // 숫자만 입력받기
        var trans_num = $(this).val().replace(/-/gi,'');
	var k = e.keyCode;

	if(trans_num.length >= 11 && ((k >= 48 && k <=126) || (k >= 12592 && k <= 12687 || k==32 || k==229 || (k>=45032 && k<=55203)) )){
  	    e.preventDefault();
	}
    }).on('blur', function(){ // 포커스를 잃었을때 실행합니다.
        if($(this).val() == '') return;

        // 기존 번호에서 - 를 삭제합니다.
        var trans_num = $(this).val().replace(/-/gi,'');

        // 입력값이 있을때만 실행합니다.
        if(trans_num != null && trans_num != '')
        {
            // 총 핸드폰 자리수는 11글자이거나, 10자여야 합니다.
            if(trans_num.length==11 || trans_num.length==10)
            {
                // 유효성 체크
                var regExp_ctn = /^(01[016789]{1}|02|0[3-9]{1}[0-9]{1})([0-9]{3,4})([0-9]{4})$/;
                if(regExp_ctn.test(trans_num))
                {
                    // 유효성 체크에 성공하면 하이픈을 넣고 값을 바꿔줍니다.
                    trans_num = trans_num.replace(/^(01[016789]{1}|02|0[3-9]{1}[0-9]{1})-?([0-9]{3,4})-?([0-9]{4})$/, "$1-$2-$3");
                    $(this).val(trans_num);
                }
                else
                {
                    alert("연락처를 다시 입력해주세요.");
                    $(this).val("");
                    $(this).focus();
                }
            }
            else
            {
                alert("연락처를 다시 입력해주세요.");
                $(this).val("");
                $(this).focus();
            }
      }
  });
});
</script>
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?v=${fileVersion}"></script>