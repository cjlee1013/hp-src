<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />

<div class="com wrap-popup system">
    <div class="com wrap-title">
        <h2 class="title font-malgun">
            <i class="fa fa-square mg-r-5"></i> 점포배송상품설명 등록/수정
        </h2>
    </div>
    <!-- 주문정보 상세 - 구매자정보 테이블 -->
    <div class="ui wrap-table horizontal mg-t-10">
        <form id="orderStoreShipMemoInfoForm">
            <table class="ui table" id="orderStoreShipMemoInfoTable">
                <caption>주문관리 - 점포배송상품설명</caption>
                <colgroup>
                    <col width="100%">
                </colgroup>
                <tbody>
                    <tr>
                        <td>
                            <div class="ui form inline">
                                <textarea id="shipMemo" name="shipMemo" class="ui input mg-t-5" style="float:left; width: 100%; height: 122px;" maxlength="150"></textarea>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
            <input type="hidden" id="shipAddrNo" name="shipAddrNo" />
        </form>
    </div>
    <div class="ui center-button-group">
        <span class="inner">
            <button id="saveBtn" class="ui button xlarge font-malgun">저장</button>
            <button id="closeBtn" class="ui button xlarge font-malgun" onclick="self.close()">취소</button>
        </span>
    </div>
</div>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/orderUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/gridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/pop/orderStoreShipMemoPop.js?v=${fileVersion}"></script>

<script>
    const purchaseOrderNo = '${purchaseOrderNo}';
    const bundleNo = '${bundleNo}';
    $(document).ready(function() {
        CommonAjaxBlockUI.global();
        orderStoreShipMemoPop.init();
    });

</script>