<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<div class="com wrap-popup system">
    <div class="com wrap-title">
        <h2 class="title font-malgun">
            <i class="fa fa-square mg-r-5"></i> 쿠폰 할인정보
        </h2>
    </div>
    <div class="ui wrap-table horizontal mg-t-10 mg-b-10">
        <table class="ui table" id="paymentInfoTable">
            <caption>쿠폰할인정보 합계</caption>
            <colgroup>
                <col width="100%">
            </colgroup>
            <tbody>
            <tr>
                <th>
                    <label class="ui text medium inline">
                        총 할인금액 : <span id="totalDiscountAmt">0</span>원 (상품할인 <span id="itemDiscountAmt">0</span>원 + 배송비할인 : <span id="shipDiscountAmt">0</span>원 + 중복쿠폰 : <span id="addDiscountAmt">0</span>원 + 장바구니할인 : <span id="cartDiscountAmt">0</span>원)
                    </label>
                </th>
            </tr>
            </tbody>
        </table>
    </div>
    <div id="orderCouponInfoGrid" style="width: 100%; height: 230px;"></div>
    <div class="ui center-button-group">
        <span class="inner">
            <button id="closeBtn" class="ui button xlarge font-malgun" onclick="self.close()">닫기</button>
        </span>
    </div>
</div>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/orderUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/gridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/pop/orderDiscountPop.js?v=${fileVersion}"></script>

<script>

    ${orderCouponInfoGridBaseInfo}
    const purchaseOrderNo = '${purchaseOrderNo}';
    const orderCouponInfoGrid = GridUtil.initializeGrid("orderCouponInfoGrid", orderCouponInfoGridBaseInfo);

    $(document).ready(function() {
        CommonAjaxBlockUI.global();
        orderCouponDiscountInfo.init();
    });

</script>