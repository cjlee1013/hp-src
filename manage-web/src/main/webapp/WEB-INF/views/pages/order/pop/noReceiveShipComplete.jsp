<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp" />
<div class="com wrap-popup system">
    <div class="com wrap-title">
        <h2 class="title font-malgun">
            <i class="fa fa-square mg-r-5"></i> <span id="completeTitle">미수취 신고</span>
        </h2>
    </div>
    <div class="topline mg-t-10"></div>
    <!-- 주문정보 상세 - 구매자정보 테이블 -->
    <div class="ui wrap-table horizontal">
        <form id="noRcvProcessCompleteForm">
            <table class="ui table" id="noRcvProcessCompleteTable">
                <caption>주문관리 - 미수취 신고 처리완료</caption>
                <colgroup>
                    <col width="20%">
                    <col width="35%">
                    <col width="20%">
                    <col width="35%">
                </colgroup>
                <tbody>
                    <tr>
                        <th>신고일시</th>
                        <td><span id="noRcvDeclrDt"></span></td>
                        <th>등록자</th>
                        <td><span id="noRcvRegId"></span></td>
                    </tr>
                    <tr>
                        <th>신고사유</th>
                        <td colspan="3"><span id="noRcvDeclrType"></span></td>
                    </tr>
                    <tr>
                        <th>상세사유</th>
                        <td colspan="3"><span id="noRcvDetailReason"></span></td>
                    </tr>
                    <tr>
                        <th>처리일시</th>
                        <td><span id="noRcvProcessDt"></span></td>
                        <th>처리자</th>
                        <td><span id="noRcvChgId"></span></td>
                    </tr>
                    <tr>
                        <th>처리결과</th>
                        <td colspan="3"><span id="noRcvProcessResult"></span></td>
                    </tr>
                    <tr id="noRcvProcessCntntArea" style="display: none;">
                        <th>처리내용</th>
                        <td colspan="3"><span id="noRcvProcessCntnt"></span></td>
                    </tr>
                </tbody>
            </table>
        </form>
    </div>
    <div class="topline"></div>
    <div class="ui center-button-group">
        <span class="inner">
            <button id="closeBtn" class="ui button xlarge font-malgun" onclick="self.close()">닫기</button>
        </span>
    </div>
</div>
<!-- script -->
<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/orderUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/gridUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/validateUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/order/pop/noReceiveShip.js?v=${fileVersion}"></script>

<script>
    const noRcvShipParam = ${noRcvShipParam};

    $(document).ready(function() {
        CommonAjaxBlockUI.global();
        noRcvShipComplete.init();
    });
</script>