<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">앱광고 푸시 수신동의 통계</h2>
    </div>

    <div id="groupSearch" class="ui wrap-table horizontal mg-t-10">
        <form name="searchForm" onsubmit="return false;">
            <table class="ui table">
                <caption>앱광고 푸시 수신동의 통계</caption>
                <colgroup>
                    <col style="width:120px;"/>
                    <col style="width:60%;"/>
                    <col/>
                </colgroup>
                <tbody>
                <tr>
                    <th >조회기간</th>
                    <td>
                        <div class="ui form inline">
                            <span class="ui form inline">
                                    <select class="ui input inline medium mg-r-10" id="currentYear" name="currentYear" style="width: 100px;"
                                            onchange="appPushAgreeStatMain.setMonth();">

                                        <c:forEach var="years" items="${getYear}" varStatus="status">
                                            <option value="${years.optionValue}">${years.optionStr}</option>
                                        </c:forEach>
                                    </select>
                                    <select class="ui input inline medium mg-r-10" id="currentMonth" name="currentMonth" style="width: 100px;">
                                    </select>
                                </span>
                        </div>
                    </td>
                    <td>
                        <div style="text-align: center;" class="ui form">
                            <button type="submit" id="searchBtn" class="ui button large cyan "><i class="fa fa-search"></i>검색</button><br/>
                            <button type="button" id="initBtn" class="ui button large white mg-t-5">초기화</button><br/>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </form>
    </div>

    <div class="com wrap-title sub">
        <!-- 그리드 -->
        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="searchListCount">0</span>건</h3>
        </div>
        <div class="topline"></div>

        <div id="appPushAgreeStatGrid" style="background-color:white;width:100%;height:700px;"></div>
        <!-- // 그리드 -->

    </div>
</div>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/app/appPushAgreeStatMain.js?v=${fileVersion}"></script>
<script>
    ${appPushAgreeStatGridBaseInfo}

    $(document).ready(function () {
        CommonAjaxBlockUI.global();
        appPushAgreeStatGrid.init();
        appPushAgreeStatMain.init();
    });
</script>