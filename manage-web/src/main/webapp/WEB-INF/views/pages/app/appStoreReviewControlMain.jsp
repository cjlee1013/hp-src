<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">앱스토어 심사 관리</h2>
    </div>

    <div class="com wrap-title sub">
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>버전관리</caption>
                    <colgroup>
                        <col style="width:120px;"/>
                        <col style="width:35%;"/>
                        <col style="width:120px;"/>
                        <col style="width:35%;"/>
                        <col/>
                    </colgroup>
                    <tbody>
                    <tr>
                        <th colspan="2" style="text-align: center;">HOME</th>
                        <th colspan="2" style="text-align: center;">CLUB</th>
                    </tr>
                    <tr>
                        <th>앱버전</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="version_home" name="version" class="ui input medium mg-r-5" style="width: 250px;" minlength="5" maxlength="20" />
                                <input type="hidden" id="seq_home" name="seq" value="1" />
                                <input type="hidden" id="siteType_home" name="siteType" value="HOME" />
                            </div>
                        </td>
                        <th>앱버전</th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="version_club" name="version" class="ui input medium mg-r-5" style="width: 250px;" minlength="5" maxlength="20" />
                                <input type="hidden" id="seq_club" name="seq" value="2" />
                                <input type="hidden" id="siteType_club" name="siteType" value="CLUB" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>노출 여부</th>
                        <td>
                            <select id="controlYn_home" style="width: 120px;" name="controlYn"
                                    class="ui input medium">
                                <option value="">선택해 주세요.</option>
                                <option value="Y">사용</option>
                                <option value="N">사용안함</option>
                            </select>
                        </td>
                        <th>노출 여부</th>
                        <td>
                            <select id="controlYn_club" style="width: 120px;" name="controlYn"
                                    class="ui input medium">
                                <option value="">선택해 주세요.</option>
                                <option value="Y">사용</option>
                                <option value="N">사용안함</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <%-- 저장 버튼 --%>
                        <td colspan="2" style="text-align: center;">
                            <div style="text-align: center;" class="ui form">
                                <button type="submit" id="saveBtn_home" class="ui button large cyan ">
                                    <i class="fa fa-search"></i> 저장
                                </button>
                            </div>
                        </td>
                        <%-- 저장 버튼 --%>
                        <td colspan="2" style="text-align: center;">
                            <div style="text-align: center;" class="ui form">
                                <button type="submit" id="saveBtn_club" class="ui button large cyan ">
                                    <i class="fa fa-search"></i> 저장
                                </button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>

    </div>
</div>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/app/appStoreReviewControlMain.js?v=${fileVersion}"></script>
<script>
    $(document).ready(function () {
        CommonAjaxBlockUI.global();
        appStoreReviewControlMain.init();
    });
</script>