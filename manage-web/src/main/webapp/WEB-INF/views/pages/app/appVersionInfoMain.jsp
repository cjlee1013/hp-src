<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<jsp:include page="/WEB-INF/views/pages/core/includeRealGrid.jsp"/>

<div class="com wrap-title">
    <div class="com wrap-title has-border">
        <h2 class="title font-malgun">앱 버전 관리</h2>
    </div>

    <div class="com wrap-title sub">
        <form id="searchForm" name="searchForm" onsubmit="return false;">
            <div class="ui wrap-table horizontal mg-t-10">
                <table class="ui table">
                    <caption>회원관리</caption>
                    <colgroup>
                        <col style="width:120px;"/>
                        <col style="width:35%;"/>
                        <col style="width:120px;"/>
                        <col style="width:35%;"/>
                        <col style="width:160px;"/>
                        <col/>
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>앱구분</th>
                        <td>
                            <select id="siteType" style="width: 120px;" name="siteType"
                                    class="ui input medium">
                                <option value="">전체</option>
                                <option value="HOME">홈플러스</option>
                                <option value="CLUB">더클럽</option>
                            </select>
                        </td>

                        <th>OS구분</th>
                        <td>
                            <select id="devicePlatform" style="width: 120px;" name="devicePlatform"
                                    class="ui input medium">
                                <option value="">전체</option>
                                <option value="ANDROID">Android</option>
                                <option value="IOS">iOS</option>
                            </select>
                        </td>
                        <%-- 검색 버튼 --%>
                        <td rowspan="2">
                            <div style="text-align: center;" class="ui form">
                                <button type="submit" id="searchBtn" class="ui button large cyan ">
                                    <i class="fa fa-search"></i> 검색
                                </button>
                                <br/>
                                <button type="button" id="searchResetBtn"
                                        class="ui button large white mg-t-5">초기화
                                </button>
                                <br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>사용여부</th>
                        <td colspan="3">
                            <select id="isUsed" style="width: 120px;" name="isUsed"
                                    class="ui input medium">
                                <option value="">전체</option>
                                <option value="true">사용</option>
                                <option value="false">사용안함</option>
                            </select>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </form>

        <!-- 그리드 -->
        <div class="com wrap-title sub">
            <h3 class="title">• 검색결과 : <span id="searchListCount">0</span>건</h3>
        </div>
        <div class="topline"></div>

        <div id="appVersionInfoGrid" style="background-color:white;width:100%;height:300px;"></div>
        <!-- // 그리드 -->

        <!-- 입력/수정 -->
        <form id="appVersionRegistForm" name="appVersionRegistForm" onsubmit="return false">
            <input type="hidden" id="registSeq" name="registSeq">
            <div class="com wrap-title sub">
                <h3 class="title">앱버전정보 등록/수정</h3>
            </div>
            <div class="topline"></div>
            <div class="ui wrap-table horizontal" style="width:100%;">

                <table class="ui table">
                    <caption>앱버전 등록/수정</caption>
                    <colgroup>
                        <col width="120px">
                        <col>
                        <col width="120px">
                        <col>
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>앱관리번호</th>
                        <td><span id="registSeqView" class="text"></span></td>
                        <th><span class="text-red-star">앱구분</span></th>
                        <td>
                            <select id="registSiteType" style="width: 120px;" name="registSiteType"
                                    class="ui input medium">
                                <option value="HOME">홈플러스</option>
                                <option value="CLUB">더클럽</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th><span class="text-red-star">OS구분</span></th>
                        <td>
                            <select id="registDevicePlatform" style="width: 120px;"
                                    name="registDevicePlatform" class="ui input medium">
                                <option value="ANDROID">Android</option>
                                <option value="IOS">iOS</option>
                            </select>
                        </td>
                        <th><span class="text-red-star">버전</span></th>
                        <td>
                            <input id="registVersion" name="registVersion" type="text"
                                   class="ui input mg-r-5" style="width: 100px;">
                        </td>
                    </tr>
                    <tr>
                        <th><span class="text-red-star">강제업데이트여부</span></th>
                        <td>
                            <label class="ui radio inline">
                                <input type="radio" name="registForceUpdate" class="ui input medium"
                                       value="N" checked><span>선택</span>
                            </label>
                            <label class="ui radio inline">
                                <input type="radio" name="registForceUpdate" class="ui input medium"
                                       value="Y"><span>강제</span>
                            </label>
                        </td>
                        <th><span class="text-red-star">업데이트추천</span></th>
                        <td>
                            <label class="ui radio inline">
                                <input type="radio" name="registUpdateNotification"
                                       class="ui input medium" value="N" checked><span>제공안함</span>
                            </label>
                            <label class="ui radio inline">
                                <input type="radio" name="registUpdateNotification"
                                       class="ui input medium" value="Y"><span>제공</span>
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <th><span class="text-red-star">버전업데이트일</span></th>
                        <td>
                            <div class="ui form inline">
                                <input type="text" id="registUpdateApplyDt"
                                       name="registUpdateApplyDt" class="ui input medium mg-r-5"
                                       style="width:135px;">
                            </div>
                        </td>
                        <th><span class="text-red-star">사용여부</span></th>
                        <td>
                            <label class="ui radio inline">
                                <input type="radio" name="registIsUsed" class="ui input medium"
                                       value="true" checked><span>사용</span>
                            </label>
                            <label class="ui radio inline">
                                <input type="radio" name="registIsUsed" class="ui input medium"
                                       value="false"><span>미사용</span>
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <th>업데이트내용</th>
                        <td colspan="3">
                            <textarea id="registMemo" name="registMemo"
                                      class="ui input medium mg-r-5"
                                      style="width: 70%; height: 60px;" minlength="2"></textarea>
                        </td>
                    </tr>
                    </tbody>
                </table>

            </div>

            <div class="ui center-button-group mg-t-15">
                <span class="inner">
                <button type="button" class="ui button xlarge cyan font-malgun"
                        id="registBtn">저장</button>
                <button type="button" class="ui button xlarge white font-malgun"
                        id="registResetBtn">초기화</button>
                </span>
            </div>
        </form>
        <!-- //입력/수정 -->

    </div>
</div>

<script type="text/javascript" src="/static/js/jquery.form.min.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/app/appVersionInfoMain.js?v=${fileVersion}"></script>
<script type="text/javascript" src="/static/js/common/UiUtil.js?v=${fileVersion}"></script>
<script>
    ${appVersionMngGridBaseInfo}

    $(document).ready(function () {
        CommonAjaxBlockUI.global();
        appVersionMain.init();
        appVersionInfoGrid.init();
    });
</script>