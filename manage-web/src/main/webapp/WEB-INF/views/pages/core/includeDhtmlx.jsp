<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!-- dhtmlx tabbar -->
<link rel="stylesheet" type="text/css" href="/static/jslib/dhtmlx5/codebase/fonts/font_roboto/roboto.css"/>
<link rel='stylesheet' type='text/css' href='/static/jslib/dhtmlx5/codebase/dhtmlx.css'>
<link rel='stylesheet' type='text/css' href='/static/jslib/dhtmlx5/codebase/dhtmlxtabbar_custom.css?v=${fileVersion}'>

<link rel='stylesheet' type='text/css' href='/static/jslib/dhtmlx5/skins/web/dhtmlx.css'>
<script type="text/javascript" src="/static/jslib/dhtmlx5/codebase/dhtmlx.js"></script>
<script type="text/javascript" src="/static/jslib/dhtmlx5/codebase/dhtmlx_deprecated.js"></script>
<script type="text/javascript" src="/static/jslib/gridUtil.js?v=${fileVersion}"></script>

<!-- 그리드 헤더에 padding-left:10px 없애는 CSS -->
<style>
    div.gridbox_dhx_web.gridbox table.hdr td div.hdrcell{padding-left:0px;width:auto;}
    _:-ms-input-placeholder, :root .gridbox{box-sizing:border-box}
</style>
