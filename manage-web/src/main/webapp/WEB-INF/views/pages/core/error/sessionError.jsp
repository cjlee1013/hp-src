<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script>
    alert("일정시간 동안 시스템을 사용하지 않아,\n자동 로그아웃 되었습니다.\n다시 로그인해주시기 바랍니다.");
    parent.location.href="${certificationRedirectParam}";
</script>
