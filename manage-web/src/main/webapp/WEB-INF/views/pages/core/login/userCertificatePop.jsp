<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<link href="/static/css/userCertificatePop.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<script type="text/javascript">
    $( document ).ready(function() {
        userCertificate.init();
    });
    const RETURN_CODE_SUCCESS = "SUCCESS";
    const INVALID_INPUT_MESSAGE = "입력된 정보가 불일치 하거나 미입력되었습니다.\n다시 확인해주세요.";
    const userCertificate = {
    /**
     * init function
     */
    init: function () {
        $("#userNm").focus();
        userCertificate.buttonEvent();
    },
    /**
     * buttonEvent
     */
    buttonEvent: function () {
        $("#confirmBtn").on('click', function(){
            userCertificate.cert();
        });

        $("input").keydown(function(key) {
            if (key.keyCode === 13) {
                $("#confirmBtn").trigger("click");
            }
        });
    },
    /**
     * 계정 확인
     */
    cert: function () {
        if (userCertificate.invalid()) {
            return false;
        }

        const data = _F.getFormDataByJson($("#certForm"));

        CommonAjax.basic(
            {
                url:'/admin/user/certificate.json',
                data: data,
                contentType: 'application/json',
                method: "POST",
                successMsg: null,
                callbackFunc: function(res) {
                    if (res.returnCode !== RETURN_CODE_SUCCESS) {
                        alert(res.returnMessage);
                    } else {
                        const responseObj = res.data;
                        if ($.jUtil.isNotEmpty(responseObj.empId) && $.jUtil.isNotEmpty(
                            responseObj.userId)) {
                            //비밀번호 변경팝업 호출
                            adminUpdateUserPw(responseObj.empId, responseObj.userId);

                            self.close();
                        }
                    }
                }
            });
    },
    /**
     * 유효성 체크
     * @returns {boolean}
     */
    invalid: function () {
        const userNm = $("#userNm").val();
        const userId = $("#userId").val();
        const empId = $("#empId").val();
        //운영자명
        if ($.jUtil.isEmpty(userNm)) {
            $.jUtil.alert(INVALID_INPUT_MESSAGE, "userNm");
            return true;
        }

        if ($.jUtil.isNotEmpty(userNm)
            && userNm.length > 30) {
            $.jUtil.alert(INVALID_INPUT_MESSAGE, "userNm");
            return true;
        }

        if ($.jUtil.isNotEmpty(userNm)
            && !$.jUtil.isAllowInput(userNm, ['KOR', 'ENG', 'SPC_2', 'NUM'])) {
            $.jUtil.alert(INVALID_INPUT_MESSAGE, "userNm");
            return true;
        }

        //로그인 ID
        if ($.jUtil.isEmpty(userId)) {
            $.jUtil.alert(INVALID_INPUT_MESSAGE, "userId");
            return true;
        }

        if ($.jUtil.isNotEmpty(userId)
            && $.jUtil.isNotAllowInput(userId, ['SPACE'])) {
            $.jUtil.alert(INVALID_INPUT_MESSAGE, "userId");
            return true;
        }

        //사번
        if ($.jUtil.isEmpty(empId)) {
            $.jUtil.alert(INVALID_INPUT_MESSAGE, "empId");
            return true;
        }

        if ($.jUtil.isNotEmpty(empId)
            && !$.jUtil.isAllowInput(empId, ['NUM', 'NOT_SPACE'])) {
            $.jUtil.alert(INVALID_INPUT_MESSAGE, "empId");
            return true;
        }


        return false;
    },
}
</script>

<div class="modal" role="dialog">
    <h2 class="h2">계정확인</h2>

    <form id="certForm" name="certForm" method="POST">
    <div class="modal-cont mgt25">
        <p class="fs14 fc03 mgb15">비밀번호 변경을 위해, 아래 항목을 정확히 입력해주세요.</p>
        <div class="tbl-form">
            <table>
                <caption>계정확인</caption>
                <colgroup>
                    <col style="width:30%">
                    <col style="width:auto">
                </colgroup>
                <tbody>
                <tr>
                    <th scope="row">이름<i class="bul-require">이름</i></th>
                    <td>
                        <input type="text" class="inp-base" title="이름" id="userNm" name="userNm"
                               maxlength="30"/>
                    </td>
                </tr>
                <tr>
                    <th scope="row">로그인ID<i class="bul-require">로그인ID</i></th>
                    <td>
                        <input type="text" class="inp-base" title="로그인ID" id="userId" name="userId"/>
                    </td>
                </tr>
                <tr>
                    <th scope="row">사번<i class="bul-require">사번</i></th>
                    <td>
                        <input type="text" class="inp-base" title="로그인ID" id="empId" name="empId"
                               maxlength="10"/>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="btn-wrap mgt20">
            <button type="button" class="btn-base-l type4" id="confirmBtn"><i>확인</i></button>
        </div>
    </div>
    </form>
</div>

