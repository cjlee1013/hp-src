<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ page import="com.google.gson.Gson" %>
<%@ page import="org.apache.commons.lang3.ObjectUtils" %>
<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="java.util.List" %>

<div style="padding: 60px;margin: 20px;border: 10px solid #eee;text-align: center;">
    <i class="fa fa-exclamation-circle" style="font-size: 60px; color: #99A4B8;"></i>
    <h1 style="margin: 12px 0px; font-size: 26px; color: #626d83;">알림</h1>
    <div style="font-size: 14px; color: #999;">해당 화면에 접근권한이 없습니다.</div>

        <%-- error --%>
        <div class="login-form" style="display:none;">
        <%-- todo: 에러상세 정보 --%>
        </div>
</div>