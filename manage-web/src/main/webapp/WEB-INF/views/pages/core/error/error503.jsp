<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>홈플러스 어드민</title>
    <meta http-equiv="pragma" content="no-cache">
</head>
<body>
    <div style="padding: 60px;margin: 20px;border: 10px solid #eee;text-align: center;">
        <i class="fa fa-warning" style="font-size: 60px; color: #99A4B8;"></i>
        <h1 style="margin: 12px 0px; font-size: 26px; color: #626d83;">시스템 점검 안내</h1>
        <div style="font-size: 14px; color: #999;">원활하고 안정된 서비스 제공을 위해 현재 시스템 점검 중에 있습니다.<br/>점검중에는 시스템 이용이 제한됩니다.</div>
        <div style="display: inline-block;padding: 8px 14px;margin-top: 10px;background-color: #eff3f1;border-radius: 4px;">
            <span>점검일시 : </span><span>3월 8일 </span><span>13:00 ~ 14:00</span>
        </div>
    </div>
</body>
</html>
