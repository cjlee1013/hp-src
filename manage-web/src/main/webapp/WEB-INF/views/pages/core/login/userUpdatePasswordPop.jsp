<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<link href="/static/css/userCertificatePop.css?v=${fileVersion}" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/static/js/common/formUtil.js?v=${fileVersion}"></script>
<%
    String empId = "";
    String userId = "";
    String loginType = "";
    if (request.getAttribute("empId") != null) {
        empId = (String) request.getAttribute("empId");
    }
    if (request.getAttribute("userId") != null) {
        userId = (String) request.getAttribute("userId");
    }
    if (request.getAttribute("loginType") != null) {
        loginType = (String) request.getAttribute("loginType");
    }
%>

<script type="text/javascript">
    $( document ).ready(function() {
        userUpdatePassword.init();
    });

    const RETURN_CODE_SUCCESS = "SUCCESS";

    const VALID_USER_PW_IS_EMPTY_MESSAGE = "모든 정보를 입력해주세요.";
    const VALID_USER_PW_MESSAGE = "비밀번호 생성 규칙을 확인하여 다시 입력해주세요.";
    const VALID_USER_PW_MISMATCH_MESSAGE = "신규로 입력한 비밀번호와 비밀번호 확인 시 일치하지 않습니다.\n다시 입력해주세요.";

    const UPDATE_USER_PASSWORD_DEFAULT_SUCCESS_MESSAGE = "비밀번호가 변경되었습니다. 변경된 비밀번호로 로그인 하실 수 있습니다.";
    const UPDATE_USER_PASSWORD_SUCCESS_RE_LOGIN_MESSAGE = "비밀번호가 변경되었습니다. 다음번 로그인 시 변경된 비밀번호로 로그인 하실 수 있습니다.";

    const PASSWORD_90DAYS_EXCEEDED_GUIDE_PHRASE = "90일동안 동일한 비밀번호를 사용중입니다. 비밀번호 변경 후 이용하세요.";

    const userUpdatePassword = {
        /**
         * init function
         */
        init: function () {
            $("#userPw").focus();
            userUpdatePassword.setDisplayPw90DaysExceeded();
            userUpdatePassword.buttonEvent();
            userUpdatePassword.setSuccessMessage();
        },
        /**
         * button event
         */
        buttonEvent: function () {
            $("#confirmBtn").on('click', function(){
                userUpdatePassword.updateUserPw();
            });

            $("input").keydown(function(key) {
                if (key.keyCode === 13) {
                    $("#confirmBtn").trigger("click");
                }
            });
        },
        /**
         * 비밀번호 변경 성공시 메세지 문구 설정
         */
        setSuccessMessage: function() {
          const loginType = $("#loginType");
            if (loginType.val() === "FIRST_LOGIN" || loginType.val() === "90DAYS_EXCEEDED") {
                loginType.val(UPDATE_USER_PASSWORD_SUCCESS_RE_LOGIN_MESSAGE);
            } else {
                loginType.val(UPDATE_USER_PASSWORD_DEFAULT_SUCCESS_MESSAGE);
            }
        },
        /**
         * 90일 비밀번호 변경일 경우 안내문구, 버튼 노출
         */
        setDisplayPw90DaysExceeded: function() {
            const loginType = $("#loginType").val();
            if (loginType === "90DAYS_EXCEEDED") {
                $("#guidePhrase").html(PASSWORD_90DAYS_EXCEEDED_GUIDE_PHRASE);
                $("#closeBtn").show();
            }
        },
        /**
         * 비밀번호 변경
         */
        updateUserPw: function () {
            if (userUpdatePassword.invalid()) {
                return false;
            }

            const data = _F.getFormDataByJson($("#updateForm"));

            CommonAjax.basic(
                {
                    url:'/admin/user/update/password.json',
                    data: data,
                    contentType: 'application/json',
                    method: "POST",
                    successMsg: null,
                    callbackFunc: function(res) {
                        if (res.returnCode !== RETURN_CODE_SUCCESS) {
                            alert(res.returnMessage);
                        } else {
                            alert($("#loginType").val());
                            self.close();
                        }
                    }
                });

        },
        /**
         * 유효성 체크
         * @returns {boolean}
         */
        invalid: function () {
            const userId = $("#userId").val();
            const userPw = $("#userPw").val();
            const userConfirmPw = $("#userConfirmPw").val();

            if ($.jUtil.isEmpty(userPw) && $.jUtil.isEmpty(userConfirmPw)) {
                $.jUtil.alert(VALID_USER_PW_IS_EMPTY_MESSAGE, "userPw");
                return true;
            }
            //신규 비밀번호 체크
            if (userId === userPw) {
                $.jUtil.alert(VALID_USER_PW_MESSAGE, "userPw");
                return true;
            }

            if (userPw.length < 10 || userPw.length > 15) {
                $.jUtil.alert(VALID_USER_PW_MESSAGE, "userPw");
                return true;
            }
            //공백체크
            if (userPw.search(/\s/) != -1) {
                $.jUtil.alert(VALID_USER_PW_MESSAGE, "userPw");
                return true;
            }

            //동일 문자열 체크
            if (/(\w)\1\1/.test(userPw)) {
                $.jUtil.alert(VALID_USER_PW_MESSAGE, "userPw");
                return true;
            }

            const regEngNumSpe = /^[a-z0-9!@#%&\\$\\^\\*]+$/;
            if ($.jUtil.isEmpty(userPw.match(regEngNumSpe))) {
                $.jUtil.alert(VALID_USER_PW_MESSAGE, "userPw");
                return true;
            }
            //조합 체크
            const regNum = userPw.search(/[0-9]/g);
            const regEng = userPw.search(/[a-z]/ig);
            const regSpe = userPw.search(/[!@#%&\\$\\^\\*]/gi);
            if ((regNum < 0 && regEng < 0) || (regEng < 0 && regSpe < 0)
                || (regSpe < 0 && regNum < 0)) {
                $.jUtil.alert(VALID_USER_PW_MESSAGE, "userPw");
                return true;
            }

            //비밀번호 확인 체크
            if ($.jUtil.isEmpty(userConfirmPw)) {
                $.jUtil.alert(VALID_USER_PW_IS_EMPTY_MESSAGE, "userConfirmPw");
                return true;
            }

            if (userConfirmPw.length < 10 || userConfirmPw.length > 15) {
                $.jUtil.alert(VALID_USER_PW_MESSAGE, "userConfirmPw");
                return true;
            }

            if (userPw !== userConfirmPw) {
                $.jUtil.alert(VALID_USER_PW_MISMATCH_MESSAGE, "userConfirmPw");
                return true;
            }

            return false;
        },
    }

</script>

<div class="modal" role="dialog">
    <h2 class="h2">비밀번호 변경</h2>

    <form id="updateForm" name="updateForm" method="POST">
    <input type="hidden" id="empId" name="empId" value="<%=empId%>">
    <input type="hidden" id="userId" name="userId" value="<%=userId%>">
    <input type="hidden" id="loginType" name="loginType" value="<%=loginType%>">
    <input type="hidden" id="message" name="message" value="">
    <div class="modal-cont mgt25">
        <p class="fs14 fc03 mgb15" id="guidePhrase">비밀번호를 재설정하세요.</p>
        <div class="tbl-form">
            <table>
                <caption>비밀번호 변경</caption>
                <colgroup>
                    <col style="width:30%">
                    <col style="width:auto">
                </colgroup>
                <tbody>
                <tr>
                    <th scope="row">로그인ID</th>
                    <td><%=userId%></td>
                </tr>
                <tr>
                    <th scope="row">신규 비밀번호<i class="bul-require">신규 비밀번호</i></th>
                    <td>
                        <input type="password" class="inp-base" title="신규 비밀번호" id="userPw" name="userPw" maxlength="15" autocomplete="off" />
                        <p class="bul-p mgt10">
                            비밀번호는 영문소문자, 숫자, 특수기호 중 2개 이상 조합으로 10 ~ 15자리 설정 가능합니다.
                        </p>
                        <p class="bul-p mgt10">
                            특수기호는 ! @ # $ % ^ & * 만 사용가능
                        </p>
                    </td>
                </tr>
                <tr>
                    <th scope="row">비밀번호 확인<i class="bul-require">비밀번호 확인</i></th>
                    <td>
                        <input type="password" class="inp-base" title="비밀번호 확인" id="userConfirmPw" name="userConfirmPw" maxlength="15" autocomplete="off" />
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="btn-wrap mgt20">
            <button type="button" class="btn-base-l type3" id="closeBtn" style="display: none;"
                    onclick="self.close();"><i>다음에 변경</i></button>
            <button type="button" class="btn-base-l type4" id="confirmBtn"><i>확인</i></button>
        </div>
    </div>
    </form>

</div>

