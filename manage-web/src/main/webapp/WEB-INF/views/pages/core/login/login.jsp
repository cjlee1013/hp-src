<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String saveUserId = "";
    if (request.getAttribute("saveUserId") != null) {
        saveUserId = (String) request.getAttribute("saveUserId");
    }

    boolean chkSaveUserId = false;
    if (request.getAttribute("chkSaveUserId") != null) {
        chkSaveUserId = (boolean) request.getAttribute("chkSaveUserId");
    }
%>
<link href="/static/css/login.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
const RETURN_CODE_SUCCESS = "SUCCESS";
$( document ).ready(function() {
    CommonAjaxBlockUI.global();

    documentReadyInputFocus();
});

$(function () {
    $("input").keydown(function(key) {
        if (key.keyCode === 13) {
            $("#loginBtn").trigger("click");
        } else {
            hideReturnMessage();
        }
    });
});
/**
 * 최초 진입시 아이디 저장여부에 따라 input focus 설정
 */
function documentReadyInputFocus() {
    let chkSaveUserId = ${chkSaveUserId};
    if (chkSaveUserId) {
        $("#userPw").focus();
    } else {
        $("#userId").focus();
    }
}

/**
 * 에러 메세지 노출처리
 */
function displayReturnMessage(userId, returnMessage) {
    if ($.jUtil.isNotEmpty(returnMessage)) {
        const valid_txt = $("#valid_txt");
        valid_txt.text(returnMessage);
        valid_txt.show();

        setUserIdByFailed(userId);
    }
}
/**
 * 입력 시도 시 에러메세지 제거처리
 */
function hideReturnMessage() {
    const valid_txt = $("#valid_txt");
    if (valid_txt.text().length > 0) {
        valid_txt.text("");
        valid_txt.hide();
    }
}

/**
 * 예외 발생시 기존 입력한 로그인 아이디 설정
 */
function setUserIdByFailed(userId) {
    if ($.jUtil.isNotEmpty(userId)) {
        $("#userId").val(userId);
        $("#userPw").focus();
    }
}

/**
 * 초기 비밀번호로 최초 로그인 진입 시 팝업 노출
 */
function firstLoginPopup(empId, userId) {
    if ($.jUtil.isNotEmpty(empId) && $.jUtil.isNotEmpty(userId)) {
        adminUpdateUserPw(empId, userId, "FIRST_LOGIN");
    }
}

/**
 * 로그인 처리
 **/
function loginProcess() {
    const userId = $("#userId").val();
    const userPw = $("#userPw").val();
    const chkSaveUserId = $("input:checkbox[id='chkSaveUserId']").is(":checked");

    if (invalid(userId, userPw)) {
        return false;
    }

    const loginParam = {
        userId : userId,
        userPw : userPw,
        chkSaveUserId : chkSaveUserId
    };

    CommonAjax.basic({
        url: "/login.json",
        data : loginParam,
        method: "POST",
        callbackFunc: function (res) {
            if (res.returnCode !== RETURN_CODE_SUCCESS) {
                alert(res.returnMessage);
                return false;
            } else {
                //로그인 팝업이 노출될 경우 로그인 처리 후 팝업창 닫기
                if (self !== top && $("#loginGatewayFrameResource", parent.document).length > 0) {
                    $(location).attr('href', res.data);
                    window.open('about:blank','_parent').parent.close();

                //탭바 내 iframe 에서 로그인 페이지가 노출될 경우, 로그인 처리 후 기존에 접근 하려던 메뉴로 이동
                } else if (self !== top){
                    const currentTabId = parent.initialTab.currentTabId;
                    const parentUrl = parent.Tabbar.tabs(currentTabId).getFrame().getAttribute('src');
                    $(location).attr('href', parentUrl);
                //기본 로그인 페이지
                } else {
                    $(location).attr('href', res.data);
                }
            }
        },
        errorCallbackFunc: function (resError) {
            if (resError.responseJSON != null) {
                <%-- 로그인 인증 실패 시--%>
                if (resError.status == 401 && resError.responseJSON.returnCode == -9200) {
                    displayReturnMessage($.jUtil.nvl(resError.responseJSON.data.userId),
                        $.jUtil.nvl(resError.responseJSON.returnMessage));
                <%-- 최초 로그인 시 --%>
                } else if(resError.status == 403 && resError.responseJSON.returnCode == -9208) {
                    displayReturnMessage($.jUtil.nvl(resError.responseJSON.data.userId),
                        $.jUtil.nvl(resError.responseJSON.returnMessage));

                    firstLoginPopup($.jUtil.nvl(resError.responseJSON.data.empId),
                        $.jUtil.nvl(resError.responseJSON.data.userId));
                <%-- 계정잠금 처리 --%>
                } else if (resError.status == 401 && resError.responseJSON.returnCode == -9206){
                    displayReturnMessage($.jUtil.nvl(resError.responseJSON.data.userId),
                        $.jUtil.nvl(resError.responseJSON.returnMessage));

                    userCertificatePopup();
                <%-- 기타 인증에러 처리 --%>
                } else {
                    displayReturnMessage($.jUtil.nvl(resError.responseJSON.data.userId),
                        $.jUtil.nvl(resError.responseJSON.returnMessage));
                }
            } else {
                displayReturnMessage("", $.jUtil.nvl(resError.responseJSON.returnMessage));
            }
        }

    });
}
/**
 * 로그인 유효성 체크
 */
function invalid(userId, userPw) {
    const valid_txt = $("#valid_txt");

    if ($.jUtil.isEmpty(userId) && $.jUtil.isEmpty(userPw)) {
        valid_txt.text("아이디, 비밀번호를 입력하세요.");
        valid_txt.show();
        $("#userId").focus();
        return true;
    }

    if ($.jUtil.isEmpty(userId)) {
        valid_txt.text("아이디를 입력하세요.");
        valid_txt.show();
        $("#userId").focus();
        return false;
    }
    if(userId.length < 3 || userId.length > 20) {
        valid_txt.text("아이디는 3에서 20자리 이내로 입력해주세요.");
        valid_txt.show();
        $("#userId").focus();
        return true;
    }

    if($.jUtil.isEmpty(userPw)) {
        valid_txt.text("비밀번호를 입력하세요.");
        valid_txt.show();
        $("#userPw").focus();
        return true;
    }

    return false;
}

</script>
<div class="login-layout">
    <h1><img src="/static/images/logo_admin2.png" title="Homeplus 어드민" alt="Homeplus 어드민"></h1>
    <div class="login-box">
        <h2>로그인</h2>
        <form id="oForm" name="oForm" method="POST">
            <fieldset>
                <legend>로그인 폼</legend>
                <p class="id">
                    <input type="text" id="userId" name="userId" class="input_text" maxlength="20"
                           style="ime-mode: disabled;" placeholder="아이디를 입력하세요." value="${saveUserId}">
                </p>
                <p class="pw">
                    <input type="password" id="userPw" name="userPw" class="input_text" maxlength="15"
                           placeholder="비밀번호를 입력하세요." value="" autocomplete="off">
                </p>

                <div class="form_txt">
	                <span>
	                    <input type="checkbox" id="chkSaveUserId" name="chkSaveUserId"
                            <c:if test="${chkSaveUserId eq true}">
                                   checked="checked"
                            </c:if>
                        >
	                    <label for="chkSaveUserId">아이디 저장</label>
	                </span>
                    <span>
	                	<button type="button" id="updateUserPw" onclick="userCertificatePopup();">비밀번호변경</button>
	                </span>
                </div>
                <button type="button" name="loginBtn" id="loginBtn" class="login_btn" onclick="loginProcess();">로그인</button>
                <p id="valid_txt" class="pw" style="color: red; display: none;">아이디, 비밀번호를 다시 확인해 주세요.</p>
            </fieldset>
        </form>
    </div>
</div>

