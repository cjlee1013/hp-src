<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ page import="com.google.gson.Gson" %>
<%@ page import="kr.co.homeplus.plus.web.client.model.ResponseError" %>
<%@ page import="kr.co.homeplus.plus.web.client.model.ResponseObject" %>
<%@ page import="org.apache.commons.lang3.ObjectUtils" %>
<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="java.util.List" %>

<div style="padding: 60px;margin: 20px;border: 10px solid #eee;text-align: center;">
    <i class="fa fa-exclamation-circle" style="font-size: 60px; color: #99A4B8;"></i>
    <h1 style="margin: 12px 0px; font-size: 26px; color: #626d83;">알림</h1>
    <div style="font-size: 14px; color: #999;">이 화면은 VDESK 환경 또는 고객만족센터망에서만 접속 가능합니다.<br/>(VDI 환경은 그룹웨어에서 접근 신청 가능합니다.)</div>

        <%-- error --%>
        <div class="login-form" style="display:none;">
            <%
                if (!"prd".equals(request.getAttribute("profilesActive")) &&
                        request.getAttribute("responseObject") != null) {
                    ResponseObject responseObject = (ResponseObject) request.getAttribute("responseObject");

                    if (responseObject.hasError()) {

                        List<ResponseError> responseErrors = responseObject.getErrors();
                        for (ResponseError responseError : responseErrors) {

                            if (StringUtils.isNotBlank(responseError.getStatus())) {
                                out.println("\n status : " + responseError.getStatus());
                            }
                            if (StringUtils.isNotBlank(responseError.getTitle())) {
                                out.println("\n title : " + responseError.getTitle());
                            }
                            if (responseError.getMeta() != null) {
                                out.println("\n meta : " + responseError.getMeta());
                            }
                            if (responseError.getSource() != null) {
                                out.println("\n source : " + ObjectUtils.toString(responseError.getSource().getPointer()));
                            }
                            if(StringUtils.isNotBlank(responseError.getDetail())) {
                                out.println("\n detail : " + responseError.getDetail());
                            }
                        }

                    } else {
                        out.println(new Gson().toJson(responseObject));
                    }
                }
            %>
        </div>
</div>