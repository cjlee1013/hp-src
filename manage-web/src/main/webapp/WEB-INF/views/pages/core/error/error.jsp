<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ page import="com.google.gson.Gson" %>
<%@ page import="org.apache.commons.lang3.ObjectUtils" %>
<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="java.util.List" %>

<div style="padding: 60px;margin: 20px;border: 10px solid #eee;text-align: center;">
    <i class="fa fa-frown-o" style="font-size: 60px; color: #99A4B8;"></i>
    <h1 style="margin: 12px 0px; font-size: 26px; color: #626d83;">시스템 에러</h1>
    <div style="font-size: 14px; color: #999;">요청하신 페이지에서 일시적인 에러가 발생하였습니다.<br/>동일한 문제가 지속적으로 발생할 경우 관리자에게 문의해주시기 바랍니다.</div>

        <%-- error --%>
        <div class="login-form" style="display:none;">
        <%-- todo: 에러상세 정보 --%>
        </div>
</div>