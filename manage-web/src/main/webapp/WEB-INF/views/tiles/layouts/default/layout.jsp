<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<html>
<head>
	<title><tiles:insertAttribute name="title" /></title>
	<tiles:insertAttribute name="header" />
</head>
<body>


<!-- 전체 레이아웃  -->
<div class="layout wrap">


	<!-- 사이드 메뉴 시작 -->
	<div class="layout aside">
			<tiles:insertAttribute name="menu" />
	</div>
	<!-- 사이드 메뉴 끝 -->


	<!-- 우측 컨텐츠 시작 -->
	<div class="layout container" style="min-width: 1000px">

		<tiles:insertAttribute name="bodytab" />

		<tiles:insertAttribute name="body" />

	</div>
	<!-- 우측 컨텐츠 끝  -->


</div>
<!-- 전체 레이아웃 끝 -->

<tiles:insertAttribute name="footer" />

</body>
</html>