<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>

<%@ page import="kr.co.homeplus.admin.web.core.dto.AuthMenuListDto" %>
<%@ page import="kr.co.homeplus.admin.web.core.dto.UserInfo" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<jsp:include page="/WEB-INF/views/pages/core/includeDhtmlx.jsp"/>

<%
	UserInfo userInfo = new UserInfo();
	if (request.getAttribute("userInfo") != null) {
		userInfo = (UserInfo) request.getAttribute("userInfo");
	}
	List<AuthMenuListDto> menuList = new ArrayList<>();
	if( request.getAttribute("menuList") != null ) {
		menuList = (List<AuthMenuListDto>) request.getAttribute("menuList");
	}

	Boolean checkEnable = null;
	if( request.getAttribute("checkEnable") != null ) {
		checkEnable = (Boolean) request.getAttribute("checkEnable");
	}

	Boolean userPw90DaysExceeded = null;
	if (request.getAttribute("userPw90DaysExceeded") != null) {
		userPw90DaysExceeded = (Boolean) request.getAttribute("userPw90DaysExceeded");
	}

	List<String> lnbHideMenuList = new ArrayList<>();
	if( request.getAttribute("lnbHideMenuList") != null ) {
		lnbHideMenuList = (List<String>) request.getAttribute("lnbHideMenuList");
	}
%>

<!-- 팝업 제어 스크립트 -->
<script type="text/javascript">
	function dEI(elementID){
		return document.getElementById(elementID);
	}
	function openLayer(IdName , tpos, lpos){
		var pop = dEI(IdName);
		pop.style.display = "block";
		pop.style.top =  tpos + "px";
		pop.style.left = lpos + "px";

		var wrap = dEI("dimmed");
		var reservation = document.createElement("div");
		reservation.setAttribute("id", "outage");
		wrap.style.zIndex = "2";
		wrap.appendChild(reservation);
	}
	function closeLayer( IdName ){
		var pop = dEI(IdName);
		pop.style.display = "none";
		var clearEl=parent.dEI("outage");
		var momEl = parent.dEI("dimmed");
		momEl.style.zIndex = "0";
		momEl.removeChild(clearEl);
	}
	function mnav_sub(){
		var $topnav = $(".list-snb"),
				$topnavmn = $topnav.find(">li"),
				$topnavsbmn = $topnavmn.find(">div");
		$(".list-snb > .active > .collapse").show();
		$topnavmn.children("a").click(function() {
			var $self = $(this);
			if ($(this).parent().is("li:has('div')")) {
				if (!$(this).parent("li").is(".active")) {
					$topnavmn.removeClass("active");
					$topnavsbmn.slideUp(250);
					$(this).parent("li").addClass("active");
					$(this).parent().find("> div").slideDown(250).removeClass("active");
				} else {
					$(this).parent("li").removeClass("active");
					$(this).parent().find("> div").slideUp(250);
				}
				return false;
			} else {
				$topnavmn.removeClass("active");
				$topnavsbmn.slideUp(250);
				$(this).parent("li").addClass("active");
			}

		});
		// 메뉴 선택처리
		$(".sub-menu li a").on("click", function () {
			$(".sub-menu li").removeClass('selected');
			$(this).parent().addClass('selected');
		});
	}

	//동일 비밀번호 90일 사용 초과시 팝업 노출
	function userPw90DaysExceededPopup() {
		let isPw90DaysExceeded = <%=userInfo.isUserPw90DaysExceeded()%>;
		let empId = "<%=userInfo.getEmpId()%>";
		let userId = "<%=userInfo.getUserId()%>";

		if ($.jUtil.isNotEmpty(isPw90DaysExceeded) && $.jUtil.isNotEmpty(empId) &&
				$.jUtil.isNotEmpty(userId) && isPw90DaysExceeded) {
			adminUpdateUserPw(empId, userId, "90DAYS_EXCEEDED");
		}
	}

	$(function() {
		//메뉴 네비
		mnav_sub();
		//비밀번호 변경팝업 노출
		userPw90DaysExceededPopup();
	});
</script>


<h1 class="logo">
	<span class="admin-title">
		<img src="/static/images/logo.png?v1" width="50" alt="로고">
		<a href="" class="admin-name font-malgun">
			어드민
		</a>
		<p class="aside-userinfo font-malgun mg-t-15">
			<i class="fa fa-user"></i>
			<%=userInfo.getUserNm()%>
			<span class="vline"> | </span>
			<%=userInfo.getEmpId()%>
		</p>
	</span>
	<!-- 버튼 -->
	<div class="sidetop-button-group mg-t-10">
		<button class="ui button small sidetop" onclick="location.href='/logout'">로그아웃</button>
	</div>
</h1>

<div class="menu-group">
	<ul class="list-snb" data-id="accordion">
				<li id="menu_home">
					<a href="" class="single">
						홈
					</a>
				</li>

				<c:forEach var="depth1" items="<%=menuList%>" varStatus="status">
					<li data-sidemenu-section="snb-${status.count}" data-sidemenu-class="active" class="">
						<a href="javascript:void(0)" data-sidemenu-toggle="snb-${status.count}">${depth1.menuName}</a>
						<div class="collapse" data-sidemenu-panel="snb-${status.count}">
							<ul class="sub-menu">
								<c:if test="${ !empty depth1.childMenuList }">
									<c:forEach var="depth2" items="${depth1.childMenuList}" varStatus="status2">
										<div class="title-depth2">${depth2.menuName}</div>
										<c:if test="${ !empty depth2.childMenuList }">
											<c:forEach var="depth3" items="${depth2.childMenuList}" varStatus="status3">
												<c:choose>
													<c:when test="${depth3.isHold eq 'n'}">
														<%-- checkEnable 옵션 추가
															ㅇ checkEnable 옵션
														      application.yml에 authorization.menu-check-enable 설정에 따라 "메인 좌측의 메뉴" 노출 여부를 관리합니다.
														        - false로 할 경우 : 메뉴 권한을 소유하고 있지 않아도 메뉴를 노출합니다.
														        - true로 할 경우 : 메뉴 권한을 소유하고 있지 않을 경우 메뉴를 미 노출 합니다.
														--%>
														<c:choose>
															<c:when test="${checkEnable eq false}">
																<%-- 상세 탭페이지 LNB 노출 막기 --%>
																<c:if test="${!lnbHideMenuList.contains(depth3.menuUrl)}">
																	<li class="" id="menu_${status.count}_${status2.count}_${status3.count}">
																		<a href="javascript:initialTab.addTab('menu_${status.count}_${status2.count}_${status3.count}', '${depth3.menuName}', '${depth3.menuUrl}');">${depth3.menuName}</a>
																	</li>
																</c:if>
																<%--//  상세 탭페이지 LNB 노출 막기 --%>

															</c:when>
														</c:choose>
														<%-- // checkEnable 옵션 추가 --%>
													</c:when>
													<c:otherwise>
														<%-- 상세 탭페이지 LNB 노출 막기 --%>
														<c:if test="${!lnbHideMenuList.contains(depth3.menuUrl)}">
															<li class="" id="menu_${status.count}_${status2.count}_${status3.count}">
																<a href="javascript:initialTab.addTab('menu_${status.count}_${status2.count}_${status3.count}', '${depth3.menuName}', '${depth3.menuUrl}');">${depth3.menuName}</a>
															</li>
														</c:if>
														<%--// 상세 탭페이지 LNB 노출 막기 --%>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</c:if>

									</c:forEach>

								</c:if>
							</ul>
						</div>
					</li>
				</c:forEach>
			</ul>
		</div>