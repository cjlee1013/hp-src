<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%
    //로그인 만료시간 - application.yml #admin.config.login-expiration-hour
    int loginExpirationMin = 0;
    if (request.getAttribute("loginExpirationMin") != null) {
        loginExpirationMin = (int) request.getAttribute("loginExpirationMin");
    }
    //로그인 만료시간
    if (loginExpirationMin > 0) {
        loginExpirationMin = loginExpirationMin;
    }
%>
<!-- 자동로그아웃 레이어 팝업-->
<%-- 지정한 시간 logOutCheckTime 동안 동작 없을경우 로그아웃 --%>
<div id="logout_popup_wrap" style="padding: 60px;margin: 20px; width: 400px; border: 10px solid #eee;text-align: center; display: none;">
    <i class="fa fa-warning" style="font-size: 60px; color: #99A4B8;"></i>
    <h1 style="margin: 12px 0px; font-size: 26px; color: #626d83;">자동 로그아웃 안내</h1>
    <div style="font-size: 13px; color: #999; width: 370px;">
        <%--<span style="color: #EF5555;" class="logOutCheckTime"></span> 동안 시스템 사용이 없어서 남은시간 후 자동 로그아웃 됩니다.--%>
        장시간 활동이 없어 자동으로 로그아웃 됩니다.<br>
        계속 이용 하시려면 로그인 시간을 연장해주세요.
    </div>
    <div style="display: inline-block;padding: 8px 14px;margin-top: 10px;background-color: #eff3f1;border-radius: 4px;">
        <span>남은시간 : </span><span style="color: #EF5555;" id="left_time"></span>
    </div>
    <div class="ui mg-t-30">
	        <span class="inner">
                <button type="button" style="width:120px;" class="ui button midium mg-r-10 font-malgun" id="pop_logout_btn">로그아웃</button>
		        <button type="button" style="width:120px;" class="ui button midium btn-danger font-malgun" id="extend_btn">연장하기</button>
	        </span>
    </div>
</div>
<!-- //자동로그아웃 레이어 팝업-->

<script type="text/javascript">
    /** 자동로그아웃 스크립트 */
    // 자동로그아웃 현재 일시
    const nowTime = new Date();

    //  팝업 노출 후 타이머 시간(자동 로그아웃 팝업창의 타이머 시간, 분 단위).
    const logOutTimerMinute = 5;

    // 자동 로그아웃 팝업노출 대기시간(팝업노출 대기시간은 (로그인 기본 만료시간 - 팝업 노출 후 시간), 분 단위 )
    // ex) 1시간 = 55분 + 5분(로그아웃 경고창 타이머)
    const logOutCheckMinute = (<%=loginExpirationMin%>) - logOutTimerMinute;

    const defaultTimeSec = 60 * logOutCheckMinute * 1000;
    const defaultMaxTimeSec = 60 * logOutTimerMinute * 1000;

    if( logOutCheckMinute % 60 == 0 && logOutCheckMinute > 0 ){
        $(".logOutCheckTime").text( (logOutCheckMinute / 60) + "시간");
    } else {
        $(".logOutCheckTime").text( logOutCheckMinute + "분");
    }

    let autoLogout = {
        nowDate : nowTime,
        setTime : defaultTimeSec,
        maxTime : defaultMaxTimeSec,
        logoutTime : 0,
        popupTime : 0,
        leftTime : 0,

        // 자동로그아웃 타이머
        logoutTimer : function() {
            autoLogout.logoutTime = setTimeout(function(){
                autoLogout.openLogoutPop();
            }, autoLogout.setTime);
        },

        // 로그아웃 연장 팝업 노출여부 체크
        openLogoutPop : function() {
            const storageTime = new Date(localStorage.getItem(('sso_time')));
            const timeDiff = storageTime - autoLogout.nowDate;

            // local storage의 시간에서 현재 페이지 로드한 시간을 뺀 값이 0보다 클 경우, 다른 탭에서 사용중으로 간주
            if ( timeDiff > 0 ) {
                autoLogout.setTime = defaultTimeSec - (defaultTimeSec - timeDiff);
                autoLogout.nowDate = storageTime;
                autoLogout.logoutTimer();
            }
            else {
                // 0보다 작거나 같을 경우, 로그인 연장 팝업 노출
                localStorage.setItem('sso_extend_yn', 0);
                localStorage.setItem('sso_logout_yn', 0);
                $('#logout_popup_wrap').dialog({
                                                   closeText: '',
                                                   width: 550,
                                                   autoOpen: false,
                                                   modal: true,
                                                   resizable: false,
                                                   closeOnEscape : false,
                                                   open: function (event, ui) {
                                                       //다이얼로그 타이틀바 제거
                                                       $(this).dialog().parents(".ui-dialog").find(".ui-dialog-titlebar").remove();
                                                       $('.ui-dialog').css('z-index',999);
                                                       $('.ui-widget-overlay').css('z-index',998);
                                                   },
                                               });
                $('#logout_popup_wrap').dialog('open');
                // 연장팝업 타이머 호출
                autoLogout.popupTimer();
            }
        },

        // 로그아웃 연장 팝업 타이머
        popupTimer : function() {
            autoLogout.maxTime = defaultMaxTimeSec;
            autoLogout.popupTime = setTimeout(function(){
                autoLogout.autoLogout();
            }, autoLogout.maxTime);
            autoLogout.leftTimer();
        },

        // 남은시간 표시, 다른탭에서 로그아웃 or 연장하기 버튼 눌렀는지 확인
        leftTimer : function() {
            // 다른탭에서 로그아웃 버튼 눌렀을 경우
            if ( localStorage.getItem('sso_logout_yn') == 1 ) {
                top.location.href="/logout";
            }
            // 다른탭에서 연장하기 눌렀을 경우
            else if ( localStorage.getItem('sso_extend_yn') == 1 ) {
                // 로그아웃 실행 타이머 중단하고 팝업닫음
                clearTimeout(autoLogout.popupTime);
                $('#logout_popup_wrap').dialog('close');

                // 누른시간 기준으로 현재페이지에 자동로그아웃 타이머 다시 걸어줌
                const now = new Date();
                const storageTime = new Date(localStorage.getItem(('sso_time')));
                const timeDiff = storageTime - now;

                autoLogout.setTime = defaultTimeSec + timeDiff;
                autoLogout.nowDate = storageTime;
                autoLogout.logoutTimer();

            }
            else {
                let text = '';
                let minutes = Math.floor( autoLogout.maxTime / 1000 / 60 );
                let seconds = Math.round( autoLogout.maxTime / 1000 - ( 60 * minutes ) );

                text = minutes + '분 ' + seconds + '초';
                $("#left_time").html(text);
                autoLogout.maxTime -= 1000;
                if ( autoLogout.maxTime >= 0 ) {
                    autoLogout.leftTime = setTimeout("autoLogout.leftTimer()", 1000);
                }
            }
        },

        // 자동로그아웃 처리
        autoLogout : function() {
            $('#logout_popup_wrap').dialog('close');

            setTimeout(function() {
                alert("일정시간 동안 시스템을 사용하지 않아, 자동 로그아웃 되었습니다.\n다시 로그인해주시기 바랍니다.");
                top.location.href = "/logout";
            }, 300);
        },

        // 로그인 연장처리
        extendLogin : function() {
            clearTimeout(autoLogout.logoutTime);
            clearTimeout(autoLogout.popupTime);
            clearTimeout(autoLogout.leftTime);
            localStorage.setItem('sso_extend_yn', 1);
            // 현재시간으로 로그아웃 타이머 초기화
            const extendTime = new Date();
            localStorage.setItem('sso_time', extendTime);
            autoLogout.nowDate = extendTime;
            autoLogout.setTime = defaultTimeSec;

            autoLogout.logoutTimer();
        }
    };
    $(window).bind('resize', function() {
        $('#logout_popup_wrap').position();
    });
    /** // 자동로그아웃 스크립트 */


    /** 탭 영역 스크립트 */
    var Tabbar = new dhtmlXTabBar({
        parent: "ContentsTabbar",
        arrows_mode: "auto",
        skin: "material"
    });

    var initialTab = {
        currentTabId: "",
        homeTabId : 'ContentsHomeTab',

        init: function() {
            this.makeHomeTab();
            this.onSelectTabEvent();
        },

        makeHomeTab: function() {
            //브라우저 체크
            if (initialTab.isNotChrome()) {
                initialTab.accessNotChromeAttachWarnTab();
            } else {
                Tabbar.addTab(initialTab.homeTabId, "홈", 50, null, true, false);
                Tabbar.tabs(initialTab.homeTabId).attachURL("/home");
                Tabbar.enableAutoReSize();
            }
            initialTab.currentTabId = initialTab.homeTabId;
        },
        /**
         * 현재 접속한 브라우저가 크롬이 아닐 경우 true, 크롬일 경우 false를 리턴합니다.<p>
         * header.jsp의 chkBrowser.js와 연계되어 동작합니다.
         */
        isNotChrome: function () {
            return $('html').attr('class') != "chrome";

        },
        /**
         * 크롬으로 접속하지 않을 경우 '경고 탭'을 추가합니다.
         */
        accessNotChromeAttachWarnTab: function () {
            const homeTabId = initialTab.homeTabId;
            const tabs = Tabbar.getAllTabs();

            if (tabs.indexOf(homeTabId) != -1) {
                Tabbar.tabs(homeTabId).setActive();
            } else {
                Tabbar.addTab(homeTabId, "홈", 50, null, true, false);

                let homeContents = '';
                homeContents += '<div class="content min-w1000">';
                homeContents += '    <div style="padding: 60px;margin: 20px;border: 10px solid #eee;text-align: center;">';
                homeContents += '    <i class="fa fa-chrome" style="font-size: 60px; color: #99A4B8;"></i>';
                homeContents += '    <h1 style="margin: 12px 0px; font-size: 26px; color: #626d83;">어드민 이용을 지원하지 않는 브라우저에 접속했습니다.</h1>';
                homeContents += '    <div style="font-size: 20px; color: #fd0303;">';
                homeContents += '    크롬으로 접속해서 이용해주세요.';
                homeContents += '    </div>';
                homeContents += '</div>';

                Tabbar.tabs(homeTabId).attachHTMLString(homeContents);
            }
        },

        checkExistTab: function(tabId, attachUrl) {
            const tabs = Tabbar.getAllTabs();
            if(tabs.indexOf(tabId) != -1) {
                //exist tab iframe reload
                Tabbar.tabs(tabId).attachURL(attachUrl);
                Tabbar.tabs(tabId).setActive();
                return true;
            }
            return false;
        },

        checkTabNums: function() {
            const numberOfTabs = Tabbar.getNumberOfTabs();
            if (numberOfTabs > 15) {
                alert('더 이상 탭을 추가할 수 없습니다. 사용하지 않는 열려있는 탭을 닫은 후 사용해 주세요.');
                return false;
            }
            return true;
        },

        addTab: function(tabId, tabName, attachUrl) {
            //브라우저 체크
            if (initialTab.isNotChrome()) {
                initialTab.accessNotChromeAttachWarnTab();
                return false;
            }

            //attachUrl에 빈 문자열('')이나 null이 들어올 경우 alert으로 경고 처리
            if(attachUrl == '' || attachUrl == null) {
                alert('해당 메뉴의 URL이 등록되어 있지 않습니다.');
                return false;
            }

            if(!this.checkExistTab(tabId, attachUrl) && this.checkTabNums()) {
                const tabSize = (tabName.length * 15) + 60;      // 탭 글자에 따라 길이 계산
                Tabbar.addTab(tabId, tabName, tabSize, null, true, true);
                Tabbar.tabs(tabId).attachURL(attachUrl);
                Tabbar.enableAutoReSize();

                initialTab.currentTabId = tabId;
            }

            //자동 로그아웃 시간 초기화
            autoLogout.extendLogin();
        },
        /**
         * 탭바 active 상태일 경우 이벤트
         */
        onSelectTabEvent: function () {
            Tabbar.attachEvent("onSelect", function (selectTabId, lastId) {
                $(".sub-menu li").removeClass('selected');

                if (selectTabId === initialTab.homeTabId) {
                    $("#menu_home").parent().children("[class='active']").children('a').click();

                } else {
                    const tabId = $("#" + selectTabId);
                    tabId.addClass('selected');

                    // 상위 메뉴 선택 처리
                    if( tabId.parents('li').hasClass('active') === false ) {
                        tabId.parents('li').children('a').click();
                    }
                }
                initialTab.currentTabId = selectTabId;

                //자동 로그아웃 시간 초기화
                autoLogout.extendLogin();
                return true;
            });
        },
    };

    initialTab.init();
    /** // 탭 영역 스크립트 */


    $(document).ready(function(){

        // logOutCheckMinute 분동안 동작 없을경우 자동로그아웃
        if( ('localStorage' in window) && window['localStorage'] !== null) {
            localStorage.setItem('sso_time', autoLogout.nowDate);
            autoLogout.logoutTimer();

            // 로그아웃 버튼 클릭
            $("#pop_logout_btn").click(function(){
                localStorage.setItem('sso_logout_yn', 1);
                top.location.href="/logout";
            });

            // 연장하기 버튼 클릭
            $("#extend_btn").click(function(){
                $('#logout_popup_wrap').dialog('close');
                autoLogout.extendLogin();
            });
        }

        // 메뉴 선택처리
        $(".sub-menu li a").click(function () {
            $(".sub-menu li").removeClass('selected');
            $(this).parent().addClass('selected');
        });

    });
</script>
