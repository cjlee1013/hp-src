# Mypage-API SERVER

Mypage-api 서버는 APP/WEB의 고객들의 마이페이지 내에 필요한 데이터를 관리하는 서버로
주문/취소/반품/교환 등과 같은 데이터를 관리합니다.

해당 소스는 아래와 같은 환경에서 구현하였습니다.

- 개발환경
    - IntelliJ
    - Java 8 +
    - Gradle 5.0 +
    - Git
    - Tomcat 9 +
    - Docker
    - FrameWork
        - SpringBoot : 2.16 RELEASE
        - PlusFramework : 1.0.0 RELEASE 

- 자세한 개발환경은 [모바일플랫폼혁신팀-벡엔드-03.개발-프로젝트설정 Confluence] 를 참고하시면 됩니다.

# Setting 시 유의사항.

Setting 중 서버 기동시 Cryptokey-api 서버와 관련하여 오류가 발생되는 경우.
Docker로 해당 서버를 기동해주어야 하셔야 합니다.
[관련 Confluence] 
### Docker 설정

- Cryptokey-api Docker image 다운
- 다운받은 image를 Docker에 Load
```shell script
docker load -i cryptokey-api.tar cryptokey-api:v1
```

- Docker에 Load한 이미지 구동
```shell script
#docker run -it -d -p {연결하고자하는 LocalPort}:{docker 내부에서 연결할 Port} --name {원하는 서비스이름} {docker에 load한 이미지이름}
docker run -it -d -p 8088:8080 --name cryptokey-api cryptokey-api:v1
```

※ 문의는 [모바일플렛폼혁신팀] 으로 메일 주시면 됩니다.



[모바일플렛폼혁신팀]: mailto:V200002224@homeplus.co.kr
[관련 Confluence]: http://confluence.homeplusnet.co.kr/display/~chongmoon.cho/2.+docker+container
[모바일플랫폼혁신팀-벡엔드-03.개발-프로젝트설정 Confluence]: http://confluence.homeplusnet.co.kr/pages/viewpage.action?pageId=7188969