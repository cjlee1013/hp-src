package kr.co.homeplus.mypage.constants;

public class CustomConstants {

    public static final String PLUS = "+";
    public static final String MINUS = "-";
    public static final String MULTIPLY = "*";
    public static final String DIVISION = "/";
    public static final String MOD = "%";
    public static final String SINGLE_QUOTE = "'";
    public static final String DOUBLE_QUOTE = "\"";
    public static final String ROUND_BRACKET_START = "(";
    public static final String ROUND_BRACKET_END = ")";
    public static final String CURLY_BRACKET_START = "{";
    public static final String CURLY_BRACKET_END = "}";
    public static final String SQUARE_BRACKET_START = "{";
    public static final String SQUARE_BRACKET_END = "}";
    public static final String ANGLE_BRACKET_LEFT = "<";
    public static final String ANGLE_BRACKET_RIGHT = ">";
    public static final String SPLIT_DOT = "\\.";
    public static final String NOT_EQUAL = "!=";
    public static final String IN = "IN";
    public static final String OUT = "OUT";
    public static final String YYYYMMDD = "%Y-%m-%d";

}
