package kr.co.homeplus.mypage.constants;

public class Constants {
    public static final String SPACE = " ";
    public static final String DOT = ".";
    public static final String CARET = "^";
    public static final String COMMNA = ",";
    public static final String PIPE = "|";
    public static final String COLON = ":";
    public static final String HYPHEN = "-";
    public static final String EMPTY_STRING = "";
    public static final String STAR = "*";
    public static final String EQUAL = "=";
    public static final String TAB = "\t";
    public static final String NEWLINE = "\n";
    public static final String UNDERLINE = "_";
    public static final String BEGIN_BRACE = "{";
    public static final String END_BRACE = "}";
    public static final String QUESTION = "?";
    public static final String SLASH = "/";
    public static final String AMP = "&";

    public static final String USE_Y = "Y";
    public static final String USE_N = "N";

    //무이자
    public static final String INSTALLMENT_TYPE_N = "N";
    //부분무이자
    public static final String INSTALLMENT_TYPE_P = "P";

    public static final String RESULT_MSG_SUCCESS = "SUCCESS";
    public static final String RESULT_MSG_FAIL    = "FAIL";
    public static final String RESULT_MSG_OK      = "OK";
    public static final int RESULT_CODE_SUCCESS   = 200;
    public static final int RETURN_SUCCESS        = 1;

    public static final String DATE_FORMAT_YMDHMS = "yyyyMMddHHmmss";
    public static final String DATE_FORMAT_YMD = "yyyyMMdd";
    public static final String DATE_FORMAT_YMDH = "yyyyMMddHH";

    // Url connection
    public static final String URL_BASIC = "http://";
    public static final String URL_HTTPS = "https://";
    public static final String HTTP_STATUS_SUCCESS = "SUCCESS";

    //number
    public static final int ZERO=0;
    public static final int ONE=1;
    public static final int TWO=2;
    public static final int THREE=3;
    public static final int FOUR=4;
    public static final int FIVE=5;
    public static final Double DOUBLE_ZERO=0.0;
    public static final String SHIFT_1 = "10";
    public static final String SHIFT_2 = "20";
    public static final String SHIFT_3 = "30";
    public static final String SHIFT_4 = "40";
    public static final String SHIFT_5 = "50";
    public static final String SHIFT_6 = "60";

    // time
    public static final String START_TIME_FORMAT = " 00:00:00";
    public static final String END_TIME_FORMAT = " 23:59:59";

    //setting
    public static final String JSON = "JSON";
    public static final String UTF8 = "UTF-8";

    // profiles
    public static final String PROFILES_LOCAL   = "local";
    public static final String PROFILES_DEV     = "dev";
    public static final String PROFILES_QA      = "qa";
    public static final String PROFILES_STG     = "stg";
    public static final String PROFILES_PRD    = "prd";

    //
    public static final String OMNI_IF_REG_ID = "OMNI";
    public static final String OMNI_IF_CHG_ID = "OMNI";

    // 공통 > 시스템 구분자
    public static final String SYS_DIVISION = "∑";

    public static final String SPC_1 = "SPC_1";
    public static final String SPC_2 = "SPC_2";
    public static final String SPC_3 = "SPC_3";

    // 특수문자[/ & ']
    public static final String SPC_1_FORMAT = "\\/\\&\\'";
    public static final String SPC_1_PATTERN = "^[\\/\\&\\']*$";

    // 특수문자[- _ / ? ! ~ ( ) & % [ ] + ! * - , . ·]
    public static final String SPC_2_FORMAT = "-_/!~&%,·\\.\\?\\(\\)\\[\\]\\+\\*";
    public static final String SPC_2_PATTERN = "^[-_/!~&%,·\\.\\?\\(\\)\\[\\]\\+\\*]*$";

    // 특수문자[~ ! @ # $ % ^ & * \ " ' + = ` | ( ) [ ] : ; - _ ※ ☆ ★ ○ ● ◎ △ ▲ ▽ ▼ → ← ↑ ↓ ↔ ◁ ◀ ▷ ▶ ♡ ♥ ? / , ·]
    public static final String SPC_3_FORMAT = " ~!@#%&\\\\\\\"`\\'=:;\\-_※☆★○●◎△▲▽▼→←↑↓↔◁◀▷▶♡♥,·₩￦\\.\\+\\*\\?\\^\\$\\<\\>\\[\\]\\{\\}\\(\\)\\|\\/";
    public static final String SPC_3_PATTERN = "^[~!@#%&\\\\\\\"`\\'=:;\\-_※☆★○●◎△▲▽▼→←↑↓↔◁◀▷▶♡♥,·₩￦\\.\\+\\*\\?\\^\\$\\<\\>\\[\\]\\{\\}\\(\\)\\|\\/]*$";
}
