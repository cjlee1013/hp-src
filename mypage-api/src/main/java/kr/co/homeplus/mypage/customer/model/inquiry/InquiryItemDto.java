package kr.co.homeplus.mypage.customer.model.inquiry;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "1:1문의 주문/상품 정보")
public class InquiryItemDto {

    @ApiModelProperty(value = "문의관리번호", position = 1)
    private long inqryNo;

    @ApiModelProperty(value = "주문번호", position = 2)
    private long purchaseOrderNo;

    @ApiModelProperty(value = "상품주문번호", position = 3)
    private long orderItemNo;

    @ApiModelProperty(value = "아이템번호", position = 4)
    private String itemNo;

    @ApiModelProperty(value = "아이템명", position = 5)
    private String itemNm1;

    @ApiModelProperty(value = "아이템옵션명", position = 6)
    private String itemOptNm;

    @ApiModelProperty(value = "주문일자", position = 7)
    private String orderDt;

}
