package kr.co.homeplus.mypage.user.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Data;

@Data
@ApiModel(description = "회원등급 조회 DTO")
public class UserGradeInfoDto {

    @ApiModelProperty(value = "나의 회원등급 정보")
    private GradeInfoDto myGradeInfo;

    @ApiModelProperty(value = "회원 등급정의 리스트")
    private List<UserGradeDto> gradeDefinitionList;

}
