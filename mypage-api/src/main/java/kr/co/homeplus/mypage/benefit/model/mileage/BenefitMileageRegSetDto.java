package kr.co.homeplus.mypage.benefit.model.mileage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "외부연동 > 마이페이지 > 마일리지 등록")
public class BenefitMileageRegSetDto {
    @ApiModelProperty(value = "마일리지 등록번호", position = 1)
    private String mileageCouponNo;
    @ApiModelProperty(value = "유저번호", position = 2, hidden = true)
    private long userNo;
}
