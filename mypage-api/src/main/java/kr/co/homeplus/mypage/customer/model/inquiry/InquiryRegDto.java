package kr.co.homeplus.mypage.customer.model.inquiry;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "1:1 문의 등록")
public class InquiryRegDto {
    @ApiModelProperty(value = "유저번호", position = 1)
    private long userNo;
    @ApiModelProperty(value = "문의타입", position = 2)
    private String inqryType;
    @ApiModelProperty(value = "문의상세타입", position = 3)
    private String inqryDetailType;
    @ApiModelProperty(value = "문의카테고리(STORE:매장상품,DLV:택배상품", position = 4)
    private String inqryCategory;
    @ApiModelProperty(value = "문의제목", position = 5)
    private String inqryTitle;
    @ApiModelProperty(value = "문의내용", position = 6)
    private String inqryCntnt;
    @ApiModelProperty(value = "전화번호", position = 7)
    private String mobileNo;
    @ApiModelProperty(value = "이메일", position = 8)
    private String email;
    @ApiModelProperty(value = "SMS알람여부", position = 9)
    private String smsAlamYn;
    @ApiModelProperty(value = "이메일알람여부", position = 10)
    private String emailAlamYn;
    @ApiModelProperty(value = "이미지1(업로드된파일의주소)", position = 11)
    private String inqryImg1;
    @ApiModelProperty(value = "이미지2(업로드된파일의주소)", position = 12)
    private String inqryImg2;
    @ApiModelProperty(value = "이미지3(업로드된파일의주소)", position = 13)
    private String inqryImg3;
    @ApiModelProperty(value = "문의주문/상품관계여부(관계없을경우 N, 상품/주문 선택시 Y)", position = 14)
    private String inqryRelYn;
    @ApiModelProperty(value = "상품/주문 선택 정보", position = 15)
    private List<InquiryItemDto> inquiryItem;
    @ApiModelProperty(value = "점포유형", position = 16)
    private String storeType;
    @ApiModelProperty(value = "파트너ID", position = 17)
    private String partnerId;
    @ApiModelProperty(value = "문의관리번호", hidden = true, position = 18)
    private long inqryNo;
    @ApiModelProperty(value = "사이트타입", hidden = true, position = 19)
    private String siteType;
}
