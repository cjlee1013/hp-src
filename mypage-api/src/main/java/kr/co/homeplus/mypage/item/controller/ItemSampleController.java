package kr.co.homeplus.mypage.item.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.HashMap;
import java.util.List;
import kr.co.homeplus.mypage.enums.AppImpl;
import kr.co.homeplus.mypage.enums.ResponseCode;
import kr.co.homeplus.mypage.item.model.ItemCollectDto;
import kr.co.homeplus.mypage.order.service.OrderService;
import kr.co.homeplus.mypage.factory.ResponseFactory;
import kr.co.homeplus.plus.api.support.client.header.WebHeaderKey;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/prod", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(tags = "Mypage 상품 조회", value = "Mypage 상품 조회 관련 컨트롤러")
@Slf4j
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
}
)
@RequiredArgsConstructor
public class ItemSampleController {

    private final OrderService orderService;

    @ApiOperation(value = "찜리스트 Sample", response = String.class, notes = "상품 조회 sample")
    @ApiImplicitParams({
        @ApiImplicitParam(name = WebHeaderKey.USER_NO, value = "사용자번호", required = true),
        @ApiImplicitParam(name = "siteType", value = "사이트유형(HMP:홈플러스,CLUB:더클럽)", defaultValue = "HMP", required = true)
    })
    @PostMapping(value = "/collectList")
    public ResponseObject<List<ItemCollectDto>> collectList(
        @RequestHeader(value = WebHeaderKey.USER_NO, defaultValue = "0") long userNo,
        @RequestHeader(value = "siteType", defaultValue = "") String siteType
    ) throws Exception {
        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS);
    }

    @ApiOperation(value = "최근 검색 (본) Sample", response = String.class, notes = "상품 조회 sample")
    @ApiImplicitParams({
        @ApiImplicitParam(name = WebHeaderKey.USER_NO, value = "사용자번호", required = true),
        @ApiImplicitParam(name = "siteType", value = "사이트유형(HMP:홈플러스,CLUB:더클럽)", defaultValue = "HMP", required = true)
    })
    @PostMapping(value = "/recentProdList")
    public ResponseObject<List<ItemCollectDto>> recentProdList(
        @RequestHeader(value = WebHeaderKey.USER_NO, defaultValue = "0") long userNo,
        @RequestHeader(value = "siteType", defaultValue = "") String siteType
    ) throws Exception {
        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS);
    }

    @ApiOperation(value = "최근 구매 Sample", response = String.class, notes = "상품 조회 sample")
    @ApiImplicitParams({
        @ApiImplicitParam(name = WebHeaderKey.USER_NO, value = "사용자번호", required = true),
        @ApiImplicitParam(name = "siteType", value = "사이트유형(HMP:홈플러스,CLUB:더클럽)", defaultValue = "HMP", required = true)
    })
    @PostMapping(value = "/recentBuyList")
    public ResponseObject<List<ItemCollectDto>> recentBuyList(
        @RequestHeader(value = WebHeaderKey.USER_NO, defaultValue = "0") long userNo,
        @RequestHeader(value = "siteType", defaultValue = "") String siteType
    ) throws Exception {
        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS);
    }

    @ApiOperation(value = "상품 제외 Sample", response = String.class, notes = "상품 조회 sample")
    @ApiImplicitParams({
        @ApiImplicitParam(name = WebHeaderKey.USER_NO, value = "사용자번호", required = true),
        @ApiImplicitParam(name = "siteType", value = "사이트유형(HMP:홈플러스,CLUB:더클럽)", defaultValue = "HMP", required = true)
    })
    @RequestMapping(value = "/removeProd/{itemNo}", method = RequestMethod.GET)
    public ResponseObject<HashMap<String, Object>> claimDetailList (
        @RequestHeader(value = WebHeaderKey.USER_NO, defaultValue = "0") long userNo,
        @RequestHeader(value = "siteType", defaultValue = "") String siteType,
        @PathVariable("itemNo") String itemNo
    ) throws Exception {
        log.info("user_no :: {} / siteType ::: {}", userNo, siteType);
        return ResponseFactory.getResponseObject(
            AppImpl.APP_COMMON_CODE_SUCCESS, new HashMap<String, Object>(){{put("itemNo", itemNo);}});
    }

}
