package kr.co.homeplus.mypage.receipt.model.order.purchase;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class OrderPurchaseReceiptItemInfo {

    @JsonIgnore
    @ApiModelProperty(value = "주문번호", position = 1)
    private long purchaseOrderNo;

    @JsonIgnore
    @ApiModelProperty(value = "주민일자", position = 2)
    private String orderDt;

    @JsonIgnore
    @ApiModelProperty(value = "총배송비", position = 3)
    private long totShipAmt;

    @JsonIgnore
    @ApiModelProperty(value = "총결제비용", position = 4)
    private long totPaymentAmt;

    @JsonIgnore
    @ApiModelProperty(value = "노출 상품명", position = 5)
    private String displayItemNm;

    @JsonIgnore
    @ApiModelProperty(value = "구매자명", position = 6)
    private String buyerNm;

    @JsonIgnore
    @ApiModelProperty(value = "상점아이디", position = 7)
    private int storeId;

    @JsonIgnore
    @ApiModelProperty(value = "상품번호", position = 8)
    private String itemNo; // 상품번호

    @ApiModelProperty(value = "상품명", position = 9)
    private String itemNm1;

    @ApiModelProperty(value = "상품수량", position = 10)
    private int itemQty;

    @ApiModelProperty(value = "주문금액", position = 11)
    private long orderPrice;

    @JsonIgnore
    @ApiModelProperty(value = "파트너아이디", position = 12)
    private String partnerId;

    @ApiModelProperty(value = "파트너명", position = 13)
    private String partnerNm;

    @ApiModelProperty(value = "대표자명", position = 14)
    private String partnerOwner;

    @ApiModelProperty(value = "사업자번호", position = 15)
    private String partnerNo;

    @ApiModelProperty(value = "점포유형", position = 16)
    private String storeType;

    @ApiModelProperty(value = "세금여부(Y:과세,N:면세,Z:영세)", position = 17)
    private String taxYn;
    @ApiModelProperty(value = "임직원할인금액", position = 18)
    private long empDiscountAmt;
    @ApiModelProperty(value = "상품할인금액", position = 19)
    private long itemDiscountAmt;
    @ApiModelProperty(value = "카드상품할인금액", position = 20)
    private long cardDiscountAmt;
    @ApiModelProperty(value = "장바구니쿠폰할인금액", position = 21)
    private long cartDiscountAmt;
    @ApiModelProperty(value = "행사할인금액", position = 22)
    private long promoDiscountAmt;

}