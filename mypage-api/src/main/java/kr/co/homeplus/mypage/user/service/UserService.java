package kr.co.homeplus.mypage.user.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import kr.co.homeplus.mypage.core.exception.LogicException;
import kr.co.homeplus.mypage.enums.ExternalInfo;
import kr.co.homeplus.mypage.enums.ResponseCode;
import kr.co.homeplus.mypage.enums.SiteType;
import kr.co.homeplus.mypage.external.service.ExternalService;
import kr.co.homeplus.mypage.factory.ResponseFactory;
import kr.co.homeplus.mypage.user.model.GradeInfoDto;
import kr.co.homeplus.mypage.user.model.GradeOrderInfoDto;
import kr.co.homeplus.mypage.user.model.RefundAccountSearchGetDto;
import kr.co.homeplus.mypage.user.model.RefundAccountSetDto;
import kr.co.homeplus.mypage.user.model.ShippingAddressGetDto;
import kr.co.homeplus.mypage.user.model.ShippingAddressSetDto;
import kr.co.homeplus.mypage.user.model.UserGradeCouponDto;
import kr.co.homeplus.mypage.user.model.UserGradeDto;
import kr.co.homeplus.mypage.user.model.UserGradeGetDto;
import kr.co.homeplus.mypage.user.model.UserGradeInfoDto;
import kr.co.homeplus.mypage.user.model.UserInfoGetDto;
import kr.co.homeplus.mypage.utils.RequestUtils;
import kr.co.homeplus.mypage.utils.SetParameter;
import kr.co.homeplus.mypage.utils.StringUtil;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserService {

    // 외부 통신 Service
    private final ExternalService externalService;

    /**
     * 마이페이지 > 회원정보관리 > 회원정보조회
     *
     * @return 회원정보조회 데이터
     * @throws Exception 오류시 오류처리
     */
    public ResponseObject<UserInfoGetDto> userInfoSearch() throws Exception {
        return ResponseFactory.getResponseObject(
            ResponseCode.SUCCESS,
            externalService.getTransfer(
                ExternalInfo.USER_API_SEARCH_USER_INFO,
                new ArrayList<SetParameter>() {
                    {
                        add(SetParameter.create("userNo", externalService.getUserNo()));
                    }
                }, UserInfoGetDto.class)
        );
    }

    /**
     * 마이페이지 > 회원정보관리 > 회원배송지목록조회
     *
     * @return 회원배송지목록조회 조회 데이터
     * @throws Exception 오류시 오류처리
     */
    public ResponseObject<ShippingAddressGetDto> addressListSearch(int page) throws Exception {
        ResponseObject<ShippingAddressGetDto> result =
            externalService.getTransferResponse(
                ExternalInfo.USER_API_SHIPPING_ADDRESS_LIST,
                new ArrayList<SetParameter>() {{ add(SetParameter.create("page", page)); }},
                RequestUtils.createUserLoginHeaders(),
                ShippingAddressGetDto.class
            );
        log.debug("addressListSearch : {}, {}", result.getData(), result.getPagination());
        return ResponseFactory.getResponseObject(
            ResponseCode.SUCCESS,
            result.getData(),
            result.getPagination()
        );
    }

    /**
     * 마이페이지 > 회원정보관리 > 회원배송지 등록/수정
     *
     * @return 회원배송지 등록/수정 결과
     * @throws Exception 오류시 오류처리
     */
    public ResponseObject<Boolean> userAddressRequestSet(
        ShippingAddressSetDto shippingAddressSetDto) throws Exception {
        boolean result;//회원배송지 등록/수정 결과

        /* 1.등록/수정 여부 확인 [seq] not null : 등록, null : 수정 */
        if (shippingAddressSetDto.getSeq() == null || shippingAddressSetDto.getSeq() == 0) {
            log.debug("회원배송지 등록 : {}", shippingAddressSetDto);
            result = externalService.postTransfer(
                ExternalInfo.USER_API_SHIPPING_ADDRESS_ADD, shippingAddressSetDto,
                RequestUtils.createUserLoginHeaders(), Boolean.class);
        } else {
            log.debug("회원배송지 수정 : {}", shippingAddressSetDto);
            result = externalService.postTransfer(
                ExternalInfo.USER_API_SHIPPING_ADDRESS_MODIFY, shippingAddressSetDto,
                RequestUtils.createUserLoginHeaders(), Boolean.class);
        }

        /* 회원배송지 등록/수정 결과 */
        if (!result) {
            log.error("회원배송지 등록/수정 실패");
            throw new LogicException(ResponseCode.USER_ADDRESS_SET_ERR_01);
        }

        /* return */
        return ResponseFactory.getResponseObject(ResponseCode.USER_ADDRESS_SET_SUCCESS);
    }

    /**
     * 마이페이지 > 회원정보관리 > 환불계좌조회
     *
     * @return 환불계좌조회 데이터
     * @throws Exception 오류시 오류처리
     */
    public ResponseObject<RefundAccountSearchGetDto> refundAccountInfoSearch() throws Exception {
        return ResponseFactory.getResponseObject(
            ResponseCode.SUCCESS,
            externalService.getTransfer(
                ExternalInfo.USER_API_SEARCH_REFUND_ACCOUNT, null,
                RequestUtils.createUserLoginHeaders(), RefundAccountSearchGetDto.class)
        );
    }

    /**
     * 마이페이지 > 회원정보관리 > 환불계좌등록/수정
     *
     * @return 환불계좌등록/수정 결과
     * @throws Exception 오류시 오류처리
     */
    public ResponseObject<String> refundAccountRequestSet(
        RefundAccountSetDto refundAccountSetDto) throws Exception {
        boolean result;//환불계좌등록/수정 결과

        /* 1.환불계좌 등록여부 조회 */
        ResponseObject<RefundAccountSearchGetDto> regRefundAccount = this.refundAccountInfoSearch();

        /* 2-2.환불계좌 등록
         * 등록된 계좌가 없을 경우
         * */
        if (StringUtil.isEmpty(regRefundAccount.getData())) {
            log.info("환불계좌 등록 : {}", refundAccountSetDto);
            result = externalService.postTransfer(
                ExternalInfo.USER_API_REGISTER_REFUND_ACCOUNT, refundAccountSetDto,
                RequestUtils.createUserLoginHeaders(), boolean.class);
        } else {
            /* 2-1.환불계좌 수정
             * 이미 등록된 계좌가 있을 경우
             * */
            log.info("환불계좌 수정 : {}", refundAccountSetDto);

            /* 2-2. 등록된 계좌seq와 요청 계좌seq 동일 여부 체크*/
            if(regRefundAccount.getData().getSeq() != refundAccountSetDto.getSeq()){
                throw new LogicException(ResponseCode.REFUND_ACCOUNT_SET_ERR_02);
            }

            result = externalService.postTransfer(
                ExternalInfo.USER_API_MODIFY_REFUND_ACCOUNT, refundAccountSetDto,
                RequestUtils.createUserLoginHeaders(), boolean.class);
        }

        /* 환불계좌등록/수정 결과 */
        if (!result) {
            log.error("환불계좌등록/수정 실패");
            throw new LogicException(ResponseCode.REFUND_ACCOUNT_SET_ERR_01);
        }

        /* return */
        return ResponseFactory.getResponseObject(ResponseCode.REFUND_ACCOUNT_SET_SUCCESS);
    }

    public ResponseObject<UserGradeGetDto> getUserGradeInfo(SiteType siteType) throws Exception {
        UserGradeInfoDto userGradeInfo = externalService.getTransfer(ExternalInfo.USER_API_USER_GRADE_INFO, new ArrayList<SetParameter>() { { add(SetParameter.create("userNo", externalService.getUserNo())); }}, UserGradeInfoDto.class);
        // 고객 등급 sort
        List<UserGradeDto> gradeList = userGradeInfo.getGradeDefinitionList().stream().sorted(Comparator.comparing(UserGradeDto::getGradeSeq).reversed()).collect(Collectors.toList());
        GradeOrderInfoDto orderInfoDto =
            externalService.getTransfer(
                ExternalInfo.CLAIM_API_GRADE_ORDER_INFO,
                new ArrayList<SetParameter>() {{
                    add(SetParameter.create("userNo", externalService.getUserNo()));
                    add(SetParameter.create("siteType", siteType));
                }},
                GradeOrderInfoDto.class
            );

        int orderCnt = (orderInfoDto.getOrderCnt() < 0) ? 0 : orderInfoDto.getOrderCnt();
        int orderAmt = (orderInfoDto.getOrderPrice() < 0) ? 0 : orderInfoDto.getOrderPrice();
        int selected = 0;
        for(UserGradeDto dto : gradeList) {
            //조건1 : 주문건수
            Boolean condition1 = dto.getOrderCnt() <= orderCnt;
            //조건2 : 최저금액 이상 최저금액 이하
            Boolean condition2 = (dto.getOrderMinAmount() <= orderAmt && dto.getOrderMaxAmount() >= orderAmt);

            if(dto.getComparisonRule().equalsIgnoreCase("AND")){
                if(condition1 && condition2){
                    break;
                }
            } else {
                if(condition1){
                    break;
                }
            }
            // check
            selected++;
        }
        UserGradeDto next = gradeList.get(selected);

        List<UserGradeCouponDto> couponDto = externalService.getListTransfer(
            ExternalInfo.FRONT_API_USER_GRADE_COUPON_LIST,
            new ArrayList<SetParameter>() {{
                add(SetParameter.create("siteType", siteType.name()));
                add(SetParameter.create("userNo", externalService.getUserNo()));
                add(SetParameter.create("gradeSeq", userGradeInfo.getMyGradeInfo().getGradeSeq()));
            }},
            UserGradeCouponDto.class
        );

        return ResponseFactory.getResponseObject(
            ResponseCode.USER_GRADE_INFO_SUCCESS,
            UserGradeGetDto.builder()
                .current(userGradeInfo.getMyGradeInfo())
                .next(GradeInfoDto.builder().gradeSeq(next.getGradeSeq()).gradeNm(next.getGradeNm()).orderAmount(orderInfoDto.getOrderPrice()).orderCnt(orderInfoDto.getOrderCnt()).build())
                .expect(gradeList.get((selected <= 0) ? 0 : (selected - 1)))
                .couponInfo(couponDto)
                .build()
        );

    }
}
