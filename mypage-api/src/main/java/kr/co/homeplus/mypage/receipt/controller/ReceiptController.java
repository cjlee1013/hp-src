package kr.co.homeplus.mypage.receipt.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.mypage.enums.SiteType;
import kr.co.homeplus.mypage.receipt.model.claim.ClaimPurchaseMethodReceiptSetDto;
import kr.co.homeplus.mypage.receipt.model.claim.ClaimPurchaseReceiptGetDto;
import kr.co.homeplus.mypage.receipt.model.claim.ClaimPurchaseReceiptSetDto;
import kr.co.homeplus.mypage.receipt.model.claim.card.ClaimCardReceiptGetDto;
import kr.co.homeplus.mypage.receipt.model.claim.cash.ClaimCashReceiptGetDto;
import kr.co.homeplus.mypage.receipt.model.order.OrderPurchaseMethodReceiptSetDto;
import kr.co.homeplus.mypage.receipt.model.order.OrderPurchaseReceiptGetDto;
import kr.co.homeplus.mypage.receipt.model.order.OrderPurchaseReceiptSetDto;
import kr.co.homeplus.mypage.receipt.model.order.card.OrderPurchaseCardReceiptGetDto;
import kr.co.homeplus.mypage.receipt.model.order.cash.OrderPurchaseCashReceiptGetDto;
import kr.co.homeplus.mypage.receipt.model.order.order.ClaimRefundReceiptGetDto;
import kr.co.homeplus.mypage.receipt.model.order.order.ClaimRefundReceiptSetDto;
import kr.co.homeplus.mypage.receipt.service.ReceiptService;
import kr.co.homeplus.plus.api.support.client.header.UserDeviceType;
import kr.co.homeplus.plus.api.support.client.header.WebHeaderKey;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/receipt", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(tags = "Mypage 영수증조회", value = "Mypage 영수증 조회 컨트롤러")
@Slf4j
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
}
)
@RequiredArgsConstructor
public class ReceiptController {
    private final ReceiptService receiptService;

    @ApiOperation(value = "주문_구매영수증조회", response = OrderPurchaseReceiptGetDto.class, notes = "주문_구매영수증")
    @PostMapping(value = "/getOrderPurchaseReceipt")
    public ResponseObject<OrderPurchaseReceiptGetDto> getOrderPurchaseReceipt(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestBody @Valid OrderPurchaseReceiptSetDto setDto
    ) throws Exception {
        return receiptService.getOrderPurchaseReceiptInfo(setDto);
    }

    @ApiOperation(value = "주문_카드영수증조회", response = OrderPurchaseCardReceiptGetDto.class, notes = "주문_카드영수증")
    @PostMapping(value = "/getOrderPurchaseCardReceipt")
    public ResponseObject<OrderPurchaseCardReceiptGetDto> getOrderPurchaseCardReceipt(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestBody @Valid OrderPurchaseMethodReceiptSetDto setDto
    ) throws Exception {
        return receiptService.getOrderPurchaseCardReceiptInfo(setDto);
    }

    @ApiOperation(value = "주문_현금영수증조회", response = OrderPurchaseCashReceiptGetDto.class, notes = "주문_현금영수증")
    @PostMapping(value = "/getOrderPurchaseCashReceipt")
    public ResponseObject<OrderPurchaseCashReceiptGetDto> getOrderPurchaseCashReceipt(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestBody @Valid OrderPurchaseMethodReceiptSetDto setDto
    ) throws Exception {
        return receiptService.getOrderPurchaseCashReceiptInfo(setDto);
    }

    @ApiOperation(value = "클레임_구매영수증조회", response = ClaimPurchaseReceiptGetDto.class, notes = "주문_구매영수증")
    @PostMapping(value = "/getClaimPurchaseReceipt")
    public ResponseObject<ClaimPurchaseReceiptGetDto> getClaimPurchaseReceipt(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestBody @Valid ClaimPurchaseReceiptSetDto setDto
    ) throws Exception {
        return receiptService.getClaimPurchaseReceiptInfo(setDto);
    }

    @ApiOperation(value = "클레임_카드영수증조회", response = ClaimCardReceiptGetDto.class, notes = "주문_카드영수증")
    @PostMapping(value = "/getClaimCardPurchaseReceipt")
    public ResponseObject<ClaimCardReceiptGetDto> getClaimCardPurchaseReceipt(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestBody @Valid ClaimPurchaseMethodReceiptSetDto setDto
    ) throws Exception {
        return receiptService.getClaimCardPurchaseReceiptInfo(setDto);
    }

    @ApiOperation(value = "클레임_현금영수증조회", response = ClaimCashReceiptGetDto.class, notes = "주문_현금영수증")
    @PostMapping(value = "/getClaimCashPurchaseReceipt")
    public ResponseObject<ClaimCashReceiptGetDto> getClaimCashPurchaseReceipt(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestBody @Valid ClaimPurchaseMethodReceiptSetDto setDto
    ) throws Exception {
        return receiptService.getClaimCashPurchaseReceiptInfo(setDto);
    }

    @ApiOperation(value = "주문상세-클레임취소금액영수증", response = ClaimRefundReceiptGetDto.class, notes = "주문/배송_환불내역")
    @PostMapping(value = "/getOrderRefundReceipt")
    public ResponseObject<List<ClaimRefundReceiptGetDto>> getOrderRefundReceipt(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestBody @Valid ClaimRefundReceiptSetDto setDto
    ) throws Exception {
        return receiptService.getOrderRefundReceipt(setDto);
    }
}
