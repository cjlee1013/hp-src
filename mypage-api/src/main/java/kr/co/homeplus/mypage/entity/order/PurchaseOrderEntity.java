package kr.co.homeplus.mypage.entity.order;

public class PurchaseOrderEntity {

    // 구매주문번호
    public String purchaseOrderNo;

    // 결제번호
    public String paymentNo;

    // 장바구니통로번호
    public String cartRouteNo;

    // 고객번호(비회원주문시 신규생성)
    public String userNo;

    // 주문일자(주문데이터 추출 기준 Key값)
    public String orderDt;

    // 사이트유형(HMP:홈플러스,CLUB:더클럽)
    public String siteType;

    // 총상품금액
    public String totItemPrice;

    //
    public String totItemQty;

    // 총배송가격(총 배송비 가격:고객지불금액)
    public String totShipPrice;

    // 총추가배송가격(총 도서산간 추가 배송비:고객지불금액)
    public String totIslandShipPrice;

    // 총상품할인금액(제휴즉시,상품즉시,상품쿠폰)
    public String totItemDiscountAmt;

    // 총배송비할인금액(배송비즉시,배송비쿠폰)
    public String totShipDiscountAmt;

    // 총임직원할인금액
    public String totEmpDiscountAmt;

    // 총장바구니할인금액(장바구니쿠폰,카드사즉시할인)
    public String totCartDiscountAmt;

    // PG결제금액
    public String pgAmt;

    // 마일리지결제금액
    public String mileageAmt;

    // MHC결제금액
    public String mhcAmt;

    // DGV결제금액
    public String dgvAmt;

    // OCB결제금액
    public String ocbAmt;

    // 주문자명
    public String buyerNm;

    // 주문자모바일번호
    public String buyerMobileNo;

    // 비회원주문여부(Y:비회원,N:회원)
    public String nomemOrderYn;

    // 플랫폼(PC,APP,MWEB)
    public String platform;

    // OS유형(모바일기기OS:ANDROID,IOS)
    public String osType;

    // 제휴코드
    public String affiliateCd;

    // 요청IP
    public String reqIp;

    // 임직원번호
    public String empNo;

    // 임직원명
    public String empNm;

    // 임직원할인남은금액
    public String empDiscountRemainAmt;

    // 등록일시
    public String regDt;

    // 등록자ID
    public String regId;

    // 수정일시
    public String chgDt;

    // 수정자ID
    public String chgId;

}
