package kr.co.homeplus.mypage.user.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "고객 등급별 쿠폰 정보")
public class UserGradeCouponInfoDto {
    @ApiModelProperty(value = "암호화된 쿠폰번호", position = 1)
    private String couponNoEnc;

    @ApiModelProperty(value = "전시쿠폰명", position = 2)
    private String displayCouponNm;

    @ApiModelProperty(value = "쿠폰종류 (1:장바구니쿠폰, 2:배송비쿠폰, 3:상품쿠폰)", position = 3)
    private String couponType;

    @ApiModelProperty(value = "할인타입(2:정액, 1:정률)", position = 4)
    private String discountType;

    @ApiModelProperty(value = "최대할인금액", position = 5)
    private Integer discountMax;

    @ApiModelProperty(value = "최소구매금액", position = 6)
    private Integer purchaseMin;

    @ApiModelProperty(value = "할인타입에 따른 할인금액/할인율", position = 7)
    private Integer discount;

    @ApiModelProperty(value = "다운로드 가능 여부(다운로드 버튼 활성화 여부) Y:활성 N: 비활성", position = 8)
    private String downloadAvailYn;

    @ApiModelProperty(value = "상점타입", position = 9)
    private String storeType;

    @ApiModelProperty(value = "쿠폰번호", position = 100, hidden = true)
    private Long couponNo;
}
