package kr.co.homeplus.mypage.wishitem.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Min;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "마이페이지 > 찜 리스트 - 셀러샵 리스트")
public class WishSellerShopListSetDto {

    @ApiModelProperty(value = "회원일련번호", hidden = true)
    private String userNo;

    @ApiModelProperty(value = "페이지 No ( 최소값 1 )", required = true)
    @Min(value = 1, message = "페이지 No 최소값")
    private Integer page;

    @ApiModelProperty(value = "페이지 리스트 수 ( 최소값 10 )", required = true)
    @Min(value = 10, message = "페이지 리스트 수 최소값")
    private Integer perPage;
}
