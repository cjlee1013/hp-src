package kr.co.homeplus.mypage.customer.model.personal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Pattern;
import lombok.Data;

@Data
@ApiModel(description = "개인정보 이용내역 조회요청 DTO")
public class PersonalSetDto {

    @ApiModelProperty(value = "이용내역조회타입(prod:상품공급,ship:운송,liquor:주류", position = 1)
    @Pattern(regexp = "prod|ship|liquor", message = "이용내역조회타입")
    private String personalType;
    @ApiModelProperty(value = "조회시작일", position = 2)
    private String schStartDt;
    @ApiModelProperty(value = "조회종료일", position = 3)
    private String schEndDt;
    @ApiModelProperty(value = "페이지", position = 4)
    private Integer page;
    @ApiModelProperty(value = "페이지당뷰수", position = 5)
    private Integer perPage;
    @ApiModelProperty(value = "고객번호", hidden = true, position = 6)
    private Long userNo;
    @ApiModelProperty(value = "사이트타입", hidden = true, position = 7)
    private String siteType;
}
