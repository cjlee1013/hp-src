package kr.co.homeplus.mypage.enums;

import kr.co.homeplus.mypage.enums.impl.EnumImpl;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum AppImpl implements EnumImpl {

    APP_COMMON_CODE_SUCCESS("0000", "정상처리 되었습니다."),
    APP_ORDER_ERR01("1000", "오류가 발생되었습니다.");

    @Getter
    private final String responseCode;

    @Getter
    private final String responseMessage;

}
