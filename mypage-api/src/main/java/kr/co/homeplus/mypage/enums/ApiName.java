package kr.co.homeplus.mypage.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum  ApiName {

    NONE("")
    , MILEAGE_REMAIN_INFO("/user/userMileageCheck")
    , MHC_REMAIN_INFO("/mhc/searchMhcPointByUserNo");

    private final String apiUri;

}
