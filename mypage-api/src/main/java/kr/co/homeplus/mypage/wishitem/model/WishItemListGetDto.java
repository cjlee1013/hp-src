package kr.co.homeplus.mypage.wishitem.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.Pattern.Flag;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "마이페이지 > 찜 리스트 - 상품 찜 리스트")
public class WishItemListGetDto {

    @ApiModelProperty(value = "문서 번호", position = 1)
    private String docId;
    @ApiModelProperty(value = "상품 번호", position = 2)
    private String itemNo;
    @ApiModelProperty(value = "상품명", position = 3)
    private String itemNm;
    @ApiModelProperty(value = "옵션 번호", position = 4)
    private Long optNo;
    @ApiModelProperty(value = "브랜드명", position = 5)
    private String brandNm;
    @ApiModelProperty(value = "점포형태", position = 6)
    private String storeType;
    @ApiModelProperty(value = "상품 유형", position = 7)
    private String itemType;
    @ApiModelProperty(value = "성인 유형", example = "NORMAL, ADULT, LOCAL_LIQUOR", position = 8)
    private String adultType;

    @ApiModelProperty(value = "점포 아이디", position = 9)
    private String storeId;
    @ApiModelProperty(value = "셀러샵 아이디", position = 10)
    private String partnerId;
    @ApiModelProperty(value = "셀러샵 이름", position = 11)
    private String partnerNm;
    @ApiModelProperty(value = "셀러샵 소개", position = 12)
    private String partnerIntroduce;
    @ApiModelProperty(value = "기본 가격", position = 13)
    private Long salePrice = 0L;
    @ApiModelProperty(value = "판매 가격", position = 14)
    private Long discountPrice = 0L;
    @ApiModelProperty(value = "단위당 가격 텍스트", example = "100g당 2,190원", position = 15)
    private String unitText;
    @ApiModelProperty(value = "판매 수량", position = 16)
    private Long saleCount = 0L;
    @ApiModelProperty(value = "일시품절 여부", position = 17)
    private boolean soldOut = false;

    @ApiModelProperty(value = "상품평 평균점수", position = 18)
    private Double comentAvg = 0.0;
    @ApiModelProperty(value = "상품평 개수", position = 19)
    private Integer comentCnt = 0;

    @ApiModelProperty(value = "상품 플래그", position = 20)
    private List<Flag> itemFlagList;
    @ApiModelProperty(value = "행사 플래그", position = 21)
    private List<Flag> promotionFlagList = new ArrayList<>();
    @ApiModelProperty(value = "배송 플래그", position = 22)
    private List<Flag> deliveryFlagList;
    @ApiModelProperty(value = "혜택 플래그", position = 23)
    private List<Flag> benefitFlagList = new ArrayList<>();

    @ApiModelProperty(value = "상품 노출 유형", example = "IMAGE, VIDEO", position = 24)
    private String exposureType;

    @ApiModelProperty(value = "검색 태그", position = 25)
    private List<String> searchTagList = new ArrayList<>();
    @ApiModelProperty(value = "장바구니 노출여부", position = 26)
    private String cartDispYn = "Y";
}
