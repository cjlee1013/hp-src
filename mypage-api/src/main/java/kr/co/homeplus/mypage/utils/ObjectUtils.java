package kr.co.homeplus.mypage.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.util.ReflectionUtils;

public class ObjectUtils {

    /**
     * Object to Map[bean]
     *
     * @param object DTO 클래스
     * @return HashMap
     * @throws Exception 예외가 발생할 경우 던짐
     */
    public static LinkedHashMap<String, Object> getConvertMap(Object object) throws Exception{
        if(object == null){
            return null;
        }

        LinkedHashMap<String, Object> returnMap = new LinkedHashMap<>();
        Class<?> clz = object.getClass();

        for(Field field : clz.getDeclaredFields()){
            if(Modifier.isPrivate(field.getModifiers())){
                ReflectionUtils.makeAccessible(field);
            }
            if(Modifier.isFinal(field.getModifiers())){
                ReflectionUtils.makeAccessible(field);
            }
            returnMap.put(field.getName(), field.get(object));
        }
        return returnMap;
    }

    public static long toLong(Object obj) {
        return NumberUtils.toLong(String.valueOf(obj));
    }

    public static int toInt(Object obj) {
        return NumberUtils.toInt(String.valueOf(obj));
    }

    public static String toString(Object obj) {
        if(obj != null){
            return obj.toString();
        }
        return null;
    }

    public static double toDouble(Object obj) {
        return NumberUtils.toDouble(String.valueOf(obj));
    }

    public static Object[] toArray(Object... objects){
        return Arrays.stream(objects).toArray();
    }

    public static List<Object> toList(Object... objects) {
        return Arrays.asList(objects);
    }
}
