package kr.co.homeplus.mypage.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ExternalInfo {
    CLAIM_API_OLD_ORDER_LIST("claim", "/mp/order/getOrderList"),
    CLAIM_API_ORDER_LIST("claim", "/mp/order/getNewOrderList"),
    CLAIM_API_ORDER_DETAIL_LIST("claim", "/mp/order/getOrderDetailList"),
    CLAIM_API_MULTI_ORDER_DETAIL_LIST("claim", "/mp/order/getMultiOrderDetailList"),
    CLAIM_API_MULTI_ORDER_DETAIL_INFO("claim", "/mp/order/getMultiOrderDetailInfo"),
    CLAIM_API_MULTI_ORDER_GROUP_ITEM_LIST("claim", "/mp/order/getMultiOrderItemList"),
    CLAIM_API_ORDER_TICKET_DETAIL_LIST("claim", "/mp/order/getOrderTicketDetailInfo"),
    CLAIM_API_MAIN_ORDER_INFO("claim", "/mp/order/getMainOrderInfo/"),
    CLAIM_API_MAIN_ORDER_CNT("claim", "/mp/order/getMainOrderCnt/"),
    CLAIM_API_MAIN_ORDER_LIST("claim", "/mp/order/getMainOrderList/"),
    CLAIM_API_SHIP_ADDR_CHANGE("claim", "/mp/order/modifyOrderShipChange/"),
    CLAIM_API_MULTI_SHIP_ADDR_CHANGE("claim", "/mp/order/modifyMultiOrderShipInfo/"),
    CLAIM_API_MULTI_SHIP_BAISC_CHANGE("claim", "/mp/order/modifyMultiOrderShipBasicInfo/"),
    CLAIM_API_CLAIM_REGISTER("claim", "/claim/claimCancelReg"),
    CLAIM_API_MULTI_BUNDLE_CLAIM_REGISTER("claim", "/claim/multiClaimCancelReg"),
    CLAIM_API_CHG_PAYMENT_REFUND("claim", "/claim/chgPaymentRefund"),
    CLAIM_API_ORDER_GIFT_ITEM_LIST("claim", "/claim/getOrderGiftItemList"),
    CLAIM_API_DLV_CD_LIST("claim", "/ship/dlvCdSearch"),
    CLAIM_API_SHIP_MSG_CD_LIST("claim", "/ship/shipMsgSearch"),
    CLAIM_API_ORDER_PICK_STORE_INFO("claim", "/mp/order/getPickupStoreInfo"),
    CLAIM_API_CHG_CLAIM_STATUS("claim", "/claim/reqChange"),
    CLAIM_API_GRADE_ORDER_INFO("claim", "/mp/order/getUserGradeOrderInfo"),
    CLAIM_API_INQUIRY_ORDER_INFO("claim", "/mp/order/getInquiryOrderList"),

    SHIPPING_API_SHIP_INFO_LIST("shipping", "/mypage/order/getShippingHistory"),
    SHIPPING_MANUAL_COMPLETE("shipping", "/admin/shipManage/setShipCompleteByBundle"),
    SHIPPING_API_PROVIDE_DELIVERY_INFO("shipping", "/mypage/personal/getShipInfo"),
    SHIPPING_API_MULTI_SHIP_INFO_LIST("shipping", "/mypage/order/getMultiShippingHistory"),
    SHIPPING_MULTI_MANUAL_COMPLETE("shipping", "/admin/shipManage/manualMultiShipComplete"),

    SHIPPING_API_PICK_SHIP_HISTORY("shipping", "/mypage/order/getPickHistory"),
    SHIPPING_API_EXCH_SHIP_HISTORY("shipping", "/mypage/order/getExchHistory"),

    GET_LOGIN_INFO("user", "/login/getLoginInfo"),

    MILEAGE_API_MILEAGE_AMOUNT_CHECK("mileage", "/user/userMileageCheck"),
    MILEAGE_API_MILEAGE_LIST("mileage", "/external/externalMileageList"),
    MILEAGE_API_MILEAGE_COUPON_REG("mileage", "/external/externalCouponReg"),

    ESCROW_API_MHC_POINT_CHECK("escrow", "/mhc/external/searchMhcPoint"),
    ESCROW_API_SLOT_CHANGE_INFO("escrow", "/slot/external/getChangeShipTimeSlot"),
    ESCROW_API_SLOT_CHANGE_MODIFY("escrow", "/slot/external/changeShipInfoAndOrderSlotStock"),
    ESCROW_API_TICKET_RE_SEND("escrow", "/ticket/external/resendTicket"),

    CLAIM_API_CLAIM_SEARCH_LIST("claim", "/claim/claimSearchList"),
    CLAIM_API_CLAIM_DETAIL_LIST("claim", "/claim/claimDetailList/"),
    CLAIM_API_CLAIM_REASON_CODE_LIST("claim", "/claim/claimReasonCodeSearch/"),
    CLAIM_API_GET_PRE_REFUND_PRICE("claim", "/claim/preRefundPrice"),
    CLAIM_API_GET_MULTI_PRE_REFUND_PRICE("claim", "/claim/preMultiRefundPrice"),

    CLAIM_API_NO_RCV_LIST("claim", "/claim/getNoRcvList"),
    CLAIM_API_NO_RCV_REG("claim", "/claim/addNoRcvInfo"),
    CLAIM_API_NO_RCV_MODIFY("claim", "/claim/modifyNoRcvInfo"),

    FRONT_API_WISH_ITEM_LIST_SEARCH("front", "/wishitem/getWishItemList"),
    FRONT_API_WISH_SELLER_SHOP_LIST_SEARCH("front", "/wishSellerShop/getWishSellerShop"),
    FRONT_API_WISH_SELLER_SHOP_MODIFY("front", "/wishSellerShop/setWishSellerShop"),

    FRONT_API_QNA_LIST_SEARCH("front", "/qna/getMyQna"),
    FRONT_API_QNA_REPLY_LIST_SEARCH("front", "/qna/getQnaReply"),
    FRONT_API_QNA_REGISTER("front", "/qna/setQna"),

    CRYPTOKEY_API_RKM_DEC("cryptokey", "/rkm/dec"),

    CLAIM_API_REVIEW_POSSIBLE_LIST("claim", "/mp/review/getReviewPossible"),
    CLAIM_API_USING_PERSONAL_INFO_LIST("claim", "/mp/customer/getUsePersonalInfo"),


    FRONT_API_USER_COUPON_COUNT("front", "/coupon/front/getUserCouponCount"),
    FRONT_API_USER_COUPON_LIST("front", "/coupon/front/getUserCouponList"),
    FRONT_API_USER_COUPON_REG("front", "/wishitem/getWishItemList"),
    FRONT_API_USER_GRADE_COUPON_LIST("front", "/coupon/front/getGradeCouponList"),

    USER_API_SEARCH_USER_INFO("user", "/search/basic/getUserInfo"),
    USER_API_SHIPPING_ADDRESS_LIST("user", "/shipping/address/getAddressListPerPage"),
    USER_API_SHIPPING_ADDRESS_ADD("user", "/shipping/address/add"),
    USER_API_SHIPPING_ADDRESS_MODIFY("user", "/shipping/address/modify"),
    USER_API_SEARCH_REFUND_ACCOUNT("user", "/refund/bank/account/getAccount"),
    USER_API_MODIFY_REFUND_ACCOUNT("user", "/refund/bank/account/modify"),
    USER_API_REGISTER_REFUND_ACCOUNT("user", "/refund/bank/account/add"),
    USER_API_USER_GRADE_INFO("user", "/search/mypage/getGradeInfo"),
    USER_API_USER_INFO("user", "/search/mypage/getMhcInfo"),
    USER_API_BLACK_CONSUMER_INFO("user", "/blackconsumer/getRestrict"),

    USERMNG_API_INQUIRY_REG("usermng", "/mypage/inquiryReg"),
    USERMNG_API_INQUIRY_REPLY_REG("usermng", "/mypage/replyInquiryReg"),
    USERMNG_API_INQUIRY_CANCEL("usermng", "/mypage/cancelInquiry"),
    USERMNG_API_INQUIRY_SEARCH("usermng", "/mypage/inquiryInfo"),
    USERMNG_API_INQUIRY_COMMON_CODE_SEARCH("usermng", "/mypage/inquiryCategory"),
    USERMNG_API_SET_CUSTOMER_SUGGEST("usermng", "/suggest/set"),
    STORAGE_API_ADD_CLAIM_IMAGE("upload", "/store/single/image"),

    ESCROW_API_PURCHASE_ORDER_RECEIPT("escrow", "/receipt/getPurchaseOrderReceipt"),
    ESCROW_API_PURCHASE_ORDER_CARD_RECEIPT("escrow", "/receipt/getPurchaseOrderCardReceipt"),
    ESCROW_API_PURCHASE_ORDER_CASH_RECEIPT("escrow", "/receipt/getPurchaseOrderCashReceipt"),

    CLAIM_API_ORDER_REFUND_RECEIPT("claim", "/receipt/getOrderRefundReceipt"),

    CLAIM_API_PURCHASE_CLAIM_RECEIPT("claim", "/receipt/getPurchaseClaimReceipt"),
    CLAIM_API_PURCHASE_CLAIM_CARD_RECEIPT("claim", "/receipt/getPurchaseClaimCardReceipt"),
    CLAIM_API_PURCHASE_CLAIM_CASH_RECEIPT("claim", "/receipt/getPurchaseClaimCashReceipt")


    ;

    @Getter
    private final String apiId;

    @Getter
    private final String uri;

}
