package kr.co.homeplus.mypage.order.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "티켓 주문 상세 DTO")
public class MpOrderTicketDetailGetDto {
    @ApiModelProperty(value = "주문번호", position = 1)
    private long purchaseOrderNo;
    @ApiModelProperty(value = "배송번호", position = 2)
    private long bundleNo;
    @ApiModelProperty(value = "아이템번호", position = 3)
    private String itemNo;
    @ApiModelProperty(value = "아이템명", position = 4)
    private String itemNm1;
    @ApiModelProperty(value = "아이템구매수량", position = 5)
    private Integer itemQty;
    @ApiModelProperty(value = "티켓아이템리스트", position = 6)
    private List<MpOrderTicketItemDto> ticketItemList;

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    @ApiModel(description = "티켓정보")
    public static class MpOrderTicketItemDto {
        @ApiModelProperty(value = "티켓번호")
        private Long orderTicketNo;
        @ApiModelProperty(value = "티켓유효기간")
        private String ticketValidEndDt;
        @ApiModelProperty(value = "티켓사용일시")
        private String ticketUseDt;
        @ApiModelProperty(value = "티켓상태(T0:미발행/T1:사용가능/T2:사용완료/T3:취소요청/T4:취소완료)")
        private String ticketStatus;
        @ApiModelProperty(value = "재발행횟수")
        private Integer ticketSendCnt;
        @ApiModelProperty(value = "티켓발송대상번호")
        private String shipMobileNo;
        @ApiModelProperty(value = "클레임가능여부")
        private String claimYn;
        @ApiModelProperty(value = "클레임번호")
        private Long claimNo;

        private Integer setTicketSendCnt(Integer ticketSendCnt){
            if(ticketSendCnt != 0) {
                return this.ticketSendCnt = (ticketSendCnt - 1);
            }
            return this.ticketSendCnt = ticketSendCnt;
        }
    }
}
