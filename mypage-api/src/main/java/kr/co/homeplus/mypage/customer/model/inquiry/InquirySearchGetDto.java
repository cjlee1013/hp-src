package kr.co.homeplus.mypage.customer.model.inquiry;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "1:1문의 조회 응답파라미터")
public class InquirySearchGetDto {

    @ApiModelProperty(value = "1:1문의관리번호")
    private long inqryNo;

    @ApiModelProperty(value = "문의상품구분(유형1)")
    private String inqryCategory;

    @ApiModelProperty(value = "문의구분(유형2)")
    private String inqryType;

    @ApiModelProperty(value = "문의상세구분(유형3)")
    private String inqryDetailType;

    @ApiModelProperty(value = "문의제목")
    private String inqryTitle;

    @ApiModelProperty(value = "문의내용")
    private String inqryCntnt;

    @ApiModelProperty(value = "상품/주문연결여부")
    private String inqryRelYn;

    @ApiModelProperty(value = "답변여부")
    private String inqryAnswrYn;

    @ApiModelProperty(value = "재문의여부")
    private String reInqryYn;

    @ApiModelProperty(value = "재문의답변여부")
    private String reInqryAnswrYn;

    @ApiModelProperty(value = "문의등록일")
    private String inqryRegDt;

    @ApiModelProperty(value = "문의상품구분(유형1)명")
    private String inqryCategoryNm;
    @ApiModelProperty(value = "문의구분(유형2)명")
    private String inqryTypeNm;
    @ApiModelProperty(value = "문의상세구분(유형3)명")
    private String inqryDetailTypeNm;

    @ApiModelProperty(value = "문의주문/상품정보리스트")
    private List<InquiryItemDto> inqryItemList;

    @ApiModelProperty(value = "문의이미지정보")
    private InquiryImageDto inqryImgInfo;

    @ApiModelProperty(value = "문의답변정보")
    private InquiryAnswerDto inqryAnswrInfo;

    @ApiModelProperty(value = "재문의정보")
    private ReplyInquiryDto reInqryInfo;

    @ApiModelProperty(value = "재문의답변정보")
    private ReplyInquiryAnswerDto reInqryAnswrInfo;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @ApiModel(description = "1:1문의 이미지 정보")
    public static class InquiryImageDto {
        @ApiModelProperty(value = "업로드이미지1")
        private String inqryImg1;
        @ApiModelProperty(value = "업로드이미지2")
        private String inqryImg2;
        @ApiModelProperty(value = "업로드이미지3")
        private String inqryImg3;
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    @ApiModel(description = "1:1문의 답변 정보")
    public static class InquiryAnswerDto {
        @ApiModelProperty(value = "문의답변내용")
        private String inqryAnswrCntnt;
        @ApiModelProperty(value = "문의답변일자")
        private String inqryAnswrDt;
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    @ApiModel(description = "1:1문의 재문의 정보")
    public static class ReplyInquiryDto {
        @ApiModelProperty(value = "재문의내용")
        private String reInqryCntnt;
        @ApiModelProperty(value = "재문의등록일자")
        private String reInqryDt;
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    @ApiModel(description = "1:1문의 재문의 답변 정보")
    public static class ReplyInquiryAnswerDto {
        @ApiModelProperty(value = "재문의답변내용")
        private String reInqryAnswrCntnt;
        @ApiModelProperty(value = "재문의답변등록일자")
        private String reInqryAnswrDt;
    }
}