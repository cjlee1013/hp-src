package kr.co.homeplus.mypage.claim.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import kr.co.homeplus.mypage.claim.model.upload.UploadFileDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 이미지 파일 업로드 시에 받아오는 데이터. file info Dto.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClaimAddImageGetDto {

    @ApiModelProperty(value = "이미지 정보", position = 1)
    private UploadFileDto fileStoreInfo;

    @ApiModelProperty(value = "에러명", position = 2, required = true, hidden = true)
    private String error;

    @ApiModelProperty(position = 3, required = true, hidden = true)
    private List<UploadFileDto> sliceFileStoreInfos;

    @ApiModelProperty(position = 4, required = true, hidden = true)
    private List<UploadFileDto> thumbnailFileStoreInfos;

    // errors 가 있는지 확인하여 true, false 반환.
    public boolean hasError() {
        return this.error != null;
    }
}
