package kr.co.homeplus.mypage.customer.model.consumer;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(description = "프론트 블랙컨슈머 이용제한 내역 조회 DTO")
public class UserRestrictDto {
    private UserBlackConsumerDto communityLimit;
    private UserBlackConsumerDto orderLimit;
}
