package kr.co.homeplus.mypage.order.service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kr.co.homeplus.mypage.enums.TicketIssueCode;
import kr.co.homeplus.mypage.order.model.MpOrderProductListGetDto;
import kr.co.homeplus.mypage.order.model.MpOrderTicketDetailGetDto;
import kr.co.homeplus.mypage.order.model.inquiry.OrderInquiryGetDto;
import kr.co.homeplus.mypage.order.model.inquiry.OrderInquirySetDto;
import kr.co.homeplus.mypage.order.model.multi.MpMultiOrderDetailInfoGetDto;
import kr.co.homeplus.mypage.order.model.multi.MpMultiOrderDetailInfoSetDto;
import kr.co.homeplus.mypage.order.model.multi.MpMultiOrderDetailListGetDto;
import kr.co.homeplus.mypage.order.model.order.OrderListGetDto;
import kr.co.homeplus.mypage.order.model.order.OrderListSetDto;
import kr.co.homeplus.mypage.order.model.ship.MultiOrderShipAddrEditDto;
import kr.co.homeplus.mypage.order.model.ship.MultiOrderShipBasicInfoEditDto;
import kr.co.homeplus.mypage.order.model.ticket.MpOrderTicketReSenderGetDto;
import kr.co.homeplus.mypage.order.model.ticket.MpOrderTicketReSenderSetDto;
import kr.co.homeplus.mypage.enums.ExternalInfo;
import kr.co.homeplus.mypage.enums.ResponseCode;
import kr.co.homeplus.mypage.external.service.ExternalService;
import kr.co.homeplus.mypage.factory.ResponseFactory;
import kr.co.homeplus.mypage.order.model.MpOrderDetailListGetDto;
import kr.co.homeplus.mypage.order.model.MpOrderDetailListSetDto;
import kr.co.homeplus.mypage.order.model.MpOrderListGetDto;
import kr.co.homeplus.mypage.order.model.MpOrderListSetDto;
import kr.co.homeplus.mypage.order.model.MpOrderMainDto;
import kr.co.homeplus.mypage.order.model.MpOrderMainDto.MyPageMainOrderCount;
import kr.co.homeplus.mypage.order.model.MpOrderMainDto.MyPageMainOrderList;
import kr.co.homeplus.mypage.order.model.MpOrderPickupStoreInfoDto;
import kr.co.homeplus.mypage.order.model.ship.OrderShipAddrEditDto;
import kr.co.homeplus.mypage.order.model.ship.OrderShipListGetDto;
import kr.co.homeplus.mypage.order.model.slot.FrontStoreSlot;
import kr.co.homeplus.mypage.order.model.slot.ShipSlotStockChangeDto;
import kr.co.homeplus.mypage.utils.ObjectUtils;
import kr.co.homeplus.mypage.utils.SetParameter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.crypto.RefitCryptoService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class OrderService {
    // 외부 통신 Service
    private final ExternalService externalService;

    private final RefitCryptoService refitCryptoService;

    /**
     * 마이페이지 > 주문/배송 > 주문조회(구버젼)
     * 
     * @param setDto 주문조회 데이터
     * @return 주문조회 내역
     * @throws Exception 오류시 오류처리
     */
    public ResponseObject<List<MpOrderListGetDto>> getOldOrderList(MpOrderListSetDto setDto) throws Exception {
        return ResponseFactory.getResponseObject(
            ResponseCode.SUCCESS,
            externalService.postListTransfer(
                ExternalInfo.CLAIM_API_OLD_ORDER_LIST,
                externalService.addParameter(ObjectUtils.getConvertMap(setDto)),
                MpOrderListGetDto.class
            )
        );
    }

    /**
     * 마이페이지 > 주문/배송 > 주문조회(리펙토링버젼)
     *
     * @param setDto 주문조회 데이터
     * @return 주문조회 내역
     * @throws Exception 오류시 오류처리
     */
    public ResponseObject<List<OrderListGetDto>> getOrderList(OrderListSetDto setDto) throws Exception {
        return ResponseFactory.getResponseObject(
            ResponseCode.SUCCESS,
            externalService.postListTransfer(
                ExternalInfo.CLAIM_API_ORDER_LIST,
                externalService.addParameter(ObjectUtils.getConvertMap(setDto)),
                OrderListGetDto.class
            )
        );
    }

    /**
     * 마이페이지 > 주문/배송 > 주문상세 조회
     *
     * @param setDto 주문상세 조회 데이터
     * @return 주문상세 조회 리스트
     * @throws Exception 오류시 오류처리
     */
    public ResponseObject<MpOrderDetailListGetDto> getOrderDetailList(MpOrderDetailListSetDto setDto) throws Exception {
        ResponseObject<MpOrderDetailListGetDto> responseData
            = ResponseFactory.getResponseObject(
                ResponseCode.SUCCESS,
                externalService.postTransfer( ExternalInfo.CLAIM_API_ORDER_DETAIL_LIST, externalService.addParameter(ObjectUtils.getConvertMap(setDto)),
                MpOrderDetailListGetDto.class)
        );
        if("SUCCESS,0000".contains(responseData.getReturnCode())){
            responseData.getData().getShipInfo().stream().filter(dto -> StringUtils.isNotEmpty(dto.getDriverTelNo()) && dto.getDriverTelNo().length() > 20).forEach(
                dto -> {
                    try {
                        dto.setDriverTelNo(refitCryptoService.decrypt(dto.getDriverTelNo()));
                    } catch (Exception e){
                        log.error("마이페이지 주문상세 조회 중 배송기사 전화번호 복호화에 실패 :: {}", e.getMessage());
                        dto.setDriverTelNo("");
                    }
                }
            );
        }

        return responseData;
    }

    /**
     * 마이페이지 > 메인 > 내 주문내역 조회(건수/주문리스트)
     *
     * 로그인시 발급받은 키를 통하여 UserNo를 Get하여,
     * 외부 통신을 한다.
     *
     * @return 내 주문내역 조회(건수/주문리스트) 데이터
     * @throws Exception 오류시 오류처리
     */
    public ResponseObject<MpOrderMainDto> getMainOrderInfo(String siteType) throws Exception {
        return ResponseFactory.getResponseObject(
            ResponseCode.SUCCESS,
            externalService.getTransfer(
                ExternalInfo.CLAIM_API_MAIN_ORDER_INFO,
                new ArrayList<SetParameter>(){{
                    add(SetParameter.create("userNo", externalService.getUserNo()));
                    add(SetParameter.create("siteType", siteType));
                }},
                MpOrderMainDto.class
            )
        );
    }

    /**
     * 마이페이지 > 메인 > 내 주문내역 조회(건수)
     *
     * 로그인시 발급받은 키를 통하여 UserNo를 Get하여,
     * 외부 통신을 한다.
     *
     * @return 내 주문내역 조회(건수) 리스트
     * @throws Exception 오류시 오류처리
     */
    public ResponseObject<List<MyPageMainOrderCount>> getMainOrderCount(String siteType) throws Exception {
        return ResponseFactory.getResponseObject(
            ResponseCode.SUCCESS,
            externalService.getListTransfer(
                ExternalInfo.CLAIM_API_MAIN_ORDER_CNT,
                new ArrayList<SetParameter>(){{
                    add(SetParameter.create("userNo", externalService.getUserNo()));
                    add(SetParameter.create("siteType", siteType));
                }},
                MyPageMainOrderCount.class
            )
        );
    }

    /**
     * 마이페이지 > 메인 > 내 주문내역 조회(주문리스트)
     *
     * 로그인시 발급받은 키를 통하여 UserNo를 Get하여,
     * 외부 통신을 한다.
     *
     * @return 내 주문내역 조회(주문리스트) 리스트
     * @throws Exception 오류시 오류처리
     */
    public ResponseObject<List<MyPageMainOrderList>> getMainOrderList(String siteType) throws Exception {
        return ResponseFactory.getResponseObject(
            ResponseCode.SUCCESS,
            externalService.getListTransfer(
                ExternalInfo.CLAIM_API_MAIN_ORDER_LIST,
                new ArrayList<SetParameter>(){{
                    add(SetParameter.create("userNo", externalService.getUserNo()));
                    add(SetParameter.create("siteType", siteType));
                }},
                MyPageMainOrderList.class
            )
        );
    }

    /**
     * 마이페이지 > 주문/배송 > 배송조회
     *
     * 운송 번호를 입력받아 Shipping-api 로 배송조회를 한다.
     *
     * @param shipNo 운송번호
     * @return 배송조회 데이터
     * @throws Exception 오류시 오류처리
     */
    public ResponseObject<OrderShipListGetDto> getShipOrderList(long shipNo) throws Exception {
        return ResponseFactory.getResponseObject(
            ResponseCode.SUCCESS,
            externalService.getTransfer(
                ExternalInfo.SHIPPING_API_SHIP_INFO_LIST,
                new ArrayList<SetParameter>(){{ add(SetParameter.create("shipNo", shipNo)); }},
                OrderShipListGetDto.class
            )
        );
    }

    /**
     * 마이페이지 > 주문/배송 > 다중배송조회
     *
     * 운송 번호를 입력받아 Shipping-api 로 배송조회를 한다.
     *
     * @param multiOrderItemNo 다중배송아이템번호
     * @return 배송조회 데이터
     * @throws Exception 오류시 오류처리
     */
    public ResponseObject<OrderShipListGetDto> getMultiShipOrderList(long multiOrderItemNo) throws Exception {
        return ResponseFactory.getResponseObject(
            ResponseCode.SUCCESS,
            externalService.getTransfer(
                ExternalInfo.SHIPPING_API_MULTI_SHIP_INFO_LIST,
                new ArrayList<SetParameter>(){{ add(SetParameter.create("multiOrderItemNo", multiOrderItemNo)); }},
                OrderShipListGetDto.class
            )
        );
    }

    /**
     * 마이페이지 > 주문/배송 > 수동배송완료 처리
     *
     * 운 번호를 입력받아 배송완료처리한다.
     *
     * @param bundleNo 운송번호
     * @return 배송처리 결과
     * @throws Exception 오류시 오류처리
     * **/
    public ResponseObject<String> manualShipComplete(long bundleNo) throws Exception {
        return ResponseFactory.getResponseObject(
            ResponseCode.SUCCESS,
            externalService.postTransfer(
                ExternalInfo.SHIPPING_MANUAL_COMPLETE,
                new LinkedHashMap<String, Object>(){{
                    put("bundleNo", bundleNo);
                    put("chgId", "MYPAGE");
                }},
                String.class
            )
        );
    }

    /**
     * 마이페이지 > 주문/배송 > 다중배송 수동배송완료 처리
     *
     * 운 번호를 입력받아 배송완료처리한다.
     *
     * @param multiBundleNo 운송번호
     * @return 배송처리 결과
     * @throws Exception 오류시 오류처리
     * **/
    public ResponseObject<String> manualMultiShipComplete(long multiBundleNo) throws Exception {
        return ResponseFactory.getResponseObject(
            ResponseCode.SUCCESS,
            externalService.getTransfer(
                ExternalInfo.SHIPPING_MULTI_MANUAL_COMPLETE,
                new ArrayList<SetParameter>(){{ add(SetParameter.create("multiBundleNo", multiBundleNo)); }},
                String.class
            )
        );
    }

    /**
     * 배송지 변경
     *
     * @param addrEditDto 배송지변경 데이터
     * @return 배송지 변경 처리 결과
     * @throws Exception 오류시 오류 처리.
     */
    public ResponseObject<String> modifyOrderShipAddrChange(OrderShipAddrEditDto addrEditDto) throws Exception {
        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS,
            externalService.postTransfer(
                ExternalInfo.CLAIM_API_SHIP_ADDR_CHANGE,
                addrEditDto,
                String.class
            )
        );
    }

    /**
     * 다중배송주문 - 배송지 변경
     *
     * @param multiOrderShipAddrEditDto 배송지변경 데이터
     * @return 배송지 변경 처리 결과
     * @throws Exception 오류시 오류 처리.
     */
    public ResponseObject<String> modifyMultiOrderShipAddrChange(MultiOrderShipAddrEditDto multiOrderShipAddrEditDto) throws Exception {
        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS,
            externalService.postTransfer(
                ExternalInfo.CLAIM_API_MULTI_SHIP_ADDR_CHANGE,
                multiOrderShipAddrEditDto,
                String.class
            )
        );
    }

    /**
     * 다중배송주문 - 배송기본정보 변경
     *
     * @param multiOrderShipBasicInfoEditDto 배송지변경 데이터
     * @return 배송지 변경 처리 결과
     * @throws Exception 오류시 오류 처리.
     */
    public ResponseObject<String> modifyMultiOrderShipBasicChange(MultiOrderShipBasicInfoEditDto multiOrderShipBasicInfoEditDto) throws Exception {
        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS,
            externalService.postTransfer(
                ExternalInfo.CLAIM_API_MULTI_SHIP_BAISC_CHANGE,
                multiOrderShipBasicInfoEditDto,
                String.class
            )
        );
    }

    /**
     * 배송슬롯변경
     * @param changeDto
     * @return
     * @throws Exception
     */
    public ResponseObject<String> setShipSlotChangeModify(ShipSlotStockChangeDto changeDto) throws Exception {
        changeDto.setHistoryRegId(String.valueOf(externalService.getUserInfo().getUserNo()));
        changeDto.setHistoryRegChannel("MYPAGE");
        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS,
            externalService.postTransfer(
                ExternalInfo.ESCROW_API_SLOT_CHANGE_MODIFY,
                changeDto,
                String.class
            )
        );
    }

    /**
     * 배송슬롯조회
     * @param purchaseOrderNo
     * @param bundleNo
     * @return
     * @throws Exception
     */
    public ResponseObject<FrontStoreSlot> getShipSlotInfo(long purchaseOrderNo, long bundleNo) throws Exception {
        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS,
            externalService.getTransfer(
                ExternalInfo.ESCROW_API_SLOT_CHANGE_INFO,
                new ArrayList<SetParameter>(){{
                    add(SetParameter.create("purchaseOrderNo", purchaseOrderNo));
                    add(SetParameter.create("bundleNo", bundleNo));
                }},
                FrontStoreSlot.class
            )
        );
    }

    /**
     * 주문-픽업상점정보
     * @param purchaseOrderNo
     * @return
     * @throws Exception
     */
    public ResponseObject<MpOrderPickupStoreInfoDto> getOrderPickupStoreInfo(long purchaseOrderNo) throws Exception {
        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS,
            externalService.getTransfer(
                ExternalInfo.CLAIM_API_ORDER_PICK_STORE_INFO,
                purchaseOrderNo,
                MpOrderPickupStoreInfoDto.class
            )
        );
    }

    /**
     * 마이페이지 > 주문/배송 > 주문상세 조회
     *
     * @param setDto 주문상세 조회 데이터
     * @return 주문상세 조회 리스트
     * @throws Exception 오류시 오류처리
     */
    public ResponseObject<MpOrderTicketDetailGetDto> getOrderTicketDetailInfo(MpOrderDetailListSetDto setDto) throws Exception {
        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS,
            externalService.postTransfer(
                ExternalInfo.CLAIM_API_ORDER_TICKET_DETAIL_LIST,
                externalService.addParameter(ObjectUtils.getConvertMap(setDto)),
                MpOrderTicketDetailGetDto.class
            )
        );
    }

    public ResponseObject<MpOrderTicketReSenderGetDto> orderTicketReSend(MpOrderTicketReSenderSetDto setDto) throws Exception {
        setDto.setUserNo(externalService.getUserNo());
        Object result = externalService.postTransfer( ExternalInfo.ESCROW_API_TICKET_RE_SEND, externalService.addParameter(ObjectUtils.getConvertMap(setDto)), Object.class );
        int resultCnt = ObjectUtils.toInt(result);
        if(resultCnt > 1){
            resultCnt -= 1;
        }

        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS, MpOrderTicketReSenderGetDto.builder().orderTicketNo(setDto.getOrderTicketNo()).sendCnt(resultCnt).build());
    }

    public ResponseObject<Map<String, String>> getTicketStatusCode() {
        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS, TicketIssueCode.getIssueMap());
    }

    public ResponseObject<List<OrderInquiryGetDto>> getInquiryOrderItemList(OrderInquirySetDto setDto) throws Exception {
        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS,
            externalService.postListTransfer(
                ExternalInfo.CLAIM_API_INQUIRY_ORDER_INFO,
                externalService.addParameter(ObjectUtils.getConvertMap(setDto)),
                OrderInquiryGetDto.class
            )
        );
    }

    /**
     * 마이페이지 > 주문/배송 > 다중배송 배송지리스트 조회
     *
     * @param setDto 주문상세 조회 데이터
     * @return 주문상세 조회 리스트
     * @throws Exception 오류시 오류처리
     */
    public ResponseObject<MpMultiOrderDetailListGetDto> getMultiOrderDetailList(MpOrderDetailListSetDto setDto) throws Exception {
        return ResponseFactory.getResponseObject(
            ResponseCode.SUCCESS,
            externalService.postTransfer( ExternalInfo.CLAIM_API_MULTI_ORDER_DETAIL_LIST, externalService.addParameter(ObjectUtils.getConvertMap(setDto)), MpMultiOrderDetailListGetDto.class)
        );
    }

    /**
     * 마이페이지 > 주문/배송 > 다중배송 개별주문 조회
     *
     * @param setDto 주문상세 조회 데이터
     * @return 주문상세 조회 리스트
     * @throws Exception 오류시 오류처리
     */
    public ResponseObject<MpMultiOrderDetailInfoGetDto> getMultiOrderDetailInfo(MpMultiOrderDetailInfoSetDto setDto) throws Exception {
        return ResponseFactory.getResponseObject(
            ResponseCode.SUCCESS,
            externalService.postTransfer( ExternalInfo.CLAIM_API_MULTI_ORDER_DETAIL_INFO, externalService.addParameter(ObjectUtils.getConvertMap(setDto)), MpMultiOrderDetailInfoGetDto.class)
        );
    }

    /**
     * 마이페이지 > 주문/배송 > 다중배송 배송지리스트 조회
     *
     * @param setDto 주문상세 조회 데이터
     * @return 주문상세 조회 리스트
     * @throws Exception 오류시 오류처리
     */
    public ResponseObject<List<MpOrderProductListGetDto>> getMultiOrderItemList(MpOrderDetailListSetDto setDto) throws Exception {
        return ResponseFactory.getResponseObject(
            ResponseCode.SUCCESS,
            externalService.postListTransfer( ExternalInfo.CLAIM_API_MULTI_ORDER_GROUP_ITEM_LIST, externalService.addParameter(ObjectUtils.getConvertMap(setDto)), MpOrderProductListGetDto.class)
        );
    }

    /**
     * UserNo 파라미터 추가용.
     *
     * @param parameter 입력받은 파라미터
     * @return userNo를 추가한 Map
     * @throws Exception 오류처리.
     */
    private LinkedHashMap<String, Object> addParameter(LinkedHashMap<String, Object> parameter) throws Exception {
        if(parameter == null){
            parameter = new LinkedHashMap<>();
        }
        parameter.put("userNo", getUserNo());
        return parameter;
    }

    /**
     * userNo
     * @return userNo
     * @throws Exception
     */
    private long getUserNo() throws Exception {
        return externalService.getUserInfo().getUserNo();
    }

}
