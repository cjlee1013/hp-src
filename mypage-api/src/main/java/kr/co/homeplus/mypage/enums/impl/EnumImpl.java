package kr.co.homeplus.mypage.enums.impl;

public interface EnumImpl {
    String getResponseCode();
    String getResponseMessage();
}
