package kr.co.homeplus.mypage.order.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "아이템별 사은품정보")
public class MpOrderGiftItemListDto {

    @ApiModelProperty(value = "주문번호", position = 1)
    private Long purchaseOrderNo;
    @ApiModelProperty(value = "점포ID", position = 2)
    private Integer storeId;
    @ApiModelProperty(value = "사은품번호", position = 3)
    private String giftNo;
    @ApiModelProperty(value = "사은품메시지(영수증 노출 문구)", position = 4)
    private String giftMsg;
    @ApiModelProperty(value = "아이템번호", position = 5)
    private String itemNo;
    @ApiModelProperty(value = "아이템적용타입(13:개별,14:금액)", position = 6)
    private String giftTargetType;
    @ApiModelProperty(value = "아이템적용기준금액", position = 7)
    private String stdVal;

}
