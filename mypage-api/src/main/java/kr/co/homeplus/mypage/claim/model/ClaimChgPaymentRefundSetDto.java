package kr.co.homeplus.mypage.claim.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "환불수단 변경 DTO")
public class ClaimChgPaymentRefundSetDto {

    @ApiModelProperty(value = "고객번호", position = 1, hidden = true)
    private long userNo;

    @ApiModelProperty(value= "구매주문번호", position = 2)
    @NotNull(message = "구매주문번호")
    private long purchaseOrderNo;

    @ApiModelProperty(value= "클레임번호", position = 3)
    @NotNull(message = "클레임번호")
    private long claimNo;

    @ApiModelProperty(value= "클레임번호", position = 4)
    @NotNull(message = "클레임번호")
    private long claimBundleNo;

    @ApiModelProperty(value= "예금주명", position = 5)
    @NotEmpty (message = "예금주명")
    private String bankAccountName;

    @ApiModelProperty(value= "환불계좌번호", position = 6)
    @NotEmpty (message = "환불계좌번호")
    private String bankAccountNo;

    @ApiModelProperty(value= "은행코드", position = 7)
    @NotEmpty(message = "은행코드")
    private String bankCode;
}
