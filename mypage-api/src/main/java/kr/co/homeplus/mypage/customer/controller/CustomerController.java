package kr.co.homeplus.mypage.customer.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.mypage.customer.model.QnaListGetDto;
import kr.co.homeplus.mypage.customer.model.QnaListSetDto;
import kr.co.homeplus.mypage.customer.model.QnaRegisterSetDto;
import kr.co.homeplus.mypage.customer.model.QnaReplyListGetDto;
import kr.co.homeplus.mypage.customer.model.inquiry.InquiryCategorySearchGetDto;
import kr.co.homeplus.mypage.customer.model.inquiry.InquiryCategorySearchSetDto;
import kr.co.homeplus.mypage.customer.model.inquiry.InquiryModifyDto;
import kr.co.homeplus.mypage.customer.model.inquiry.InquiryReRegDto;
import kr.co.homeplus.mypage.customer.model.inquiry.InquiryRegDto;
import kr.co.homeplus.mypage.customer.model.inquiry.InquirySearchGetDto;
import kr.co.homeplus.mypage.customer.model.inquiry.InquirySearchSetDto;
import kr.co.homeplus.mypage.customer.model.personal.PersonalGetDto;
import kr.co.homeplus.mypage.customer.model.personal.PersonalSetDto;
import kr.co.homeplus.mypage.customer.model.review.ReviewSearchGetDto;
import kr.co.homeplus.mypage.customer.model.review.ReviewSearchSetDto;
import kr.co.homeplus.mypage.customer.service.CustomerService;
import kr.co.homeplus.mypage.enums.SiteType;
import kr.co.homeplus.mypage.response.ResponseResult;
import kr.co.homeplus.plus.api.support.client.header.UserDeviceType;
import kr.co.homeplus.plus.api.support.client.header.WebHeaderKey;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/customer")
@Api(tags = "Mypage 쇼핑활동", value = "Mypage 쇼핑활동")
public class CustomerController {

    private final CustomerService customerService;

    @PostMapping("/qnaListSearch")
    @ApiOperation(value = "상품QnA", response = QnaListGetDto.class)
    public ResponseObject<QnaListGetDto> qnaListSearch(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestBody QnaListSetDto qnaListGetDto
    ) throws Exception {
        return customerService.qnaListSearch(qnaListGetDto);
    }

    @GetMapping("/qnaReplySearch")
    @ApiOperation(value = "QnA 답변 조회", response = QnaReplyListGetDto.class)
    public ResponseObject<List<QnaReplyListGetDto>> qnaReplySearch(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestParam @ApiParam(name = "qnaNo", value = "Q&A번호", required = true) String qnaNo
        ) throws Exception {
        return customerService.qnaReplySearch(qnaNo);
    }

    @PostMapping("/qnaRegister")
    @ApiOperation(value = "QnA 등록", response = QnaListGetDto.class)
    public ResponseObject<ResponseResult> qnaRequestSet(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestBody QnaRegisterSetDto qnaRegisterSetDto
        ) throws Exception {
        return customerService.qnaRegister(qnaRegisterSetDto);
    }

    @PostMapping("/setInquiryReg")
    @ApiOperation(value = "1:1문의 등록", response = String.class)
    public ResponseObject<String> setInquiryReg(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestBody @Valid InquiryRegDto inquiryRegDto
    ) throws Exception {
        inquiryRegDto.setSiteType(siteType.name());
        return customerService.addInquiryReg(inquiryRegDto);
    }

    @PostMapping("/setReplyInquiryReg")
    @ApiOperation(value = "1:1문의 재문의 등록", response = String.class)
    public ResponseObject<String> setReplyInquiryReg(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestBody @Valid InquiryReRegDto inquiryReRegDto
    ) throws Exception {
        return customerService.addReplyInquiryReg(inquiryReRegDto);
    }

    @PostMapping("/cancelInquiryReg")
    @ApiOperation(value = "1:1문의 취소", response = String.class)
    public ResponseObject<String> setCancelInquiryReg(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestBody @Valid InquiryModifyDto inquiryModifyDto
    ) throws Exception {
        return customerService.setCancelInquiryReg(inquiryModifyDto);
    }

    @PostMapping("/searchInquiryInfo")
    @ApiOperation(value = "1:1문의 내역조회", response = InquirySearchGetDto.class)
    public ResponseObject<List<InquirySearchGetDto>> getInquiryInfo(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestBody @Valid InquirySearchSetDto inquirySearchSetDto
    ) throws Exception {
        inquirySearchSetDto.setSiteType(siteType.name());
        return customerService.getInquiryInfo(inquirySearchSetDto);
    }

    @PostMapping("/searchInquiryCommonCode")
    @ApiOperation(value = "1:1문의 공통코드조회", response = InquirySearchGetDto.class)
    public ResponseObject<List<InquiryCategorySearchGetDto>> getInquiryCommonCodeInfo(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestBody @Valid InquiryCategorySearchSetDto inquiryCategorySearchSetDto
    ) throws Exception {
        inquiryCategorySearchSetDto.setSiteType(siteType.name());
        return customerService.getInquiryCommonCodeInfo(inquiryCategorySearchSetDto);
    }

    @PostMapping("/searchReviewPossible")
    @ApiOperation(value = "리뷰가능조회", response = ReviewSearchGetDto.class)
    public ResponseObject<List<ReviewSearchGetDto>> getReviewPossible(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestBody @Valid ReviewSearchSetDto reviewSearchSetDto
    ) throws Exception {
        reviewSearchSetDto.setSiteType(siteType.name());
        return customerService.getReviewPossible(reviewSearchSetDto);
    }

    @PostMapping("/searchPersonalInfo")
    @ApiOperation(value = "개인정보이용내역", response = PersonalGetDto.class)
    public ResponseObject<List<PersonalGetDto>> getPersonalInfo(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestBody @Valid PersonalSetDto personalSetDto
    ) throws Exception {
        personalSetDto.setSiteType(siteType.name());
        return customerService.getPersonalInfo(personalSetDto);
    }
}
