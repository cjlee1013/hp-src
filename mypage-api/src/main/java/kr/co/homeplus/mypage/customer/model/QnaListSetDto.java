package kr.co.homeplus.mypage.customer.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "마이페이지 > 홈 - QnA리스트")
public class QnaListSetDto {

    @ApiModelProperty(value = "상품번호", position = 1)
    private String itemNo;

    @ApiModelProperty(value = "Qna 처리상태(답변대기-READY, 답변완료-COMP)", position = 2)
    @Pattern(regexp = "(READY|COMP)", message="Qna 처리상태(답변대기-READY, 답변완료-COMP)")
    private String qnaStatus;

    @ApiModelProperty(value = "비밀글 여부(Y,N)", position = 3)
    @Pattern(regexp = "[Y|N]{1}", message="비밀글 여부(Y,N)")
    private String secretYn;

    @ApiModelProperty(value = "나의 문의(Y,N)", position = 4)
    @Pattern(regexp = "[Y|N]{1}", message="나의 문의(Y,N)")
    private String myQnaYn;

    @ApiModelProperty(value = "Qna 타입 (상품-ITEM, 배송-SHIP, 교환/반품-CLAIM, 기타-ETC)", position = 5)
    @Pattern(regexp = "(ITEM|SHIP|CLAIM|ETC)", message="Qna 타입 (상품-ITEM, 배송-SHIP, 교환/반품-CLAIM, 기타-ETC)")
    private String qnaType;

    @ApiModelProperty(value = "페이지 No ( 최소값 1 )", required = true, position = 6)
    @Min(value = 1, message = "페이지 No 최소값")
    private Integer page;

    @ApiModelProperty(value = "페이지 리스트 수 ( 최소값 10 )", required = true, position = 7)
    @Min(value = 10, message = "페이지 리스트 수 최소값")
    private Integer perPage;
}
