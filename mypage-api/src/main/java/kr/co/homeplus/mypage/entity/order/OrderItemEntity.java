package kr.co.homeplus.mypage.entity.order;

public class OrderItemEntity {

    // 주문상품번호
    public String orderItemNo;

    // 결제번호
    public String paymentNo;

    // 구매주문번호
    public String purchaseOrderNo;

    // 번들번호
    public String bundleNo;

    // 원천점포ID(매출용실제점포)
    public String originStoreId;

    // 점포ID (주문서에서 넘어온 점포ID)
    public String storeId;

    // FC점포ID
    public String fcStoreId;

    // FC상품여부
    public String fcItemYn;

    // 점포택배여부(Y:점포택배,N:점포상품)
    public String storeDlvYn;

    // 상품유형(B:반품,N:새상품,R:리퍼,U:중고)
    public String itemType;

    // 점포유형(Hyper, Club, Exp, DS)
    public String storeType;

    // 점포종류(NOR:일반점, DLV:택배점)
    public String storeKind;

    // 몰유형(DS:업체상품,TD:매장상품)
    public String mallType;

    // 상품번호
    public String itemNo;

    // 상품명1
    public String itemNm1;

    // 구매최소수량
    public String purchaseMinQty;

    // 원금액(할인 미적용금액)
    public String orgPrice;

    // 상품금액(상품 판매가)
    public String itemPrice;

    // 상품수량 (SUM(상품옵션수량))
    public String itemQty;

    // 주문금액 ((상품가격+옵션가격N)*옵션수량N) + (추가상품가격*추가상품수량)
    public String orderPrice;

    // 할인금액(주문상품번호별 할인받은 전체금액, 상품기준 할인금액)
    public String discountAmt;

    // 임직원할인율
    public String empDiscountRate;

    // 임직원할인금액
    public String empDiscountAmt;

    // 파트너ID
    public String partnerId;

    // 세금여부(Y:과세,N:면세)
    public String taxYn;

    // 업체상품코드
    public String sellerItemCd;

    // MD임직원번호
    public String mdEmpNo;

    // 수수료유형(FA:정액,FR:정률)
    public String commissionType;

    // 수수료율
    public String commissionRate;

    // 수수료가격
    public String commissionPrice;

    // 정산금액(개당)
    public String costAmt;

    // 대체여부(상품별 대체가능여부 Y:대체가능,N:대체불가능)
    public String substitutionYn;

    // 옵션선택사용여부
    public String optSelUseYn;

    // 옵션텍스트사용여부
    public String optTxtUseYn;

    // 추가구성사용여부
    public String addOptUseYn;

    // 대카테고리코드
    public String lcateCd;

    // 중카테고리코드
    public String mcateCd;

    // 소카테고리코드
    public String scateCd;

    // 세카테고리코드
    public String dcateCd;

    // 레거시카테고리3
    public String deptNo;

    // 레거시카테고리4
    public String classNo;

    // 레거시카테고리5
    public String subClassNo;

    // ISBN코드(ISBN 10자리 또는 13자리코드)
    public String isbnCd;

    // 등록일시
    public String regDt;

    // 등록자ID
    public String regId;

    // 수정일시
    public String chgDt;

    // 수정자ID
    public String chgId;

}
