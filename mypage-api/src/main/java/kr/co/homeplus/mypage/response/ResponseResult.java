package kr.co.homeplus.mypage.response;


import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Data;
import lombok.RequiredArgsConstructor;


@Data
@RequiredArgsConstructor
@ApiModel("Response Entity > ResponseResult Entity")
public class ResponseResult<T> {

    @ApiModelProperty(value = "결과코드", required = true, position = 1)
    private String returnCode = "0";

    @ApiModelProperty(value = "결과메세지", required = true, position = 5)
    private String returnMsg = "저장 되었습니다.";

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(value = "결과Key", position = 10)
    private String returnKey;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(value = "결과개수", position = 15)
    private int returnCnt;

    @ApiModelProperty(value = "결과리스트", position = 20)
    private List<T> resultList;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(value = "ResponseResult Entity", position = 50)
    private ResponseResult<T> subResponseResult;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(value = "ResponseResult Entity List", position = 70)
    private List<ResponseResult<T>> subResponseResultList;

    public <T extends Number> ResponseResult(int returnCnt, T returnKey, String returnMsg) {
        this.returnCnt = returnCnt;
        this.returnKey = String.valueOf(returnKey);
        this.returnMsg = returnMsg;
    }

    public <T extends String> ResponseResult(int returnCnt, T returnKey, String returnMsg) {
        this.returnCnt = returnCnt;
        this.returnKey = returnKey;
        this.returnMsg = returnMsg;
    }

    public static <T extends String> ResponseResult<T> getResponseResult(int returnCnt, T returnKey, String returnMsg) {
        return new ResponseResult<T>(returnCnt, returnKey, returnMsg);
    }

    public static <T extends Number> ResponseResult<T> getResponseResult(int returnCnt, T returnKey, String returnMsg) {
        return new ResponseResult<T>(returnCnt, returnKey, returnMsg);
    }

    public static <T> ResponseResult<T> getEmpty() {
        return new ResponseResult<T>(0, 0, "");
    }
}
