package kr.co.homeplus.mypage.user.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "마이페이지 > 회원정보관리 > 환불계좌등록/환불계좌수정")
public class RefundAccountSetDto {

    @ApiModelProperty(value = "일련번호", position = 1)
    private Integer seq;

    @ApiModelProperty(value = "은행명", position = 2)
    @NotEmpty
    private String bankNm;

    @ApiModelProperty(value = "은행코드 (04:국민,88:신한,81:하나,20:우리,03:기업)", position = 3)
    @NotEmpty
    private String bankCd;

    @ApiModelProperty(value = "계좌번호", position = 4)
    @NotEmpty
    private String bankAccount;

    @ApiModelProperty(value = "계좌주", position = 5)
    @NotEmpty
    private String bankOwner;
}
