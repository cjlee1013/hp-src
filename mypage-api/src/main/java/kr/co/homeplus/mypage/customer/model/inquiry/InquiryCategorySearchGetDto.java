package kr.co.homeplus.mypage.customer.model.inquiry;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "문의유형 코드 응답")
public class InquiryCategorySearchGetDto {
    @ApiModelProperty(value = "공통코드")
    private String mcCd;
    @ApiModelProperty(value = "코드명")
    private String mcNm;
}
