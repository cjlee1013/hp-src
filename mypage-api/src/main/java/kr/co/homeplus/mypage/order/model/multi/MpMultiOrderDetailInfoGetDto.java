package kr.co.homeplus.mypage.order.model.multi;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "마이페이지 > 주문/배송 - 다중배송 주문 아이템 상세")
public class MpMultiOrderDetailInfoGetDto {
    @ApiModelProperty(value = "배송정보", position = 1)
    private MpMultiOrderShipInfoDto shipInfo;
    @ApiModelProperty(value = "상품상세리스트", position = 2)
    private List<MpMultiOrderProductListGetDto> orderDetailInfo;
}
