package kr.co.homeplus.mypage.core.db.construct;

import kr.co.homeplus.mypage.core.db.annotation.PrimaryConnection;
import kr.co.homeplus.mypage.core.db.annotation.SecondaryConnection;
import kr.co.homeplus.mypage.core.db.properties.DataSourceProperties;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

class MyBatisConfig {

    public static final String BASE_PACKAGE = "kr.co.homeplus";
}

@Configuration
@EnableTransactionManagement
@MapperScan(basePackages = MyBatisConfig.BASE_PACKAGE, annotationClass = PrimaryConnection.class, sqlSessionFactoryRef = "primarySqlSessionFactory")
class PrimaryMyBatisConfig {
    @Bean(name = "primarySqlSessionFactory")
    public SqlSessionFactory primarySqlSessionFactory(@Qualifier("primaryDataSource") BasicDataSource primaryDataSource) throws Exception {
        return SqlSessionFactoryBuilder.build(primaryDataSource);
    }

    @Primary
    @Bean(name = "primaryDataSource", destroyMethod = "")
    public BasicDataSource dataSource(@Qualifier("primaryDatabaseProperties") DataSourceProperties dataSourceProperties) {
        //dataSourceProperties.setDefaultReadOnly(false);
        return DataSourceBuilder.build(dataSourceProperties);
    }

    @Bean
    public PlatformTransactionManager transactionManager(@Qualifier("primaryDataSource") BasicDataSource primaryDataSource) {
        DataSourceTransactionManager transactionManager = new DataSourceTransactionManager(primaryDataSource);
        transactionManager.setGlobalRollbackOnParticipationFailure(false);
        return transactionManager;
    }

    @Bean
    @ConfigurationProperties("spring.datasource.primary")
    public DataSourceProperties primaryDatabaseProperties() {
        return new DataSourceProperties();
    }
}

@Configuration
@MapperScan(basePackages = MyBatisConfig.BASE_PACKAGE, annotationClass = SecondaryConnection.class, sqlSessionFactoryRef = "secondarySqlSessionFactory")
class SecondaryMyBatisConfig {
    @Bean(name = "secondarySqlSessionFactory")
    public SqlSessionFactory secondarySqlSessionFactory(@Qualifier("secondaryDataSource") BasicDataSource secondaryDataSource) throws Exception {
        return SqlSessionFactoryBuilder.build(secondaryDataSource);
    }

    @Bean(name = "secondaryDataSource", destroyMethod = "")
    public BasicDataSource dataSource(@Qualifier("slaveDatabaseProperties") DataSourceProperties dataSourceProperties) {
        return DataSourceBuilder.build(dataSourceProperties);
    }

    @Bean
    @ConfigurationProperties("spring.datasource.secondary")
    public DataSourceProperties slaveDatabaseProperties() {
        return new DataSourceProperties();
    }
}
