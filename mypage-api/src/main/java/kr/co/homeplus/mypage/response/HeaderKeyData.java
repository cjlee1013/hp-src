package kr.co.homeplus.mypage.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
@ApiModel(description = "Request Header 에서 넘어온 헤더키값용 Vo.")
public class HeaderKeyData {

    @ApiModelProperty(value = "유저번호", position = 1)
    private final long userNo;

    @ApiModelProperty(value = "사이트타입", position = 2)
    private final String siteType;
}
