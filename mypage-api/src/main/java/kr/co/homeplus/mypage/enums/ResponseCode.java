package kr.co.homeplus.mypage.enums;

import kr.co.homeplus.mypage.enums.impl.EnumImpl;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ResponseCode implements EnumImpl {

    // 주문
    // 2000
    // 혜택
    // 3000
    // 상품
    // 4000
    // 클레임
    // 5000 ~ 8000
    // 찜리스트
    // 8000
    // 오류
    // 9000
    SUCCESS("0000", "성공하였습니다."),
    ORDER_SEARCH_ERR_01("2001", "조회가 실패 하였습니다."),
    ORDER_SEARCH_DETAIL_ERR_01("2101", "상세조회에 실패 하였습니다."),
    ORDER_SHIP_SEARCH_SUCCESS("0000", "배송조회에 성공하였습니다."),
    ORDER_SHIP_SEARCH_ERR_01("2201", " 배송조회에 실패 하였습니다."),
    ORDER_SHIP_MANUAL_SUCCESS("0000", "배송수동완료에 성공하였습니다."),
    ORDER_SHIP_MANUAL_ERR_01("2301", "배송메뉴얼조회에 실패 하였습니다."),
    ORDER_SHIP_SLOT_CHANGE_ERR_01("2401", "슬롯변경에 실패하였습니다."),


    NO_RCV_LIST_SUCCUESS("0000", "미수취 내역 조회에 성공하였습니다."),
    NO_RCV_LIST_ERR01("6101", "미수취 내역 조회에 실패하였습니다."),
    NO_RCV_REG_SUCCUESS("0000", "미수취 내역 등록에 성공하였습니다."),
    NO_RCV_REG_ERR01("6111", "미수취 내역 등록에 실패하였습니다."),
    NO_RCV_MODIFY_SUCCUESS("0000", "미수취 내역 수정에 성공하였습니다."),
    NO_RCV_MODIFY_ERR01("6121", "미수취 내역 수정에 실패하였습니다."),

    REVIEW_BLACK_CONSUMER_ERR("6401", "리뷰가능조회를 하실 수 없는 고객입니다."),

    BENEFIT_MILEAGE_SUCCESS("0000", "마일리지 조회에 성공하였습니다."),
    BENEFIT_MILEAGE_ERR_01("3101", "마일리지 잔액 조회에 실패하였습니다."),
    BENEFIT_MILEAGE_ERR_02("3102", "마일리지 적립/차감 내역 조회에 실패하였습니다."),
    BENEFIT_MILEAGE_ERR_03("3103", "마일리지 등록에 실패하였습니다."),
    BENEFIT_MHC_SUCCESS("0000", "마이 홈플러스 포인트 조회에 성공하였습니다."),
    BENEFIT_MHC_ERR_01("3201", "마이 홈플러스 포인트 잔액 조회에 실패하였습니다."),
    BENEFIT_MHC_CARD_SUCCESS("0000", "마이 홈플러스 카드정보 조회에 성공하였습니다."),
    BENEFIT_MHC_CARD_ERR_01("3211", "마이 홈플러스 카드정보 조회에 실패하였습니다."),
    BENEFIT_COUPON_COUNT_SUCCESS("0000", "사용가능한 쿠폰 건수 조회에 성공하였습니다."),
    BENEFIT_COUPON_COUNT_ERR01("3301", "사용가능한 쿠폰 건수 조회에 실패하였습니다."),
    BENEFIT_COUPON_LIST_SUCCESS("0000", "전체 쿠폰 조회에 성공하였습니다."),
    BENEFIT_COUPON_LIST_ERR01("3311", "전체 쿠폰 조회에 실패하였습니다."),
    BENEFIT_MILEAGE_COUPON_REG_SUCCESS("0000", "마일리지 쿠폰등록에 성공하였습니다."),
    BENEFIT_TOTAL_SEARCH_ERR01("3401", "마일리지 잔액조회에 실패하였습니다."),
    BENEFIT_TOTAL_SEARCH_ERR02("3402", "마이홈플러스 잔액조회에 실패하였습니다."),
    BENEFIT_TOTAL_SEARCH_ERR03("3403", "쿠폰 조회에 실패하였습니다."),

    BENEFIT_SUB_MAIN_SUCCESS("0000", "쿠폰/마이홈플러스포인트/마일리지 조회에 성공하였습니다."),

    BENEFIT_MILEAGE_COUPON_REG_ERR01("3501", "마일리지 등록번호가 잘못되었습니다."),
    BENEFIT_MILEAGE_COUPON_REG_ERR02("3502", "마일리지 등록번호 자릿수가 잘못되었습니다."),

    CLAIM_REGISTER_SUCCESS("0000","클레임 등록에 성공하였습니다."),
    CLAIM_REGISTER_ERR01("5101","클레임 등록에 실패하였습니다."),

    WISH_ITEM_LIST_SUCCESS("0000", "찜상품 조회에 성공하였습니다."),
    WISH_ITEM_LIST_ERR_01("8101", "찜상품 조회에 실패하였습니다."),
    WISH_SELLER_SHOP_LIST_SUCCESS("0000", "찜샐러샵 조회에 성공하였습니다."),
    WISH_SELLER_SHOP_LIST_ERR_01("8201", "찜샐러샵 조회에 실패하였습니다."),

    REFUND_ACCOUNT_SET_SUCCESS("0000", "환불계좌등록/수정에 성공하였습니다."),
    REFUND_ACCOUNT_SET_ERR_01("8301", "환불계좌등록/수정에 실패하였습니다."),
    REFUND_ACCOUNT_SET_ERR_02("8302", "환불계좌수정 요청 SEQ가 올바르지 않습니다."),


    INQUIRY_CHECK_EMOJI_ERROR("6301", "등록하고자 하는 내용에 Emoji가 포함되어 있습니다."),

    USER_ADDRESS_SET_SUCCESS("0000", "회원주소지 등록/수정에 성공하였습니다."),
    USER_ADDRESS_SET_ERR_01("0000", "회원주소지 등록/수정에 실패하였습니다."),
    USER_GRADE_INFO_SUCCESS("0000", "회원의 등급조회에 성공하였습니다."),

    CLAIM_IMAGE_UPLOAD_ERR01("4001", "이미지는 jpg,gif 확장자만 업로드가 가능합니다."),
    CLAIM_IMAGE_UPLOAD_ERR02("4002", "이미지업로드 중 오류가 발생되었습니다."),

    USER_INFO_ERR_01("9201", "회원정보 조회에 실패하였습니다.");

    private final String code;
    private final String description;

    @Override
    public String getResponseCode() {
        return this.code;
    }

    @Override
    public String getResponseMessage() {
        return this.description;
    }

}
