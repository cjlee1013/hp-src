package kr.co.homeplus.mypage.entity.order;

public class PaymentInfoEntity {

    // 결제정보순번
    public String paymentInfoSeq;

    // 결제번호
    public String paymentNo;

    // 결제유형(PG:PG결제,MILEAGE:마일리지,BenefitMhcCardInfoDto:BenefitMhcCardInfoDto,DGV:상품권,OCB:OKCashBag)
    public String paymentType;

    // 상위결제수단코드(CARD:카드,VBANK:무통장입금,PHONE:휴대폰결제,RBANK:실시간계좌이체,KAKAOPAY:카카오페이,NAVERPAY:네이버페이,SAMSUNGPAY:삼성페이)
    public String parentMethodCd;

    // 결제수단코드
    public String methodCd;

    // 결제상태(P1:결제요청,P2:입금대기중(무통장),P3:결제완료(입금확인),P4:결제실패,P8:결제철회(무통장,카드,계좌이체,휴대폰 주문취소요청),P9:만료(무통장))
    public String paymentStatus;

    // 결제금액
    public String amt;

    // PG종류(INICIS,KICC)
    public String pgKind;

    // PG 상점ID
    public String pgMid;

    // PG 인증Key값
    public String pgOid;

    // 카드번호
    public String cardNo;

    // 카드할부개월(신용카드 할부개월수 1:일시불,2:2개월,3:3개월…)
    public String cardInstallmentMonth;

    // 카드할부개월입력값(신용카드 할부개월수 화면값)
    public String cardInstallmentMonthIpt;

    // 카드이자여부(Y:유이자,N:무이자)
    public String cardInterestYn;

    // 카드이자여부입력값(Y:유이자,N:무이자) 화면 입력값
    public String cardInterestYnIpt;

    // 승인번호(카드,간편결제,상품권 등)
    public String approvalNo;

    // 가상계좌 발급번호(무통장 계좌번호)
    public String vbankNo;

    // 가상계좌 예금주명
    public String vbankDepositorNm;

    // 가상계좌 송금자명
    public String vbankRemitterNm;

    // 가상계좌 만료일자
    public String vbankExpireDt;

    // 모바일결제 이동통신사유형
    public String mpayMcomType;

    // 모바일결제 모바일번호
    public String mpayMobileNo;

    // 모바일결제 VAN코드
    public String mpayVanCd;

    // 결제요청일자(결제Data 생성일자)
    public String paymentReqDt;

    // 결제완료일자(승인완료일자, 무통장은 입금완료일자)
    public String paymentFshDt;

    // PG 승인Key값
    public String pgTid;

    // OCB고유거래id (OCB:mctTransactionId, 홈플러스고유거래번호)
    public String ocbUniqTradeId;

    // OCB트랜잭션토큰 (OCB:ocbUseTrAuthToken)
	  public String ocbTranToken;

	  // OCB트랜잭션id (OCB:ocbTransactionId)
	  public String ocbTranId;

    // 결과메시지(주문생성 사유 저장)
    public String resultMsg;

    // 승인결과코드(0000:정상,그외:비정상)
    public String resultCd;

    // 등록일시
    public String regDt;

    // 등록자ID
    public String regId;

    // 수정일시
    public String chgDt;

    // 수정자ID
    public String chgId;

}
