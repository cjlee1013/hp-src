package kr.co.homeplus.mypage.order.model.ship;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "주문리스트 내 배송 상태 응답 DTO")
public class OrderShipListGetDto {

    @ApiModelProperty(notes = "택배사명", position = 1)
    private String dlvNm;
    @ApiModelProperty(notes = "택배사코드", position = 2)
    private String dlvCd;
    @ApiModelProperty(notes = "송장번호", position = 3)
    private String invoiceNo;
    @ApiModelProperty(notes = "배송상태", position = 3)
    private String status;
    @ApiModelProperty(notes = "추적결", position = 3)
    private String trackingResult;
    @ApiModelProperty(notes = "배송추적리스트", position = 4)
    private List<OrderShipHistoryDto> historyList;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class OrderShipHistoryDto {
        @ApiModelProperty(notes = "처리일시")
        private String shipTranDt;
        @ApiModelProperty(notes = "배송상세")
        private String shipDetail;
        @ApiModelProperty(notes = "택배위치")
        private String shipWhere;
        @ApiModelProperty(notes = "배송기사명")
        private String shipMan;
        @ApiModelProperty(notes = "배송기사 연락처")
        private String shipManPhone;
        @ApiModelProperty(notes = "배송예정시간")
        private String shipEstmateDt;
        @ApiModelProperty(notes = "택배사")
        private String dlvNm;
        @ApiModelProperty(notes = "송장번호")
        private String invoiceNo;
        @ApiModelProperty(notes = "배송level(1:배송준비,2:집화완료,3:배송진행중,4:지점도착,5:배송출발,6:배송완료")
        private String shipLevel;
    }
}
