package kr.co.homeplus.mypage.claim.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Min;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "취소/반품/교환 내역 확인 리스트 dto")
public class ClaimSearchListSetDto {

    @ApiModelProperty(value = "조회시작일", position = 1)
    private String schStartDt;

    @ApiModelProperty(value = "조회종료일", position = 2)
    private String schEndDt;

    @ApiModelProperty(value = "클레임 타입", position = 3)
    private String claimType;

    @ApiModelProperty(value="고객번호", hidden = true, position = 4)
    private long userNo;

    @ApiModelProperty(value = "조회페이지", position = 5)
    @Min(value = 1, message = "페이지 No 최소값")
    private Integer page;

    @ApiModelProperty(value = "페이지리스트수", position = 6)
    @Min(value = 10, message = "페이지 리스트 수 최소값")
    private Integer perPage;

    @JsonIgnore
    @ApiModelProperty(value = "사이트타입", position = 7)
    private String siteType;

}
