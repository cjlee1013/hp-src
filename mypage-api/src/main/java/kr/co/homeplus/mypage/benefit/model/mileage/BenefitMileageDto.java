package kr.co.homeplus.mypage.benefit.model.mileage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "마일리지 > 마일리지 사용/소멸 금액 조회")
public class BenefitMileageDto {

    @ApiModelProperty(value = "고객번호", position = 1)
    private String userNo;

    @ApiModelProperty(value = "마일리지금액", position = 2)
    private long mileageAmt;

    @ApiModelProperty(value = "소멸예정금액(2주이내)", position = 3)
    private long expiredMileageAmt;
}
