package kr.co.homeplus.mypage.customer.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "마이페이지 > 홈 - QnA리스트")
public class QnaListGetDto {

    @ApiModelProperty(value = "구매 여부", position = 1)
    private String purchaseYn;

    @ApiModelProperty(value = "구매주문번호", position = 2)
    private long purchaseOrderNo;

    @ApiModelProperty(value = "상품평리스트", position = 3)
    List<CsQnaListSelectDto> csQnaListSelectDtoList;
}
