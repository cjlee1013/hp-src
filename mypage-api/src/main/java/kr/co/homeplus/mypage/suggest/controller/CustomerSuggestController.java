package kr.co.homeplus.mypage.suggest.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javax.validation.Valid;
import kr.co.homeplus.mypage.suggest.model.CustomerSuggestRequest;
import kr.co.homeplus.mypage.suggest.model.CustomerSuggestResponse;
import kr.co.homeplus.mypage.suggest.service.CustomerSuggestService;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Api(tags = "Mypage 고객 개선 제안")
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
})
@RestController
@RequestMapping(value = "/suggest", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class CustomerSuggestController {

    private final CustomerSuggestService customerSuggestService;

    @ApiImplicitParam(name = "deviceModel", dataType = "String", paramType = "header")
    @ApiOperation(value = "고객 개선 제안 요청")
    @ApiResponse(code = 200, message = "고객 개선 제안 요청 완료", response = CustomerSuggestResponse.class)
    @PostMapping("/set")
    public ResponseObject<CustomerSuggestResponse> setCustomerSuggest(
        @RequestHeader HttpHeaders httpHeaders,
        @ApiParam(value = "고객 개선 제안 요청", required = true) @Valid @RequestBody final CustomerSuggestRequest request)
        throws Exception {
        return ResponseObject.Builder.<CustomerSuggestResponse>builder()
            .data(customerSuggestService.setCustomerSuggest(httpHeaders, request))
            .build();
    }
}
