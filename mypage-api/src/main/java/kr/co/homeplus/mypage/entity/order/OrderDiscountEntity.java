package kr.co.homeplus.mypage.entity.order;

public class OrderDiscountEntity {

    // 주문할인번호
    public String orderDiscountNo;

    // 구매주문번호
    public String purchaseOrderNo;

    // 번들번호
    public String bundleNo;

    // 주문상품번호
    public String orderItemNo;

    // 상품번호
    public String itemNo;

    // 고객번호
    public String userNo;

    // 할인종류(상품쿠폰:item_coupon, 그룹상품쿠폰:group_coupon, 배송비쿠폰:ship_coupon, 장바구니쿠폰:cart_coupon, 상품할인:prod_discount, 제휴할인:affi_discount, 카드사할인:card_discount)
    public String discountKind;

    // 할인번호
    public String discountNo;

    // 쿠폰번호
    public String couponNo;

    // 쿠폰명
    public String couponNm;

    // 발급번호(쿠폰발급번호)
    public String issueNo;

    // 적용유형(BASIC:기본할인,ADD:추가할인)
    public String applyType;

    // 할인유형(PRICE:정액,RATE:정률)
    public String discountType;

    // 할인율
    public String discountRate;

    // 할인금액
    public String discountAmt;

    // 할인최대(최대할인금액)
    public String discountMax;

    // 구매최소구매금액
    public String purchaseMin;

    // 쿠폰유효시작일시
    public String couponValidStartDt;

    //쿠폰유효종료일시
    public String couponValidEndDt;

    // 홈플러스부담율
    public String hmpChargeRate;

    // 홈플러스부담가격
    public String hmpChargePrice;

    // 판매자부담율
    public String sellerChargeRate;

    // 판매자부담가격
    public String sellerChargePrice;

    // 카드사부담율
    public String cardShareRate;

    // 카드사부담가격
    public String cardSharePrice;

    // 등록일시
    public String regDt;

}
