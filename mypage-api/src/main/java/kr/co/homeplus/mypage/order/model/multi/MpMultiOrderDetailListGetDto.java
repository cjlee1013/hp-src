package kr.co.homeplus.mypage.order.model.multi;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import kr.co.homeplus.mypage.order.model.MpOrderGiftListDto;
import kr.co.homeplus.mypage.order.model.MpOrderPaymentListDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "마이페이지 > 주문/배송 - 다중배송 주문 상세")
public class MpMultiOrderDetailListGetDto {

    @ApiModelProperty(value = "주문번호", position = 1)
    private long purchaseOrderNo;

    @ApiModelProperty(value = "주문일자", position = 2)
    private String orderDt;

    @ApiModelProperty(value = "배송정보", position = 6)
    private List<MpMultiOrderShipListDto> shipInfo;

    @ApiModelProperty(value = "결제정보", position = 9)
    private MpOrderPaymentListDto orderPaymentInfo;

    @ApiModelProperty(value = "사은품정보", position = 11)
    private List<MpOrderGiftListDto> orderGiftList;

    @ApiModelProperty(value = "클레임정보", position = 12)
    private List<MpMultiOrderDetailClaimDto> claimInfoList;

    @ApiModelProperty(value = "주문타입", position = 13, hidden = true)
    private String orderType;

    @Data
    public static class MpMultiOrderDetailClaimDto {
        @ApiModelProperty(value = "번들번호")
        private long bundleNo;
        @ApiModelProperty(value = "다중번들번호")
        private long multiBundleNo;
        @ApiModelProperty(value = "클레임가능여부")
        private String claimYn;
    }
}
