package kr.co.homeplus.mypage.order.model.order;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "마이페이지 > 주문/배송 - 주문조회 응답")
public class OrderListGetDto {
    @ApiModelProperty(value = "주문번호", position = 1)
    private long purchaseOrderNo;
    @ApiModelProperty(value = "주문일", position = 2)
    private String orderDt;
    @ApiModelProperty(value = "결제상태", position = 3)
    private String paymentStatus;
    @ApiModelProperty(value = "합배송주문존재여부", position = 4)
    private String cmbnYn;
    @ApiModelProperty(value = "대체주문존재여부", position = 5)
    private String substitutionOrderYn;
    @ApiModelProperty(value = "전체취소버튼가능여부", position = 6)
    private String orderAllCancelPossibleYn;
    @ApiModelProperty(value = "비회원주문여부", position = 7)
    private String noMemberOrderYn;
    @ApiModelProperty(value = "배송정보리스트", position = 8)
    private List<OrderBundleListDto> bundleList;
}
