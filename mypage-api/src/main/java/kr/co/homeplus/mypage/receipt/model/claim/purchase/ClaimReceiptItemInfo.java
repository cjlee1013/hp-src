package kr.co.homeplus.mypage.receipt.model.claim.purchase;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ClaimReceiptItemInfo {

    @JsonIgnore
    @ApiModelProperty(value = "클레임번호", position = 1)
    private long claimNo;

    @ApiModelProperty(value = "상품명", position = 9)
    private String itemName;

    @ApiModelProperty(value = "수량", position = 10)
    private int completeQty;

    @ApiModelProperty(value = "구매금액(취소금액)", position = 11)
    private long completeAmt;

    @ApiModelProperty(value = "파트너아이디", position = 12)
    private String partnerId;

    @ApiModelProperty(value = "파트너명", position = 13)
    private String partnerName;

    @ApiModelProperty(value = "점포타입", position = 7)
    private String storeType;

    @ApiModelProperty(value = "세금여부(Y:과세,N:면세,Z:영세)", position = 8)
    private String taxYn;

    @ApiModelProperty(value = "임직원할인금액", position = 9)
    private long empDiscountAmt;
    @ApiModelProperty(value = "상품할인금액", position = 10)
    private long itemDiscountAmt;
    @ApiModelProperty(value = "카드상품할인금액", position = 11)
    private long cardDiscountAmt;
    @ApiModelProperty(value = "장바구니쿠폰할인금액", position = 12)
    private long cartDiscountAmt;
    @ApiModelProperty(value = "행사할인금액", position = 13)
    private long promoDiscountAmt;
}