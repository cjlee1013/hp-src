package kr.co.homeplus.mypage.receipt.model.order.card;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "주문_카드구매영수증")
public class OrderPurchaseCardReceiptGetDto {

    /** 신용카드 전표 노출 시 사용 **/
    @ApiModelProperty(value = "카드번호", position = 1)
    private String cardNo;

    @ApiModelProperty(value = "카드할부 (0: 일시불)", position = 2)
    private int cardInstallmentMonth;

    @ApiModelProperty(value = "주문번호", position = 3)
    private long purchaseOrderNo;

    @ApiModelProperty(value = "승일인시", position = 4)
    private String paymentFshDt;

    @ApiModelProperty(value = "결제수단", position = 5)
    private String methodNm;

    @ApiModelProperty(value = "승인번호", position = 6)
    private String approvalNo;

    @ApiModelProperty(value = "상품명", position = 7)
    private String itemNm;

    @ApiModelProperty(value = "총금액", position = 8)
    private long totPayAmt;

    @ApiModelProperty(value = "부가세", position = 9)
    private long totVatAmt;

    @ApiModelProperty(value = "금액", position = 9)
    private long payAmt;

    /** 전표 판매자 정보 노출 시 사용 **/
    @ApiModelProperty(value = "파트너명", position = 10)
    private String partnerNm;

    @ApiModelProperty(value = "대표자명", position = 11)
    private String partnerOwner;

    @ApiModelProperty(value = "사업자등록번호", position = 12)
    private String partnerNo;

    @ApiModelProperty(value = "고객센터", position = 13)
    private String partnerInfoCenter;
}
