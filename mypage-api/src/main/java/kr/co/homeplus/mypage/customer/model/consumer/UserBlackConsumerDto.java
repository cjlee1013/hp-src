package kr.co.homeplus.mypage.customer.model.consumer;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(description = "블랙컨슈머 조회 data")
public class UserBlackConsumerDto {
    private long blackConsumerType;
    private String startDt;
    private String endDt;
}
