package kr.co.homeplus.mypage.order.model.order;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "주문리스트 - 번들별 클레임정보")
public class OrderClaimInfoDto {
    @ApiModelProperty(value = "그룹클레임번호", position = 1)
    private long claimNo;
    @ApiModelProperty(value = "클레임타입(C:취소,R:반품,X:교환)", position = 2)
    private String claimType;
    @ApiModelProperty(value = "클레임상태", position = 3)
    private String claimStatus;
    @ApiModelProperty(value = "수거배송상태", position = 4)
    private String pickShipStatus;
    @ApiModelProperty(value = "교환배송상태", position = 5)
    private String exchangeShipStatus;
    @ApiModelProperty(value = "클레임진행여부", position = 6)
    private String claimProcessYn;
    @ApiModelProperty(value = "배송별상품전체취소여부", position = 7)
    private String isBundleAllCancel;
    @ApiModelProperty(value = "환불실패여부", position = 8)
    private String isRefundFail;
}
