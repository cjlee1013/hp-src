package kr.co.homeplus.mypage.order.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import kr.co.homeplus.mypage.order.model.MpOrderPaymentInfoDto.OrderBenefitDto;
import kr.co.homeplus.mypage.order.model.MpOrderPaymentInfoDto.OrderCouponDiscountDto;
import kr.co.homeplus.mypage.order.model.MpOrderPaymentInfoDto.OrderPaymentListDto;
import kr.co.homeplus.mypage.order.model.MpOrderPaymentInfoDto.OrderPromoDiscountDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(description = "마이페이지 > 조회/배송 상세 > 결제정보")
public class MpOrderClaimPaymentInfoDto {

    @ApiModelProperty(value = "총주문금액", position = 1)
    private int totItemPrice;

    @ApiModelProperty(value = "총할인금액", position = 2)
    private int totDiscountPrice;

    @ApiModelProperty(value = "총배송금액", position = 3)
    private int totShipPrice;

    @ApiModelProperty(value = "총도서산간배송금액", position = 4)
    private int totIslandShipPrice;

    @ApiModelProperty(value = "총추가배송비", position = 5)
    private int totAddShipPrice;

    @ApiModelProperty(value = "총추가도서산간배송금액", position = 6)
    private int totAddIslandShipPrice;

    @ApiModelProperty(value = "총결제금액", position = 7)
    private int totPaymentAmt;

    @ApiModelProperty(value = "총임직원할인", position = 8)
    private int totEmpDiscountAmt;

    @ApiModelProperty(value = "대체주문금액", position = 9)
    private int substitutionAmt;

    @ApiModelProperty(value = "쿠폰할인리스트", position = 10)
    private List<OrderCouponDiscountDto> couponDiscountList;

    @ApiModelProperty(value = "프로모션할인리스트", position = 11)
    private List<OrderPromoDiscountDto> promoDiscountList;

    @ApiModelProperty(value = "결제수단리스트", position = 12)
    private List<OrderPaymentListDto> orderPaymentListList;

    @ApiModelProperty(value = "혜택/적립리스트", position = 13)
    private List<OrderBenefitDto> orderBenefitList;

}
