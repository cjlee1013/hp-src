package kr.co.homeplus.mypage.user.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(description = "회원등급 정보")
public class GradeInfoDto {
    @ApiModelProperty(value = "등급번호", position = 1)
    private Integer gradeSeq;
    @ApiModelProperty(value = "등급", position = 2)
    private String gradeNm;
    @ApiModelProperty(value = "주문건수", position = 3)
    private Integer orderCnt;
    @ApiModelProperty(value = "주문금액", position = 4)
    private Integer orderAmount;
}
