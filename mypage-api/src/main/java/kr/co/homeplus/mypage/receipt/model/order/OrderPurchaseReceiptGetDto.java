package kr.co.homeplus.mypage.receipt.model.order;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import kr.co.homeplus.mypage.receipt.model.order.purchase.OrderPaymentReceiptInfo;
import kr.co.homeplus.mypage.receipt.model.order.purchase.OrderPurchaseReceiptItemInfo;
import kr.co.homeplus.mypage.receipt.model.order.purchase.OrderPurchaseReceiptMainInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "주문_구매영수증 응답 DTO")
public class OrderPurchaseReceiptGetDto {
    @ApiModelProperty(value = "구매정보", position = 1)
    OrderPurchaseReceiptMainInfo purchaseReceiptMainInfo;
    @ApiModelProperty(value = "구매아이템정보", position = 2)
    List<OrderPurchaseReceiptItemInfo> purchaseOrderItemInfoList;
    @ApiModelProperty(value = "결제정보", position = 3)
    List<OrderPaymentReceiptInfo> receiptPaymentInfoList;
}
