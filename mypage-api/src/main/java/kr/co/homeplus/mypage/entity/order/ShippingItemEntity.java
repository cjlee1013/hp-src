package kr.co.homeplus.mypage.entity.order;

public class ShippingItemEntity {

    // 배송번호
    public String shipNo;

    // 구매주문번호
    public String purchaseOrderNo;

    // 번들번호
    public String bundleNo;

    // 주문상품번호
    public String orderItemNo;

    // 고객번호
    public String userNo;

    // 상품번호
    public String itemNo;

    // 파트너ID
    public String partnerId;

    // 배송방법(DS_DLV:택배배송,DS_DRCT:직접배송,DS_PICK:방문수령/픽업,DS_POST:우편배송,DS_QUICK:퀵배송,TD_DLV:택배배송,TD_DRCT:직접배송,TD_PICK:방문수령,TD_POST:우편배송,TD_QUICK:퀵배송)
    public String shipMethod;

    // 배송주소번호
    public String shipAddrNo;

    // 배송상태(NN:대기,D1:주문확인전(결제완료),D2:주문확인(상품준비중),D3:배송중,D4:배송완료,D5:구매확정)
    public String shipStatus;

    // 택배코드
    public String dlvCd;

    // 송장번호
    public String invoiceNo;

    // 배송추적요청여부
    public String trackingRequest;

    // 배송추적응답여부(R:추적대기,T:추적여부수신(스윗트래커콜백수신시),F1:실패-필수 파라미터가 빈 값이거나 없음,F2:실패-운송장번호 유효성 검사 실패,F3:이미 등록된 운송장번호,F4:지원하지 않는 택배사코드,F9:시스템오류)
    public String trackingResult;

    // 송장등록ID
    public String invoiceRegId;

    // 원발송일자(주문일+상품출고기한)
    public String orgShipDt;

    // 판매자 확인일자(D1>D2)
    public String confirmDt;

    // 배송시작일(D2>D3)
    public String shippingDt;

    // 배송완료일(D3>D4) 홈플러스 시스템 기준 배송완료일자
    public String completeDt;

    // 택배사 배송완료일
    public String completeRegDt;

    // 배송완료등록자(mypage,partner,batch,그외:사번)
    public String completeRegId;

    // 배송예정일자(직접배송 등록시 입력받은 배송일)
    public String scheduleShipDt;

    // 2차발송기한(발송지연시 1회등록가능)
    public String delayShipDt;

    // 발송지연사유코드(SS:재고부족,PO:주문폭주,CR:고객요청,ET:기타)
    public String delayShipCd;

    // 2차발송기한등록일시
    public String delayShipRegDt;

    // 발송지연사유메시지
    public String delayShipMsg;

    // 기타배송메시지
    public String shipEtcMsg;

    // 등록일시
    public String regDt;

    // 수정일시
    public String chgDt;

    // 수정자
    public String chgId;

}
