package kr.co.homeplus.mypage.claim.model.noRcv;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "주문관리 > 상세 > 미수취 신청내역 응답")
public class NoRcvShipInfoGetDto {

    @ApiModelProperty(value= "미수취신고유형(NA:상품미도착,NR:도난의심,NS:배송조회안됨,NE:ETC)", position = 1)
    private String noRcvDeclrType;

    @ApiModelProperty(value= "미수취 상세 사유", position = 2)
    private String noRcvDetailReason;

    @ApiModelProperty(value= "미수취 신고 일자", position = 3)
    private String noRcvDeclrDt;

    @ApiModelProperty(value= "미수취 처리 일자", position = 4)
    private String noRcvProcessDt;

    @ApiModelProperty(value= "미수취처리유형(R : 재배송(사용안함) C : 수취확인 요청 W : 고객요청에 의한 미수취신고 철회)", position = 5)
    private String noRcvProcessType;

    @ApiModelProperty(value= "처리내용", position = 6)
    private String noRcvProcessCntnt;

    @ApiModelProperty(value= "처리결과", position = 7)
    private String noRcvProcessResult;

    @ApiModelProperty(value= "등록자", position = 8)
    private String noRcvRegId;

    @ApiModelProperty(value= "처리자", position = 9)
    private String noRcvChgId;

}
