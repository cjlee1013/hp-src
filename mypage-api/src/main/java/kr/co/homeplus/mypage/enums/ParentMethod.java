package kr.co.homeplus.mypage.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ParentMethod {
    CARD("CARD","신용카드"),
    VBANK("VBANK","무통장"),
    RBANK("RBANK","실시간계좌이체"),
    PHONE("PHONE","휴대폰"),
    KAKAOPAY("KAKAOPAY","카카오페이"),
    NAVERPAY("NAVERPAY", "네이버페이"),
    SAMSUNGPAY("SAMSUNGPAY", "삼성페이"),
    TOSS("TOSS", "토스"),
    MILEAGE("MILEAGE","마일리지"),
    MHC("MHC","MHC포인트"),
    DGV("DGV","홈플러스상품권"),
    OCB("OCB","OkCashBack"),
    FREE("FREE", "0원결제"),
    NONE("NONE","결제수단없음")
    ;
    private String methodCd;
    private String methodNm;
}