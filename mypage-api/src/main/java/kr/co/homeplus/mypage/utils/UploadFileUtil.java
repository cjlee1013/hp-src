package kr.co.homeplus.mypage.utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.plus.api.support.client.exception.ResourceClientException;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.web.multipart.MultipartFile;

public class UploadFileUtil {

    // MultipartFile 을 받아서 ByteArrayResource 를 반환. fileName 은 UTF-8 URLEncode 된 상태로 넘어감.
    public static List<ByteArrayResource> getResourceFromMultipartFile(List<MultipartFile> files)
        throws IOException {
        if (files == null) {
            throw new IllegalArgumentException("file is null");
        }
        List<ByteArrayResource> result = new ArrayList<>();

        for (MultipartFile file : files) {
            result.add(getByteArrayResource(file));
        }
        return result;
    }

    public static ByteArrayResource getByteArrayResource(final MultipartFile file)
        throws IOException {
        return new ByteArrayResource(file.getBytes()) {
            @Override
            public String getFilename() throws IllegalStateException {
                return URLEncoder.encode(file.getOriginalFilename(), StandardCharsets.UTF_8);
            }
        };
    }

    /**
     * 파일 업로드시 resourceClientException 발생 시 처리
     *
     * @param e resourceClientException
     * @return
     */
    public static String setUploadFileResponseError(ResourceClientException e) {
        JsonObject jsonObject = new JsonParser().parse(new String(e.getResponseBody()))
            .getAsJsonObject();
        JsonArray jsonArray = jsonObject.getAsJsonArray("errors");

        return jsonArray.get(0).getAsJsonObject().get("detail").getAsString();
    }
}
