package kr.co.homeplus.mypage.customer.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "마이페이지 > 홈 - QnA등록")
public class QnaRegisterSetDto {

    @ApiModelProperty(value = "Qna 일련번호(신규 등록시 미 전송)")
    private Integer qnaNo;

    @ApiModelProperty(value = "상품번호", required = true)
    @NotEmpty(message = "상품번호")
    private String itemNo;

    @ApiModelProperty(value = "Qna 타입 (상품-ITEM, 배송-SHIP, 교환/반품-CLAIM, 기타-ETC)", required = true)
    @NotEmpty(message = "Qna 타입 (상품-ITEM, 배송-SHIP, 교환/반품-CLAIM, 기타-ETC)")
    @Pattern(regexp = "(ITEM|SHIP|CLAIM|ETC)", message="Qna 타입 (상품-ITEM, 배송-SHIP, 교환/반품-CLAIM, 기타-ETC)")
    private String qnaType;

    @ApiModelProperty(value = "문의내용", required = true)
    @NotEmpty(message = "문의내용")
    @Size(min=2, max=500, message = "문의내용")
    private String qnaContents;

    @ApiModelProperty(value = "비밀글 여부", required = true)
    @Pattern(regexp = "[Y|N]{1}", message="비밀글 여부")
    @NotEmpty(message = "비밀글 여부")
    private String secretYn;

    @ApiModelProperty(value = "프론트 노출 여부(노출중-DISP, 노출중지-PAUSE 삭제-DEL)", required = true)
    @Pattern(regexp = "(DISP|PAUSE|DEL)", message="프론트 노출 여부(노출중-DISP, 노출중지-PAUSE 삭제-DEL)")
    private String displayStatus;

    @ApiModelProperty(value = "등록자 아이디", hidden = true)
    private String partnerId;

    @ApiModelProperty(value = "구매여부", hidden = true)
    private String purchaseYn;

    @ApiModelProperty(value = "구매주문번호", hidden = true)
    private String purchaseOrderNo;

    @ApiModelProperty(value = "등록자 아이디", hidden = true)
    private String regId ="USER";
}
