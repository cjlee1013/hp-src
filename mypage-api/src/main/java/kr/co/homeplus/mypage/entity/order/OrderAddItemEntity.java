package kr.co.homeplus.mypage.entity.order;

public class OrderAddItemEntity {

    // 주문추가상품번호
    public String orderAddItemNo;

    // 주문상품번호
    public String orderItemNo;

    // 추가구성번호(상품Key값)
    public String addOptNo;

    // 상품번호(추가구성상품)
    public String itemNo;

    // 추가구성타이틀
    public String addOptTitle;

    // 추가구성값
    public String addOptVal;

    // 추가구성금액
    public String addOptPrice;

    // 추가구성수량
    public String addOptQty;

    // 추가구성주문금액(추가구성금액*추가구성수량)
    public String addOptOrderPrice;

    // 업체추가구성코드
    public String sellerAddOptCd;

    // 재고매수번호
    public String stockBuyNo;

    // 등록일시
    public String regDt;

}
