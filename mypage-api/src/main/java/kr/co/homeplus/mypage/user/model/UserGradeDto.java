package kr.co.homeplus.mypage.user.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "등급에 대한 정보 DTO")
public class UserGradeDto {
    @ApiModelProperty(value = "등급번호", hidden = true)
    private Integer gradeSeq;
    @ApiModelProperty(value = "등급", position = 1)
    private String gradeNm;
    @ApiModelProperty(value = "다음등급달성 주문건수", position = 2)
    private Integer orderCnt;
    @ApiModelProperty(value = "다음등급달성 주문최소금액", position = 3)
    private Integer orderMinAmount;
    @ApiModelProperty(value = "다음등급달성 주문최대금액", position = 4)
    private Long orderMaxAmount;
    @ApiModelProperty(value = "다음등급달성 비교규칙(OR, AND)", position = 5)
    private String comparisonRule;

}
