package kr.co.homeplus.mypage.enums;


import kr.co.homeplus.mypage.enums.impl.EnumImpl;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * [공통] 에러코드 정의
 * 1001 ~ 8999 사용자 에러코드
 * 9000 ~ 9999 시스템에러 SP에러(9999) 등등
 */
@RequiredArgsConstructor
public enum ExceptionCode implements EnumImpl {

	SYS_ERROR_CODE_9001("9001", "시스템 에러 : 관리자에게 문의해 주세요.")
	,   SYS_ERROR_CODE_9002("9002", "통신 에러 : 잘못된 URL을 호출 하였습니다.")
	,   SYS_ERROR_CODE_9003("9003", "통신 에러 : Http Method가 잘못 되었습니다.")
	,   SYS_ERROR_CODE_9004("9004", "DB 에러! 재시도 해주세요.")
	,   SYS_ERROR_CODE_9005("9005", "CACHE 에러!")
	,   SYS_ERROR_CODE_9006("9006", "(%d) 통신에 실패 하였습니다. 재시도 해주세요.")
	,   SYS_ERROR_CODE_9007("9007", "(%d) 통신 에러! 재시도 해주세요.")
	,   SYS_ERROR_CODE_9008("9008", "NOSQL 에러! 재시도 해주세요.")
	,   SYS_ERROR_CODE_9009("9009", "(%d) 통신에 실패 하였습니다. 재시도 해주세요. (%s)")
	,	SYS_ERROR_CODE_9010("9010", "(%s)에 해당하는 쿼리를 생성하지 못하였습니다.")
	,   SYS_ERROR_CODE_9999("9999", "DB 에러 : 프로시저 오류! 재시도 해주세요.")

	,   ERROR_CODE_1001("1001", " 파라미터 유효성 검사 오류입니다.")
	,   ERROR_CODE_1002("1002", " 필수값을 입력해주세요.")
	,   ERROR_CODE_1003("1003", "(%d) 필수값을 입력해주세요.")
	,   ERROR_CODE_1004("1004", " 파라미터 타입 오류입니다.")
	,   ERROR_CODE_1005("1005", " 자 사이로 입력해주세요.")
	,   ERROR_CODE_1006("1006", " %d 파라미터 유효성 검사 오류입니다.")
	,   ERROR_CODE_1007("1007", " 자 이하로 입력해주세요.")
	,   ERROR_CODE_1021("1021", " 형식에 맞지 않습니다.")
	,   ERROR_CODE_1022("1022", " 초과의 값으로 입력해주세요.")
	,   ERROR_CODE_1023("1023", " 미만의 값으로 입력해주세요.")
	,   ERROR_CODE_1028("1028", " 이상으로 입력해주세요.")
	,   ERROR_CODE_1029("1029", " 이하로 입력해주세요.")
	,   ERROR_CODE_1090("1090", "(%d) 줄바꿈 공백없이 입력해주세요.")
	,   ERROR_CODE_1091("1091", "검색 결과 수가 많아 모두 출력할 수가 없습니다.\n검색 조건을 추가하여 다시 검색해주세요.")
	;

	@Getter
	private final String responseCode;

	@Getter
	private final String responseMessage;


	@Override
	public String toString() {
		return responseCode + ": " + responseMessage;
	}

}
