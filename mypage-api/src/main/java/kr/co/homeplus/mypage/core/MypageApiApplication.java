package kr.co.homeplus.mypage.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"kr.co.homeplus.mypage", "kr.co.homeplus.plus"})
public class MypageApiApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(MypageApiApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(MypageApiApplication.class);
    }
}
