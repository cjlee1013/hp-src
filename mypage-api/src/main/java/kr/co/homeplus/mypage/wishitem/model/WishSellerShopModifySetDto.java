package kr.co.homeplus.mypage.wishitem.model;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "마이페이지 > 홈 - 찜 상품/샐러 리스트")
public class WishSellerShopModifySetDto {

    @ApiModelProperty(value = "회원일련번호", hidden = true)
    private Long userNo;

    @ApiModelProperty(value = "셀러샵 Url", required = true, position = 1)
    private String shopUrl;

    @ApiModelProperty(value = "사용여부 (찜 여부)", required = true, position = 2)
    @NotEmpty(message = "사용여부 (찜 여부)")
    @Pattern(regexp = "(Y|N)", message="사용여부 (찜 여부)")
    private String useYn;



}
