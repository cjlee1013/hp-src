package kr.co.homeplus.mypage.user.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "마이페이지 > 회원정보관리 > 회원정보조회")
public class UserInfoGetDto {

    @ApiModelProperty(value = "회원일련번호", position = 1)
    private Long userNo;

    @ApiModelProperty(value = "회원아이디", position = 2)
    private String userId;

    @ApiModelProperty(value = "회원이름", position = 3)
    private String userNm;

    @ApiModelProperty(value = "생년월일", position = 4)
    private String birth;

    @ApiModelProperty(value = "성별 - 0:미지정 1:남자 2:여자", position = 5)
    private Integer gender;

    @ApiModelProperty(value = "이메일", position = 6)
    private String email;

    @ApiModelProperty(value = "휴대폰", position = 7)
    private String mobile;

    @ApiModelProperty(value = "회원상태", position = 8)
    private Integer userStatus;

    @ApiModelProperty(value = "회원등급번호", position = 9)
    private Integer gradeSeq;

    @ApiModelProperty(value = "회원등급명", position = 10)
    private String gradeNm;

    @ApiModelProperty(value = "최종로그인일시", position = 11)
    private String lastLoginDt;

    @ApiModelProperty(value = "본인인증 수단 - 0 미인증, 1 핸드폰 2 아이핀", position = 12)
    private Integer certificateType;

    @ApiModelProperty(value = "CI(본인인증 연계정보)", position = 13)
    private String userCi;

    @ApiModelProperty(value = "성인인증일자", position = 14)
    private String adultCertificateDt;

    @ApiModelProperty(value = "성인인증만료일자", position = 15)
    private String adultCertificateExpireDt;

    @ApiModelProperty(value = "본인인증일자", position = 16)
    private String certDt;

    @ApiModelProperty(value = "OCB 연동여부", position = 17)
    private String ocbYn;

    @ApiModelProperty(value = "통합회원여부", position = 18)
    private String unionYn;

    @ApiModelProperty(value = "카드발급유형 0021:마이홈플러스(신용) 0022:마이홈플러스(체크) 0023:마이홈플러스임직원(신용) 0024:마이홈플러스임직원(체크) 0030:마이홈플러스(일반) 1000:신한제휴카드 2000:삼성제휴카드 3000:KB제휴카드 9000:기타제휴카드", position = 19)
    private String mhcCardType;

    @ApiModelProperty(value = "BenefitMhcCardInfoDto ucid", position = 20)
    private String mhcUcid;

    @ApiModelProperty(value = "BenefitMhcCardInfoDto 대체카드번호 암호화", position = 21)
    private String mhcCardnoEnc;

    @ApiModelProperty(value = "BenefitMhcCardInfoDto 대체카드번호 HMAC", position = 22)
    private String mhcCardnoHmac;

    @ApiModelProperty(value = "BenefitMhcCardInfoDto 카드발급일자", position = 23)
    private String mhcCardIssueDt;

    @ApiModelProperty(value = "임직원아이디", position = 24)
    private String empId;

    @ApiModelProperty(value = "가입일자", position = 25)
    private String regDt;

    @ApiModelProperty(value = "수정일자", position = 26)
    private String modDt;

    @ApiModelProperty(value = "개인통관고유부호", position = 27)
    private String personalOverseaNo;

    @ApiModelProperty(value = "배송지정보(기본배송지)", position = 28)
    private ShippingAddressDto shippingAddressBasic;

}
