package kr.co.homeplus.mypage.core.exception;

import kr.co.homeplus.mypage.enums.impl.EnumImpl;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;

@Data
@EqualsAndHashCode(callSuper = false)
public class LogicException extends Exception {

    private String responseCode;
    private String responseMessage;
    private Object data;
    private HttpStatus httpStatus;


    public LogicException(EnumImpl commonCode){
        this.responseCode = commonCode.getResponseCode();
        this.responseMessage = commonCode.getResponseMessage();
    }

    public LogicException(EnumImpl commonCode, Object data){
        this.responseCode = commonCode.getResponseCode();
        this.responseMessage = commonCode.getResponseMessage();
        this.data = data;
    }


    public LogicException(EnumImpl commonCode, String replaceCode, String replaceStr){
        this.responseCode = commonCode.getResponseCode();
        this.responseMessage = StringUtils.replace(commonCode.getResponseMessage(), replaceCode, replaceStr);
    }

    public LogicException(EnumImpl commonCode, Object data, String returnMessage, boolean isConvert){
        this.responseCode = commonCode.getResponseCode();
        this.responseMessage = (isConvert ? returnMessage : commonCode.getResponseMessage());
        this.data = data;
    }

    public LogicException(String responseCode, String responseMsg) {
        this.responseCode = responseCode;
        this.responseMessage = responseMsg;
    }

}
