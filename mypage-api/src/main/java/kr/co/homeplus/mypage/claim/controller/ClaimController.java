package kr.co.homeplus.mypage.claim.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.mypage.claim.ClaimService;
import kr.co.homeplus.mypage.claim.model.ClaimAddImageGetDto;
import kr.co.homeplus.mypage.claim.model.ClaimChgPaymentRefundSetDto;
import kr.co.homeplus.mypage.claim.model.ClaimDetailGetDto;
import kr.co.homeplus.mypage.claim.model.ClaimPreRefundCalcGetDto;
import kr.co.homeplus.mypage.claim.model.ClaimPreRefundCalcSetDto;
import kr.co.homeplus.mypage.claim.model.ClaimReasonSearchGetDto;
import kr.co.homeplus.mypage.claim.model.ClaimRequestChangeStatusSetDto;
import kr.co.homeplus.mypage.claim.model.ClaimSearchListGetDto;
import kr.co.homeplus.mypage.claim.model.ClaimSearchListSetDto;
import kr.co.homeplus.mypage.claim.model.noRcv.NoRcvShipEditDto;
import kr.co.homeplus.mypage.claim.model.noRcv.NoRcvShipInfoGetDto;
import kr.co.homeplus.mypage.claim.model.noRcv.NoRcvShipInfoSetDto;
import kr.co.homeplus.mypage.claim.model.noRcv.NoRcvShipSetGto;
import kr.co.homeplus.mypage.claim.model.register.ClaimRegSetDto;
import kr.co.homeplus.mypage.enums.ExternalInfo;
import kr.co.homeplus.mypage.enums.ResponseCode;
import kr.co.homeplus.mypage.enums.SiteType;
import kr.co.homeplus.mypage.external.service.ExternalService;
import kr.co.homeplus.mypage.factory.ResponseFactory;
import kr.co.homeplus.mypage.order.model.MpOrderGiftItemListDto;
import kr.co.homeplus.mypage.order.model.MpOrderGiftListDto;
import kr.co.homeplus.mypage.utils.SetParameter;
import kr.co.homeplus.plus.api.support.client.header.UserDeviceType;
import kr.co.homeplus.plus.api.support.client.header.WebHeaderKey;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/claim")
@Api(tags = "Mypage - 취소/반품/교환", value="Mypage - 취소/반품/교환")
public class ClaimController {

    private final ExternalService externalService;
    private final ClaimService claimService;

    @PostMapping("/claimSearchList")
    @ApiOperation(value = "취소/반품/교환 조회", response = ClaimSearchListGetDto.class)
    public ResponseObject<List<ClaimSearchListGetDto>> getClaimSearchList(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestBody ClaimSearchListSetDto claimSearchListSetDto
    ) throws Exception {
        claimSearchListSetDto.setSiteType(siteType.name());
        return claimService.getClaimSearchList(claimSearchListSetDto);
    }

    @GetMapping("/claimDetailList/{claimNo}")
    @ApiOperation(value = "취소/반품/교환 상세내역 조회", response = ClaimDetailGetDto.class)
    public ResponseObject<ClaimDetailGetDto> getClaimDetailList(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @PathVariable("claimNo")long claimNo
    ) throws Exception {
        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS, externalService.getTransfer( ExternalInfo.CLAIM_API_CLAIM_DETAIL_LIST,
            new ArrayList<SetParameter>(){{
                    add(SetParameter.create("userNo", externalService.getUserNo()));
                    add(SetParameter.create("claimNo", claimNo));
                }}, ClaimDetailGetDto.class));
    }

    @GetMapping("/claimReasonCodeSearch/{claimType}")
    @ApiOperation(value = "클레임 사유 코드 조회", response = ClaimReasonSearchGetDto.class)
    public ResponseObject<List<ClaimReasonSearchGetDto>> getClaimReason(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @ApiParam(name = "claimType", value = "클레임종류(C:취소,R:반품,X:교환)", defaultValue = "", required = true) @PathVariable("claimType") String claimType
    ) throws Exception {
        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS, externalService.getListTransfer( ExternalInfo.CLAIM_API_CLAIM_REASON_CODE_LIST, claimType, ClaimReasonSearchGetDto.class));
    }

    @PostMapping("/preRefundPrice")
    @ApiOperation(value = "환불예정금액 조회", response = ClaimPreRefundCalcGetDto.class)
    public ResponseObject<ClaimPreRefundCalcGetDto> getPreRefundPrice(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestBody ClaimPreRefundCalcSetDto claimPreRefundCalcSetDto
    ) throws Exception {
        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS, externalService.postTransfer(ExternalInfo.CLAIM_API_GET_PRE_REFUND_PRICE, claimPreRefundCalcSetDto, ClaimPreRefundCalcGetDto.class));
    }

    @PostMapping("/getNoRcvList")
    @ApiOperation(value = "미수취 내역 조회", response = NoRcvShipInfoGetDto.class)
    public ResponseObject<NoRcvShipInfoGetDto> getNoRcvList(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestBody NoRcvShipInfoSetDto noRcvShipInfoSetDto
    ) throws Exception {
        return claimService.getNoRcvInfoList(noRcvShipInfoSetDto);
    }

    @PostMapping("/addNoRcvInfo")
    @ApiOperation(value = "미수취 내역 등록", response = NoRcvShipInfoGetDto.class)
    public ResponseObject<String> addNoRcvInfo(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestBody @Valid NoRcvShipSetGto noRcvShipSetGto
    ) throws Exception {
        return claimService.setNoRcvInfoReg(noRcvShipSetGto);
    }

    @PostMapping("/modifyNoRcvInfo")
    @ApiOperation(value = "미수취 내역 수정", response = NoRcvShipInfoGetDto.class)
    public ResponseObject<String> modifyNoRcvInfo(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestBody NoRcvShipEditDto noRcvShipEditDto
    ) throws Exception {
        return claimService.setNoRcvInfoModify(noRcvShipEditDto);
    }

    @PostMapping("/addClaimImage")
    @ApiOperation(value = "클레임 사진 업로드", response = ClaimAddImageGetDto.class)
    public ResponseObject<ClaimAddImageGetDto> addClaimImage(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @ApiParam(value = "업로드 할 파일", required = true) @RequestParam("file") final MultipartFile multipartFile
    ) throws Exception {
        return claimService.addClaimImage(multipartFile);
    }

    @PostMapping("/claimRegister")
    @ApiOperation(value = "클레임등록 (주문취소/취소신청/교환/반품 신청)")
    public ResponseObject<Long> claimRegister(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestBody ClaimRegSetDto claimRegSetDto
    ) throws Exception {
        return claimService.claimRegister(claimRegSetDto);
    }

    @PostMapping("/chgPaymentRefund")
    @ApiOperation(value = "환불수단 변경")
    public ResponseObject<String> chgPaymentRefund(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestBody ClaimChgPaymentRefundSetDto claimChgPaymentRefundSetDto
    ) throws Exception {
        return claimService.chgPaymentRefund(claimChgPaymentRefundSetDto);
    }

    @GetMapping("/getOrderGiftItemList")
    @ApiOperation(value = "사은품 상품 리스트 조회", response = MpOrderGiftItemListDto.class)
    public ResponseObject<List<MpOrderGiftItemListDto>> getOrderGiftItemList(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestParam(name = "purchaseOrderNo")long purchaseOrderNo,
        @RequestParam(name="bundleNo")long bundleNo) throws Exception {
        return claimService.getOrderGiftItemList(purchaseOrderNo, bundleNo);
    }

    @PostMapping("/chgClaimStatus")
    @ApiOperation(value = "클레임 상태 변경 요청", response = String.class)
    public ResponseObject<String> getOrderGiftItemList(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestBody ClaimRequestChangeStatusSetDto claimRequestSetDto) throws Exception {
        return claimService.chgClaimStatus(claimRequestSetDto);
    }

    @PostMapping("/preMultiRefundPrice")
    @ApiOperation(value = "환불예정금액 조회(다중배송)", response = ClaimPreRefundCalcGetDto.class)
    public ResponseObject<ClaimPreRefundCalcGetDto> getPreMultiRefundPrice(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestBody ClaimPreRefundCalcSetDto claimPreRefundCalcSetDto
    ) throws Exception {
        return ResponseFactory.getResponseObject(ResponseCode.SUCCESS, externalService.postTransfer(ExternalInfo.CLAIM_API_GET_MULTI_PRE_REFUND_PRICE, claimPreRefundCalcSetDto, ClaimPreRefundCalcGetDto.class));
    }

    @PostMapping("/multiClaimRegister")
    @ApiOperation(value = "다중배송-클레임등록(주문취소)")
    public ResponseObject<Long> multiClaimRegister(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestBody ClaimRegSetDto claimRegSetDto
    ) throws Exception {
        return claimService.multiClaimRegister(claimRegSetDto);
    }
}
