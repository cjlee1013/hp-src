package kr.co.homeplus.mypage.enums;

import java.util.Arrays;
import java.util.Locale;
import java.util.Optional;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

/**
 * 점포타입
 * HYPER, AURORA, EXP, DS | CLUB
 */
@AllArgsConstructor
@Getter
public enum StoreType {
    /* StoreType 의 비교는 code(대문자보장) 으로 하는것이 안전합니다. */
    HYPER("HYPER"),
    AURORA("AURORA"),
    EXP("EXP"),
    CLUB("CLUB"),
    DS("DS"),
    hyper("HYPER"),
    aurora("AURORA"),
    exp("EXP"),
    club("CLUB"),
    ds("DS")
    ;
    String code;

    public static StoreType getStoreType(String storeTypeStr) {
        storeTypeStr = storeTypeStr.toUpperCase(Locale.KOREA);

        String finalStoreTypeStr = storeTypeStr;
        Optional<StoreType> optionalStoreType = Arrays.stream(StoreType.values())
            .filter(st -> StringUtils.equals(st.getCode(), finalStoreTypeStr))
            .findFirst();
        return optionalStoreType.orElse(null);
    }
}