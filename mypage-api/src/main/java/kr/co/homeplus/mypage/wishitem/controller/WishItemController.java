package kr.co.homeplus.mypage.wishitem.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.util.List;
import kr.co.homeplus.mypage.enums.SiteType;
import kr.co.homeplus.mypage.response.ResponseResult;
import kr.co.homeplus.mypage.wishitem.model.WishListGetDto;
import kr.co.homeplus.mypage.wishitem.model.WishListSetDto;
import kr.co.homeplus.mypage.wishitem.model.WishItemListGetDto;
import kr.co.homeplus.mypage.wishitem.model.WishItemListSetDto;
import kr.co.homeplus.mypage.wishitem.model.WishSellerShopListGetDto;
import kr.co.homeplus.mypage.wishitem.model.WishSellerShopListSetDto;
import kr.co.homeplus.mypage.wishitem.model.WishSellerShopModifySetDto;
import kr.co.homeplus.mypage.wishitem.service.WishItemService;
import kr.co.homeplus.plus.api.support.client.header.UserDeviceType;
import kr.co.homeplus.plus.api.support.client.header.WebHeaderKey;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/wishitem")
@Api(tags = "Mypage - 찜 리스트", value = "Mypage - 찜 리스트")
public class WishItemController {

    private final WishItemService wishItemService;

    @PostMapping("/wishItemListSearch")
    @ApiOperation(value = "찜목록 조회", response = WishItemListGetDto.class)
    public ResponseObject<List<WishItemListGetDto>> wishItemListSearch(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestBody WishItemListSetDto wishItemListSetDto
    ) throws Exception {
        wishItemListSetDto.setSiteType(siteType);
        return wishItemService.getWishItemList(wishItemListSetDto);
    }

    @PostMapping("/wishSellerShopListSearch")
    @ApiOperation(value = "찜 샐러샵 조회", response = WishSellerShopListGetDto.class)
    public ResponseObject<List<WishSellerShopListGetDto>> wishSellerShopListSearch(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestBody WishSellerShopListSetDto wishSellerShopListSetDto
    ) throws Exception {
        return wishItemService.wishSellerShopListSearch(wishSellerShopListSetDto);
    }

    @PostMapping("/wishListSearch")
    @ApiOperation(value = "찜한 상품/찜한 샐러샵 조회", response = WishListGetDto.class)
    public ResponseObject<WishListGetDto> wishSellerShopListSearch(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestBody WishListSetDto wishInfoListSetDto
    ) throws Exception {
        wishInfoListSetDto.setSiteType(siteType);
        return wishItemService.getWishItemSellerShopList(wishInfoListSetDto);
    }

    @PostMapping("/wishSellerShopModify")
    @ApiOperation(value = "찜 샐러샵 등록/수정", response = WishSellerShopListGetDto.class)
    public ResponseObject<ResponseResult> wishSellerShopModify(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestBody WishSellerShopModifySetDto wishSellerShopModifySetDto
    ) throws Exception {
        return wishItemService.wishSellerShopModify(wishSellerShopModifySetDto);
    }
}
