package kr.co.homeplus.mypage.utils;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SetParameter {

    private String key;
    private Object value;

    public static SetParameter create(String key, Object value){
        return new SetParameter(key, value);
    }
}
