package kr.co.homeplus.mypage.benefit.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@ApiModel(description = "마이페이지 > 서브메인 > 쿠폰/BenefitMhcCardInfoDto/마일리지 조회")
public class BenefitSubMainDto {

    @ApiModelProperty(notes = "사용가능한쿠폰수", required = true, position = 1)
    private Integer couponCnt;

    @ApiModelProperty(notes = "마이홈플러스_보유포인트", required = true, position = 2)
    private Long mhcOwnPoint;

    @ApiModelProperty(notes = "마이홈플러스_가용포인트", required = true, position = 3)
    private Long mgcAvlPoint;

    @ApiModelProperty(notes = "마일리지금액", required = true, position = 4)
    private Long mileageAmt;

    @ApiModelProperty(notes = "MHC카드번호", required = true, position = 5)
    private String mhcCardNo;

    @ApiModelProperty(notes = "성공유무(1:성공,0:실패)[쿠폰,MHC,마일리지,MHC카드]", required = true, position = 5)
    private String responseCode;

}
