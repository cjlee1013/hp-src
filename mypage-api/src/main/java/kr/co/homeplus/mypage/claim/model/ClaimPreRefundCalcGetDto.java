package kr.co.homeplus.mypage.claim.model;


import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClaimPreRefundCalcGetDto {

    @ApiModelProperty(value = "응답코드", position = 1)
    private int returnValue;

    @ApiModelProperty(value = "응답메시지", position = 2)
    private String returnMsg;

    @ApiModelProperty(value = "주문금", position = 3)
    private long purchaseAmt;

    @ApiModelProperty(value = "상품금액", position = 4)
    private long itemPrice;

    @ApiModelProperty(value = "배송금액", position = 5)
    private long shipPrice;

    @ApiModelProperty(value = "도서산간배송금액", position = 6)
    private long islandShipPrice;

    @ApiModelProperty(value = "할인금액", position = 7)
    private long discountAmt;

    @ApiModelProperty(value = "상품할인금액", position = 8)
    private long itemDiscountAmt;

    @ApiModelProperty(value = "장바구니할인금액", position = 9)
    private long cartDiscountAmt;

    @ApiModelProperty(value = "배송비할인금액", position = 10)
    private long shipDiscountAmt;

    @ApiModelProperty(value = "추가배송비", position = 11)
    private long addShipPrice;

    @ApiModelProperty(value = "추가도서산간배송비", position = 12)
    private long addIslandShipPrice;

    @ApiModelProperty(value = "임직원할인금액", position = 13)
    private long empDiscountAmt;

    @ApiModelProperty(value = "카드할인금액", position = 14)
    private long cardDiscountAmt;

    @ApiModelProperty(value = "행사할인금액", position = 15)
    private long promoDiscountAmt;

    @ApiModelProperty(value = "환불금액", position = 16)
    private long refundAmt;

    @ApiModelProperty(value = "PG결제금액", position = 17)
    private long pgAmt;

    @ApiModelProperty(value = "DGV금액", position = 18)
    private long dgvAmt;

    @ApiModelProperty(value = "MHC금액", position = 19)
    private long mhcAmt;

    @ApiModelProperty(value = "OCB금액", position = 20)
    private long ocbAmt;

    @ApiModelProperty(value = "마일리지금액", position = 21)
    private long mileageAmt;

    @ApiModelProperty(value = "결제수단 리스트", position = 22)
    private List<ClaimPaymentType> claimPaymentTypeList;

    @ApiModelProperty(value = "총상품금액", position = 23)
    private long totItemPrice;

    @ApiModelProperty(value = "총결제금액", position = 24)
    private long totPaymentAmt;

    @ApiModelProperty(value = "사은품 정보", position = 25)
    private String giftMsg;

    @ApiModelProperty(value = "쿠폰중복금액", position = 26)
    private long addCouponDiscountAmt;
}
