package kr.co.homeplus.mypage.benefit.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import kr.co.homeplus.mypage.benefit.model.BenefitSubMainDto;
import kr.co.homeplus.mypage.benefit.model.coupon.BenefitCouponDto;
import kr.co.homeplus.mypage.benefit.model.mhc.BenefitMhcCardInfoDto;
import kr.co.homeplus.mypage.benefit.model.mhc.BenefitMhcDto;
import kr.co.homeplus.mypage.benefit.model.mileage.BenefitMileageDto;
import kr.co.homeplus.mypage.benefit.model.mileage.BenefitMileageListGetDto;
import kr.co.homeplus.mypage.benefit.model.mileage.BenefitMileageListSetDto;
import kr.co.homeplus.mypage.benefit.model.mileage.BenefitMileageRegSetDto;
import kr.co.homeplus.mypage.core.exception.LogicException;
import kr.co.homeplus.mypage.enums.ExternalInfo;
import kr.co.homeplus.mypage.enums.ResponseCode;
import kr.co.homeplus.mypage.enums.SiteType;
import kr.co.homeplus.mypage.enums.SortType;
import kr.co.homeplus.mypage.external.model.LoginHeaderInfoDto;
import kr.co.homeplus.mypage.external.service.ExternalService;
import kr.co.homeplus.mypage.factory.ResponseFactory;
import kr.co.homeplus.mypage.utils.ObjectUtils;
import kr.co.homeplus.mypage.utils.RequestUtils;
import kr.co.homeplus.mypage.utils.SetParameter;
import kr.co.homeplus.mypage.utils.SqlUtils;
import kr.co.homeplus.mypage.utils.StringUtil;
import kr.co.homeplus.plus.api.support.client.header.WebHeaderKey;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class BenefitService {
    // 외부 통신 Service
    private final ExternalService externalService;

    /**
     * 마일리지 연동
     * 사용자 마일리지 내역 조회
     *
     * @return 마일리지 조회결과
     * @throws Exception 오류발생시 지정된 Exception으로 처리.
     */
    public ResponseObject<BenefitMileageDto> getUserMileageCheck() throws Exception {
        try {
            return ResponseFactory.getResponseObject(
                ResponseCode.BENEFIT_MILEAGE_SUCCESS,
                externalService.getTransfer(
                    ExternalInfo.MILEAGE_API_MILEAGE_AMOUNT_CHECK,
                    new ArrayList<SetParameter>(){{ add(SetParameter.create("userNo", externalService.getUserNo())); }},
                    BenefitMileageDto.class
                )
            );
        } catch (LogicException le) {
            log.error(" 마일리지 포인트 조회 실패 ::: [{} / {}]", le.getResponseCode(), le.getResponseMessage());
            throw new LogicException(ResponseCode.BENEFIT_MILEAGE_ERR_01);
        }
    }

    /**
     * 마일리지 연동
     * 마일리지 등록
     *
     * @return 마일리지 조회결과
     * @throws Exception 오류발생시 지정된 Exception으로 처리.
     */
    public ResponseObject<String> addMileageCouponRegInfo(BenefitMileageRegSetDto setDto) throws Exception {
        try {
            this.checkMileageCouponNo(setDto.getMileageCouponNo());
            return ResponseFactory.getResponseObject(
                ResponseCode.BENEFIT_MILEAGE_COUPON_REG_SUCCESS,
                externalService.postTransfer(
                    ExternalInfo.MILEAGE_API_MILEAGE_COUPON_REG,
                    this.createMileageCouponRegDto(setDto.getMileageCouponNo()),
                    String.class
                )
            );
        } catch (LogicException le) {
            log.error(" 마일리지 등록 실패 ::: [{} / {}]", le.getResponseCode(), le.getResponseMessage());
            throw new LogicException(le.getResponseCode(), le.getResponseMessage());
        }
    }

    /**
     * 마일리지 연동
     * 마일리지 적립/차감 내역 조회
     *
     * @param listSetDto 마일리지 적립/차감내역 조회 파라미터
     * @return 마일리지 적립/차감 내역 리스트
     * @throws Exception 오류발생시 지정된 Exception으로 처리.
     */
    public ResponseObject<List<BenefitMileageListGetDto>> getUserMileageList( BenefitMileageListSetDto listSetDto) throws Exception {
        try{
            ResponseObject<List<BenefitMileageListGetDto>> result =
                externalService.postListResponse(ExternalInfo.MILEAGE_API_MILEAGE_LIST, externalService.addParameter(ObjectUtils.getConvertMap(listSetDto)), BenefitMileageListGetDto.class);

            return ResponseFactory.getResponseObject(
                ResponseCode.BENEFIT_MILEAGE_SUCCESS,
                result.getData(),
                result.getPagination()
            );

        } catch (LogicException le) {
            log.error(" 마일리지 적립/차감내역 조회 실패 ::: [{} / {}]", le.getResponseCode(), le.getResponseMessage());
            throw new LogicException(ResponseCode.BENEFIT_MILEAGE_ERR_02);
        }
    }

    /**
     * Escrow연동
     * 마이홈프러스포인트 잔액 조회
     *
     * @return 마이홈플러스포인트 잔액 조회 결과
     * @throws Exception 오류발생시 지정된 Exception으로 처리.
     */
    public ResponseObject<BenefitMhcDto> getUserMhcPoint() throws Exception {
        try {
            // MHC대체카드번호 획득
            BenefitMhcCardInfoDto mhcCardInfo = externalService.getTransfer( ExternalInfo.USER_API_USER_INFO, new ArrayList<SetParameter>(){{ add(SetParameter.create("userNo", externalService.getUserNo())); }}, BenefitMhcCardInfoDto.class );

            if(mhcCardInfo == null || StringUtil.isEmpty(mhcCardInfo.getMhcCardnoHmac())){
                throw new LogicException(ResponseCode.BENEFIT_MHC_CARD_ERR_01);
            }

            BenefitMhcDto benefitMhcInfo = externalService.postTransfer(
                ExternalInfo.ESCROW_API_MHC_POINT_CHECK,
                new HashMap<>(){{ put("mhcCardNoHmac", mhcCardInfo.getMhcCardnoHmac()); }},
                BenefitMhcDto.class
            );

            if(!benefitMhcInfo.getReturnCode().equalsIgnoreCase("SUCCESS") && !benefitMhcInfo.getReturnCode().equals("0000")){
                throw new LogicException(ResponseCode.BENEFIT_MHC_ERR_01);
            }
            return ResponseFactory.getResponseObject(ResponseCode.BENEFIT_MHC_SUCCESS, benefitMhcInfo);
        } catch (LogicException le) {
            log.error("마이 홈플러스 포인트 조회 실패 ::: [{} / {}]", le.getResponseCode(), le.getResponseMessage());
            throw new LogicException(ResponseCode.BENEFIT_MHC_ERR_01);
        }
    }


    /**
     * 사용가능 한 쿠폰 건수 조회
     *
     * @param siteType 사이트타입
     * @return 사용 가능한 쿠폰 건수
     * @throws Exception 오류발생시 지정된 Exception으로 처리.
     */
    public ResponseObject<Integer> getUserCouponCount(SiteType siteType) throws Exception {
        try {
            return ResponseFactory.getResponseObject(
                ResponseCode.BENEFIT_COUPON_COUNT_SUCCESS,
                externalService.getTransfer(
                    ExternalInfo.FRONT_API_USER_COUPON_COUNT,
                    new ArrayList<SetParameter>(){{ add(SetParameter.create("siteType", siteType)); }},
                    RequestUtils.createHttpHeaders( new LinkedHashMap<>(){{ put(WebHeaderKey.USER_NO, externalService.getUserNo()); }} ),
                    Integer.class
                )
            );
        } catch (LogicException le) {
            log.error("사용가능 한 쿠폰 건수 조회 실패 ::: [{} / {}]", le.getResponseCode(), le.getResponseMessage());
            throw new LogicException(ResponseCode.BENEFIT_COUPON_COUNT_ERR01);
        }
    }

    /**
     * 사용자의 쿠폰내역 조회
     *
     * @param siteType 사이트타입
     * @param sortType 정렬타입
     * @return 사용자의 쿠폰내역 리스트
     * @throws Exception 오류발생시 지정된 Exception으로 처리.
     */
    public ResponseObject<List<BenefitCouponDto>> getUserCouponList(SiteType siteType, SortType sortType) throws Exception {
        try{
            return ResponseFactory.getResponseObject(
                ResponseCode.BENEFIT_COUPON_LIST_SUCCESS,
                externalService.getListTransfer(
                    ExternalInfo.FRONT_API_USER_COUPON_LIST,
                    new ArrayList<SetParameter>(){{
                        add(SetParameter.create("siteType", siteType));
                        add(SetParameter.create("sortType", sortType));
                    }},
                    RequestUtils.createHttpHeaders( new LinkedHashMap<>(){{ put(WebHeaderKey.USER_NO, externalService.getUserNo()); }} ),
                    BenefitCouponDto.class
                )
            );
        } catch (LogicException le) {
            log.error("사용자의 전체 쿠폰내역 조회 실패 ::: [{} / {}]", le.getResponseCode(), le.getResponseMessage());
            throw new LogicException(ResponseCode.BENEFIT_COUPON_LIST_ERR01);
        }
    }

    /**
     * 서브메인 조회 - 쿠폰/마이홈플러스포인트/마일리지 조회
     *
     * @param siteType 사이트타입
     * @return 쿠폰/마이홈플러스포인트/마일리지 조회
     * @throws Exception 오류발생시 지정된 Exception으로 처리.
     */
    public ResponseObject<BenefitSubMainDto> getSubMainSearch(SiteType siteType) throws Exception {
        ResponseCode responseCode = ResponseCode.BENEFIT_SUB_MAIN_SUCCESS;
        Integer couponCnt;
        BenefitMhcDto benefitMhcDto;
        BenefitMileageDto mileageDto;
        BenefitMhcCardInfoDto mhcCardInfoDto;
        Boolean[] isTrans = new Boolean[]{Boolean.TRUE, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE};

        try {
            couponCnt = this.getUserCouponCount(siteType).getData();
        } catch (Exception e) {
            isTrans[0] = Boolean.FALSE;
            couponCnt = 0;
        }

        try {
            benefitMhcDto = this.getUserMhcPoint().getData();
        } catch (Exception e) {
            isTrans[1] = Boolean.FALSE;
            benefitMhcDto = BenefitMhcDto.builder().totAvlPoint(0L).totOwnPoint(0L).build();
        }

        try {
            mileageDto = this.getUserMileageCheck().getData();
        } catch (Exception e) {
            isTrans[2] = Boolean.FALSE;
            mileageDto = BenefitMileageDto.builder().expiredMileageAmt(0L).mileageAmt(0L).build();
        }

        try {
            mhcCardInfoDto = this.getMhcCardInfoSearch().getData();

            if(mhcCardInfoDto.getUnionYn().equals("Y")){
                if(mhcCardInfoDto.getMhcCardnoHmac() == null){
                    isTrans[3] = Boolean.FALSE;
                    mhcCardInfoDto = null;
                }
            } else {
                isTrans[3] = Boolean.FALSE;
                isTrans[4] = Boolean.FALSE;
            }

        } catch (Exception e) {
            isTrans[3] = Boolean.FALSE;
            mhcCardInfoDto = null;
        }

        return ResponseFactory.getResponseObject(
            responseCode,
            BenefitSubMainDto.builder()
                .couponCnt(couponCnt)
                .mgcAvlPoint(benefitMhcDto.getTotAvlPoint())
                .mhcOwnPoint(benefitMhcDto.getTotOwnPoint())
                .mileageAmt(mileageDto.getMileageAmt())
                .mhcCardNo(mhcCardInfoDto != null ? getRkmDec(mhcCardInfoDto.getMhcCardnoEnc()) : "-")
                .responseCode(Arrays.stream(isTrans).map(aBoolean -> aBoolean ? "1" : "0").collect(Collectors.joining("")))
                .build()
        );
    }

    public ResponseObject<BenefitMhcCardInfoDto> getMhcCardInfoSearch() throws Exception {
        try {
            return ResponseFactory.getResponseObject(
                ResponseCode.BENEFIT_MHC_CARD_SUCCESS,
                externalService.getTransfer( ExternalInfo.USER_API_USER_INFO, new ArrayList<SetParameter>(){{ add(SetParameter.create("userNo", externalService.getUserNo())); }}, BenefitMhcCardInfoDto.class )
            );
        } catch (LogicException le) {
            log.error("마이 홈플러스 카드정보 조회 실패 ::: [{} / {}]", le.getResponseCode(), le.getResponseMessage());
            throw new LogicException(ResponseCode.BENEFIT_MHC_CARD_ERR_01);
        }
    }

    private String getRkmDec(String encData) throws Exception {
        return ObjectUtils.toString(externalService.postTransfer( ExternalInfo.CRYPTOKEY_API_RKM_DEC, new HashMap<>(){{ put("data", encData); }}, ResponseObject.class).getData());
    }

    private void checkMileageCouponNo(String mileageCouponNo) throws Exception {
        if(mileageCouponNo.length() > 2  && mileageCouponNo.length() < 20){
            Pattern couponPatternCheck1 = Pattern.compile("^[A-Z0-9#-]*$");
            if(!couponPatternCheck1.matcher(mileageCouponNo.toUpperCase(Locale.KOREA)).find()){
                throw new LogicException(ResponseCode.BENEFIT_MILEAGE_COUPON_REG_ERR01);
            }
        } else {
            throw new LogicException(ResponseCode.BENEFIT_MILEAGE_COUPON_REG_ERR02);
        }
    }

    private LinkedHashMap<String, Object> createMileageCouponRegDto(String couponNo) throws Exception {
        LoginHeaderInfoDto loginInfo = externalService.getUserInfo();
        return new LinkedHashMap<>(){{
            put("userNo", loginInfo.getUserNo());
            put("userId", loginInfo.getUserId());
            put("userNm", loginInfo.getUserNm());
            put("requestId", "MYPAGE");
            put("couponNo", couponNo);
        }};
    }

}
