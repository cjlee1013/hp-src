package kr.co.homeplus.mypage.benefit.model.coupon;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel("쿠폰 내역 리스트")
public class BenefitCouponDto {

    @ApiModelProperty(value = "쿠폰번호", position = 1, hidden = true)
    private long couponNo;

    @ApiModelProperty(value = "발급번호", position = 3)
    private long issueNo;

    @ApiModelProperty(value = "전시 쿠폰명", position = 5)
    private String displayCouponNm;

    @ApiModelProperty(value = "쿠폰종류 (1:장바구니쿠폰, 2:배송비쿠폰, 3:상품쿠폰)", position = 7)
    private String couponType;

    @ApiModelProperty(value = "할인타입(2:정액, 1:정률)", position = 9)
    private String discountType;

    @ApiModelProperty(value = "할인타입에 따른 할인금액/할인율", position = 11)
    private Integer discount;

    @ApiModelProperty(value = "쿠폰적용 최소구매금액", position = 13)
    private Integer purchaseMin;

    @ApiModelProperty(value = "적용가능 시작일시", position = 15)
    private String validStartDt;

    @ApiModelProperty(value = "적용가능 종료일시", position = 17)
    private String validEndDt;

    @ApiModelProperty(value = "적용가능여부 PC", position = 19)
    private Character applyPcYn;

    @ApiModelProperty(value = "적용가능여부 APP", position = 21)
    private Character applyAppYn;

    @ApiModelProperty(value = "적용가능여부 Mweb", position = 23)
    private Character applyMwebYn;
}
