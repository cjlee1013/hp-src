package kr.co.homeplus.mypage.user.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "마이페이지 > 회원정보관리 > 환불계좌조회")
public class RefundAccountSearchGetDto {
    @ApiModelProperty(value = "일련번호", position = 1)
    private Integer seq;

    @ApiModelProperty(value = "은행명", position = 2)
    private String bankNm;

    @ApiModelProperty(value = "은행코드", position = 3)
    private String bankCd;

    @ApiModelProperty(value = "계좌번호", position = 4)
    private String bankAccount;

    @ApiModelProperty(value = "계좌주", position = 5)
    private String bankOwner;
}
