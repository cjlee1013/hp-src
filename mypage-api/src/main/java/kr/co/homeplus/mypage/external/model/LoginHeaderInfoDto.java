package kr.co.homeplus.mypage.external.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "회원 > 로그인회원정보")
public class LoginHeaderInfoDto {

    @ApiModelProperty(value = "회원일련번호")
    private Long userNo;

    @ApiModelProperty(value = "회원아이디(마스킹)")
    private String userId;

    @ApiModelProperty(value = "회원이름(마스킹)")
    private String userNm;

    @ApiModelProperty(value = "생년월일")
    private String birth;

    @ApiModelProperty(value = "성별 - 0:미지정 1:남자 2:여자")
    private Integer gender;

    @ApiModelProperty(value = "회원상태 - 0:정상")
    private Integer userStatus;

    @ApiModelProperty(value = "본인인증 수단 - 0 미인증, 1 핸드폰")
    private Integer certificateType;

    @ApiModelProperty(value = "성인인증만료일자 - 성인인증일자 +1년")
    private String adultCertificateExpireDt;

    @ApiModelProperty(value = "회원등급")
    private String grade;

}
