package kr.co.homeplus.mypage.entity.order;

public class ShippingAddrEntity {

    // 배송주소번호
    public String shipAddrNo;

    // 번들번호
    public String bundleNo;

    // 받는사람명
    public String receiverNm;

    // 우편번호
    public String zipcode;

    // 기본주소
    public String baseAddr;

    // 상세주소
    public String detailAddr;

    // 도로명 기본주소
    public String roadBaseAddr;

    // 도로명 상세주소
    public String roadDetailAddr;

    // 배송모바일번호(휴대폰번호)
    public String shipMobileNo;

    // 점포배송 메시지코드
    public String storeShipMsgCd;

    // 점포배송 메시지
    public String storeShipMsg;

    // 택배배송 메시지코드
    public String dlvShipMsgCd;

    // 택배배송 메시지
    public String dlvShipMsg;

    // 안심번호사용여부(Y:사용,N:미사용)
    public String safetyUseYn;

    // 개인통관고유번호
    public String personClearanceNo;

    // 등록일시
    public String regDt;

    // 수정일시
    public String chgDt;

    // 수정자ID
    public String chgId;

}
