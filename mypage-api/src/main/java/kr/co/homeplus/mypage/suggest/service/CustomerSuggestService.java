package kr.co.homeplus.mypage.suggest.service;

import java.util.Collection;
import java.util.Optional;
import kr.co.homeplus.mypage.enums.ExternalInfo;
import kr.co.homeplus.mypage.external.service.ExternalService;
import kr.co.homeplus.mypage.suggest.model.CustomerSuggestRequest;
import kr.co.homeplus.mypage.suggest.model.CustomerSuggestResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
@Slf4j
public class CustomerSuggestService {

    private final ExternalService externalService;

    public CustomerSuggestResponse setCustomerSuggest(final HttpHeaders httpHeaders,
        final CustomerSuggestRequest request)
        throws Exception {
        try {
            request.setDeviceModel(Optional.ofNullable(httpHeaders.get("deviceModel"))
                .stream()
                .flatMap(Collection::stream)
                .reduce((total, s) -> total + "," + s)
                .orElse(null));
            return externalService.postTransfer(ExternalInfo.USERMNG_API_SET_CUSTOMER_SUGGEST,
                request, CustomerSuggestResponse.class);
        } catch (Exception e) {
            log.error("CustomerSuggestService Exception", e);
            throw e;
        }
    }
}
