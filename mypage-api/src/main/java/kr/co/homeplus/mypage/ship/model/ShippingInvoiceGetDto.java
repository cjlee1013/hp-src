package kr.co.homeplus.mypage.ship.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@ApiModel(description = "배송 히스토리 정보")
@AllArgsConstructor
@NoArgsConstructor
public class ShippingInvoiceGetDto {
    @ApiModelProperty(notes = "택배사")
    private String dlvNm;

    @ApiModelProperty(notes = "택배사코드")
    private String dlvCd;

    @ApiModelProperty(notes = "송장번호")
    private String invoiceNo;

    @ApiModelProperty(notes = "송장번호")
    private String status;

    @ApiModelProperty(notes = "송장번호")
    private String trackingResult;

    @ApiModelProperty(notes = "변경내역")
    private List<ShippingHistoryGetDto> historyList;

    public String getDlvCd() {
        return dlvCd;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setHistoryList(
        List<ShippingHistoryGetDto> historyList) {
        this.historyList = historyList;
    }
}
