package kr.co.homeplus.mypage.claim.model.noRcv;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "주문관리 > 상세 > 미수취등록")
public class NoRcvShipSetGto {

    @ApiModelProperty(value= "구매주문번호", position = 1)
    @NotNull(message = "구매주문번호")
    @Min(value = 1, message = "구매주문번호")
    private long purchaseOrderNo;

    @ApiModelProperty(value= "배송번호", position = 4)
    @NotNull(message = "배송번호")
    @Min(value = 1, message = "배송번호")
    private long bundleNo;

    @ApiModelProperty(value= "고객번호", position = 5)
    @NotNull(message = "고객번호")
    @Min(value = 1, message = "고객번호")
    private long userNo;

    @ApiModelProperty(value= "파트너ID", position = 6)
    private String partnerId;

    @ApiModelProperty(value= "미수취신고유형(NA:상품미도착,NR:도난의심,NT:배송조회안됨,NE:ETC)", position = 7)
    @Pattern(regexp = "NA|NR|NT|NE", message = "미수취신고유형")
    @NotNull(message = "미수취신고유형")
    private String noRcvDeclrType;

    @ApiModelProperty(value= "미수취 상세 사유", position = 8)
    private String noRcvDetailReason;

    @ApiModelProperty(value= "미수취 등록자(mypage : user_no 참조하여 구매자정보 노출,  partner : partner_id 참조하여 파트너 정보 노출,  batch : 배치작업,  admin : 사번으로 인식하여 사원정보 노출)", position = 9)
    @NotNull(message = "미수취 등록자")
    @NotEmpty(message = "미수취 등록자")
    private String regId;

}
