package kr.co.homeplus.mypage.external.service;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import kr.co.homeplus.mypage.core.exception.LogicException;
import kr.co.homeplus.mypage.enums.ExceptionCode;
import kr.co.homeplus.mypage.enums.ExternalInfo;
import kr.co.homeplus.mypage.enums.ResponseCode;
import kr.co.homeplus.mypage.external.model.LoginHeaderInfoDto;
import kr.co.homeplus.mypage.utils.ObjectUtils;
import kr.co.homeplus.mypage.utils.RequestUtils;
import kr.co.homeplus.mypage.utils.SetParameter;
import kr.co.homeplus.mypage.utils.StringUtil;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.exception.ResourceClientException;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.ResolvableType;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;

@Slf4j
@Service
@RequiredArgsConstructor
public class ExternalService {

    private final ResourceClient resourceClient;

    public LoginHeaderInfoDto getUserInfo() throws Exception {
        LoginHeaderInfoDto loginDto = getTransfer(ExternalInfo.GET_LOGIN_INFO, null, RequestUtils.createUserLoginHeaders(), LoginHeaderInfoDto.class);
        if(loginDto == null){
            throw new LogicException(ResponseCode.USER_INFO_ERR_01);
        }
        return loginDto;
    }

    /**
     * 외부연동 - METHOD.GET
     * GET방식으로 외부연동을 한다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param returnClass 응답받고자 하는 Class
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    public <T> T getTransfer(ExternalInfo apiInfo, Object parameter, Class<T> returnClass) throws Exception {
        return getResponseObject(getTransfer(apiInfo, parameter, null, createType(returnClass)));
    }

    /**
     * 외부연동 - METHOD.GET (feat. List)
     * GET방식으로 외부연동을 한다.
     * LIST 로 응답 받는다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param returnClass 응답받고자 하는 Class
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    @SuppressWarnings("unchecked")
    public <T> List<T> getListTransfer(ExternalInfo apiInfo, Object parameter, Class<T> returnClass) throws Exception {
        return (List<T>) getResponseObject(getTransfer(apiInfo, parameter, null, createListType(returnClass)));
    }

    /**
     * 외부연동 - METHOD.GET (include. header)
     * GET방식으로 외부연동을 한다.
     * HEADER 정보를 입력받아서 해당 헤더정보로 통신한다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param httpHeaders 헤더 정보
     * @param returnClass 응답받고자 하는 Class
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    public <T> T getTransfer(ExternalInfo apiInfo, Object parameter, HttpHeaders httpHeaders, Class<T> returnClass) throws Exception {
        return getResponseObject(getTransfer(apiInfo, parameter, httpHeaders, createType(returnClass)));
    }

    /**
     * 외부연동 - METHOD.GET (include. header)
     * GET방식으로 외부연동을 한다.
     * HEADER 정보를 입력받아서 해당 헤더정보로 통신한다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param httpHeaders 헤더 정보
     * @param returnClass 응답받고자 하는 Class
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    public <T> ResponseObject<T> getTransferResponse(ExternalInfo apiInfo, Object parameter, HttpHeaders httpHeaders, Class<T> returnClass) throws Exception {
        return (ResponseObject<T>) getTransfer(apiInfo, parameter, httpHeaders, createType(returnClass));
    }

    /**
     * 외부연동 - METHOD.GET (include. header / feat. List)
     * GET방식으로 외부연동을 한다.
     * HEADER 정보를 입력받아서 해당 헤더정보로 통신하며,
     * LIST 로 응답 받는다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param httpHeaders 헤더 정보
     * @param returnClass 응답받고자 하는 Class
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    @SuppressWarnings("unchecked")
    public <T> List<T> getListTransfer(ExternalInfo apiInfo, Object parameter, HttpHeaders httpHeaders, Class<T> returnClass) throws Exception {
        return (List<T>) getResponseObject(getTransfer(apiInfo, parameter, httpHeaders, createListType(returnClass)));
    }

    /**
     * 외부연동 - METHOD.GET (include. header, List / Feat. ResponseObject)
     * GET 방식으로 외부연동을 한다.
     * HEADER 정보를 입력받아서 해당 헤더정보로 통신하며,
     * ResponseObject<LIST> 로 응답 받는다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param httpHeaders 헤더 정보
     * @param returnClass 응답받고자 하는 Class
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    @SuppressWarnings("unchecked")
    public <T> ResponseObject<List<T>> getResponseTransfer(ExternalInfo apiInfo, Object parameter, HttpHeaders httpHeaders, Class<T> returnClass) throws Exception {
        return (ResponseObject<List<T>>) getResponseObject(getTransfer(apiInfo, parameter, httpHeaders, createResponseType(returnClass)));
    }

    /**
     * 외부연동 - METHOD.GET (include. header, List / Feat. ResponseObject)
     * GET 방식으로 외부연동을 한다.
     * HEADER 정보를 입력받아서 해당 헤더정보로 통신하며,
     * ResponseObject<LIST> 로 응답 받는다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param httpHeaders 헤더 정보
     * @param returnClass 응답받고자 하는 Class
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    @SuppressWarnings("unchecked")
    public <T> ResponseObject<List<T>> getResponseListTransfer(ExternalInfo apiInfo, Object parameter, HttpHeaders httpHeaders, Class<T> returnClass) throws Exception {
        return (ResponseObject<List<T>>) getTransfer(apiInfo, parameter, httpHeaders, createListType(returnClass));
    }

    /**
     * 외부연동 - METHOD.POST
     * POST 방식으로 외부연동을 한다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param returnClass 응답받고자 하는 Class
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    public <T> T postTransfer(ExternalInfo apiInfo, Object parameter, Class<T> returnClass) throws Exception {
        return getResponseObject(postTransfer(apiInfo, parameter, null, createType(returnClass)));
    }

    /**
     * 외부연동 - METHOD.POST (Feat. List)
     * POST 방식으로 외부연동을 한다.
     * LIST 로 응답 받는다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param returnClass 응답받고자 하는 Class
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    @SuppressWarnings("unchecked")
    public <T> List<T> postListTransfer(ExternalInfo apiInfo, Object parameter, Class<T> returnClass) throws Exception {
        return (List<T>) getResponseObject(postTransfer(apiInfo, parameter, null, createListType(returnClass)));
    }

    @SuppressWarnings("unchecked")
    public <T> ResponseObject<List<T>> postListResponse(ExternalInfo apiInfo, Object parameter, Class<T> returnClass) throws Exception {
        return (ResponseObject<List<T>>) postTransfer(apiInfo, parameter, null, createListType(returnClass));
    }

    /**
     * 외부연동 - METHOD.POST (include. header)
     * POST 방식으로 외부연동을 한다.
     * HEADER 정보를 입력받아서 해당 헤더정보로 통신한다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param httpHeaders 헤더 정보
     * @param returnClass 응답받고자 하는 Class
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    public <T> T postTransfer(ExternalInfo apiInfo, Object parameter, HttpHeaders httpHeaders, Class<T> returnClass) throws Exception {
        return getResponseObject(postTransfer(apiInfo, parameter, httpHeaders, createType(returnClass)));
    }

    /**
     * 이미지 업로드 외부연동 - METHOD.POST (include. header, List / Feat. ResponseObject)
     * POST 방식으로 외부연동을 한다.
     * HEADER 정보를 입력받아서 해당 헤더정보로 통신하며,
     * ResponseObject<LIST> 로 응답 받는다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param data RequestData
     * @param returnClass 응답받고자 하는 Class
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    public <T> ResponseObject<T> getImageFileInfoDtoResponseObject(ExternalInfo apiInfo, MultiValueMap<String, Object> data, Class<T> returnClass){
        return  resourceClient.postForFileUploadResponseObject(
            apiInfo.getApiId(),
            data,
            apiInfo.getUri(),
            createType(returnClass)
            );
    }

    /**
     * Get 방식 통신 모듈
     * 헤더 정보를 입력받아 해당 헤더 정보로 통신한다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param httpHeaders 헤더정보
     * @param typeReference 응답받고자 하는 클레스의 ParameterizedTypeReference
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    @SuppressWarnings("unchecked")
    private <T> ResponseObject<T> getTransfer(ExternalInfo apiInfo, Object parameter, HttpHeaders httpHeaders, ParameterizedTypeReference<ResponseObject<T>> typeReference) throws Exception {
        if(httpHeaders == null){
            httpHeaders = RequestUtils.createHttpHeaders();
        }
        try{
            if(parameter == null) {
                return resourceClient.getForResponseObject( apiInfo.getApiId(), apiInfo.getUri(), httpHeaders, typeReference );
            } else if(parameter instanceof List){
                return resourceClient.getForResponseObject( apiInfo.getApiId(), apiInfo.getUri() + StringUtil.getParameter((List<SetParameter>) parameter), httpHeaders, typeReference );
            }
            return resourceClient.getForResponseObject( apiInfo.getApiId(), apiInfo.getUri().concat("/").concat(ObjectUtils.toString(parameter)), httpHeaders, typeReference );

        } catch (ResourceClientException rce) {
            rce.printStackTrace();
            log.error("통신오류 :: {}", Arrays.toString(rce.getResponseBody()));
        }
        throw new LogicException(ExceptionCode.SYS_ERROR_CODE_9001);
    }

    /**
     * POST 방식 통신 모듈
     * 헤더 정보를 입력받아 해당 헤더 정보로 통신한다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param httpHeaders 헤더정보
     * @param typeReference 응답받고자 하는 클레스의 ParameterizedTypeReference
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    @SuppressWarnings("unchecked")
    private <T> ResponseObject<T> postTransfer(ExternalInfo apiInfo, Object parameter, HttpHeaders httpHeaders, ParameterizedTypeReference<ResponseObject<T>> typeReference) throws Exception {
        try{
            if(httpHeaders == null){
                httpHeaders = RequestUtils.createHttpHeaders();
            }
            if(parameter instanceof List){
                return resourceClient.postForResponseObject( apiInfo.getApiId(), null, apiInfo.getUri() + StringUtil.getParameter((List<SetParameter>) parameter), httpHeaders, typeReference );
            }
            return resourceClient.postForResponseObject( apiInfo.getApiId(), parameter, apiInfo.getUri(), httpHeaders, typeReference );
        } catch (ResourceClientException rce) {
            rce.printStackTrace();
            log.error("통신오류 :: {}", Arrays.toString(rce.getResponseBody()));
        }
        throw new LogicException(ExceptionCode.SYS_ERROR_CODE_9001);
    }

    private <T> ParameterizedTypeReference<ResponseObject<T>> createType(Class<T> returnClass){
        ResolvableType resolvableType = ResolvableType.forClassWithGenerics(ResponseObject.class, returnClass);
        return ParameterizedTypeReference.forType(resolvableType.getType());
    }

    private <T> ParameterizedTypeReference<ResponseObject<T>> createListType(Class<T> returnClass){
        ResolvableType resolvableListType = ResolvableType.forClassWithGenerics(List.class, returnClass);
        ResolvableType resolvableType = ResolvableType.forClassWithGenerics(ResponseObject.class, resolvableListType);
        return ParameterizedTypeReference.forType(resolvableType.getType());
    }

    private <T> ParameterizedTypeReference<ResponseObject<T>> createResponseType(Class<T> returnClass){
        ResolvableType resolvableListType = ResolvableType.forClassWithGenerics(List.class, returnClass);
        ResolvableType resolvableResponseType = ResolvableType.forClassWithGenerics(ResponseObject.class, resolvableListType);
        ResolvableType resolvableType = ResolvableType.forClassWithGenerics(ResponseObject.class, resolvableResponseType);
        return ParameterizedTypeReference.forType(resolvableType.getType());
    }

    private <T> T getResponseObject(ResponseObject<T> responseObject) throws Exception {
        // responseObject Null Check
        assert responseObject != null;
        // Return Code Check
        if(responseObject.getReturnCode().equalsIgnoreCase("0000") || responseObject.getReturnCode().equalsIgnoreCase("SUCCESS")) {
            return responseObject.getData();
        } else {
            // 정상이 아닐 경우 오류 처리.
            throw new LogicException(responseObject.getReturnCode(), responseObject.getReturnMessage());
        }
    }

    /**
     * UserNo 파라미터 추가용.
     *
     * @param parameter 입력받은 파라미터
     * @return userNo를 추가한 Map
     * @throws Exception 오류처리.
     */
    public LinkedHashMap<String, Object> addParameter(LinkedHashMap<String, Object> parameter) throws Exception {
        if(parameter == null){
            parameter = new LinkedHashMap<>();
        }
        parameter.put("userNo", getUserNo());
        return parameter;
    }

    /**
     * userNo
     * @return userNo
     * @throws Exception 오류시 오류처리.
     */
    public long getUserNo() throws Exception {
        return getUserInfo().getUserNo();
    }

}
