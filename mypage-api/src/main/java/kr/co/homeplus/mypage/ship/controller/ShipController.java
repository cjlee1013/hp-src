package kr.co.homeplus.mypage.ship.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import kr.co.homeplus.mypage.claim.model.DlvCdGetDto;
import kr.co.homeplus.mypage.enums.SiteType;
import kr.co.homeplus.mypage.ship.model.ShipMsgCdGetDto;
import kr.co.homeplus.mypage.ship.model.ShippingInvoiceGetDto;
import kr.co.homeplus.mypage.ship.service.ShipService;
import kr.co.homeplus.plus.api.support.client.header.UserDeviceType;
import kr.co.homeplus.plus.api.support.client.header.WebHeaderKey;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/ship", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(tags = "Mypage 배송관련 정보", value = "Mypage 배송관련 정보")
@Slf4j
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
}
)
@RequiredArgsConstructor
public class ShipController {

    private final ShipService shipService;

    @GetMapping("/shipMsgSearch")
    @ApiOperation(value = "배송시 요청내역 코드 조회", response = ShipMsgCdGetDto.class)
    public ResponseObject<List<ShipMsgCdGetDto>> shipMsgSearch(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType
    ) throws Exception {
        return shipService.shipMsgSearch();
    }

    @GetMapping("/dlvCdSearch")
    @ApiOperation(value = "택배사 코드 조회", response = DlvCdGetDto.class)
    public ResponseObject<List<DlvCdGetDto>> dlvCdSearch(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType
    ) throws Exception {
        return shipService.dlvCdSearch();
    }

    @GetMapping("/getClaimShipHistory")
    @ApiOperation(value = "수거/배송 내역 조회", response = ShippingInvoiceGetDto.class)
    public ResponseObject<ShippingInvoiceGetDto> dlvCdSearch(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestParam("claimType") String claimType,
        @RequestParam("claimShippingNo") long claimShippingNo
    ) throws Exception {
        return shipService.getClaimShipHistory(claimType, claimShippingNo);
    }



}
