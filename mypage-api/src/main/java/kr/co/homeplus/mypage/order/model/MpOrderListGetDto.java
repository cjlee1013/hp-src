package kr.co.homeplus.mypage.order.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Data;

@Data
@ApiModel(description = "마이페이지 > 주문/배송 - 주문조회 응답")
public class MpOrderListGetDto {

    @ApiModelProperty(value = "주문번호", position = 1)
    private long purchaseOrderNo;

    @ApiModelProperty(value = "배송번호", position = 2)
    private long bundleNo;

    @ApiModelProperty(value = "주문일시", position = 3)
    private String orderDt;

    @ApiModelProperty(value = "점포유형(HYPER, CLUB, AURORA, EXP, DS)", position = 4)
    private String storeType;

    @ApiModelProperty(value = "파트너ID", position = 5)
    private String partnerId;

    @ApiModelProperty(value = "파트너명", position = 6)
    private String partnerNm;

    @ApiModelProperty(value = "배송방법(DS_DLV:택배배송,DS_DRCT:직접배송,DS_PICK:방문수령/픽업,DS_POST:우편배송,DS_QUICK:퀵배송,TD_DLV:택배배송,TD_DRCT:직접배송,TD_PICK:방문수령,TD_POST:우편배송,TD_QUICK:퀵배송)", position = 7)
    private String shipMethod;

    @ApiModelProperty(value = "결제상태(P1:결제요청,P3:결제완료(입금확인),P4:결제실패,P8:결제철회(무통장,카드,계좌이체,휴대폰 주문취소요청))", position = 8)
    private String paymentStatus;

    @Deprecated
    @ApiModelProperty(value = "결제상태_DESC", position = 9)
    private String paymentStatusNm;

    @ApiModelProperty(value = "배송상태(NN:대기,D1:주문확인전(결제완료),D2:주문확인(상품준비중),D3:배송중,D4:배송완료,D5:구매확정)", position = 10)
    private String shipStatus;

    @Deprecated
    @ApiModelProperty(value = "배송상태_DESC", position = 11)
    private String shipStatusNm;

    @ApiModelProperty(value = "배송일시", position = 12)
    private String shipDt;

    @ApiModelProperty(value = "배송시작시간", position = 13)
    private String shipStartTime;

    @ApiModelProperty(value = "배송종료시간", position = 14)
    private String shipEndTime;

    @ApiModelProperty(value = "배송슬롯마감시간", position = 15)
    private String shipCloseTime;

    @ApiModelProperty(value = "배송타입(TD,DS,AURORA,PICK,RESERVE,MULTI,SUM)", position = 16)
    private String shipType;

    @ApiModelProperty(value = "합배송여부", position = 17)
    private String cmbnYn;

    @ApiModelProperty(value = "리뷰작성여부", position = 18)
    private String reviewYn;

    @ApiModelProperty(value = "대체여부", position = 19)
    private String substitutionOrderYn;

    @ApiModelProperty(value = "착불여부", position = 20)
    private String prepaymentYn;

    @ApiModelProperty(value = "아이템리스트", position = 21)
    private List<MpOrderItemListDto> itemList;

    @ApiModelProperty(value = "합배송아이템리스트", position = 22)
    private List<MpOrderItemListDto> combineItemList;

    @ApiModelProperty(value = "클레임정보", position = 23)
    private List<MpOrderClaimInfoDto> claimInfo;

    @ApiModelProperty(value = "선물하기정보", position = 24)
    private MpOrderPresentDto presentInfo;
}
