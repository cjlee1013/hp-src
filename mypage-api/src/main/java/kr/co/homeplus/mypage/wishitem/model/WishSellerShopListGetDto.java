package kr.co.homeplus.mypage.wishitem.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "마이페이지 > 찜 리스트 - 셀러샵 리스트")
public class WishSellerShopListGetDto {
    @ApiModelProperty(value = "셀러샵 이름", position = 1)
    private String shopNm;

    @ApiModelProperty(value = "셀러샵 URL", position = 2)
    private String shopUrl;

    @ApiModelProperty(value = "셀러샵 Profile Img", position = 3)
    private String shopProfileImg;

    @ApiModelProperty(value = "셀러샵 로고 노출 여부", position = 4)
    private String shopLogoYn;

    @ApiModelProperty(value = "셀러샵 소개", position = 5)
    private String shopInfo;

}
