package kr.co.homeplus.mypage.wishitem.model;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "마이페이지 > 홈 - 찜 상품/샐러샵 리스트")
public class WishListGetDto {

    @ApiModelProperty(value = "찜상품 리스트", position = 1)
    List<WishItemListGetDto> wishItemList;

    @ApiModelProperty(value = "찜샐러샵 리스트", position = 2)
    List<WishSellerShopListGetDto> wishSellerShopList;
}
