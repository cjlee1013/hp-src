package kr.co.homeplus.mypage.order.model.order;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "마이페이지 > 주문/배송 - 주문조회 응답(아이템정보)")
public class OrderItemDto {
    @ApiModelProperty(value = "주문번호", position = 1)
    private long purchaseOrderNo;
    @ApiModelProperty(value = "결제번호", position = 2)
    private long paymentNo;
    @ApiModelProperty(value = "상품주문번호", position = 3)
    private long orderItemNo;
    @ApiModelProperty(value = "배송번호", position = 4)
    private long bundleNo;
    @ApiModelProperty(value = "원상품번호", position = 5)
    private String orgItemNo;
    @ApiModelProperty(value = "상품번호", position = 6)
    private String itemNo;
    @ApiModelProperty(value = "상품명", position = 7)
    private String itemNm;
    @ApiModelProperty(value = "주문수량", position = 8)
    private long orderItemQty;
    @ApiModelProperty(value = "클레임완료수량", position = 9)
    private long claimItemQty;
    @ApiModelProperty(value = "클레임진행수량", position = 10)
    private long processItemQty;
    @ApiModelProperty(value = "상품금액", position = 11)
    private long itemPrice;
    @ApiModelProperty(value = "운송번호", position = 12)
    private long shipNo;
    @ApiModelProperty(value = "판매단위", position = 13)
    private int saleUnit;
    @ApiModelProperty(value = "리뷰가능여부", position = 14)
    private String reviewPossibleYn;
    @ApiModelProperty(value = "대체주문상품여부", position = 15)
    private String substitutionItemYn;
}
