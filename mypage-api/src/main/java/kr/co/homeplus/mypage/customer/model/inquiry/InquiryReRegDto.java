package kr.co.homeplus.mypage.customer.model.inquiry;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "1:1문의_재문의등록")
public class InquiryReRegDto {

    @ApiModelProperty(value = "문의관리번호", position = 1)
    private long inqryNo;

    @ApiModelProperty(value = "유저번호", position = 2)
    private long userNo;

    @ApiModelProperty(value = "재문의내용", position = 3)
    private String reInqryCntnt;
}
