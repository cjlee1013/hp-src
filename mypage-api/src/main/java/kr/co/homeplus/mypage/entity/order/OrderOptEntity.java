package kr.co.homeplus.mypage.entity.order;

public class OrderOptEntity {

    // 주문옵션번호
    public String orderOptNo;

    // 주문상품번호
    public String orderItemNo;

    // 옵션번호
    public String optNo;

    // 선택옵션1타이틀
    public String selOpt1Title;

    // 선택옵션1값
    public String selOpt1Val;

    // 선택옵션2타이틀
    public String selOpt2Title;

    // 선택옵션2값
    public String selOpt2Val;

    // 텍스트옵션1타이틀
    public String txtOpt1Title;

    // 텍스트옵션1값
    public String txtOpt1Val;

    // 텍스트옵션2타이틀
    public String txtOpt2Title;

    // 텍스트옵션2값
    public String txtOpt2Val;

    // 옵션금액 (+/-)를 가짐
    public String optPrice;

    // 옵션수량(구매자가 입력한 구매수량)
    public String optQty;

    // 옵션주문금액(상품금액+옵션금액,고객구매금액)
    public String optOrderPrice;

    // 업체옵션코드
    public String sellerOptCd;

    // 재고매수번호
    public String stockBuyNo;

    // 등록일자
    public String regDt;

}
