package kr.co.homeplus.mypage.claim.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.AllArgsConstructor;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "클레임 상세정보 응답 DTO")
public class ClaimDetailGetDto {

  @ApiModelProperty(value = "그룹 클레임 번호", position = 1)
    private long claimNo;

    @ApiModelProperty(value = "주문번호", position = 2)
    private long purchaseOrderNo;

    @ApiModelProperty(value = "신청일자", position = 3)
    private String requestDt;

    @ApiModelProperty(value="완료일자", position = 4)
    public String completeDt;

    @ApiModelProperty(value = "신청사유", position = 5)
    public String claimReasonType;

    @ApiModelProperty(value = "신청사유상세", position = 6)
    public String claimReasonDetail;

    @ApiModelProperty(value="클레임 보류 사유코드", position = 7)
    public String pendingReasonType;

    @ApiModelProperty(value="클레임 보류 상세사유", position = 8)
    public String pendingReasonDetail;

    @ApiModelProperty(value="클레임 거절 사유코드", position = 9)
    public String rejectReasonType;

    @ApiModelProperty(value="클레임 거절 상세사유", position = 10)
    public String rejectReasonDetail;

    @ApiModelProperty(value="첨부파일명", position = 11)
    public String uploadFileName;

    @ApiModelProperty(value="첨부파일명2", position = 12)
    public String uploadFileName2;

    @ApiModelProperty(value="첨부파일명3", position = 13)
    public String uploadFileName3;

    @ApiModelProperty(value = "클레임타입", position = 14)
    private String claimType;

    @ApiModelProperty(value = "클레임타입코드", position = 15)
    private String claimTypeCode;

    @ApiModelProperty(value = "클레임 반품수거 번호", position = 16)
    private String claimPickShippingNo;

    @ApiModelProperty(value = "클레임 교환배송 번호", position = 17)
    private String claimExchShippingNo;

    @ApiModelProperty(value = "반품수거상태", position = 18)
    private String pickStatus;

    @ApiModelProperty(value = "교환배송상태", position = 19)
    private String exchStatus;

    @ApiModelProperty(value = "배송비 동봉여부", position = 20)
    private String claimShipFeeEnclose;

    @ApiModelProperty(value = "교환배송 완료일", position = 21)
    private String exchD3Dt;

    @ApiModelProperty(value = "수거 완료일", position = 22)
    private String pickP3Dt;

    @ApiModelProperty(value = "반품수거방법", position = 23)
    private String pickShipType;

    @ApiModelProperty(value = "반품수거 택배사 코드", position = 24)
    private String pickDlvCd;

    @ApiModelProperty(value = "반품수거 송장번호", position = 25)
    private String pickInvoiceNo;

    @ApiModelProperty(value = "교환배송방법", position = 26)
    private String exchShipType;

    @ApiModelProperty(value = "교환배송 택배사 코드", position = 27)
    private String exchDlvCd;

    @ApiModelProperty(value = "교환배송 송장번호", position = 28)
    private String exchInvoiceNo;

    @ApiModelProperty(value="클레임 상품 리스트", position = 29)
    private List<ClaimItemListDto> claimItemList;

    @ApiModelProperty(value="클레임 환불정보", position = 30)
    public ClaimRegisterPreRefundInfoDto claimPreRefundInfo;

    @ApiModelProperty(value="환불수단 리스트", position = 31)
    public List<ClaimRefundType> claimRefundTypeList;

    @ApiModelProperty(value = "사은품정보", position = 32)
    private List<ClaimPurchaseGift> orderGiftList;
}
