package kr.co.homeplus.mypage.benefit.model.mileage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "외부연동 > 마이페이지 > 마일리지 적립/차감 내역")
public class BenefitMileageListGetDto {
    @ApiModelProperty(value = "적립/차감일", position = 1)
    private String issueDt;
    @ApiModelProperty(value = "적립/차감타이틀", position = 2)
    private String mileageTitle;
    @ApiModelProperty(value = "마일리지메시지", position = 3)
    private String mileageMessage;
    @ApiModelProperty(value = "거래번호", position = 4)
    private String tradeNo;
    @ApiModelProperty(value = "적립/차감금액", position = 5)
    private int mileageAmt;
    @ApiModelProperty(value = "적립/차감구분", position = 6)
    private String mileageUseType;
}
