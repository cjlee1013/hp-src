package kr.co.homeplus.mypage.order.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Min;
import lombok.Data;

@Data
@ApiModel(description = "마일페이지 > 주문/배송조회 - 배송조회 리스트")
public class MpOrderListSetDto {

    @ApiModelProperty(value = "조회시작일", reference = "YYYY-MM-DD", required = true, position = 1)
    private String schStartDt;

    @ApiModelProperty(value = "조회종료일", reference = "YYYY-MM-DD", required = true, position = 2)
    private String schEndDt;

    @ApiModelProperty(value = "유저번호", hidden = true, position = 3)
    private long userNo;

    @ApiModelProperty(value = "페이지", position = 4)
    @Min(value = 1, message = "페이지")
    private Integer page;

    @ApiModelProperty(value = "페이지조회수", position = 5)
    @Min(value = 5, message = "페이지조회수")
    private Integer perPage;

    @JsonIgnore
    @ApiModelProperty(value = "사이트타입", position = 6)
    private String siteType;

}
