package kr.co.homeplus.mypage.order.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import kr.co.homeplus.mypage.order.model.ship.MpOrderShipInfoDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "마이페이지 > 주문/배송 - 주문 상세")
public class MpOrderDetailListGetDto {

    @ApiModelProperty(value = "주문번호", position = 1)
    private long purchaseOrderNo;

    @ApiModelProperty(value = "주문일자", position = 2)
    private String orderDt;

    @ApiModelProperty(value = "합배송여부", position = 3)
    private String cmbnYn;

    @ApiModelProperty(value = "대체주문여부", position = 4)
    private String substitutionOrderYn;

    @ApiModelProperty(value = "배송정보", position = 5)
    private List<MpOrderShipInfoDto> shipInfo;

    @ApiModelProperty(value = "상품상세리스트", position = 6)
    private List<MpOrderProductListGetDto> orderDetailInfo;

    @ApiModelProperty(value = "합배송상품리스트", position = 6)
    private List<MpOrderProductListGetDto> combineDetailList;

    @ApiModelProperty(value = "결제정보", position = 7)
    private MpOrderPaymentListDto orderPaymentInfo;

    @ApiModelProperty(value = "결제정보(합배송)", position = 8)
    private MpOrderPaymentListDto combinePaymentInfo;

    @ApiModelProperty(value = "사은품정보", position = 9)
    private List<MpOrderGiftListDto> orderGiftList;

    @ApiModelProperty(value = "클레임정보", position = 10)
    private List<MpOrderDetailClaimDto> claimInfoList;

    @Data
    public static class MpOrderDetailClaimDto {
        @ApiModelProperty(value = "번들번호")
        private long bundleNo;
        @ApiModelProperty(value = "다중번들번호")
        private long multiBundleNo;
        @ApiModelProperty(value = "클레임가능여부")
        private String claimYn;
    }
}
