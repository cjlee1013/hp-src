package kr.co.homeplus.mypage.order.model.ticket;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "E-티켓 재발송 DTO")
public class MpOrderTicketReSenderSetDto {
    @ApiModelProperty(value = "주문번호", position = 1)
    private long purchaseOrderNo;
    @JsonIgnore
    @ApiModelProperty(value = "고객번호", position = 2)
    private long userNo;
    @ApiModelProperty(value = "티켓주문번호", position = 3)
    private long orderTicketNo;
}
