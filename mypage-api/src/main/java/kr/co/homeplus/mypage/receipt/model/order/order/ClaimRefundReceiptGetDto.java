package kr.co.homeplus.mypage.receipt.model.order.order;

import io.swagger.annotations.ApiModel;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "주문/배송 - 환불내역 조회 응답 DTO")
public class ClaimRefundReceiptGetDto {
    private String cancelCompleteDt;
    private List<ClaimRefundDetailReceiptDto> cancelPaymentDetail;
    private Long cancelCompleteAmt;
    private Long purchaseOrderNo;
    private Long claimNo;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ClaimRefundDetailReceiptDto {
        private String paymentType;
        private Long paymentAmt;
    }
}
