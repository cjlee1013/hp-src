package kr.co.homeplus.mypage.customer.model.review;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "리뷰 조회")
public class ReviewSearchSetDto {
    @ApiModelProperty(value = "페이지")
    private Integer page;
    @ApiModelProperty(value = "페이지당View")
    private Integer perPage;
    @ApiModelProperty(value = "유저번호", hidden = true)
    private Long userNo;
    @ApiModelProperty(value = "사이트타입", hidden = true)
    private String siteType;
}
