package kr.co.homeplus.mypage.wishitem.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Pattern;
import kr.co.homeplus.mypage.enums.SiteType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "마이페이지 > 홈 - 찜 상품/샐러 리스트")
public class WishListSetDto {
    @ApiModelProperty(value = "사이트구분 - HOME,CLUB", position = 1)
    @Pattern(regexp = "(HOME|CLUB)", message="사이트구분")
    private SiteType siteType;

}
