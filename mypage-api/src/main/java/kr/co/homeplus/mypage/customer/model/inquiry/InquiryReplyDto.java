package kr.co.homeplus.mypage.customer.model.inquiry;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel(description = "1:1정보조회용(재문의정보)")
public class InquiryReplyDto {
    @ApiModelProperty(value = "재문의내용")
    private String reInqryCntnt;
    @ApiModelProperty(value = "재문의등록일자")
    private String reInqryDt;
    @ApiModelProperty(value = "재문의답변내용")
    private String reInqryAnswrCntnt;
    @ApiModelProperty(value = "재문의답변등록일자")
    private String reInqryAnswrDt;
}
