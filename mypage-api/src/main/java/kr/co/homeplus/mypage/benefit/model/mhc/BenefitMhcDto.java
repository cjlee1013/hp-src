package kr.co.homeplus.mypage.benefit.model.mhc;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "BenefitMhcCardInfoDto 포인트 조회")
public class BenefitMhcDto {

    @ApiModelProperty(notes = "결과코드", required = true, position = 1)
    private String returnCode;

    @ApiModelProperty(notes = "결과메시지", required = true, position = 2)
    private String returnMessage;

    @ApiModelProperty(notes = "총보유포인트", required = true, position = 3)
    private Long totOwnPoint;

    @ApiModelProperty(notes = "총가용포인트", required = true, position = 4)
    private Long totAvlPoint;

}
