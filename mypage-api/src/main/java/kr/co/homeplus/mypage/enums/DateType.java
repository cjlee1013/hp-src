package kr.co.homeplus.mypage.enums;

public enum DateType {
    YEAR, MONTH, DAY, HOUR, MIN, SEC
}
