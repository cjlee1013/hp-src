package kr.co.homeplus.mypage.customer.model.inquiry;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel(description = "1:1정보조회용.")
public class InquiryInfoDto {
    @ApiModelProperty(value = "1:1문의관리번호")
    private long inqryNo;
    @ApiModelProperty(value = "문의구분1")
    private String inqryCategory;
    @ApiModelProperty(value = "문의구분2")
    private String inqryType;
    @ApiModelProperty(value = "문의구분3")
    private String inqryDetailType;
    @ApiModelProperty(value = "문의제목")
    private String inqryTitle;
    @ApiModelProperty(value = "문의내용")
    private String inqryCntnt;
    @ApiModelProperty(value = "상품/주문연결여부")
    private String inqryRelYn;
    @ApiModelProperty(value = "답변여부")
    private String inqryAnswrYn;
    @ApiModelProperty(value = "재문의여부")
    private String reInqryYn;
    @ApiModelProperty(value = "재문의답변여부")
    private String reInqryAnswrYn;
    @ApiModelProperty(value = "문의등록일")
    private String inqryRegDt;
    @ApiModelProperty(value = "문의답변내용")
    private String inqryAnswrCntnt;
    @ApiModelProperty(value = "문의답변일자")
    private String inqryAnswrDt;
    @ApiModelProperty(value = "업로드이미지1")
    private String inqryImg1;
    @ApiModelProperty(value = "업로드이미지2")
    private String inqryImg2;
    @ApiModelProperty(value = "업로드이미지3")
    private String inqryImg3;
}
