package kr.co.homeplus.mypage.constants;

public class ResourceRouteName {

    // user api
    public static final String ESCROW = "escrow";

    // mileage api
    public static final String MILEAGE = "mileage";

    // shipping api
    public static final String SHIPPING = "shipping";

    // claim api
    public static final String CLAIM = "claim";

}
