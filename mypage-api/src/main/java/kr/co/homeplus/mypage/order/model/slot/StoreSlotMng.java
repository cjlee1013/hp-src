package kr.co.homeplus.mypage.order.model.slot;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "주문서 slot")
@Getter
@Setter
public class StoreSlotMng {

    @ApiModelProperty(value = "slot 상태(ACTIVE:주문가능,END:마감,FEW:마감임박,READY:준비중)", position = 1)
    private String slotStatus;

    @ApiModelProperty(value = "배송일", position = 2)
    private String shipDt;

    @ApiModelProperty(value = "배송요일(1:일요일,2:월요일~7:토요일)", position = 2)
    @JsonIgnore
    private String shipWeekday;

    @ApiModelProperty(value = "shift ID", position = 3)
    private String shiftId;

    @ApiModelProperty(value = "slot ID", position = 4)
    private String slotId;

    @ApiModelProperty(value = "slot배송시작시간", position = 5)
    private String slotShipStartTime;

    @ApiModelProperty(value = "slot배송종료시간", position = 6)
    private String slotShipEndTime;

    @ApiModelProperty(value = "shift마감시간", position = 7)
    private String shiftCloseTime;

    @ApiModelProperty(value = "slot배송마감시간", position = 8)
    private String slotShipCloseTime;

    @ApiModelProperty(value = "사용여부", position = 9)
    @JsonIgnore
    private String useYn;

    @ApiModelProperty(value = "점포 휴무 이미지", position = 10)
    private String closeImgUrl;

    /* 매장배송 */
    @ApiModelProperty(value = "주문건수", position = 11)
    @JsonIgnore
    private int orderCnt;

    @ApiModelProperty(value = "주문변경건수", position = 12)
    @JsonIgnore
    private int orderChgCnt;

    /* 매장픽업 */
    @ApiModelProperty(value = "픽업주문예약건수", position = 13)
    @JsonIgnore
    private int pickupOrderCnt;

    @ApiModelProperty(value = "픽업주문변경건수", position = 14)
    @JsonIgnore
    private int pickupOrderChgCnt;

    @ApiModelProperty(value = "사물함주문예약건수", position = 15)
    @JsonIgnore
    private int lockerOrderCnt;

    @ApiModelProperty(value = "사물함주문변경건수", position = 16)
    @JsonIgnore
    private int lockerOrderChgCnt;

    @ApiModelProperty(value = "사전예약주문고정건수")
    @JsonIgnore
    private int reserveOrderFixCnt;

    @ApiModelProperty(value = "사전예약주문변경건수")
    @JsonIgnore
    private int reserveOrderChgCnt;

    @ApiModelProperty(value = "픽업장소번호", position = 17)
    private String placeNo;

    @ApiModelProperty(value = "픽업락커사용여부", position = 18)
    private String lockerUseYn;

    @ApiModelProperty(value = "픽업운영시간주문가능여부", position = 19)
    @JsonIgnore
    private String pickupTimeYn;

    @ApiModelProperty(value = "원거리배송가능여부", position = 20)
    @JsonIgnore
    private String remoteShipYn;

    @ApiModelProperty(value = "배송방법", position = 20)
    private String shipMethod;

    /** 화면에서 필요함 */
    @ApiModelProperty(value = "가장빠른슬롯여부", position = 21)
    @JsonIgnore
    private boolean isFirstSlot;
}