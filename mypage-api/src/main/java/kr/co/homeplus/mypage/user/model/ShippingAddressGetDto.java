package kr.co.homeplus.mypage.user.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "마이페이지 > 회원정보관리 > 회원배송지목록조회")
public class ShippingAddressGetDto {
    @ApiModelProperty(value = "기본배송지", position = 1)
    private ShippingAddressDto basicShippingAddress;
    @ApiModelProperty(value = "배송지목록", position = 1)
    private List<ShippingAddressDto> shippingAddressList;
}
