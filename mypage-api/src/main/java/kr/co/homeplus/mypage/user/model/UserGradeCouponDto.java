package kr.co.homeplus.mypage.user.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "고객 등급별 쿠폰 리스트 DTO")
public class UserGradeCouponDto {
    @ApiModelProperty(value = "등급번호", position = 1)
    private Integer gradeSeq;
    @ApiModelProperty(value = "등급명", position = 2)
    private String gradeNm;
    @ApiModelProperty(value = "쿠폰정보", position = 3)
    private List<UserGradeCouponInfoDto> couponInfoList;
}
