package kr.co.homeplus.mypage.customer.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "상품 문의")
public class CustomerItemInquiryDto {

    @ApiModelProperty(value = "제목", position = 1)
    private String title;

    @ApiModelProperty(value = "내용", position = 2)
    private String description;

    @ApiModelProperty(value = "구분", position = 3)
    private String category;

    @ApiModelProperty(value = "상품번호", position = 4)
    private String itemNo;

    @ApiModelProperty(value = "상품명", position = 5)
    private String itemNm;

}
