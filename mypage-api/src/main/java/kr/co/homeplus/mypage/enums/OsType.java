package kr.co.homeplus.mypage.enums;

import lombok.Getter;

public enum OsType {

    ANDROID("Android"),
    IOS("iOS");

    @Getter
    private final String caseName;

    OsType(String caseName) {
        this.caseName = caseName;
    }
}
