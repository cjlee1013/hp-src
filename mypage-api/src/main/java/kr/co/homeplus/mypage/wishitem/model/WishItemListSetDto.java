package kr.co.homeplus.mypage.wishitem.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Pattern;
import kr.co.homeplus.mypage.enums.SiteType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "마이페이지 > 찜 리스트 - 상품 찜 리스트")
public class WishItemListSetDto {

    @ApiModelProperty(value = "사이트구분 - HOME,CLUB", position = 3)
    @Pattern(regexp = "(HOME|CLUB)", message="사이트구분")
    private SiteType siteType;

    @ApiModelProperty(value = "페이지넘버", position = 4)
    private Integer page;

    @ApiModelProperty(value = "페이지리스트수", position = 5)
    private Integer perPage;

}
