package kr.co.homeplus.mypage.claim;

import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.mypage.claim.model.ClaimAddImageGetDto;
import kr.co.homeplus.mypage.claim.model.ClaimChgPaymentRefundSetDto;
import kr.co.homeplus.mypage.claim.model.ClaimRequestChangeStatusSetDto;
import kr.co.homeplus.mypage.claim.model.ClaimSearchListGetDto;
import kr.co.homeplus.mypage.claim.model.ClaimSearchListSetDto;
import kr.co.homeplus.mypage.claim.model.noRcv.NoRcvShipEditDto;
import kr.co.homeplus.mypage.claim.model.noRcv.NoRcvShipInfoGetDto;
import kr.co.homeplus.mypage.claim.model.noRcv.NoRcvShipInfoSetDto;
import kr.co.homeplus.mypage.claim.model.noRcv.NoRcvShipSetGto;
import kr.co.homeplus.mypage.claim.model.register.ClaimRegSetDto;
import kr.co.homeplus.mypage.core.exception.LogicException;
import kr.co.homeplus.mypage.enums.ExternalInfo;
import kr.co.homeplus.mypage.enums.ResponseCode;
import kr.co.homeplus.mypage.external.service.ExternalService;
import kr.co.homeplus.mypage.factory.ResponseFactory;
import kr.co.homeplus.mypage.order.model.MpOrderGiftItemListDto;
import kr.co.homeplus.mypage.utils.ObjectUtils;
import kr.co.homeplus.mypage.utils.SetParameter;
import kr.co.homeplus.mypage.utils.UploadFileUtil;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@Service
@RequiredArgsConstructor
public class ClaimService {

    private final ExternalService externalService;


    /**
     * 취소/반품/교환 조회 List
     * @param claimSearchListSetDto 취소/반품/교환 조회 dto
     * **/
    public ResponseObject<List<ClaimSearchListGetDto>> getClaimSearchList(ClaimSearchListSetDto claimSearchListSetDto) throws Exception {

        ResponseObject<List<ClaimSearchListGetDto>> result =
            externalService.postListResponse(
                ExternalInfo.CLAIM_API_CLAIM_SEARCH_LIST,
                externalService.addParameter(ObjectUtils.getConvertMap(claimSearchListSetDto)),
                ClaimSearchListGetDto.class);

        return ResponseFactory.getResponseObject(
           ResponseCode.SUCCESS,
           result.getData(), result.getPagination()
        );
    }

    public ResponseObject<NoRcvShipInfoGetDto> getNoRcvInfoList(NoRcvShipInfoSetDto noRcvShipInfoSetDto) throws Exception {
        noRcvShipInfoSetDto.setUserNo(externalService.getUserNo());
        try {
            return ResponseFactory.getResponseObject(
                ResponseCode.SUCCESS,
                externalService.postTransfer( ExternalInfo.CLAIM_API_NO_RCV_LIST, noRcvShipInfoSetDto, NoRcvShipInfoGetDto.class )
            );
        } catch (LogicException le) {
            log.error(" 미수취 내역 조회 에러 ::: [{} / {}]", le.getResponseCode(), le.getResponseMessage());
            throw new LogicException(ResponseCode.NO_RCV_LIST_ERR01);
        }
    }

    public ResponseObject<String> setNoRcvInfoReg(NoRcvShipSetGto noRcvShipSetGto) throws Exception {
        noRcvShipSetGto.setUserNo(externalService.getUserNo());
        try {
            return ResponseFactory.getResponseObject(
                ResponseCode.SUCCESS,
                externalService.postTransfer( ExternalInfo.CLAIM_API_NO_RCV_REG, noRcvShipSetGto, String.class )
            );
        } catch (LogicException le) {
            log.error(" 미수취 내역 등록 에러 ::: [{} / {}]", le.getResponseCode(), le.getResponseMessage());
            throw new LogicException(ResponseCode.NO_RCV_REG_ERR01);
        }
    }

    public ResponseObject<String> setNoRcvInfoModify(NoRcvShipEditDto noRcvShipEditDto) throws Exception {
        try {
            return ResponseFactory.getResponseObject(
                ResponseCode.SUCCESS,
                externalService.postTransfer( ExternalInfo.CLAIM_API_NO_RCV_MODIFY, noRcvShipEditDto, String.class )
            );
        } catch (LogicException le) {
            log.error(" 미수취 내역 수정 에러 ::: [{} / {}]", le.getResponseCode(), le.getResponseMessage());
            throw new LogicException(ResponseCode.NO_RCV_REG_ERR01);
        }
    }


    /**
     * 클레임 사진 업로드
     *
     * todo MultipartFile List 처리 가능하도록 수정예정
     * @param multipartFile
     * **/
    public ResponseObject<ClaimAddImageGetDto>  addClaimImage(MultipartFile multipartFile) throws Exception {
        if(multipartFile != null) {
            String fileExtension = FilenameUtils.getExtension(multipartFile.getOriginalFilename());
            log.info("업로드시 파일 확장자 : {}", fileExtension);
            if(fileExtension == null || "".equals(fileExtension)) {
                throw new LogicException(ResponseCode.CLAIM_IMAGE_UPLOAD_ERR01);
            } else if(!"jpg,gif".contains(fileExtension)) {
                throw new LogicException(ResponseCode.CLAIM_IMAGE_UPLOAD_ERR01);
            }
        }
        MultiValueMap<String, Object> data = new LinkedMultiValueMap<>();
        data.add("processKey", "ClaimAddImage");
        data.add("file", UploadFileUtil.getByteArrayResource(multipartFile));
        return ResponseFactory.getResponseObject(
                ResponseCode.SUCCESS,
                externalService.getImageFileInfoDtoResponseObject(
                    ExternalInfo.STORAGE_API_ADD_CLAIM_IMAGE,
                    data,
                    ClaimAddImageGetDto.class
                ).getData()
        );
    }

    /**
     * 클레임 등록
     *
     * @param claimRegSetDto
     * **/
    public ResponseObject<Long> claimRegister(ClaimRegSetDto claimRegSetDto) throws Exception {

        claimRegSetDto.setUserNo(externalService.getUserNo());
        claimRegSetDto.setRegId("MYPAGE");

       return ResponseFactory.getResponseObject(
           ResponseCode.CLAIM_REGISTER_SUCCESS,
           externalService.postTransfer(ExternalInfo.CLAIM_API_CLAIM_REGISTER, claimRegSetDto, Long.class )
       );
    }

    /**
     * 다중배송 클레임 등록
     *
     * @param claimRegSetDto
     * **/
    public ResponseObject<Long> multiClaimRegister(ClaimRegSetDto claimRegSetDto) throws Exception {

        claimRegSetDto.setUserNo(externalService.getUserNo());
        claimRegSetDto.setRegId("MYPAGE");

        return ResponseFactory.getResponseObject(
            ResponseCode.CLAIM_REGISTER_SUCCESS,
            externalService.postTransfer(ExternalInfo.CLAIM_API_MULTI_BUNDLE_CLAIM_REGISTER, claimRegSetDto, Long.class )
        );
    }

    /**
     * 환불수단 변경
     *
     * @param claimChgPaymentRefundSetDto
     * **/
    public ResponseObject<String> chgPaymentRefund (ClaimChgPaymentRefundSetDto claimChgPaymentRefundSetDto) throws Exception {
        claimChgPaymentRefundSetDto.setUserNo(externalService.getUserNo());
        return ResponseFactory.getResponseObject(
           ResponseCode.SUCCESS,
           externalService.postTransfer( ExternalInfo.CLAIM_API_CHG_PAYMENT_REFUND, claimChgPaymentRefundSetDto, String.class )
       );
    }
    /**
     * 사은품 상품 리스트 조회
     *
     * @param purchaseOrderNo
     * @param bundleNo
     * **/
    public ResponseObject<List<MpOrderGiftItemListDto>> getOrderGiftItemList (long purchaseOrderNo, long bundleNo) throws Exception {
        return ResponseFactory.getResponseObject(
           ResponseCode.SUCCESS,
           externalService.getListTransfer(
               ExternalInfo.CLAIM_API_ORDER_GIFT_ITEM_LIST,
               new ArrayList<SetParameter>()
               {
                   {
                       add(SetParameter.create("purchaseOrderNo", purchaseOrderNo));
                       add(SetParameter.create("bundleNo", bundleNo));
                   }
               },
               MpOrderGiftItemListDto.class )
       );
    }

    /**
     * 클레임 상태변경 요청
     *
     * @param claimRequestChangeStatusSetDto
     * **/
    public ResponseObject<String> chgClaimStatus(ClaimRequestChangeStatusSetDto claimRequestChangeStatusSetDto) throws Exception {
        claimRequestChangeStatusSetDto.setRegId("MYPAGE");
        claimRequestChangeStatusSetDto.setUserNo(externalService.getUserNo());

        return ResponseFactory.getResponseObject(
           ResponseCode.SUCCESS,
           externalService.postTransfer( ExternalInfo.CLAIM_API_CHG_CLAIM_STATUS, claimRequestChangeStatusSetDto, String.class )
       );
    }
}
