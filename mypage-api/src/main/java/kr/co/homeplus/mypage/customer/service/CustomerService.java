package kr.co.homeplus.mypage.customer.service;


import io.swagger.models.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;
import kr.co.homeplus.mypage.core.exception.LogicException;
import kr.co.homeplus.mypage.customer.model.QnaListGetDto;
import kr.co.homeplus.mypage.customer.model.QnaListSetDto;
import kr.co.homeplus.mypage.customer.model.QnaRegisterSetDto;
import kr.co.homeplus.mypage.customer.model.QnaReplyListGetDto;
import kr.co.homeplus.mypage.customer.model.consumer.UserRestrictDto;
import kr.co.homeplus.mypage.customer.model.inquiry.InquiryCategorySearchGetDto;
import kr.co.homeplus.mypage.customer.model.inquiry.InquiryCategorySearchSetDto;
import kr.co.homeplus.mypage.customer.model.inquiry.InquiryModifyDto;
import kr.co.homeplus.mypage.customer.model.inquiry.InquiryReRegDto;
import kr.co.homeplus.mypage.customer.model.inquiry.InquiryRegDto;
import kr.co.homeplus.mypage.customer.model.inquiry.InquirySearchGetDto;
import kr.co.homeplus.mypage.customer.model.inquiry.InquirySearchSetDto;
import kr.co.homeplus.mypage.customer.model.personal.PersonalGetDto;
import kr.co.homeplus.mypage.customer.model.personal.PersonalSetDto;
import kr.co.homeplus.mypage.customer.model.review.ReviewSearchGetDto;
import kr.co.homeplus.mypage.customer.model.review.ReviewSearchSetDto;
import kr.co.homeplus.mypage.enums.DateType;
import kr.co.homeplus.mypage.enums.ExternalInfo;
import kr.co.homeplus.mypage.enums.ResponseCode;
import kr.co.homeplus.mypage.external.service.ExternalService;
import kr.co.homeplus.mypage.factory.ResponseFactory;
import kr.co.homeplus.mypage.response.ResponseResult;
import kr.co.homeplus.mypage.utils.DateUtils;
import kr.co.homeplus.mypage.utils.RequestUtils;
import kr.co.homeplus.mypage.utils.SetParameter;
import kr.co.homeplus.plus.api.support.client.ResourceConverter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.client.model.ResponsePagination;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class CustomerService {

    private final ExternalService externalService;

    /**
     * 마이페이지 > 상품QnA > 삼품QnA 리스트
     *
     * @param qnaListSetDto 내가쓴 QnA dto
     * @return 내가쓴 QnA 조회 데이터
     * @throws Exception 오류시 오류처리
     */
    public ResponseObject<QnaListGetDto> qnaListSearch (QnaListSetDto qnaListSetDto) throws Exception {
        return externalService.getTransfer(
                ExternalInfo.FRONT_API_QNA_LIST_SEARCH,
                new ArrayList<SetParameter>() {
                    {
                        add(SetParameter.create("itemNo", qnaListSetDto.getItemNo()));
                        add(SetParameter.create("qnaStatus", qnaListSetDto.getQnaStatus()));
                        add(SetParameter.create("secretYn", qnaListSetDto.getSecretYn()));
                        add(SetParameter.create("myQnaYn", qnaListSetDto.getMyQnaYn()));
                        add(SetParameter.create("qnaType", qnaListSetDto.getQnaType()));
                        add(SetParameter.create("page", qnaListSetDto.getPage()));
                        add(SetParameter.create("perPage", qnaListSetDto.getPerPage()));
                        add(SetParameter.create("userNo", externalService.getUserNo()));
                    }
                },
            ResponseObject.class);
    }

    /**
     * 마이페이지 > 상품QnA > 삼품QnA 답변 조회
     *
     * @param qnaNo
     * @return QnA 답변 조회 데이터
     * @throws Exception 오류시 오류처리
     */
    public ResponseObject<List<QnaReplyListGetDto>> qnaReplySearch (String qnaNo) throws Exception {
        return ResponseFactory.getResponseObject(
                ResponseCode.SUCCESS,
                externalService.getListTransfer(
                    ExternalInfo.FRONT_API_QNA_REPLY_LIST_SEARCH,
                    new ArrayList<SetParameter>() {{add(SetParameter.create("qnaNo", qnaNo));}},
                    QnaReplyListGetDto.class
                )
        );
    }

    /**
     * 마이페이지 > 상품QnA > QnA 등록
     *
     * @param qnaRegisterSetDto QnA 등록 dto
     * @return QnA 등록 결과 데이터
     * @throws Exception 오류시 오류처리
     */
    public ResponseObject<ResponseResult> qnaRegister(QnaRegisterSetDto qnaRegisterSetDto) throws Exception {
        return ResponseFactory.getResponseObject(
                ResponseCode.SUCCESS,
                externalService.postTransfer(
                    ExternalInfo.FRONT_API_QNA_REGISTER,
                    qnaRegisterSetDto,
                    RequestUtils.createUserInfoHeaders(externalService.getUserNo()),
                    ResponseResult.class
                )
        );
    }

    public ResponseObject<String> addInquiryReg(InquiryRegDto inquiryRegDto) throws Exception {
        inquiryRegDto.setUserNo(externalService.getUserNo());
        if(this.isEmoji(inquiryRegDto.getInqryCntnt()) || this.isEmoji(inquiryRegDto.getInqryTitle())){
            throw new LogicException(ResponseCode.INQUIRY_CHECK_EMOJI_ERROR);
        }
        return ResponseFactory.getResponseObject( ResponseCode.SUCCESS, externalService.postTransfer( ExternalInfo.USERMNG_API_INQUIRY_REG, inquiryRegDto, String.class ) );
    }

    public ResponseObject<String> addReplyInquiryReg(InquiryReRegDto inquiryReRegDto) throws Exception {
        inquiryReRegDto.setUserNo(externalService.getUserNo());
        if(this.isEmoji(inquiryReRegDto.getReInqryCntnt())){
            throw new LogicException(ResponseCode.INQUIRY_CHECK_EMOJI_ERROR);
        }
        return ResponseFactory.getResponseObject( ResponseCode.SUCCESS, externalService.postTransfer( ExternalInfo.USERMNG_API_INQUIRY_REPLY_REG, inquiryReRegDto, String.class ) );
    }

    public ResponseObject<String> setCancelInquiryReg(InquiryModifyDto inquiryModifyDto) throws Exception {
        inquiryModifyDto.setUserNo(externalService.getUserNo());
        return ResponseFactory.getResponseObject( ResponseCode.SUCCESS, externalService.postTransfer( ExternalInfo.USERMNG_API_INQUIRY_CANCEL, inquiryModifyDto, String.class ) );
    }

    public ResponseObject<List<InquirySearchGetDto>> getInquiryInfo(InquirySearchSetDto inquirySearchSetDto) throws Exception {
        inquirySearchSetDto.setUserNo(externalService.getUserNo());
        ResponseObject<List<InquirySearchGetDto>> result = externalService.postListResponse( ExternalInfo.USERMNG_API_INQUIRY_SEARCH, inquirySearchSetDto, InquirySearchGetDto.class);
        return ResponseFactory.getResponseObject( ResponseCode.SUCCESS, result.getData(), result.getPagination());
    }

    public ResponseObject<List<InquiryCategorySearchGetDto>> getInquiryCommonCodeInfo(InquiryCategorySearchSetDto inquiryCategorySearchSetDt) throws Exception {
        ResponseObject<List<InquiryCategorySearchGetDto>> result = externalService.postListResponse( ExternalInfo.USERMNG_API_INQUIRY_COMMON_CODE_SEARCH, inquiryCategorySearchSetDt, InquiryCategorySearchGetDto.class);
        return ResponseFactory.getResponseObject( ResponseCode.SUCCESS, result.getData(), result.getPagination());
    }

    public ResponseObject<List<ReviewSearchGetDto>> getReviewPossible(ReviewSearchSetDto reviewSearchSetDto) throws Exception {
        reviewSearchSetDto.setUserNo(externalService.getUserNo());
//        // 블랙리스트 여부 판단.
//        UserRestrictDto userResult = externalService.getTransfer(ExternalInfo.USER_API_BLACK_CONSUMER_INFO, new ArrayList<SetParameter>(){{ add(SetParameter.create("userNo", reviewSearchSetDto.getUserNo())); }}, UserRestrictDto.class);
//        if(userResult != null){
//            if(userResult.getCommunityLimit() != null){
//                if(DateUtils.isAfter(userResult.getCommunityLimit().getStartDt()) && DateUtils.isBefore(userResult.getCommunityLimit().getEndDt())){
//                    throw new LogicException(ResponseCode.REVIEW_BLACK_CONSUMER_ERR);
//                }
//            }
//        }
        ResponseObject<List<ReviewSearchGetDto>> result = externalService.postListResponse( ExternalInfo.CLAIM_API_REVIEW_POSSIBLE_LIST, reviewSearchSetDto, ReviewSearchGetDto.class);
        return ResponseFactory.getResponseObject( ResponseCode.SUCCESS, result.getData(), result.getPagination());
    }
    public ResponseObject<List<PersonalGetDto>> getPersonalInfo(PersonalSetDto setDto) throws Exception {
        setDto.setUserNo(externalService.getUserNo());
        ResponseObject<List<PersonalGetDto>> personalList = new ResponseObject<>();
        switch (setDto.getPersonalType().toUpperCase(Locale.KOREA)){
            case "SHIP" :
                personalList = externalService.postListResponse(ExternalInfo.SHIPPING_API_PROVIDE_DELIVERY_INFO, setDto, PersonalGetDto.class);
                if(personalList.getData().size() > 0){
                    for(PersonalGetDto dto : personalList.getData()){
                        dto.setUseContents("성명,휴대폰번호,주소");
                        dto.setPurpose("당일배송 및 택배배송업무");
                    }
                }
                break;
            default:
                personalList = externalService.postListResponse(ExternalInfo.CLAIM_API_USING_PERSONAL_INFO_LIST, setDto, PersonalGetDto.class);
                break;
        }
        return ResponseFactory.getResponseObject( ResponseCode.SUCCESS, personalList.getData(), personalList.getPagination());
    }

    private Boolean isEmoji(String str){
        String checkPattern = "(\\u00a9|\\u00ae|[\\u2000-\\u3300]|\\ud83c[\\ud000-\\udfff]|\\ud83d[\\ud000-\\udfff]|\\ud83e[\\ud000-\\udfff])";
        return Pattern.matches(checkPattern, str);
    }

}
