package kr.co.homeplus.mypage.receipt.model.claim.purchase;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ClaimReceiptMainInfo {

    @ApiModelProperty(value = "클레임번호", position = 1)
    private long claimNo;

    @ApiModelProperty(value = "클레임일시", position = 2)
    private String claimDt;
    //    long totShipPrice;
    @ApiModelProperty(value = "총결제금액", position = 3)
    private long totPaymentAmt;

    @ApiModelProperty(value = "노출 상품명", position = 4)
    private String displayItemNm;

    @ApiModelProperty(value = "구매자명", position = 5)
    private String buyerNm;

    @ApiModelProperty(value = "주문번호", position = 6)
    private long purchaseOrderNo;

    @ApiModelProperty(value = "취소시원주문거래일시", position = 7)
    private String paymentFshDt;
}