package kr.co.homeplus.mypage.order.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(description = "마이페이지 > 메인 > 나의 주문현황")
public class MpOrderMainDto {

    @ApiModelProperty(value = "주문상태", position = 1)
    private List<MyPageMainOrderCount> orderCount;

    @ApiModelProperty(value = "주문리스트", position = 2)
    private List<MyPageMainOrderList> orderList;

    @Data
    public static class MyPageMainOrderCount {
        @ApiModelProperty(value = "배송상태", position = 1)
        private String orderStatus;
        @ApiModelProperty(value = "배송상태_건수", position = 2)
        private int orderCnt;
    }

    @Data
    public static class MyPageMainOrderList {
        @ApiModelProperty(value = "주문일자", position = 1)
        private String orderDt;
        @ApiModelProperty(value = "주문번호", position = 2)
        private long purchaseOrderNo;
        @ApiModelProperty(value = "클레임번호", position = 3)
        private long claimNo;
        @ApiModelProperty(value = "상품명", position = 4)
        private String itemNm1;
        @ApiModelProperty(value = "구매수", position = 5)
        private int orderCnt;
        @ApiModelProperty(value = "주문상태", position = 6)
        private String orderStatus;
        @ApiModelProperty(value = "클레임구분", position = 7)
        private String claimType;
        @ApiModelProperty(value = "배송타입", position = 8)
        private String shipType;
        @ApiModelProperty(value = "수거상태", position = 9)
        private String pickStatus;
        @ApiModelProperty(value = "교환배송상태", position = 10)
        private String exchStatus;
    }

}
