package kr.co.homeplus.mypage.receipt.model.order.purchase;

import io.swagger.annotations.ApiModelProperty;
import kr.co.homeplus.mypage.enums.StoreType;
import lombok.Data;

@Data
public class OrderPaymentReceiptInfo {
    @ApiModelProperty(value = "파트너아이디", position = 1)
    private String partnerId;
    @ApiModelProperty(value = "파트너명", position = 2)
    private String partnerNm;
    @ApiModelProperty(value = "점포타입", position = 3)
    private StoreType storeType;
    @ApiModelProperty(value = "승인일시", position = 4)
    private String paymentFshDt;
    @ApiModelProperty(value = "PG결제수단", position = 5)
    private String methodNm;
    @ApiModelProperty(value = "영수증유형(CARD:카드,CASH:현금,NON:비대상)", position = 6)
    private String receiptType;
    @ApiModelProperty(value = "현금영수증 용도(PERS:개인소득공제용,EVID:지출증빙용,NONE:미발행", position = 7)
    private String cashReceiptUsage;
    @ApiModelProperty(value = "대상금액", position = 8)
    private long totAmt;      // pg -> 카드전표
    @ApiModelProperty(value = "배송번호", position = 9)
    private long bundleNo;
}
