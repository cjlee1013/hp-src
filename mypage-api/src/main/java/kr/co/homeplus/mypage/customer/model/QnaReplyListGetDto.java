package kr.co.homeplus.mypage.customer.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "마이페이지 > 홈 - QnA리스트")
public class QnaReplyListGetDto {

    @ApiModelProperty(value = "Qna 답변 일련번호", position = 1)
    private String qnaReplyNo;

    @ApiModelProperty(value = "답변내용", position = 2)
    private String qnaReply;

    @ApiModelProperty(value = "등록일시", position = 3)
    private String regDt;

}
