package kr.co.homeplus.mypage.customer.model.inquiry;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Min;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@ApiModel(description = "1:1문의조회 DTO")
public class InquirySearchSetDto {

    @ApiModelProperty(value = "조회시작일", position = 1)
    private String schStartDt;

    @ApiModelProperty(value = "조회종료일", position = 2)
    private String schEndDt;

    @ApiModelProperty(value = "고객번호", position = 3, hidden = true)
    private long userNo;

    @ApiModelProperty(value = "사이트타입", position = 4, hidden = true)
    private String siteType;

    @ApiModelProperty(value = "조회페이지", position = 5)
    @Min(value = 1, message = "페이지 No 최소값")
    private Integer page;

    @ApiModelProperty(value = "페이지리스트수", position = 6)
    @Min(value = 5, message = "페이지 리스트 수 최소값")
    private Integer perPage;

}
