package kr.co.homeplus.mypage.customer.model.personal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "개인정보이용내역 응답 DTO")
public class PersonalGetDto {

    @ApiModelProperty(value = "이용일", position = 1)
    private String useDt;

    @ApiModelProperty(value = "이용항목", position = 2)
    private String useContents;

    @ApiModelProperty(value = "제공목적", position = 3)
    private String purpose;

    @ApiModelProperty(value = "위탁업체", position = 4)
    private String consignment;

}
