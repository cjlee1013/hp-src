package kr.co.homeplus.mypage.user.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "마이페이지 > 회원정보관리 > 회원배송지목록조회")
public class ShippingAddressDto {

    @ApiModelProperty(value = "일련번호", position = 1)
    private Integer seq;
    @ApiModelProperty(value = "배송지관리명", position = 2)
    private String shippingNm;
    @ApiModelProperty(value = "기본배송지여부", position = 3)
    private Boolean isBasic;
    @ApiModelProperty(value = "받는사람", position = 4)
    private String receiver;
    @ApiModelProperty(value = "휴대폰", position = 5)
    private String mobile;
    @ApiModelProperty(value = "우편번호", position = 6)
    private String zipcode;
    @ApiModelProperty(value = "주소", position = 7)
    private String addr1;
    @ApiModelProperty(value = "주소상세", position = 8)
    private String addr2;
    @ApiModelProperty(value = "지번주소", position = 9)
    private String parcelAddr1;
    @ApiModelProperty(value = "지번주소상세", position = 10)
    private String parcelAddr2;

}
