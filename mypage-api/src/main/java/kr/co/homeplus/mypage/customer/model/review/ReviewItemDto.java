package kr.co.homeplus.mypage.customer.model.review;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "작성가능리뷰-상품리스트 DTO")
public class ReviewItemDto {
    @ApiModelProperty(value = "상품주문번호", position = 1)
    private String orderItemNo;
    @ApiModelProperty(value = "상품번호", position = 2)
    private String itemNo;
    @ApiModelProperty(value = "상품명", position = 3)
    private String itemNm;
    @ApiModelProperty(value = "상점종류", position = 4)
    private String storeType;
    @ApiModelProperty(value = "상점아이디", position = 5)
    private String storeId;
}
