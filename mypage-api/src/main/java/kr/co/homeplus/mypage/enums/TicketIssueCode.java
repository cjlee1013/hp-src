package kr.co.homeplus.mypage.enums;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum  TicketIssueCode {

    ISSUE_CODE_I1("I1", "발급대기"),
    ISSUE_CODE_I2("I2", "발급중"),
    ISSUE_CODE_I3("I3", "발급완료"),
    ISSUE_CODE_I4("I4", "발급취소"),
    ISSUE_CODE_I8("I8", "발급실패"),
    ISSUE_CODE_I9("I9", "취소실패");

    private final String issueCode;
    private final String issueMsg;

    public static Map<String, String> getIssueMap(){
        return Arrays.stream(TicketIssueCode.values()).collect(Collectors.toMap(TicketIssueCode::getIssueCode, TicketIssueCode::getIssueMsg));
    }
}
