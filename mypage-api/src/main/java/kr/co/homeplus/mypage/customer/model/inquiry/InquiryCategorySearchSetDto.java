package kr.co.homeplus.mypage.customer.model.inquiry;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Pattern;
import kr.co.homeplus.mypage.enums.SiteType;
import kr.co.homeplus.mypage.enums.StoreType;
import lombok.Data;

@Data
@ApiModel(description = "1:1문의유형 코드 조회 요청")
public class InquiryCategorySearchSetDto {
    @ApiModelProperty(value = "조회구분(H:대분류,M:중분류,L:소분류)", position = 1)
    @Pattern(regexp = "H|M|L", message = "조회구분")
    private String searchType;
    @ApiModelProperty(value = "대분류코드", position = 2)
    private String firstSearchCd;
    @ApiModelProperty(value = "중분류상위코드", position = 3)
    private String secondSearchCd;
    @ApiModelProperty(value = "사이트타입", position = 4, hidden = true)
    private String siteType;
}