package kr.co.homeplus.mypage.suggest.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("개선 제안 응답")
@Data
public class CustomerSuggestResponse {

    @ApiModelProperty(value = "요청 완료 된 Sequence")
    private long customerSuggestSeq;
}
