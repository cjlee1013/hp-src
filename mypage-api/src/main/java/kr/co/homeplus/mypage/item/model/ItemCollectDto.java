package kr.co.homeplus.mypage.item.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "찜 리스트 조회 응답")
public class ItemCollectDto {

    @ApiModelProperty(value = "상품번호")
    private String itemNo;

    @ApiModelProperty(value = "상품명")
    private String itemNm1;

    @ApiModelProperty(value = "상품금액")
    private long itemPrice;

    @ApiModelProperty(value = "상품원금액")
    private long orgPrice;

}
