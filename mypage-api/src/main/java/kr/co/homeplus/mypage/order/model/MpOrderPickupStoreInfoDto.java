package kr.co.homeplus.mypage.order.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "픽업점포정보DTO")
public class MpOrderPickupStoreInfoDto {
    @ApiModelProperty(value = "장소설명", position = 1)
    private String placeExpln;
    @ApiModelProperty(value = "장소추가주소", position = 2)
    private String placeAddrExtnd;
    @ApiModelProperty(value = "이미지URL", position = 3)
    private String imgUrl;
    @ApiModelProperty(value = "시작시간", position = 4)
    private String startTime;
    @ApiModelProperty(value = "종료시", position = 5)
    private String endTime;
    @ApiModelProperty(value = "장소명", position = 6)
    private String placeNm;
    @ApiModelProperty(value = "전화번호", position = 7)
    private String telNo;
    @ApiModelProperty(value = "지점명", position = 8)
    private String storeNm;
}
