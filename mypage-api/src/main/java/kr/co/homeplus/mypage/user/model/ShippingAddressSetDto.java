package kr.co.homeplus.mypage.user.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "마이페이지 > 회원정보관리 > 회원배송지수정")
public class ShippingAddressSetDto {

    @ApiModelProperty(value = "배송지관리명", position = 1)
    @NotEmpty
    private String shippingNm;

    @ApiModelProperty(value = "기본배송지여부", position = 2)
    private Boolean isBasic;

    @ApiModelProperty(value = "받는사람", position = 3)
    @NotEmpty
    private String receiver;

    @ApiModelProperty(value = "휴대폰", position = 4)
    @NotEmpty
    @Pattern(regexp = "^01([0|1|6|7|8|9]?)-([0-9]{3,4})-([0-9]{4})$", message = "휴대폰번호 패턴이 아닙니다.")
    private String mobile;

    @ApiModelProperty(value = "우편번호", position = 5)
    @NotEmpty
    private String zipcode;

    @ApiModelProperty(value = "주소", position = 6)
    @NotEmpty
    private String addr1;

    @ApiModelProperty(value = "주소상세", position = 7)
    @NotEmpty
    private String addr2;

    @ApiModelProperty(value = "지번주소", position = 8)
    private String parcelAddr1;

    @ApiModelProperty(value = "지번주소상세", position = 9)
    private String parcelAddr2;

    @ApiModelProperty(value = "일련번호", position = 10)
    @Min(1)
    private Integer seq;
}
