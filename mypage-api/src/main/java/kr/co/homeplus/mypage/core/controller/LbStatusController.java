package kr.co.homeplus.mypage.core.controller;

import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LbStatusController {

    private static final Logger log = LoggerFactory.getLogger(LbStatusController.class);

    @ApiOperation(value = "lbStatusCheck Controller", notes = "healthy 체크에 사용됨 ")
    @GetMapping("/lbStatusCheck")
    public ResponseEntity<String> getLbStatusCheck() {
//        String lbStatusFilePath = System.getProperty("file.path.lbstatus");
//
//        if (StringUtils.isBlank(lbStatusFilePath)) {
//            log.error("file.path.lbstatus property is not found.");
//            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
//        }
//        Stream<String> lines = null;
        String result = "OK";
//        try {
//            lines = Files.lines(Paths.get(lbStatusFilePath));
//            Optional<String> stringOptional = lines.findFirst();
//            result = stringOptional.orElse(null);
//        } catch (Exception e) {
//            log.error("lb status file error: " + e.getMessage(), e);
//            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
//        } finally {
//            if (lines != null) {
//                lines.close();
//            }
//        }
//
//        if (StringUtils.isEmpty(result)) {
//            log.error("lb status file's contents is EMPTY!");
//            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
//        }

        return new ResponseEntity<>(result.trim(), HttpStatus.OK);
    }
}
