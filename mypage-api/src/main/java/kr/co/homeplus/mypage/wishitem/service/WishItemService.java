package kr.co.homeplus.mypage.wishitem.service;


import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import kr.co.homeplus.mypage.core.exception.LogicException;
import kr.co.homeplus.mypage.enums.ExternalInfo;
import kr.co.homeplus.mypage.enums.ResponseCode;
import kr.co.homeplus.mypage.external.service.ExternalService;
import kr.co.homeplus.mypage.factory.ResponseFactory;
import kr.co.homeplus.mypage.response.ResponseResult;
import kr.co.homeplus.mypage.utils.RequestUtils;
import kr.co.homeplus.mypage.utils.SetParameter;
import kr.co.homeplus.mypage.wishitem.model.WishItemListGetDto;
import kr.co.homeplus.mypage.wishitem.model.WishItemListSetDto;
import kr.co.homeplus.mypage.wishitem.model.WishListGetDto;
import kr.co.homeplus.mypage.wishitem.model.WishListSetDto;
import kr.co.homeplus.mypage.wishitem.model.WishSellerShopListGetDto;
import kr.co.homeplus.mypage.wishitem.model.WishSellerShopListSetDto;
import kr.co.homeplus.mypage.wishitem.model.WishSellerShopModifySetDto;
import kr.co.homeplus.plus.api.support.client.header.UserDeviceType;
import kr.co.homeplus.plus.api.support.client.header.WebHeaderKey;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import kr.co.homeplus.plus.api.support.header.WebHeaders;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class WishItemService {

    // 외부 통신 Service
    private final ExternalService externalService;

    /**
     * 마이페이지 > 찜 리스트 > 상품
     *
     * @param wishItemListSetDto 찜리스트 조회 dto
     * @return 찜리스트 조회 데이터
     * @throws Exception 오류시 오류처리
     */
    public ResponseObject<List<WishItemListGetDto>> getWishItemList(WishItemListSetDto wishItemListSetDto) throws Exception {
        LinkedHashMap<String, Object> parameter = new LinkedHashMap<>();
        parameter.put(WebHeaderKey.USER_NO, externalService.getUserNo());
        parameter.put(WebHeaderKey.USER_DEVICE_TYPE, WebHeaders.getUserDeviceType());
        try{
            ResponseObject<List<WishItemListGetDto>> result = externalService.getResponseTransfer(
                ExternalInfo.FRONT_API_WISH_ITEM_LIST_SEARCH,
                new ArrayList<SetParameter>() {
                    {
                        add(SetParameter.create("siteType", wishItemListSetDto.getSiteType()));
                        add(SetParameter.create("page", wishItemListSetDto.getPage()));
                        add(SetParameter.create("perPage", wishItemListSetDto.getPerPage()));
                    }
                },
                RequestUtils.createHttpHeaders(parameter),
                WishItemListGetDto.class
            );

            return ResponseFactory.getResponseObject(
                ResponseCode.WISH_ITEM_LIST_SUCCESS,
                result.getData(),
                result.getPagination()
            );
        }catch (Exception e){
            log.error(" 찜한 상품 조회 실패 ::: [{}]", e.getMessage());
            throw new LogicException(ResponseCode.WISH_ITEM_LIST_ERR_01);
        }
    }

    /**
     * 마이페이지 > 찜 리스트 > 샐러샵 조회
     *
     * @param wishSellerShopListSetDto 샐러샵 조회 dto
     * @return 찜리스트 조회 데이터
     * @throws Exception 오류시 오류처리
     */
    public ResponseObject<List<WishSellerShopListGetDto>> wishSellerShopListSearch(WishSellerShopListSetDto wishSellerShopListSetDto) throws Exception {
        log.debug("wishSellerShopListSearch ::: {}", RequestUtils.createUserInfoHeaders(externalService.getUserNo()));
        try{
            ResponseObject<List<WishSellerShopListGetDto>> result = externalService.getResponseTransfer(
                ExternalInfo.FRONT_API_WISH_SELLER_SHOP_LIST_SEARCH,
                new ArrayList<SetParameter>() {
                    {
                        add(SetParameter.create("userNo", externalService.getUserNo()));
                        add(SetParameter.create("page", wishSellerShopListSetDto.getPage()));
                        add(SetParameter.create("perPage", wishSellerShopListSetDto.getPerPage()));
                    }
                },
                RequestUtils.createUserInfoHeaders(externalService.getUserNo()),
                WishSellerShopListGetDto.class
            );

            return ResponseFactory.getResponseObject(
                ResponseCode.WISH_SELLER_SHOP_LIST_SUCCESS,
                result.getData(),
                result.getPagination()
            );
        }catch (Exception e){
            log.error(" 찜한 샐러샵 조회 실패 ::: [{}]", e.getMessage());
            throw new LogicException(ResponseCode.WISH_SELLER_SHOP_LIST_ERR_01);
        }
    }

    /**
     * 마이페이지 > 홈 - 찜한 상품/찜한 샐러샵
     *
     * @param wishListSetDto 찜리스트 조회 dto
     * @return 찜리스트 조회 데이터
     * @throws Exception 오류시 오류처리
     */
    public ResponseObject<WishListGetDto> getWishItemSellerShopList(WishListSetDto wishListSetDto) throws Exception {

        /* 1-1. 찜 리스트 조회 */
        List<WishItemListGetDto> wishItemList = this.getWishItemList(
            WishItemListSetDto.builder()
                .siteType(wishListSetDto.getSiteType())
                .page(1)      //최근 거래만 조회하기 위해 page 값을 1(최소값)으로 지정
                .perPage(10)  //최근 거래만 조회하기 위해 perPage 값을 10(최소값)로 지정
                .build()
        ).getData();

        /* 1-2. 찜리스트가 5건 이상일 경우 5번째 이후 index는 제거 */
        if(wishItemList.size() > 5){
            wishItemList.subList(5, wishItemList.size()).clear();
        }

        /* 2-21 샐러샵 조회 */
        List<WishSellerShopListGetDto> wishSellerShopList = this.wishSellerShopListSearch(
            WishSellerShopListSetDto.builder()
                .userNo(String.valueOf(externalService.getUserNo()))
                .page(1)      //최근 거래만 조회하기 위해 page 값을 1(최소값)으로 지정
                .perPage(10)  //최근 거래만 조회하기 위해 perPage 값을 10(최소값)로 지정
                .build()
        ).getData();

        /* 2-3. 샐러샵 리스트가 5건 이상일 경우 5번째 이후 index는 제거 */
        if(wishSellerShopList.size() > 5){
            wishSellerShopList.subList(5, wishSellerShopList.size()).clear();
        }

        /* return */
        return ResponseFactory.getResponseObject(
            ResponseCode.SUCCESS,
            WishListGetDto.builder()
                .wishItemList(wishItemList)
                .wishSellerShopList(wishSellerShopList)
                .build()
        );
    }

    /**
     * 마이페이지 > 홈 - 찜한 샐러샵 등록/삭제
     *
     * @param wishSellerShopModifySetDto 찜리스트 조회 dto
     * @return 등록/삭제 결과 데이터
     * @throws Exception 오류시 오류처리
     */
    public ResponseObject<ResponseResult> wishSellerShopModify(WishSellerShopModifySetDto wishSellerShopModifySetDto) throws Exception {
        wishSellerShopModifySetDto.setUserNo(externalService.getUserNo());
        return ResponseFactory.getResponseObject(
            ResponseCode.SUCCESS,externalService.postTransfer(
                ExternalInfo.FRONT_API_WISH_SELLER_SHOP_MODIFY, wishSellerShopModifySetDto
                , ResponseResult.class
            )
        );
    }
}
