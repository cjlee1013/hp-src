package kr.co.homeplus.mypage.claim.model.upload;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UploadFileDto {

    private String fileName;
    private String fileId;
    private String width;
    private String height;
    private String etag;

}
