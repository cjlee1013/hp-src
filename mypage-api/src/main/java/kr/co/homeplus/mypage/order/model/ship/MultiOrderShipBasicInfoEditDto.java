package kr.co.homeplus.mypage.order.model.ship;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class MultiOrderShipBasicInfoEditDto {

    @ApiModelProperty(value = "배송번호", position = 1)
    private long bundleNo;

    @ApiModelProperty(value = "주문번호", position = 2)
    private long purchaseOrderNo;

    @ApiModelProperty(value = "멀티번들번호", position = 3)
    private long multiBundleNo;

    @ApiModelProperty(value = "예약배송일", position = 4)
    private String reserveShipDt;

    @ApiModelProperty(value = "선물하기메시지", position = 5)
    private String giftMsg;
}
