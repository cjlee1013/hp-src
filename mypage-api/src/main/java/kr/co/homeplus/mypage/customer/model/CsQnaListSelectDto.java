package kr.co.homeplus.mypage.customer.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "마이페이지 > 홈 - QnA리스트")
public class CsQnaListSelectDto {

    @ApiModelProperty(value = "Qna 일련번호")
    private String qnaNo;

    @ApiModelProperty(value = "문의내용")
    private String qnaContents;

    @ApiModelProperty(value = "등록일시")
    private String regDt;

    @ApiModelProperty(value = "비밀글 여부")
    private String secretYn;

    @ApiModelProperty(value = "Qna 처리상태")
    private String qnaStatus;

    @ApiModelProperty(value = "Qna 처리상태 TXT")
    private String qnaStatusTxt;

    @ApiModelProperty(value = "Qna 타입 (상품-ITEM, 배송-SHIP, 교환/반품-CLAIM, 기타-ETC)")
    private String qnaType;

    @ApiModelProperty(value = "Qna 타입")
    private String qnaTypeTxt;

    @ApiModelProperty(value = "나의 문의 여부")
    private String myQnaYn;

    @ApiModelProperty(value = "구매 여부")
    private String purchaseYn;

    @ApiModelProperty(value = "회원ID")
    private String userId;

}
