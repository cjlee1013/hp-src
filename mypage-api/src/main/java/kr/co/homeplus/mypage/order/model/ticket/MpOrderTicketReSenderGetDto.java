package kr.co.homeplus.mypage.order.model.ticket;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "E-티켓 재발송결과")
public class MpOrderTicketReSenderGetDto {
    @ApiModelProperty(value = "티켓주문번호",  position = 1)
    private long orderTicketNo;
    @ApiModelProperty(value = "재발송횟수",  position = 2)
    private Integer sendCnt;

    private void setSendCnt(Integer sendCnt){
        if(sendCnt > 1){
            this.sendCnt = (sendCnt -1);
        } else {
            this.sendCnt = sendCnt;
        }
    }
}
