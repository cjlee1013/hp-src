package kr.co.homeplus.mypage.receipt.model.order;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "주문_구매영수증 조회 DTO")
public class OrderPurchaseReceiptSetDto {
    @ApiModelProperty(value = "주문번호", position = 1)
    private long purchaseOrderNo;
    @ApiModelProperty(value = "고객번호", position = 2, hidden = true)
    private long userNo;
}
