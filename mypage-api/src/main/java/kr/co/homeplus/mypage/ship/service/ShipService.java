package kr.co.homeplus.mypage.ship.service;

import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.mypage.claim.model.DlvCdGetDto;
import kr.co.homeplus.mypage.core.exception.LogicException;
import kr.co.homeplus.mypage.enums.ExternalInfo;
import kr.co.homeplus.mypage.enums.ResponseCode;
import kr.co.homeplus.mypage.external.service.ExternalService;
import kr.co.homeplus.mypage.factory.ResponseFactory;
import kr.co.homeplus.mypage.ship.model.ShipMsgCdGetDto;
import kr.co.homeplus.mypage.ship.model.ShippingInvoiceGetDto;
import kr.co.homeplus.mypage.utils.SetParameter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class ShipService {

    // 외부 통신 Service
    private final ExternalService externalService;

    /**
     * 배송메시지 코드 리스트 조회
     *
     * **/
    public ResponseObject<List<ShipMsgCdGetDto>> shipMsgSearch () throws Exception {
        return ResponseFactory.getResponseObject(
           ResponseCode.SUCCESS,
           externalService.getListTransfer(
               ExternalInfo.CLAIM_API_SHIP_MSG_CD_LIST, null, ShipMsgCdGetDto.class )
       );
    }

    /**
     * 택배코드 리스트 조회
     *
     * **/
    public ResponseObject<List<DlvCdGetDto>> dlvCdSearch () throws Exception {
        return ResponseFactory.getResponseObject(
           ResponseCode.SUCCESS,
           externalService.getListTransfer(
               ExternalInfo.CLAIM_API_DLV_CD_LIST, null, DlvCdGetDto.class )
       );
    }

    /**
     * 수거/베송 리스트 조회
     *
     * **/
    public ResponseObject<ShippingInvoiceGetDto> getClaimShipHistory (String claimType, long claimShippingNo) throws Exception {
        ExternalInfo externalInfo = null;
        List parameter = null;

        switch (claimType){
            case "R" :
                externalInfo = ExternalInfo.SHIPPING_API_PICK_SHIP_HISTORY;
                parameter = new ArrayList<SetParameter>(){{
                    add(SetParameter.create("claimPickShippingNo", claimShippingNo));
                }};
                break;
            case "X" :
                externalInfo = ExternalInfo.SHIPPING_API_EXCH_SHIP_HISTORY;
                parameter = new ArrayList<SetParameter>(){{
                    add(SetParameter.create("claimExchShippingNo", claimShippingNo));
                }};
                break;
            default:
                log.error(" 수거/배송 배송조회 타입 설정 오류 ::: [{}]", claimType);
                throw new LogicException(ResponseCode.ORDER_SHIP_SEARCH_ERR_01);
        }

        return ResponseFactory.getResponseObject(
           ResponseCode.SUCCESS,
           externalService.getTransfer(
               externalInfo,
               parameter
               , ShippingInvoiceGetDto.class )
       );
    }


}
