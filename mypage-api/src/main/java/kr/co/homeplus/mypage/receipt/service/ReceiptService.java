package kr.co.homeplus.mypage.receipt.service;

import java.util.List;
import kr.co.homeplus.mypage.enums.ExternalInfo;
import kr.co.homeplus.mypage.external.service.ExternalService;
import kr.co.homeplus.mypage.factory.ResponseFactory;
import kr.co.homeplus.mypage.receipt.model.claim.ClaimPurchaseMethodReceiptSetDto;
import kr.co.homeplus.mypage.receipt.model.claim.ClaimPurchaseReceiptGetDto;
import kr.co.homeplus.mypage.receipt.model.claim.ClaimPurchaseReceiptSetDto;
import kr.co.homeplus.mypage.receipt.model.claim.card.ClaimCardReceiptGetDto;
import kr.co.homeplus.mypage.receipt.model.claim.cash.ClaimCashReceiptGetDto;
import kr.co.homeplus.mypage.receipt.model.order.OrderPurchaseReceiptGetDto;
import kr.co.homeplus.mypage.receipt.model.order.OrderPurchaseReceiptSetDto;
import kr.co.homeplus.mypage.receipt.model.order.card.OrderPurchaseCardReceiptGetDto;
import kr.co.homeplus.mypage.receipt.model.order.OrderPurchaseMethodReceiptSetDto;
import kr.co.homeplus.mypage.receipt.model.order.cash.OrderPurchaseCashReceiptGetDto;
import kr.co.homeplus.mypage.receipt.model.order.order.ClaimRefundReceiptGetDto;
import kr.co.homeplus.mypage.receipt.model.order.order.ClaimRefundReceiptSetDto;
import kr.co.homeplus.mypage.utils.MypageUtils;
import kr.co.homeplus.mypage.utils.RequestUtils;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ReceiptService {

    private final ExternalService externalService;

    // 주문_구매영수증
    public ResponseObject<OrderPurchaseReceiptGetDto> getOrderPurchaseReceiptInfo(OrderPurchaseReceiptSetDto orderPurchaseReceiptSetDto) throws Exception {
        orderPurchaseReceiptSetDto.setUserNo(externalService.getUserNo());
        return externalService.getTransferResponse(
            ExternalInfo.ESCROW_API_PURCHASE_ORDER_RECEIPT,
            MypageUtils.setParemeter(orderPurchaseReceiptSetDto),
            RequestUtils.createUserLoginHeaders(),
            OrderPurchaseReceiptGetDto.class
        );
    }

    public ResponseObject<OrderPurchaseCardReceiptGetDto> getOrderPurchaseCardReceiptInfo(OrderPurchaseMethodReceiptSetDto orderPurchaseMethodReceiptSetDto) throws Exception {
        orderPurchaseMethodReceiptSetDto.setUserNo(externalService.getUserNo());
        ResponseObject<OrderPurchaseCardReceiptGetDto> getDto =  externalService.getTransferResponse(
            ExternalInfo.ESCROW_API_PURCHASE_ORDER_CARD_RECEIPT,
            MypageUtils.setParemeter(orderPurchaseMethodReceiptSetDto),
            RequestUtils.createUserLoginHeaders(),
            OrderPurchaseCardReceiptGetDto.class
        );
        if(getDto.getData() != null){
            getDto.getData().setPayAmt(getDto.getData().getTotPayAmt() - getDto.getData().getTotVatAmt());
        }
        return ResponseFactory.getResponseObject(getDto.getReturnCode(), getDto.getReturnMessage(), getDto.getData());
    }

    public ResponseObject<OrderPurchaseCashReceiptGetDto> getOrderPurchaseCashReceiptInfo(OrderPurchaseMethodReceiptSetDto orderPurchaseMethodReceiptSetDto) throws Exception {
        orderPurchaseMethodReceiptSetDto.setUserNo(externalService.getUserNo());
        return externalService.getTransferResponse(
            ExternalInfo.ESCROW_API_PURCHASE_ORDER_CASH_RECEIPT,
            MypageUtils.setParemeter(orderPurchaseMethodReceiptSetDto),
            RequestUtils.createUserLoginHeaders(),
            OrderPurchaseCashReceiptGetDto.class
        );
    }

    // 주문_구매영수증
    public ResponseObject<ClaimPurchaseReceiptGetDto> getClaimPurchaseReceiptInfo(ClaimPurchaseReceiptSetDto claimPurchaseReceiptSetDto) throws Exception {
        claimPurchaseReceiptSetDto.setUserNo(externalService.getUserNo());
        return externalService.getTransferResponse(
            ExternalInfo.CLAIM_API_PURCHASE_CLAIM_RECEIPT,
            MypageUtils.setParemeter(claimPurchaseReceiptSetDto),
            RequestUtils.createUserLoginHeaders(),
            ClaimPurchaseReceiptGetDto.class
        );
    }

    public ResponseObject<ClaimCardReceiptGetDto> getClaimCardPurchaseReceiptInfo(ClaimPurchaseMethodReceiptSetDto claimPurchaseMethodReceiptSetDto) throws Exception {
        claimPurchaseMethodReceiptSetDto.setUserNo(externalService.getUserNo());
        return externalService.getTransferResponse(
            ExternalInfo.CLAIM_API_PURCHASE_CLAIM_CARD_RECEIPT,
            MypageUtils.setParemeter(claimPurchaseMethodReceiptSetDto),
            RequestUtils.createUserLoginHeaders(),
            ClaimCardReceiptGetDto.class
        );
    }

    public ResponseObject<ClaimCashReceiptGetDto> getClaimCashPurchaseReceiptInfo(ClaimPurchaseMethodReceiptSetDto claimPurchaseMethodReceiptSetDto) throws Exception {
        claimPurchaseMethodReceiptSetDto.setUserNo(externalService.getUserNo());
        return externalService.getTransferResponse(
            ExternalInfo.CLAIM_API_PURCHASE_CLAIM_CASH_RECEIPT,
            MypageUtils.setParemeter(claimPurchaseMethodReceiptSetDto),
            RequestUtils.createUserLoginHeaders(),
            ClaimCashReceiptGetDto.class
        );
    }

    public ResponseObject<List<ClaimRefundReceiptGetDto>> getOrderRefundReceipt(ClaimRefundReceiptSetDto claimRefundReceiptSetDto) throws Exception {
        claimRefundReceiptSetDto.setUserNo(externalService.getUserNo());
        return externalService.getResponseListTransfer(
            ExternalInfo.CLAIM_API_ORDER_REFUND_RECEIPT,
            MypageUtils.setParemeter(claimRefundReceiptSetDto),
            RequestUtils.createUserLoginHeaders(),
            ClaimRefundReceiptGetDto.class
        );
    }
}
