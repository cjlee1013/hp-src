package kr.co.homeplus.mypage.customer.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "고객 제안")
public class CustomerSuggestionDto {

    @ApiModelProperty(value = "제목", position = 1)
    private String title;

    @ApiModelProperty(value = "내용", position = 2)
    private String description;

    @ApiModelProperty(value = "구분", position = 3)
    private String category;

}
