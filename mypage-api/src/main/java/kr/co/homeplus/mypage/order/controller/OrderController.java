package kr.co.homeplus.mypage.order.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import kr.co.homeplus.mypage.enums.SiteType;
import kr.co.homeplus.mypage.order.model.MpOrderDetailListGetDto;
import kr.co.homeplus.mypage.order.model.MpOrderDetailListSetDto;
import kr.co.homeplus.mypage.order.model.MpOrderListGetDto;
import kr.co.homeplus.mypage.order.model.MpOrderListSetDto;
import kr.co.homeplus.mypage.order.model.MpOrderMainDto;
import kr.co.homeplus.mypage.order.model.MpOrderMainDto.MyPageMainOrderCount;
import kr.co.homeplus.mypage.order.model.MpOrderMainDto.MyPageMainOrderList;
import kr.co.homeplus.mypage.order.model.MpOrderPickupStoreInfoDto;
import kr.co.homeplus.mypage.order.model.MpOrderProductListGetDto;
import kr.co.homeplus.mypage.order.model.MpOrderTicketDetailGetDto;
import kr.co.homeplus.mypage.order.model.inquiry.OrderInquiryGetDto;
import kr.co.homeplus.mypage.order.model.inquiry.OrderInquirySetDto;
import kr.co.homeplus.mypage.order.model.multi.MpMultiOrderDetailInfoGetDto;
import kr.co.homeplus.mypage.order.model.multi.MpMultiOrderDetailInfoSetDto;
import kr.co.homeplus.mypage.order.model.multi.MpMultiOrderDetailListGetDto;
import kr.co.homeplus.mypage.order.model.order.OrderListGetDto;
import kr.co.homeplus.mypage.order.model.order.OrderListSetDto;
import kr.co.homeplus.mypage.order.model.ship.MultiOrderShipAddrEditDto;
import kr.co.homeplus.mypage.order.model.ship.MultiOrderShipBasicInfoEditDto;
import kr.co.homeplus.mypage.order.model.ship.OrderShipAddrEditDto;
import kr.co.homeplus.mypage.order.model.ship.OrderShipListGetDto;
import kr.co.homeplus.mypage.order.model.slot.FrontStoreSlot;
import kr.co.homeplus.mypage.order.model.slot.ShipSlotStockChangeDto;
import kr.co.homeplus.mypage.order.model.slot.ShipSlotStockConfirmDto;
import kr.co.homeplus.mypage.order.model.ticket.MpOrderTicketReSenderGetDto;
import kr.co.homeplus.mypage.order.model.ticket.MpOrderTicketReSenderSetDto;
import kr.co.homeplus.mypage.order.service.OrderService;
import kr.co.homeplus.plus.api.support.client.header.UserDeviceType;
import kr.co.homeplus.plus.api.support.client.header.WebHeaderKey;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/order", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(tags = "Mypage 주문/배송 조회", value = "Mypage 주문/배송 조회 컨트롤러")
@Slf4j
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
}
)
@RequiredArgsConstructor
public class OrderController {
    // 주문 Service
    private final OrderService orderService;

    @ApiOperation(value = "주문조회", response = MpOrderListGetDto.class, notes = "주문/배송")
    @PostMapping(value = "/getOldOrderList")
    public ResponseObject<List<MpOrderListGetDto>> oldOrderList(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestBody @Valid MpOrderListSetDto setDto
    ) throws Exception {
        setDto.setSiteType(siteType.name());
        return orderService.getOldOrderList(setDto);
    }

    @ApiOperation(value = "주문조회", response = OrderListGetDto.class, notes = "주문/배송")
    @PostMapping(value = "/getOrderList")
    public ResponseObject<List<OrderListGetDto>> orderList(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestBody @Valid OrderListSetDto setDto
    ) throws Exception {
        setDto.setSiteType(siteType.name());
        return orderService.getOrderList(setDto);
    }

    @ApiOperation(value = "주문조회 상세", response = MpOrderDetailListGetDto.class, notes = "주문/배송")
    @PostMapping(value = "/getOrderDetailList")
    public ResponseObject<MpOrderDetailListGetDto> orderDetailList(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestBody MpOrderDetailListSetDto setDto
    ) throws Exception {
        setDto.setSiteType(siteType.name());
        return orderService.getOrderDetailList(setDto);
    }

    @ApiOperation(value = "마이페이지 메인 > 나의 주문 현황 조회", response = MpOrderMainDto.class, notes = "주문/배송")
    @PostMapping(value = "/getMainOrderInfo")
    public ResponseObject<MpOrderMainDto> mainOrderInfo(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType
    ) throws Exception {
        return orderService.getMainOrderInfo(siteType.name());
    }

    @ApiOperation(value = "마이페이지 메인 > 나의 주문 현황 조회 > 주문건수", response = MyPageMainOrderCount.class, notes = "주문/배송")
    @PostMapping(value = "/getMainOrderCount")
    public ResponseObject<List<MyPageMainOrderCount>> mainOrderCount(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType
    ) throws Exception {
        return orderService.getMainOrderCount(siteType.name());
    }

    @ApiOperation(value = "마이페이지 메인 > 나의 주문 현황 조회 > 주문리스트", response = MyPageMainOrderList.class, notes = "주문/배송")
    @PostMapping(value = "/getMainOrderList")
    public ResponseObject<List<MyPageMainOrderList>> mainOrderList(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType
    ) throws Exception {
        return orderService.getMainOrderList(siteType.name());
    }

    @GetMapping("/orderShipList/{shipNo}")
    @ApiOperation(value = "배송조회", response = OrderShipListGetDto.class)
    public ResponseObject<OrderShipListGetDto> getShipOrderList(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @PathVariable("shipNo") long shipNo
    ) throws Exception {
        // 번들이 배송중인지 확인 필요.
        return orderService.getShipOrderList(shipNo);
    }

    @GetMapping("/orderShipList/multiShip/{multiOrderItemNo}")
    @ApiOperation(value = "다중배송조회", response = OrderShipListGetDto.class)
    public ResponseObject<OrderShipListGetDto> getMultiShipOrderList(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @PathVariable("multiOrderItemNo") long multiOrderItemNo
    ) throws Exception {
        // 번들이 배송중인지 확인 필요.
        return orderService.getMultiShipOrderList(multiOrderItemNo);
    }

    @GetMapping("/manualShipComplete/{bundleNo}")
    @ApiOperation(value = "수동배송완료 처리")
    public ResponseObject<String> manualShipComplete(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @PathVariable("bundleNo")long bundleNo
    ) throws Exception {
        return orderService.manualShipComplete(bundleNo);
    }

    @GetMapping("/manualShipComplete/multiShip/{multiBundleNo}")
    @ApiOperation(value = "다중배송수동배송완료 처리")
    public ResponseObject<String> manualMultiShipComplete(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @PathVariable("multiBundleNo")long multiBundleNo
    ) throws Exception {
        return orderService.manualMultiShipComplete(multiBundleNo);
    }

    @PostMapping("/orderShipAddrChange")
    @ApiOperation(value = "배송지 변경")
    public ResponseObject<String> modifyOrderShipAddrChange(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestBody OrderShipAddrEditDto orderShipAddrEditDto
    ) throws Exception {
        return orderService.modifyOrderShipAddrChange(orderShipAddrEditDto);
    }

    @GetMapping("/getShipSlotInfo")
    @ApiOperation(value = "배송슬롯 조회", response = FrontStoreSlot.class)
    public ResponseObject<FrontStoreSlot> getShipSlotInfo(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestParam("purchaseOrderNo") long purchaseOrderNo,
        @RequestParam("bundleNo") long bundleNo
    ) throws Exception {
        return orderService.getShipSlotInfo(purchaseOrderNo, bundleNo);
    }

    @PostMapping("/modifyShipSlotInfo")
    @ApiOperation(value = "배송슬롯변경")
    public ResponseObject<String> modifyShipSlotInfo(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestBody ShipSlotStockChangeDto changeDto
    ) throws Exception {
        return orderService.setShipSlotChangeModify(changeDto);
    }


    @Deprecated
    @PostMapping("/setShipSlotInfoConfirm")
    @ApiOperation(value = "배송슬롯확정")
    public ResponseObject<String> setShipSlotRestoreInfo(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestBody ShipSlotStockConfirmDto confirmDto
    ) throws Exception {
//        return orderService.setShipSlotChangeConfirm(confirmDto);
        return null;
    }

    @GetMapping("/getOrderPickupStoreInfo")
    @ApiOperation(value = "주문픽업점포정보 조회", response = MpOrderPickupStoreInfoDto.class)
    public ResponseObject<MpOrderPickupStoreInfoDto> getOrderPickupStoreInfo(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestParam @ApiParam(name = "purchaseOrderNo", value = "주문번호", required = true) long purchaseOrderNo
    ) throws Exception {
        return orderService.getOrderPickupStoreInfo(purchaseOrderNo);
    }

    @ApiOperation(value = "티켓 주문조회 상세", response = MpOrderTicketDetailGetDto.class, notes = "주문/배송")
    @PostMapping(value = "/getOrderTicketDetailList")
    public ResponseObject<MpOrderTicketDetailGetDto> orderTicketDetailList(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestBody MpOrderDetailListSetDto setDto
    ) throws Exception {
        return orderService.getOrderTicketDetailInfo(setDto);
    }

    @ApiOperation(value = "티켓 재발송", response = MpOrderTicketReSenderGetDto.class, notes = "주문/배송")
    @PostMapping(value = "/reSendTicket")
    public ResponseObject<MpOrderTicketReSenderGetDto> orderTicketReSend(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestBody MpOrderTicketReSenderSetDto setDto
    ) throws Exception {
        return orderService.orderTicketReSend(setDto);
    }

    @ApiOperation(value = "티켓 상태 코드", response = MpOrderTicketReSenderGetDto.class, notes = "주문/배송")
    @PostMapping(value = "/getTicketStatusCode")
    public ResponseObject<Map<String, String>> getTicketStatusCode() throws Exception {
        return orderService.getTicketStatusCode();
    }

    @PostMapping("/getInquiryOrderList")
    @ApiOperation(value = "1:1문의-주문상품리스트", response = OrderInquiryGetDto.class)
    public ResponseObject<List<OrderInquiryGetDto>> getInquiryOrderList(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestBody OrderInquirySetDto setDto
    ) throws Exception {
        setDto.setSiteType(siteType.name());
        return orderService.getInquiryOrderItemList(setDto);
    }

    @PostMapping("/multiOrderShipAddrChange")
    @ApiOperation(value = "다중배송주문 - 배송지 변경")
    public ResponseObject<String> modifyMultiOrderShipAddrChange(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestBody MultiOrderShipAddrEditDto multiOrderShipAddrEditDto
    ) throws Exception {
        return orderService.modifyMultiOrderShipAddrChange(multiOrderShipAddrEditDto);
    }

    @PostMapping("/multiOrderShipBasicInfoChange")
    @ApiOperation(value = "다중배송주문 - 배송기본정보 변경")
    public ResponseObject<String> modifyMultiOrderShipBasicChange(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestBody MultiOrderShipBasicInfoEditDto multiOrderShipBasicInfoEditDto
    ) throws Exception {
        return orderService.modifyMultiOrderShipBasicChange(multiOrderShipBasicInfoEditDto);
    }

    @ApiOperation(value = "다중배송주문상세-다중배송지리스트", response = MpMultiOrderDetailListGetDto.class, notes = "주문/배송")
    @PostMapping(value = "/getMultiOrderDetailList")
    public ResponseObject<MpMultiOrderDetailListGetDto> multiOrderDetailList(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestBody MpOrderDetailListSetDto setDto
    ) throws Exception {
        setDto.setSiteType(siteType.name());
        return orderService.getMultiOrderDetailList(setDto);
    }

    @ApiOperation(value = "다중배송주문상세-다중배송별주문내역", response = MpMultiOrderDetailInfoGetDto.class, notes = "주문/배송")
    @PostMapping(value = "/getMultiOrderDetailInfo")
    public ResponseObject<MpMultiOrderDetailInfoGetDto> multiOrderDetailList(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestBody MpMultiOrderDetailInfoSetDto setDto
    ) throws Exception {
        setDto.setSiteType(siteType.name());
        return orderService.getMultiOrderDetailInfo(setDto);
    }

    @ApiOperation(value = "다중배송주문상세-다중배송주문아이템조회(클레임용)", response = MpOrderProductListGetDto.class, notes = "주문/배송")
    @PostMapping(value = "/getMultiOrderItemList")
    public ResponseObject<List<MpOrderProductListGetDto>> multiOrderItemList(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestBody MpOrderDetailListSetDto setDto
    ) throws Exception {
        setDto.setSiteType(siteType.name());
        return orderService.getMultiOrderItemList(setDto);
    }

}
