package kr.co.homeplus.mypage.customer.model.inquiry;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "1:1문의내역 수정.(문의취소등.)")
public class InquiryModifyDto {

    @ApiModelProperty(value = "문의관리번호", position = 1)
    private long inqryNo;

    @ApiModelProperty(value = "유저번호", position = 2)
    private long userNo;

    @ApiModelProperty(value = "사용유무", position = 3)
    private String useYn;

}
