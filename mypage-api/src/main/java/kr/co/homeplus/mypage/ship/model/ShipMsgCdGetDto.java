package kr.co.homeplus.mypage.ship.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "배송요청 메시지 코드 리스트 응답 dto")
public class ShipMsgCdGetDto {

    @ApiModelProperty(value = "배송요청 코드", position = 1)
    private String shipMsgCd;

    @ApiModelProperty(value = "배송 요청 메시지", position = 2)
    private String shipMsg;

    @ApiModelProperty(value = "점포유형명", position = 3)
    private String storeKind;
}
