package kr.co.homeplus.mypage.claim.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "택배사 코드 리스트 응답 dto")
public class DlvCdGetDto {

    @ApiModelProperty(value = "택배사 코드", position = 1)
    private String dlvCd;

    @ApiModelProperty(value = "택배사명", position = 2)
    private String dlvCdNm;

}
