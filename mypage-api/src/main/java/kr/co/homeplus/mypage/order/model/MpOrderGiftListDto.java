package kr.co.homeplus.mypage.order.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "사은품 행사 정보 DTO")
public class MpOrderGiftListDto {

    @ApiModelProperty(value = "주문번호", position = 1)
    private Long purchaseOrderNo;
    @ApiModelProperty(value = "점포ID", position = 2)
    private Integer storeId;
    @ApiModelProperty(value = "사은품번호", position = 3)
    private String giftNo;
    @ApiModelProperty(value = "사은품메시지(영수증 노출 문구)", position = 4)
    private String giftMsg;

}
