package kr.co.homeplus.mypage.utils;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import kr.co.homeplus.mypage.enums.impl.EnumImpl;
import org.apache.commons.lang3.StringUtils;

/**
 * mypage-api 내에 사용할 공통 모듈.
 * 기존 utils 을 Tweak 하여 사용하는 경우에 해당 소스에 추가하여 사용한다.
 */
public class MypageUtils {

    public static String getFindEnumCode(Class<?> enumClz, String category, String type){
        // enum values 정보를 가지고 온다.
        Enum<?>[] enumList = (Enum<?>[])enumClz.getEnumConstants();
        //
        boolean isEnumImpl = Boolean.FALSE;

        for(Class<?> interfaceClz : enumClz.getInterfaces()){
            if(interfaceClz.getSimpleName().equalsIgnoreCase("EnumImpl")){
                isEnumImpl = Boolean.TRUE;
                break;
            }
        }

        if(isEnumImpl){
            int ordinal = Stream.of(enumList).filter(e -> e.name().contains(category)).collect(Collectors.toMap(Enum::name, Enum::ordinal)).get(type);
            return ((EnumImpl)enumList[ordinal]).getResponseMessage();
        }

        return null;
    }


    /**
     * CamelCase 사이에 특정데이터 입력
     * 처음 UPPER_CASE 앞에 입력 데이터값을 append 한다.
     * @param targetStr 변경대상문자열
     * @param inputStr 변경할문자열
     * @return
     */
    public static String insertByFirstCamelCase(String targetStr, String inputStr) {
        String[] splitByCamelCase = StringUtils.splitByCharacterTypeCamelCase(targetStr);
        StringBuilder sb = new StringBuilder();
        sb.append(splitByCamelCase[0]).append(StringUtils.capitalize(inputStr));
        for (int idx = 1; idx < splitByCamelCase.length; idx++) {
            sb.append(splitByCamelCase[idx]);
        }
        return sb.toString();
    }

    /**
     * List<DTO>> 형식 내 중복키 제거 유틸.
     *
     * @param keyExtractor
     * @param <T>
     * @return
     */
    public static <T> Predicate<T> distinctByKey( Function<? super T, Object> keyExtractor) {
        Map<Object, Boolean> map = new HashMap<>();
        return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }

    /**
     * Object type 변수가 비어있는지 체크
     *
     * @param obj
     * @return Boolean : true / false
     */
    public static Boolean empty(Object obj) {
        if (obj instanceof String) {
            return "".equals(obj.toString().trim());
        } else if (obj instanceof List) {
            return ((List) obj).isEmpty();
        } else if (obj instanceof Map) {
            return ((Map) obj).isEmpty();
        } else if (obj instanceof Object[]) {
            return Array.getLength(obj) == 0;
        } else {
            return obj == null;
        }
    }

    /**
     * Object type 변수가 비어있지 않은지 체크
     *
     * @param obj
     * @return Boolean : true / false
     */
    public static Boolean notEmpty(Object obj) {
        return !empty(obj);
    }

    public static List<SetParameter> setParemeter(String key, Object value){
        return new ArrayList<>() {{ add(SetParameter.create(key, value)); }};
    }

    public static List<SetParameter> setParemeter(Object parameterDto) throws Exception {
        LinkedHashMap<String, Object> parameterMap = ObjectUtils.getConvertMap(parameterDto);
        List<SetParameter> returnList = new ArrayList<>();
        for(String key : parameterMap.keySet()){
            returnList.add(SetParameter.create(key, parameterMap.get(key)));
        }
        return returnList;
    }

}
