package kr.co.homeplus.mypage.benefit.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.validation.Valid;
import kr.co.homeplus.mypage.benefit.model.BenefitSubMainDto;
import kr.co.homeplus.mypage.benefit.model.coupon.BenefitCouponDto;
import kr.co.homeplus.mypage.benefit.model.mhc.BenefitMhcDto;
import kr.co.homeplus.mypage.benefit.model.mileage.BenefitMileageDto;
import kr.co.homeplus.mypage.benefit.model.mileage.BenefitMileageListGetDto;
import kr.co.homeplus.mypage.benefit.model.mileage.BenefitMileageListSetDto;
import kr.co.homeplus.mypage.benefit.model.mileage.BenefitMileageRegSetDto;
import kr.co.homeplus.mypage.benefit.service.BenefitService;
import kr.co.homeplus.mypage.enums.AppImpl;
import kr.co.homeplus.mypage.enums.SiteType;
import kr.co.homeplus.mypage.enums.SortType;
import kr.co.homeplus.mypage.factory.ResponseFactory;
import kr.co.homeplus.mypage.order.model.ship.OrderShipAddrEditDto;
import kr.co.homeplus.plus.api.support.client.header.UserDeviceType;
import kr.co.homeplus.plus.api.support.client.header.WebHeaderKey;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/benefit", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(tags = "Mypage 혜택 조회", value = "Mypage 혜택 조회 관련 컨트롤러")
@Slf4j
@ApiResponses(value = {
    @ApiResponse(code = 404, message = "Not Found"),
    @ApiResponse(code = 500, message = "Failure")
}
)
@RequiredArgsConstructor
public class BenefitController {
    // 혜택 관련 Service
    private final BenefitService benefitService;

    @ApiOperation(value = "마일리지 포인트 조회", response = BenefitMileageDto.class, notes = "혜택")
    @PostMapping(value = "/mileagePoint")
    public ResponseObject<BenefitMileageDto> mileagePoint(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType
    ) throws Exception {
        return benefitService.getUserMileageCheck();
    }

    @ApiOperation(value = "마일리지 등록", response = String.class, notes = "혜택")
    @PostMapping(value = "/mileagePointReg")
    public ResponseObject<String> mileagePointReg(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestBody BenefitMileageRegSetDto mileageRegSetDto
    ) throws Exception {
        return benefitService.addMileageCouponRegInfo(mileageRegSetDto);
    }

    @ApiOperation(value = "마일리지 적립/차감 내역 조회", response = BenefitMileageListGetDto.class, notes = "혜택")
    @PostMapping(value = "/mileageList")
    public ResponseObject<List<BenefitMileageListGetDto>> mileageList(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @RequestBody @Valid BenefitMileageListSetDto benefitMileageListSetDto
    ) throws Exception {
        return benefitService.getUserMileageList(benefitMileageListSetDto);
    }

    @ApiOperation(value = "BenefitMhcCardInfoDto 포인트 조회", response = BenefitMhcDto.class, notes = "혜택")
    @PostMapping(value = "/mhcPoint")
    public ResponseObject<BenefitMhcDto> mhcCheck(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType
    ) throws Exception {
        return benefitService.getUserMhcPoint();
    }

    @ApiOperation(value = "쿠폰건수 조회", response = Integer.class, notes = "혜택")
    @PostMapping(value = "/couponCount")
    public ResponseObject<Integer> couponCount(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType
    ) throws Exception {
        return benefitService.getUserCouponCount(siteType);
    }

    @ApiOperation(value = "쿠폰정보 조회", response = BenefitCouponDto.class, notes = "혜택")
    @PostMapping(value = "/userCouponList")
    public ResponseObject<List<BenefitCouponDto>> userCouponList(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType,
        @ApiParam(name = "sortType", value = "정렬타입(DLA:마감입박순/LAT:최근순)", defaultValue = "DLA", required = true) @RequestParam SortType sortType
    ) throws Exception {
        return benefitService.getUserCouponList(siteType, sortType);
    }

    @ApiOperation(value = "서브메인 혜택 조회", response = BenefitSubMainDto.class, notes = "혜택")
    @PostMapping(value = "/getBenefitInfo")
    public ResponseObject<BenefitSubMainDto> getBenefitSubMain(
        @ApiParam(name = WebHeaderKey.USER_DEVICE_TYPE, value = "userDeviceTypeStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.USER_DEVICE_TYPE, defaultValue = "PC", required = true) UserDeviceType userDeviceTypeStr,
        @ApiParam(name = WebHeaderKey.X_AUTH_TOKEN, value = "xAuthTokenStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.X_AUTH_TOKEN, defaultValue = "", required = true) String xAuthTokenStr,
        @ApiParam(name = WebHeaderKey.LOGIN_ACCESS_HASH, value = "loginAccessHashStr", defaultValue = "", required = true) @RequestHeader(value = WebHeaderKey.LOGIN_ACCESS_HASH, defaultValue = "", required = true) String loginAccessHashStr,
        @ApiParam(name = "siteType", value = "사이트타입", defaultValue = "", required = true) @RequestHeader SiteType siteType
    ) throws Exception {
        return benefitService.getSubMainSearch(siteType);
    }

}
