package kr.co.homeplus.mypage.receipt.model.order.purchase;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class OrderPurchaseReceiptMainInfo {

    @ApiModelProperty(value = "구매주문번호", position = 1)
    private long purchaseOrderNo;

    @ApiModelProperty(value = "주문일시", position = 2)
    private String orderDt;
    //    long totShipPrice;
    @ApiModelProperty(value = "총결제금액", position = 3)
    private long totPaymentAmt;

    @ApiModelProperty(value = "노출 상품명", position = 4)
    private String displayItemNm;

    @ApiModelProperty(value = "구매자명", position = 5)
    private String buyerNm;
}