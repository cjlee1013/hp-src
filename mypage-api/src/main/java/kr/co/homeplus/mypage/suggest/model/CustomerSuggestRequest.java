package kr.co.homeplus.mypage.suggest.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Size;
import kr.co.homeplus.mypage.enums.OsType;
import kr.co.homeplus.mypage.enums.SiteType;
import kr.co.homeplus.mypage.enums.PlatformType;
import lombok.Data;

@ApiModel("개선 제안 요청")
@Data
public class CustomerSuggestRequest {

    @ApiModelProperty(value = "고객 번호", required = true)
    private String userNo;

    @ApiModelProperty(value = "개선 내용", required = true)
    @Size(max = 500)
    private String message;

    @ApiModelProperty(value = "사이트 구분", required = true, allowableValues = "HOME, CLUB")
    private SiteType siteType;

    @ApiModelProperty(value = "앱 구분", required = true, allowableValues = "APP, MWEB")
    private PlatformType platformType;

    @ApiModelProperty(value = "단말 OS", allowableValues = "ANDROID, IOS")
    private OsType osType;

    @ApiModelProperty(value = "단말 모델 명", hidden = true)
    private String deviceModel;
}
