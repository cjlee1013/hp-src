package kr.co.homeplus.mileagebatch.batch.sample.service.impl;

import kr.co.homeplus.mileagebatch.batch.TestConfig;

import kr.co.homeplus.mileagebatch.sample.model.SampleDto;
import kr.co.homeplus.mileagebatch.sample.service.SampleService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * SampleService test
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Suite.SuiteClasses({TestConfig.class})
public class SampleServiceImplTests {
    @Autowired
    private SampleService sampleService;

    @Test
    public void test_sampleService() {
        SampleDto sampleDto = sampleService.getSampleData();

        Assert.assertEquals("123", sampleDto.getId());
        Assert.assertEquals("homeplus", sampleDto.getName());
        Assert.assertEquals((Integer) 1, sampleDto.getAge());
    }
}
