package kr.co.homeplus.mileagebatch.management.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import kr.co.homeplus.mileagebatch.enums.MileageCommonCode;
import kr.co.homeplus.mileagebatch.management.model.common.PaymentResultDto;
import kr.co.homeplus.mileagebatch.message.model.UserInfoDto;
import kr.co.homeplus.mileagebatch.utils.DateUtils;
import kr.co.homeplus.mileagebatch.utils.MessageSendUtil;
import kr.co.homeplus.mileagebatch.utils.ObjectUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class ManagementService {
    // Mileage Save/Return/Expire Processing Management Service
    private final ProcessManagementService managementService;
    // Mapper Management Service
    private final ManagementMapperService mapperService;

    /**
     * 마일리지 적립 프로세스
     * 마일리지 적립 수행일을 입력받아
     * 해당 일에 적립을 해야할 데이터들을 마일리지 적립할 수 있도록 함.
     *
     * @param executeDay 적립수행일
     * @throws Exception 오류발생시 Rollback 수행.
     */
    @Transactional(rollbackFor = Exception.class)
    public void actMileageSave(String executeDay) throws Exception {

        // 수행일을 입력받지 못하면 오늘 날짜로 수행.
        if(StringUtils.isEmpty(executeDay)){
            executeDay = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        }

        //전체 건수를 가지고 옴.( -> req_no 만 우선적으로 가지고 온다.)
        List<Long> processTargetList = mapperService.getMileageSaveReqNoList(executeDay, MileageCommonCode.MILEAGE_REQUEST_TYPE_SAVE.getResponseCode());

        // 처리 대상건들에 대한 원장 업데이트
        for(long mileageReqNo : processTargetList){
            // update mileage_req
            HashMap<String, Object> reqUpdateMap = new HashMap<>();
            reqUpdateMap.put("mileageReqNo", mileageReqNo);
            reqUpdateMap.put("requestStatus", MileageCommonCode.MILEAGE_REQUEST_STATUS_PROCESSING.getResponseCode());
            reqUpdateMap.put("chgId", "BATCH");

            // Next Step 수행 여부.
            Boolean isStepContinue = Boolean.TRUE;

            // 마일리지 지급/회수 요청 업데이트
            // 적립/회수 요청의 상태를 진행중으로 변경.
            if(!mapperService.modifyMileageReqInfo(reqUpdateMap)){
                isStepContinue = Boolean.FALSE;
            }

            // Next Step 수행이 True 일 때 진행.
            if(isStepContinue){
                // Async processing
                // 마일리지 적립 수행.
                PaymentResultDto resultDto = managementService.mileageSaveProcessing(mileageReqNo).get();

                // 마일리지 지급/회수 상태를 완료로 업데이트.
                reqUpdateMap.put("requestStatus", MileageCommonCode.MILEAGE_REQUEST_STATUS_COMPLETE.getResponseCode());
                if(resultDto.failDto.getFailCnt() > 0){
                    // 실패건이 1건이라도 존재하면 실패로 간주.
                    reqUpdateMap.put("requestStatus", MileageCommonCode.MILEAGE_REQUEST_STATUS_FAIL.getResponseCode());
                }

                //TODO::
                // 마일리지 지급/회수 상태 업데이트 오류시
                // 처리방법에 대한 확인이 필요하여 우선은 log 상태로 업데이트 수행하여 결과만 나오도록 함.
                // 추후 해당 처리방법에 대하여 정의 후 수정 예정.
                log.info("마일리지 지급/회수 최종 상태 저장 [{}]", mapperService.modifyMileageReqInfo(reqUpdateMap));
            }
        }
    }
    @Transactional(rollbackFor = Exception.class)
    public void actMileageReturn(String executeDay) throws Exception{

        // 수행일을 입력받지 못하면 오늘 날짜로 수행.
        if(StringUtils.isEmpty(executeDay)){
            executeDay = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        }

        //전체 건수를 가지고 옴.( -> req_no 만 우선적으로 가지고 온다.)
        List<Long> processTargetList = mapperService.getMileageSaveReqNoList(executeDay, MileageCommonCode.MILEAGE_REQUEST_TYPE_RETURN.getResponseCode());

        // 처리 대상건들에 대한 원장 업데이트
        for(long mileageReqNo : processTargetList) {
            // update mileage_req
            HashMap<String, Object> reqUpdateMap = new HashMap<>();
            reqUpdateMap.put("mileageReqNo", mileageReqNo);
            reqUpdateMap.put("requestStatus", MileageCommonCode.MILEAGE_REQUEST_STATUS_PROCESSING.getResponseCode());
            reqUpdateMap.put("chgId", "BATCH");

            // Next Step 수행 여부.
            Boolean isStepContinue = Boolean.TRUE;

            // 마일리지 지급/회수 요청 업데이트
            // 적립/회수 요청의 상태를 진행중으로 변경.
            if (!mapperService.modifyMileageReqInfo(reqUpdateMap)) {
                isStepContinue = Boolean.FALSE;
            }

            // Next Step 수행이 True 일 때 진행.
            if (isStepContinue) {
                // Async processing
                // 마일리지 적립 수행.
                PaymentResultDto resultDto = managementService.mileageReturnProcessing(mileageReqNo).get();

                // 마일리지 지급/회수 상태를 완료로 업데이트.
                reqUpdateMap.put("requestStatus", MileageCommonCode.MILEAGE_REQUEST_STATUS_COMPLETE.getResponseCode());
                if (resultDto.failDto.getFailCnt() > 0) {
                    // 실패건이 1건이라도 존재하면 실패로 간주.
                    reqUpdateMap.put("requestStatus", MileageCommonCode.MILEAGE_REQUEST_STATUS_FAIL.getResponseCode());
                }

                //TODO::
                // 마일리지 지급/회수 상태 업데이트 오류시
                // 처리방법에 대한 확인이 필요하여 우선은 log 상태로 업데이트 수행하여 결과만 나오도록 함.
                // 추후 해당 처리방법에 대하여 정의 후 수정 예정.
                log.info("마일리지 지급/회수 최종 상태 저장 [{}]",
                    mapperService.modifyMileageReqInfo(reqUpdateMap));
            }
        }
    }

    public void actMileageExpire(String executeDay) throws Exception {
        // 수행일을 입력받지 못하면 오늘 날짜로 수행.
        if(StringUtils.isEmpty(executeDay)){
            executeDay = DateUtils.getCurrentYmd();
        }
        log.info("aa : {}", executeDay);
        log.info("actMileageExpire {}", managementService.mileageExpireProcessing(executeDay));
    }

    public void actMileageExpireExpected(String executeDay) throws Exception {
        if(StringUtils.isEmpty(executeDay)){
            executeDay = DateUtils.getCurrentYmd();
        }
        List<LinkedHashMap<String, Object>> expireList = mapperService.getMileageExpireList(executeDay);
        log.info("마일리지 2주소멸 알림톡 안내 ({})일 기준 ({})건 처리", executeDay, expireList.size());

        for(LinkedHashMap<String, Object> map : expireList){
            log.info("마일리지 2주소멸 알림톡 안내 정보 :: mileageUseDto :: ({})", map);
            try {
                UserInfoDto userInfoDto = managementService.getUserMobileInfo(ObjectUtils.toLong(map.get("user_no")));
                map.put("ship_mobile_no", userInfoDto.getMobile());
                log.info("actMileageExpire {}", managementService.mileageExpireExpected(map));
            } catch (Exception e) {
                log.error("오류발생 : {}", e.getMessage());
            }
        }

    }

}
