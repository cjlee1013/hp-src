package kr.co.homeplus.mileagebatch.enums.impl;

public interface EnumImpl {
    String getResponseCode();
    String getResponseMessage();
}
