package kr.co.homeplus.mileagebatch.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ExternalUrlInfo {
    USER_INFO("user", "/search/basic/getUserInfo"),
    ;

    @Getter
    private final String apiId;

    @Getter
    private final String uri;

}
