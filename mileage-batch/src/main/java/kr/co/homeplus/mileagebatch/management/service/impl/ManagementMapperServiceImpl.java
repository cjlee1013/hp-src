package kr.co.homeplus.mileagebatch.management.service.impl;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import kr.co.homeplus.mileagebatch.management.mapper.ManagementReadMapper;
import kr.co.homeplus.mileagebatch.management.mapper.ManagementWriteMapper;
import kr.co.homeplus.mileagebatch.management.model.common.MileageHistoryManageSetDto;
import kr.co.homeplus.mileagebatch.management.model.payment.MileageManageDto;
import kr.co.homeplus.mileagebatch.management.model.payment.MileageSaveDto;
import kr.co.homeplus.mileagebatch.management.model.payment.MileageUseDto;
import kr.co.homeplus.mileagebatch.management.model.payment.ReturnMileageSetDto;
import kr.co.homeplus.mileagebatch.management.model.user.MileageMstDto;
import kr.co.homeplus.mileagebatch.management.service.ManagementMapperService;
import kr.co.homeplus.mileagebatch.utils.MileageCommonUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ManagementMapperServiceImpl implements ManagementMapperService {

    // slave - read only
    private final ManagementReadMapper readMapper;
    // master - read/write
    private final ManagementWriteMapper writeMapper;


    //============================================================================================================
    // 마일리지 지급/회수 관련 Method
    //============================================================================================================

    /**
     * 마일리지 지급/회수 요청번호 리스트조회
     * (해당 요청번호로 작업상태 변경)
     *
     * @param executeDay 실행일
     * @return List<Long> 요청번호 리스트
     */
    @Override
    public List<Long> getMileageSaveReqNoList(String executeDay, String requestType){
        return readMapper.selectMileageReqNoList(executeDay, requestType);
    }

    /**
     * 마일리지 마스터 회원존재 여부 체크(회원번호 기준)
     *
     * @param userNo 회원관리번호
     * @return Boolean 존재여부
     * @throws Exception 예외가 발생할 경우 던짐
     */
    @Override
    public Boolean checkUserMileageMst(String userNo) throws Exception {
        return MileageCommonUtil.isGreaterThanZero(readMapper.selectUserMileageMstCheck(userNo));
    }

    /**
     * 마일리지 마스터 회원정보
     *
     * @param userNo 회원관리번호
     * @return MileageMstDto 회원 Dto
     */
    @Override
    public MileageMstDto getUserMileageMstInfo(String userNo) {
        return readMapper.selectUserMileageMstInfo(userNo);
    }

    /**
     * 마일리지 요청정보 수정.
     *
     * @param parameterMap 마일리지 지급/회수 수정데이터 MAP
     * @return Boolean
     */
    @Override
    public Boolean modifyMileageReqInfo(HashMap<String, Object> parameterMap){
        return MileageCommonUtil.isGreaterThanZero(writeMapper.updateMileageReqStatus(parameterMap));
    }

    /**
     * 마일리지 지급/회수 대상회원 상태 수정
     *
     * @param parameterMap 마일리지 지급/회수 대상회원 상태변경 데이터
     * @return CompletableFuture<Boolean>
     */
    @Override
    public Boolean modifyMileageReqUserInfo(HashMap<String, Object> parameterMap){
        return MileageCommonUtil.isGreaterThanZero(writeMapper.updateMileageReqUserStatus(parameterMap));
    }
    //============================================================================================================

    //============================================================================================================
    // 마일리지 적립 관련 Method
    //============================================================================================================
    /**
     * 마일리지 적립대상 조회
     *
     * @param mileageReqNo 적립대상을 조회 할 마일리지 지급/회수 관리번호
     * @return List<MileageSaveDto> 적립대상정보를 List 로 리턴.
     */
    @Override
    public List<MileageSaveDto> getMileageSaveInfoList(long mileageReqNo) {
        return writeMapper.selectMakeMileageSaveInfo(mileageReqNo);
    }

    /**
     * 마일리지 적립 적재
     *
     * @param mileageSaveDto 마일리지 적립정보 DTO
     * @return Boolean 적재여부
     */
    @Override
    public Boolean addMileageSaveInfo(MileageSaveDto mileageSaveDto) {
        return MileageCommonUtil.isGreaterThanZero(writeMapper.insertMileageSaveInfo(mileageSaveDto));
    }

    /**
     * 마일리지 계좌 정보 수정.
     *
     * @param userNo 수정대상 고객관리번호
     * @param mileageAmt 마일리지 금액
     * @return Boolean 수정여부
     */
    @Override
    public Boolean modifyMileageMstInfo(String userNo, long mileageAmt) {
        return MileageCommonUtil.isGreaterThanZero(writeMapper.updateUserMileageMst(userNo, mileageAmt));
    }

    /**
     * 마일리지 적립 수정 - 사용처리.
     *
     * @param mileageSaveDto 마일리지 적립 DTO
     * @return Boolean 수정성공여부
     */
    @Override
    public Boolean modifyMileageSaveInfo(MileageSaveDto mileageSaveDto){
        return MileageCommonUtil.isGreaterThanZero(writeMapper.updateUserMileageSaveUsed(mileageSaveDto));
    }

    @Override
    public List<LinkedHashMap<String, Object>> getMileageExpireList(String executeDay) {
        return readMapper.selectMileageExpireUserInfo(executeDay);
    }
    //============================================================================================================

    //============================================================================================================
    // 마일리지 회수/소멸 관련 Method
    //============================================================================================================

    /**
     * 마일리지 회수 정보 조회(회수데이터 생성)
     *
     * @param mileageReqNo 마일리지 지급/회수 요청번호
     * @return List<ReturnMileageSetDto> 마일리지 회수 Data List.
     */
    @Override
    public List<ReturnMileageSetDto> getMileageReturnInfo(long mileageReqNo) {
        return writeMapper.selectMakeMileageReturnInfo(mileageReqNo);
    }

    /**
     * 요청한 마일리지만큼 마일리지 적재에서 조회
     * (SP를 활용하여 요청 금액 만큼 사용가능한 적립건을 리턴한다.)
     *
     * @param userNo 회원관리번호
     * @param reqAmt 사용 요청금액
     * @return List<SaveMileageDto>
     */
    @Override
    public List<MileageSaveDto> getUserMileageUsableList(String userNo, long reqAmt) {
        return readMapper.selectUserMileageUsableList(userNo, reqAmt);
    }

    /**
     * 마일리지 사용 처리(mileage_use)
     *
     * @param mileageUseDto 마일리지 사용 DB Entity
     * @return Boolean 등록 성공/실패
     */
    @Override
    public boolean addUserMileageUse(MileageUseDto mileageUseDto) {
        return MileageCommonUtil.isGreaterThanZero(writeMapper.insertUserMileageUse(mileageUseDto));
    }

    /**
     * 마일리지 관리 데이터 저장
     * (마일리지 사용을 위한 적재건들 저장)
     *
     * @param mileageManageDto 마일리지 관리 DB Entity
     * @return Boolean 저장 성공/실패
     */
    @Override
    public Boolean addMileageManageInfo(MileageManageDto mileageManageDto) {
        return MileageCommonUtil.isGreaterThanZero(writeMapper.insertMileageManageInfo(mileageManageDto));
    }

    /**
     * 마일리지 소멸 건수 조회
     * @param executeDay 실행일
     * @return List<MileageUseDto> 소멸건수 리스트
     */
    @Override
    public List<MileageUseDto> getMileageExpireInfoList(String executeDay) {
        return readMapper.selectMakeMileageExpireInfo(executeDay);
    }

    /**
     * 마일리지 소멸 가능 건 조회
     *
     * @param userNo 고객관리번호
     * @param tradeNo 마일리지 적립 관리번호
     * @return MileageSaveDto 마일리지 적립 정보.
     */
    @Override
    public List<MileageSaveDto> getMileageExpirePossibleInfo(String userNo, long tradeNo){
        return readMapper.selectMileageExpirePossibleList(userNo, tradeNo);
    }

    //============================================================================================================

    //============================================================================================================
    // 마일리지 히스토리 관련 Method
    //============================================================================================================

    /**
     * 마일리지 히스토리 적재
     *
     * @param mileageHistoryManageSetDto 마일리지 히스토리 정보 DTO
     * @return Boolean 적재여부
     */
    @Override
    public Boolean addMileageHistoryInfo(MileageHistoryManageSetDto mileageHistoryManageSetDto) {
        return MileageCommonUtil.isGreaterThanZero(writeMapper.insertUserMileageHistory(mileageHistoryManageSetDto));
    }
    //============================================================================================================

}
