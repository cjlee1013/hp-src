package kr.co.homeplus.mileagebatch.management.sql;

import java.util.Arrays;
import java.util.HashMap;
import kr.co.homeplus.mileagebatch.constants.Constants;
import kr.co.homeplus.mileagebatch.constants.SqlPatternConstants;
import kr.co.homeplus.mileagebatch.enums.MileageCommonCode;
import kr.co.homeplus.mileagebatch.enums.MysqlFunction;
import kr.co.homeplus.mileagebatch.utils.DateUtils;
import kr.co.homeplus.mileagebatch.utils.EntityFactory;
import kr.co.homeplus.mileagebatch.entity.MileageReqDetailEntity;
import kr.co.homeplus.mileagebatch.entity.MileageReqEntity;
import kr.co.homeplus.mileagebatch.entity.MileageTypeEntity;
import kr.co.homeplus.mileagebatch.utils.ObjectUtils;
import kr.co.homeplus.mileagebatch.utils.SqlUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

@Slf4j
public class MileagePaymentSql {
    // 마일리지 지급/회수 기본정보 테이블.(Alias 포함)
    public static final String paymentBasicTable = EntityFactory.getTableNameWithAlias(MileageReqEntity.class);
    // 마일리지 지급/회수 대상회원 테이블.(Alias 포함)
    public static final String paymentUserTable = EntityFactory.getTableNameWithAlias(MileageReqDetailEntity.class);
    // 마일리지 유형 테이블.(Alias 포함)
    public static final String typeTable = EntityFactory.getTableNameWithAlias(MileageTypeEntity.class);

    // 마일리지 지급/회수 기본정보 컬럼정보.(Alias 포함)
    public static final MileageTypeEntity mt = EntityFactory.createEntityIntoValue(MileageTypeEntity.class, Boolean.TRUE);
    // 마일리지 지급/회수 대상회원 컬럼정보.(Alias 포함)
    public static final MileageReqEntity mr = EntityFactory.createEntityIntoValue(MileageReqEntity.class, Boolean.TRUE);
    // 마일리지 유형 컬럼정보.(Alias 포함)
    public static final MileageReqDetailEntity mrd = EntityFactory.createEntityIntoValue(MileageReqDetailEntity.class, Boolean.TRUE);

    /**
     * 마일리지 지급/회수 기본정보 수정.
     *
     * @param parameterMap 수정정보 Map
     * @return String 마일리지 지급/회수 기본정보 수정용 Update Query.
     */
    public static String updateMileageReqStatus(HashMap<String, Object> parameterMap){
        SQL query = new SQL()
            // UPDATE 대상 Table 설정
            .UPDATE(paymentBasicTable);
        // TODO::주석부분
        //  수정정보 Map 내의 key 와 정보를 가지고 와서 SET 정보 생성하도록 함.
        //  현재는 수정내용 범위 확인이 애매하여 직접 확인 후 생성.
        //  하지만 이후, 수정내용이 확실해지면 아래 내용으로 변경할 예정.
//        parameterMap.forEach((key, value) -> {
//            if (value != null && !key.equalsIgnoreCase("mileageReqNo")) {
//                query.SET(SqlUtils.equalColumnMapping(key));
//            }
//        });
        // 수정정보 Map 에 요청상태 정보가 있으면 SET Query 생성.
        if(parameterMap.get("requestStatus") != null){
            // 요청상태 SET 쿼리를 컬럼 기준으로 생성. (mr.request_status = #{requestStatus}
            query.SET(SqlUtils.equalColumnMapping(mr.requestStatus));

            // 상태가 완료인 경우 오늘 날짜로 completeDt를 설정한다.
            if(parameterMap.get("requestStatus").equals(MileageCommonCode.MILEAGE_REQUEST_STATUS_COMPLETE.getResponseCode())){
                query.SET(
                    // 입력값을 Non SingleQuote 로 맵핑하여 쿼리 생성.
                    // mr.complete_dt = DATE_FORMAT(NOW(), '%Y-%m-%d')
                    SqlUtils.equalColumn(
                        mr.completeDt,
                        SqlPatternConstants.getConditionByPattern("DATE_NOW_INPUT", "%Y-%m-%d")
                    )
                );
            }
        }

        // 수정정보 Map 에 수정아이디가 있으면 생성.
        if(parameterMap.get("chgId") != null){
            // 입력받은 정보로 SET 할 수 있는 쿼리 생성 ( mr.chg_id = #{chgId} )
            query.SET(SqlUtils.equalColumnMapping(mr.chgId));
            // 수정아이디가 존재하면 수정일도 같이 업데이트
            // 현재 날짜로 업데이트 할 수 있는 쿼리 생성 ( mr.chg_dt = NOW() )
            query.SET(SqlUtils.equalColumn(mr.chgDt, "NOW()"));
        }
        // UPDATE 할 조건 설정
        // 입력받은 컬럼 정보로 업데이트 할 수 있는 쿼리 생성 ( mr.mileage_req_no = #{mileageReqNo} )
        query.WHERE(SqlUtils.equalColumnMapping(mr.mileageReqNo));

        return query.toString();
    }

    /**
     * 마일리지 지급/회수 대상회원 정보 수정.
     *
     * @param parameterMap 수정정보 Map
     * @return String 마일리지 지급/회수 대상회원 수정용 Update Query.
     */
    public static String updateMileageReqUserStatus(HashMap<String, Object> parameterMap){
        SQL query = new SQL()
            .UPDATE(paymentUserTable);

        parameterMap.forEach((key, value) -> {
            if (value != null && !key.equals("mileageReqDetailNo")) {
                query.SET(SqlUtils.equalColumnMapping(SqlUtils.convertCamelCaseToSnakeCase(key)));
                if(key.equals("chgId")){
                    query.SET(SqlUtils.nonSingleQuote(mrd.chgDt, "NOW()"));
                }
                if(key.equals("detailCode") && !value.equals("00")){
                    query.SET(
                        SqlUtils.nonSingleQuote(
                            mrd.detailMessage,
                            SqlUtils.sqlFunction(MysqlFunction.MILEAGE_COMMON_TABLE, mrd.detailCode, ObjectUtils.toArray(SqlUtils.appendSingleQuote(value)))
                        )
                    );
                }
            }
        });

        query.WHERE(
            SqlUtils.equalColumnMapping(mrd.mileageReqDetailNo)
        );

        log.debug("updateMileageReqUserStatus \n[{}]", query.toString());
        return query.toString();
    }

    /**
     * 마일리지 지급(적립) 대상 조회
     * (* Method Value Input 시 다중일 경우 반드시 @param 을 사용하여 처리해야함.)
     *
     * @param mileageReqNo 마일리지 지급/회수 관리번호
     * @return String 마일리지 지급(적립) 대상 조회용 Select Query.
     * @throws Exception 오류 발생시 오류 처리.
     */
    public static String selectMakeMileageSaveInfo(@Param("mileageReqNo") long mileageReqNo) throws Exception {
        // 각 테이블 컬럼에 Alias 추가
        SQL query = new SQL()
            // SELECT 할 컬럼 설정.
            .SELECT(
                mrd.mileageReqDetailNo,
                mr.mileageTypeNo,
                mrd.userNo,
                mt.mileageCategory,
                mt.mileageKind,
                mrd.mileageAmt,
                SqlUtils.sqlFunction(
                    MysqlFunction.CASE,
                    ObjectUtils.toArray(
                         SqlUtils.sqlFunction(MysqlFunction.IS_NULL, mt.useStartDt),
                        SqlUtils.sqlFunction(MysqlFunction.DATE_FULL_ZERO, "NOW()"),
                        mt.useStartDt
                    ),
                    "use_start_dt"
                ),
                "CASE mt.expire_type WHEN 'TT' THEN mt.use_end_dt ELSE DATE_FORMAT(ADDDATE(NOW(), INTERVAL mt.expire_day_inp DAY), '%Y-%m-%d 23:59:59') END expire_dt",
                SqlUtils.aliasToRow(mrd.mileageAmt, "mileage_remain_amt"),
                SqlUtils.appendSingleQuoteWithAlias("BATCH", mr.regId),
                SqlUtils.appendSingleQuoteWithAlias("BATCH", mr.chgId),
                mr.requestReason
            )
            // SELECT 대상 Table 설정.
            .FROM(paymentUserTable)
            // INNER JOIN 설정.
            .INNER_JOIN(
                // INNER JOIN 을 생성할 수 있는 Method.
                // createJoinCondition ( String, List<Object>, Object[])
                // createJoinCondition ( INNER JOIN 대상 테이블, 동일한 컬럼으로 맵핑되는 컬럼정보, 특정값을 입력하여 맵핑하는 컬럼정보)
                SqlUtils.createJoinCondition(
                    paymentBasicTable,
                    Arrays.asList(
                        // 동일컬럼으로 맵핑하는 쿼리 생성.
                        // onClause(new Object[], Object)
                        // onClause(컬럼정보, 맵핑되는 컬럼의 instance)
                        SqlUtils.onClause(
                            new Object[]{
                                mrd.mileageReqNo
                            },
                            mr
                        )
                    ),
                    new Object[]{
                        // 마일리지 지급/회수 요청정보의 사용유무가 'Y' 인 경우만 포함되도록 함.
                        // 입력받은 데이터로 맵핑하는 쿼리 생성. ( mr.use_yn = 'Y' )
                        SqlUtils.equalColumnByInput(mr.useYn, Constants.USE_Y),
                        // 마일리지 지급/회수 요청정보의 상태가 '대기' 인 경우만 포함되도록 함.
                        // 입력받은 데이터를 SingleQuote 를 추가하여 맵핑하는 쿼리 생성. ( mr.request_status = 'M1' )
                        SqlUtils.equalColumnByInput(mr.requestStatus, MileageCommonCode.MILEAGE_REQUEST_STATUS_PROCESSING.getResponseCode()),
                    }
                )
            )
            .INNER_JOIN(
                // INNER JOIN 을 생성할 수 있는 Method.
                // createJoinCondition ( String, List<Object>, Object[])
                // createJoinCondition ( INNER JOIN 대상 테이블, 동일한 컬럼으로 맵핑되는 컬럼정보, 특정값을 입력하여 맵핑하는 컬럼정보)
                SqlUtils.createJoinCondition(
                    typeTable,
                    Arrays.asList(
                        // 동일컬럼으로 맵핑하는 쿼리 생성.
                        // onClause(new Object[], Object)
                        // onClause(컬럼정보, 맵핑되는 컬럼의 instance)
                        SqlUtils.onClause(new Object[]{
                                mt.mileageTypeNo
                            },
                            mr
                        )
                    ),
                    new Object[]{
                        // 입력받은 데이터를 SingleQuote 를 추가하여 맵핑하는 쿼리 생성. ( mt.use_yn = 'M1' )
                        // 마일리지 유형의 사용유무가 Y 인 경우만 포함되도록 설정.
                        SqlUtils.equalColumnByInput(mt.useYn, Constants.USE_Y)
                    }
                )
            )
            // SELECT 대상 조회 쿼리
            // 마일리지 지급/회수 요청정보의 관리번호가 입력받은 값이 경우.
            // 입력받은 데이터를 컬럼에 맵핑하여 쿼리 생성.( mr.mileage_req_no = mileageReqNo )
            .WHERE(SqlUtils.equalColumnByInput(mr.mileageReqNo, mileageReqNo))
            // 마일리지 지급/회수 대상회원의 사용유무가 'Y' 인 경우.
            // 입려받은 데이터에 SingleQuote 를 추가하여 쿼리 생성. ( mrd.use_yn = 'Y')
            .WHERE(SqlUtils.equalColumnByInput(mrd.useYn, Constants.USE_Y))
            // 마일리지 지급/회수 대상회원의 상세코드가 '00' 인 경우.
            // 입려받은 데이터에 SingleQuote 를 추가하여 쿼리 생성. ( mrd.detailCode = '00')
            .WHERE(SqlUtils.equalColumnByInput(mrd.detailCode, "00"))
            // 마일리지 지급/회수 대상회원의 상태가 '대기' 인 경우.
            // 입려받은 데이터에 SingleQuote 를 추가하여 쿼리 생성. ( mrd.mileage_detail_status = 'D1')
            .WHERE(SqlUtils.equalColumnByInput(mrd.mileageDetailStatus, MileageCommonCode.MILEAGE_REQUEST_DETAIL_STATUS_WAIT.getResponseCode()))

            ;

        return query.toString();
    }

    /**
     * 마일리지 지급/회수 요청 건 조회
     * 실행일과 요청유형에 따른 조회 실시.
     *
     * @param executeDay 실행일
     * @param requestType 요청타입
     * @return String 마일리지 지급/회수 요청 건 조회 Select Query.
     * @throws Exception 오류 처리.
     */
    public static String selectMileageReqNoList(@Param("executeDay") String executeDay, @Param("requestType") String requestType ) throws Exception {
        SQL query = new SQL()
            // SELECT 대상 컬럼 설정.
            // 여기서 DISTINCT 하여 SELECT 하도록 설정.
            .SELECT_DISTINCT(mr.mileageReqNo)
            // SELECT 대상 Table 설정.
            .FROM(paymentBasicTable)
            // INNER JOIN 설정.
            .INNER_JOIN(
                // INNER JOIN 을 생성할 수 있는 Method.
                // createJoinCondition ( String, List<Object>, Object[])
                // createJoinCondition ( INNER JOIN 대상 테이블, 동일한 컬럼으로 맵핑되는 컬럼정보, 특정값을 입력하여 맵핑하는 컬럼정보)
                SqlUtils.createJoinCondition(
                    typeTable,
                    Arrays.asList(
                        // 동일컬럼으로 맵핑하는 쿼리 생성.
                        // onClause(new Object[], Object)
                        // onClause(컬럼정보, 맵핑되는 컬럼의 instance)
                        SqlUtils.onClause(
                            new Object[]{
                                mt.mileageTypeNo
                            },
                            mr
                        )
                    ),
                    new Object[]{
                        // 마일리지 유형의 사용유무가 'Y' 인 경우만 포함되도록 함.
                        // 입력받은 데이터로 맵핑하는 쿼리 생성. ( mt.use_yn = 'Y' )
                        SqlUtils.equalColumnByInput(mt.useYn, Constants.USE_Y)
                    }
                )
            )
            // 마일리지 지급/최수 요청정보의 사용유무가 'Y' 인 경우.
            // 입력받은 데이터에 SingleQuote 를 추가하여 쿼리 생성. ( mr.request_type = 'requestType' )
            .WHERE(
                SqlUtils.equalColumnByInput(mr.useYn, Constants.USE_Y),
                SqlUtils.equalColumnByInput(mr.requestType, requestType),
                SqlUtils.equalColumnByInput(mr.requestStatus, MileageCommonCode.MILEAGE_REQUEST_STATUS_WAIT.getResponseCode()),
                SqlUtils.equalColumnByInput(mr.processDt, DateUtils.getConvertDate(executeDay, DateUtils.DEFAULT_YMD)),
                SqlUtils.between(SqlUtils.appendSingleQuote(DateUtils.getConvertDate(executeDay, DateUtils.DEFAULT_YMD)), mt.regStartDt, mt.regEndDt)
            );
        log.debug("selectMileageReqNoList \n[{}]", query.toString());
        return query.toString();
    }

    /**
     * 마일리지 회수 정보 생성
     *
     * @param mileageReqNo 마일리지 지급/회수 관리번호
     * @return String 마일리지 회수 정보 생성 Query
     */
    public static String selectMakeMileageReturnInfo(@Param("mileageReqNo") long mileageReqNo){
        SQL query = new SQL()
            .SELECT(new String[]{
                mrd.userNo,
                SqlUtils.appendSingleQuoteWithAlias("01", "request_return_type"),
                "CASE WHEN mr.trade_no IS NULL THEN mr.mileage_req_no ELSE mr.trade_no END AS trade_no",
                SqlUtils.aliasToRow(mrd.mileageAmt, "requestReturnAmt"),
                SqlUtils.appendSingleQuoteWithAlias(mr.displayMessage, "request_Return_Message"),
                mrd.regId,
                mrd.mileageReqDetailNo
            })
            .FROM(paymentUserTable)
            .INNER_JOIN(
                // INNER JOIN 을 생성할 수 있는 Method.
                // createJoinCondition ( String, List<Object>, Object[])
                // createJoinCondition ( INNER JOIN 대상 테이블, 동일한 컬럼으로 맵핑되는 컬럼정보, 특정값을 입력하여 맵핑하는 컬럼정보)
                SqlUtils.createJoinCondition(
                    paymentBasicTable,
                    Arrays.asList(
                        // 동일컬럼으로 맵핑하는 쿼리 생성.
                        // onClause(new Object[], Object)
                        // onClause(컬럼정보, 맵핑되는 컬럼의 instance)
                        SqlUtils.onClause(
                            new Object[]{
                                mrd.mileageReqNo
                            },
                            mr
                        )
                    ),
                    new Object[]{
                        // 마일리지 지급/회수 요청정보의 사용유무가 'Y' 인 경우만 포함되도록 함.
                        // 입력받은 데이터로 맵핑하는 쿼리 생성. ( mr.use_yn = 'Y' )
                        SqlUtils.equalColumnByInput(mr.useYn, Constants.USE_Y),
                        // 마일리지 지급/회수 요청정보의 상태가 '대기' 인 경우만 포함되도록 함.
                        // 입력받은 데이터를 SingleQuote 를 추가하여 맵핑하는 쿼리 생성. ( mr.request_status = 'M1' )
                        SqlUtils.equalColumnByInput(mr.requestStatus, MileageCommonCode.MILEAGE_REQUEST_STATUS_PROCESSING.getResponseCode()),
                    }
                )
            )
            // SELECT 대상 조회 쿼리
            // 마일리지 지급/회수 요청정보의 관리번호가 입력받은 값이 경우.
            // 입력받은 데이터를 컬럼에 맵핑하여 쿼리 생성.( mr.mileage_req_no = mileageReqNo )
            .WHERE(SqlUtils.equalColumnByInput(mr.mileageReqNo, mileageReqNo))
            // 마일리지 지급/회수 대상회원의 사용유무가 'Y' 인 경우.
            // 입려받은 데이터에 SingleQuote 를 추가하여 쿼리 생성. ( mrd.use_yn = 'Y')
            .WHERE(SqlUtils.equalColumnByInput(mrd.useYn, Constants.USE_Y))
            // 마일리지 지급/회수 대상회원의 상세코드가 '00' 인 경우.
            // 입려받은 데이터에 SingleQuote 를 추가하여 쿼리 생성. ( mrd.detailCode = '00')
            .WHERE(SqlUtils.equalColumnByInput(mrd.detailCode, "00"))
            // 마일리지 지급/회수 요청정의 요청유형이  '02' 인 경우.
            // 입려받은 데이터에 SingleQuote 를 추가하여 쿼리 생성. ( mr.request_type = '02')
            .WHERE(SqlUtils.equalColumnByInput(mr.requestType, MileageCommonCode.MILEAGE_REQUEST_TYPE_RETURN.getResponseCode()))
            // 마일리지 지급/회수 대상회원의 상태가 '대기' 인 경우.
            // 입려받은 데이터에 SingleQuote 를 추가하여 쿼리 생성. ( mrd.mileage_detail_status = 'D1')
            .WHERE(SqlUtils.equalColumnByInput(mrd.mileageDetailStatus, MileageCommonCode.MILEAGE_REQUEST_DETAIL_STATUS_WAIT.getResponseCode()));

        return query.toString();
    }

}
