package kr.co.homeplus.mileagebatch.entity;

public class EmployeeInfoEntity {
    //
    public String seq;
    //
    public String compcd;
    //
    public String idno;
    //
    public String empid;
    //
    public String name;
    //
    public String deptcd;
    //
    public String deptnm;
    //
    public String jobgrdcd;
    //
    public String jobgrdnm;
    //
    public String frcd;
    //
    public String frnm;
    //
    public String incumbcd;
    //
    public String incumbnm;
    //
    public String workareacd;
    //
    public String workareanm;
    //
    public String areacd;
    //
    public String areanm;
    //
    public String bicd;
    //
    public String binm;
    //
    public String loaappntdt;
    //
    public String reappntduedt;
    //
    public String retappntdt;
    //
    public String reappntappntdt;
    //
    public String jobcd;
    //
    public String jobnm;
    //
    public String pjobgrpcd;
    //
    public String occtgcd;
    //
    public String joblvlcd;
    //
    public String jobdetnm;
    //
    public String mptel;
    //
    public String ihmail;
    //
    public String rrt;
    //
    public String prcDt;
}
