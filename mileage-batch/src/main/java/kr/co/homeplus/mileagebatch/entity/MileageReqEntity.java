package kr.co.homeplus.mileagebatch.entity;

/**
 * 마일리지 지급/회수 요청(기본정보)
 */
public class MileageReqEntity {

    //마일리지요청번호
    public String mileageReqNo;

    //마일리지유형번호
    public String mileageTypeNo;

    //요청유형
    public String requestType;

    //거래번호(존재시에만)
    public String tradeNo;

    //요청대상건수
    public String requestCnt;

    //요청대상금액
    public String requestAmt;

    //요청상태
    public String requestStatus;

    //요청사유
    public String requestReason;

    //고객노출문구
    public String displayMessage;

    //요청메시지(상세사유)
    public String requestMessage;

    //사용여부
    public String useYn;

    //즉시여부
    public String directlyYn;

    //실행예정일
    public String processDt;

    //처리완료
    public String completeDt;

    //등록일
    public String regDt;

    //등록아이디
    public String regId;

    //수정일
    public String chgDt;

    //수정아이디
    public String chgId;
}
