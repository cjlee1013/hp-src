package kr.co.homeplus.mileagebatch.management.sql;

public class MileageCommonSql {

    // 마일리지 지급/회수 회원정보의 detail_code 에 대한 정보 조회.
    public static final String getDetailCode = "(SELECT IFNULL(code_name, '''') FROM mileage_common_code mcc WHERE mcc.code_group = ''detail_code'' and mcc.code = #{detailCode})";

}
