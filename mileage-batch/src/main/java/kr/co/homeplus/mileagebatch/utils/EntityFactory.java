package kr.co.homeplus.mileagebatch.utils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.mileagebatch.constants.Constants;
import kr.co.homeplus.mileagebatch.constants.CustomConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ReflectionUtils;

@Slf4j
public class EntityFactory {

    /**
     * tableName Create.
     * Entity Class 명으로 TableName 생성.
     *
     * @param beanClass tableName 을 생성할 Class
     * @return String tableName
     */
    public static String getTableName(Class<?> beanClass) {
        return convertEntityToTableName(beanClass, Boolean.FALSE);
    }

    /**
     * tableName Create.
     * Entity Class 명으로 TableName 생성.
     *
     * @param beanClass tableName 을 생성할 Object
     * @return String tableName
     */
    public static String getTableName(Object beanClass) {
        return convertEntityToTableName(beanClass.getClass(), Boolean.FALSE);
    }

    /**
     * tableName Create.(Add Alias)
     * Entity Class 명으로 TableName 생성하고
     * 생성된 TableName 으로 Alias 를 생성한 뒤 Append.
     *
     * @param beanClass tableName 을 생성할 Class
     * @return String tableName tn
     */
    public static String getTableNameWithAlias(Class<?> beanClass) {
        return convertEntityToTableName(beanClass, Boolean.TRUE);
    }

    /**
     * tableName Create.(Add Alias)
     * Entity Class 명으로 TableName 생성하고
     * 생성된 TableName 으로 Alias 를 생성한 뒤 Append.
     *
     * @param beanClass tableName 을 생성할 Class
     * @return String tableName tn
     */
    public static String getTableNameWithAlias(Object beanClass) {
        return convertEntityToTableName(beanClass.getClass(), Boolean.TRUE);
    }

    public static String getTableNameWithAlias(String schema, Object beanClass) {
        return schema.concat(Constants.DOT).concat(convertEntityToTableName(beanClass.getClass(), Boolean.TRUE));
    }

    public static String getTableNameWithAlias(Object beanClass, String alias) {
        return SqlUtils.getAppendAliasToTableName(convertEntityToTableName(beanClass.getClass(), Boolean.FALSE), alias);
    }

    /**
     * Column Name Create.
     * Entity Class 내의 Field 로 컬럼을 생성한다.
     *
     * @param beanClass Column 으로 생성할 Class
     * @return String[] Entity 컬럼배열
     */
    public static String[] getColumnInfo(Class<?> beanClass) {
        return convertEntityToArray(beanClass);
    }

    /**
     * 내부 클래스.
     * Class 명으로 TableName 생성.
     *
     * @param beanClass TableName 을 생성할 Class
     * @param isAlias alias 추가 여부.
     * @return String tableName.
     */
    private static String convertEntityToTableName(Class<?> beanClass, Boolean isAlias) {
        String tableName = null;

        try {
            tableName = SqlUtils.convertCamelCaseToSnakeCase(beanClass.getSimpleName().replace("Entity", ""));

            if(isAlias){
                tableName = SqlUtils.getAppendAliasToTableName(tableName);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return tableName;
    }

    /**
     * 내부 클래스.
     * Class 명으로 TableName 생성.
     *
     * @param beanClass TableName 을 생성할 Class
     * @param alias alias 별칭.
     * @return String tableName.
     */
    public static String convertEntityToTableName(Class<?> beanClass, String alias) {
        return SqlUtils.getAppendAliasToTableName(SqlUtils.convertCamelCaseToSnakeCase(beanClass.getSimpleName().replace("Entity", "")), alias);
    }

    /**
     * 내부 클래스.
     * Class 내의 Field 정보로 컬럼 생성.
     *
     * @param beanClass 컬럼으로 생성할 Class
     * @return String[] 컬럼 배열.
     */
    private static String[] convertEntityToArray(Class<?> beanClass) {
        try {
            List<String> columnsInfo = new ArrayList<>();
            for (Field field : beanClass.getDeclaredFields()) {
                columnsInfo.add(SqlUtils.convertCamelCaseToSnakeCase(field.getName()));
            }
            return columnsInfo.toArray(String[]::new);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Class 에 컬럼명을 맵핑.
     * Class 로 컬럼을 생성한 뒤, Field 에 컬럼값을 맵핑한다.
     *
     * @param beanClass 생성 & 맵핑 할 Class
     * @param <T> Class Type
     */
    public static <T>void convertEntityToArrayAfterValueSet(Object beanClass){
        convertEntityToArrayAfterValueSet(beanClass, Boolean.FALSE);
    }

    /**
     * Class 에 컬럼명을 맵핑.
     * Class 로 alias 가 포함된 컬럼을 생성한 뒤,
     * Field 에 컬럼값을 맵핑한다.
     *
     * @param beanClass 생성 & 맵핑 할 Class
     * @param isAlias alias 추가여부
     * @param <T> Class Type
     */
    public static <T>void convertEntityToArrayAfterValueSet(Object beanClass, Boolean isAlias){
        try {
            String alias = null;
            if(isAlias){
                alias = getTableName(beanClass.getClass());
            }
            for (Field field : beanClass.getClass().getDeclaredFields()) {
                ReflectionUtils.makeAccessible(field);
                ReflectionUtils.setField(field, beanClass, SqlUtils.convertCamelCaseToSnakeCase(field.getName(), alias));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 컬럼 정보가 포한된 Class 생성.
     * 입력받은 Class 로 컬럼생성 뒤,
     * Class Field 에 컬럼 값을 맵핑함.
     * 맵핑된 Class 대로 Class 를 생성할 수 있게 리턴.
     *
     * @param entityClass 입력 & 맵핑 & 리턴 할 Class 정보
     * @param <T> 입력받은 Class 타입
     * @return T 입력받은 Class
     */
    public static <T>T  createEntityIntoValue(Class<T> entityClass) {
        return createEntityIntoValue(entityClass, Boolean.FALSE);
    }

    /**
     * 컬럼 정보가 포한된 Class 생성.
     * 입력받은 Class 로 컬럼생성 뒤,
     * Class Field 에 컬럼 값을 맵핑함.
     * 맵핑된 Class 대로 Class 를 생성할 수 있게 리턴.
     *
     * @param entityClass 입력 & 맵핑 & 리턴 할 Class 정보
     * @param <T> 입력받은 Class 타입
     * @param isAlias alias 추가여부
     * @return T 입력받은 Class
     */
    @SuppressWarnings("unchecked")
    public static <T>T  createEntityIntoValue(Class<T> entityClass, Boolean isAlias) {

        // alias 여부.
        String alias = null;

        if(isAlias){
            alias = getTableName(entityClass);
        }

        //값을 생성하기 위한 새로운 Instance 를 가지고 온다.
        Object instance = newInstance(entityClass);

        try {
            for (Field field : instance.getClass().getDeclaredFields()) {
                ReflectionUtils.makeAccessible(field);
                ReflectionUtils.setField(field, instance, SqlUtils.convertCamelCaseToSnakeCase(field.getName(), alias));
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        return (T) instance;
    }

    /**
     * 컬럼 정보가 포한된 Class 생성.
     * 입력된 Alias 로 처리됨..
     *
     * @param entityClass 입력 & 맵핑 & 리턴 할 Class 정보
     * @param <T> 입력받은 Class 타입
     * @param alias 추가할 Alias
     * @return T 입력받은 Class
     */
    @SuppressWarnings("unchecked")
    public static <T>T  createEntityIntoValue(Class<T> entityClass, String alias) {
        //값을 생성하기 위한 새로운 Instance 를 가지고 온다.
        Object instance = newInstance(entityClass);

        try {
            for (Field field : instance.getClass().getDeclaredFields()) {
                ReflectionUtils.makeAccessible(field);
                ReflectionUtils.setField(field, instance, SqlUtils.convertCamelCaseToSnakeCase(field.getName(), alias));
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        return (T) instance;
    }

    public static Object getFieldValue(String fieldName, Object standardInstance){
        try {
            // 입력받은 instance 를 Class 화 한다.
            Class<?> standardClass = standardInstance.getClass();

            // alias 컬럼여부 확인.
            fieldName = aliasCheck(fieldName);
            if (fieldName.contains("_")) {
                fieldName = SqlUtils.snakeCaseToCamelCase(fieldName);
            }
            return standardClass.getDeclaredField(fieldName).get(standardInstance);
        } catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Class 의 Instance 를 생성한다.
     * @param standardClass instance 를 생성하고자 하는 Class
     * @return Object newInstance 정보.
     */
    public static Object newInstance (Class<?> standardClass){
        Object object = new Object();
        try {
            //Constructor 를 class 에서 가지고 옴.
            //해당 Constructor 를 새로운 instance 로 선언.
            object = standardClass.getDeclaredConstructor().newInstance();
        } catch (Exception e){
            e.printStackTrace();
        }

        return object;
    }

    /**
     * 내부클래스
     * alias 가 포함되었는 지 확인 후,
     * 있으면 제거, 없으면 입력받은 대로 리턴.
     *
     * @param column 확인할 컬럼 값
     * @return String alias 체크 된 상태의 컬럼 값.
     */
    private static String aliasCheck(String column){
        return column.contains(Constants.DOT) ? column.split(CustomConstants.SPLIT_DOT)[1] : column;
    }
}
