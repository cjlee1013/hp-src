package kr.co.homeplus.mileagebatch.management.model.payment;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "마일리지 소멸 요청 DTO", value = "ExpireMileageSetDto")
public class ExpireMileageSetDto {

    @ApiModelProperty(value = "고객번호", position = 1)
    private String userNo;

    @ApiModelProperty(value = "마일리지소멸구분", position = 2)
    private String requestExpireType;

    @ApiModelProperty(value = "소멸요청금액", position = 3)
    private long requestExpireAmt;

    @ApiModelProperty(value = "소멸내역 상세메시지", position = 4)
    private String requestExpireMessage;

    @ApiModelProperty(value = "등록아이디", position = 5)
    private String regId;

    @ApiModelProperty(value = "거래번호", position = 6)
    private long tradeNo;

}
