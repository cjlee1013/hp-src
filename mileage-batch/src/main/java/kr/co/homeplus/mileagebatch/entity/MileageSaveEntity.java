package kr.co.homeplus.mileagebatch.entity;

/**
 * 마일리지 적립 Entity
 */
public class MileageSaveEntity {
    // 마일리지적립번호
    public String mileageSaveNo;
    // 마일리지요청상세번호
    public String mileageReqDetailNo;
    // 마일리지유형번호
    public String mileageTypeNo;
    // 고객번호
    public String userNo;
    // 마일리지구분
    public String mileageCategory;
    // 마일리지종류
    public String mileageKind;
    // 마일리지금액
    public String mileageAmt;
    // 마일리지잔액
    public String mileageRemainAmt;
    // 사용시작일
    public String useStartDt;
    // 유효기간(만료일)
    public String expireDt;
    // 등록일
    public String regDt;
    // 등록아이디
    public String regId;
    // 수정일
    public String chgDt;
    // 수정아이디
    public String chgId;
    // 요청사유
    public String requestReason;
}
