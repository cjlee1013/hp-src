package kr.co.homeplus.mileagebatch.enums;

public enum DateType {
    YEAR, MONTH, DAY, HOUR, MIN, SEC
}
