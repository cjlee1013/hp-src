package kr.co.homeplus.mileagebatch.utils;

import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import kr.co.homeplus.mileagebatch.enums.MileageCommonCode;
import kr.co.homeplus.mileagebatch.management.model.payment.ExpireMileageSetDto;
import kr.co.homeplus.mileagebatch.management.model.payment.MileageUseDto;
import kr.co.homeplus.mileagebatch.management.model.payment.ReturnMileageSetDto;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

@Slf4j
public class MileageCommonUtil {

    /**
     * Object to Map[bean]
     *
     * @param object Map 으로 변경하고자하는 DTO
     * @return HashMap<String, Object> DTO 의 정보를 가진 HashMap
     */
    public static HashMap<String, Object> getConvertMap(Object object){
        try {
            return new HashMap<String, Object>(BeanUtils.describe(object));
        } catch (Exception e){
            return new HashMap<>();
        }

    }

    /**
     * 마일리지 사용 DB용 DTO 생성
     *
     * @param object 요청 DTO
     * @return MileageUseDto
     * @throws Exception 예외가 발생할 경우 던짐
     */
    public static MileageUseDto makeUseMileageEntity(Object object) throws Exception{
        HashMap<String, Object>  parameterMap = getConvertMap(object);

        //Object 별 타입 체크
        String mileageCategory = "MILEAGE_CATEGORY_";
        String requestType;

        // 마일리지 사용 DTO 는 아래의 타입만 가능
         if(object instanceof ReturnMileageSetDto){
            requestType = "return";
        } else if (object instanceof ExpireMileageSetDto){
            requestType = "expire";
        } else {
            log.error("사용할 수 없는 타입니다. {}", object.getClass().getSimpleName());
            throw new Exception("사용할 수 없는 타입니다.");
        }

        return MileageUseDto.builder()
            .userNo(String.valueOf(parameterMap.get("userNo")))
            .tradeNo(String.valueOf(parameterMap.get("tradeNo")))
            .mileageCategory(getMileageCommonCode(mileageCategory, requestType))
            .mileageKind(String.valueOf(parameterMap.get(insertByFirstCamelCase("requestType", requestType))))
            .mileageAmt(NumberUtils.toLong(String.valueOf(parameterMap.get(insertByFirstCamelCase("requestAmt", requestType)))))
            .useMessage(String.valueOf(parameterMap.get(insertByFirstCamelCase("requestMessage", requestType))))
            .regId(String.valueOf(parameterMap.get("regId")))
            .chgId(String.valueOf(parameterMap.get("regId")))
            .build();
    }

    /**
     * MileageCommonCode Enum 에서
     * 사용하고자 하는 enum 코드 가지고 오기.
     *
     * @param mileageCategory 찾고자하는 Category
     * @param searchType 찾고자 하는 타입
     * @return String 맵핑되어 있는 Category Type 의 코드를 리턴한다.
     */
    public static String getMileageCommonCode(String mileageCategory, String searchType){
        return Stream.of(MileageCommonCode.values())
            .filter(e -> e.name().contains(mileageCategory))
            .collect(Collectors.toMap(MileageCommonCode::name, MileageCommonCode::getResponseCode))
            .get(mileageCategory.concat(searchType.toUpperCase()));
    }

    /**
     * 페이징 OFFSET
     * @param pageNum 페이지번호
     * @param viewCount 한 번에 볼 페이지 로우 수
     * @return int 페이지 오프셋 정보
     */
    public static int getPageOffset(int pageNum, int viewCount){
        return ((pageNum - 1) * viewCount);
    }

    /**
     * 전체 페이지 계산
     * @param pageCnt 페이지 수
     * @param viewCount 페이지 내에서 한번에 볼 로우 수
     * @return int 전체 페이지 수
     */
    public static int getCalculationPage(int pageCnt, int viewCount){
        return (int) Math.ceil((double) pageCnt / viewCount);
    }

    /**
     * 0보다 큰지 확인.
     * 입력받은 값이 0보다 큰지 확인.
     *
     * @param checkBit 비교할 값
     * @return Boolean 비교값이 0보다 크면 TRUE 아니면 FALSE
     */
    public static Boolean isGreaterThanZero(int checkBit){
        return checkBit > 0 ? Boolean.TRUE : Boolean.FALSE;
    }

    /**
     * compare Value 보다 큰지 확인.
     * 입력받은 값이 0보다 큰지 확인.
     *
     * @param object 기본값
     * @param compareValue 비교값
     * @return Boolean 기본값이 비교값보다 크면 TRUE 아니면 FALSE
     */
    public static Boolean isGreaterThanValue(Object object, int compareValue){
        return NumberUtils.toInt(String.valueOf(object)) > compareValue ? Boolean.TRUE : Boolean.FALSE;
    }

    /**
     * CamelCase 사이에 특정데이터 입력
     * 처음 UPPER_CASE 앞에 입력 데이터값을 append 한다.
     * @param targetStr 데이터를 입력한 대상 문자열
     * @param inputStr 추가로 입력하고자 하는 문자열
     * @return String 대상 문자열에 추가문자열이 포함된 문자열
     */
    public static String insertByFirstCamelCase(String targetStr, String inputStr) {
        String[] splitByCamelCase = StringUtils.splitByCharacterTypeCamelCase(targetStr);
        StringBuilder sb = new StringBuilder();
        sb.append(splitByCamelCase[0]).append(StringUtils.capitalize(inputStr));
        for (int idx = 1; idx < splitByCamelCase.length; idx++) {
            sb.append(splitByCamelCase[idx]);
        }
        return sb.toString();
    }

    /**
     * List<DTO>> 형식 내 중복키 제거 유틸.
     *
     * @param keyExtractor
     * @param <T>
     * @return
     */
    public static <T> Predicate<T> distinctByKey( Function<? super T, Object> keyExtractor) {
        Map<Object, Boolean> map = new HashMap<>();
        return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }

    /**
     * Object type 변수가 비어있는지 체크
     *
     * @param obj empty 체크할 값
     * @return Boolean : true / false
     */
    public static Boolean empty(Object obj) {
        if (obj instanceof String) {
            return obj == null || "".equals(obj.toString().trim());
        } else if (obj instanceof List) {
            return obj == null || ((List) obj).isEmpty();
        } else if (obj instanceof Map) {
            return obj == null || ((Map) obj).isEmpty();
        } else if (obj instanceof Object[]) {
            return obj == null || Array.getLength(obj) == 0;
        } else {
            return obj == null;
        }
    }

    /**
     * Object type 변수가 비어있지 않은지 체크
     *
     * @param obj notEmpty 를 체크할 값
     * @return Boolean : true / false
     */
    public static Boolean notEmpty(Object obj) {
        return !empty(obj);
    }
}

