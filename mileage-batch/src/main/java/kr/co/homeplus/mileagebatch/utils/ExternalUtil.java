package kr.co.homeplus.mileagebatch.utils;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import kr.co.homeplus.mileagebatch.enums.ExternalUrlInfo;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.exception.ResourceClientException;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.ResolvableType;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class ExternalUtil {

    private final ResourceClient sourceResourceClient;
    private static ResourceClient resourceClient;

    @PostConstruct
    private void initialize(){
        resourceClient = sourceResourceClient;
    }

    /**
     * 외부연동 - METHOD.GET
     * GET방식으로 외부연동을 한다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param returnClass 응답받고자 하는 Class
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    public static <T> T getTransfer(ExternalUrlInfo apiInfo, Object parameter, Class<T> returnClass) throws Exception {
        return getResponseObject(getTransfer(apiInfo, parameter, null, createType(returnClass)));
    }

    public static <T> ResponseObject<T> getNonDataTransfer(ExternalUrlInfo apiInfo, Object parameter, Class<T> returnClass) throws Exception {
        return getTransfer(apiInfo, parameter, null, createType(returnClass));
    }

    public static <T> ResponseObject<T> postNonDataTransfer(ExternalUrlInfo apiInfo, Object parameter, Class<T> returnClass) throws Exception {
        return postTransfer(apiInfo, parameter, null, createType(returnClass));
    }

    /**
     * 외부연동 - METHOD.GET (feat. List)
     * GET방식으로 외부연동을 한다.
     * LIST 로 응답 받는다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param returnClass 응답받고자 하는 Class
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    @SuppressWarnings("unchecked")
    public static <T> List<T> getListTransfer(ExternalUrlInfo apiInfo, Object parameter, Class<T> returnClass) throws Exception {
        return (List<T>) getResponseObject(getTransfer(apiInfo, parameter, null, createListType(returnClass)));
    }

    /**
     * 외부연동 - METHOD.GET (include. header, List / Feat. ResponseObject)
     * GET 방식으로 외부연동을 한다.
     * HEADER 정보를 입력받아서 해당 헤더정보로 통신하며,
     * ResponseObject<LIST> 로 응답 받는다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param returnClass 응답받고자 하는 Class
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    @SuppressWarnings("unchecked")
    public static <T> ResponseObject<List<T>> getResponseTransfer(ExternalUrlInfo apiInfo, Object parameter, Class<T> returnClass) throws Exception {
        return (ResponseObject<List<T>>) getResponseObject(getTransfer(apiInfo, parameter, null, createResponseType(returnClass)));
    }

    /**
     * 외부연동 - METHOD.GET (include. header)
     * GET방식으로 외부연동을 한다.
     * HEADER 정보를 입력받아서 해당 헤더정보로 통신한다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param httpHeaders 헤더 정보
     * @param returnClass 응답받고자 하는 Class
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    public static <T> T getTransfer(ExternalUrlInfo apiInfo, Object parameter, HttpHeaders httpHeaders, Class<T> returnClass) throws Exception {
        return getResponseObject(getTransfer(apiInfo, parameter, httpHeaders, createType(returnClass)));
    }

    /**
     * 외부연동 - METHOD.GET (include. header / feat. List)
     * GET방식으로 외부연동을 한다.
     * HEADER 정보를 입력받아서 해당 헤더정보로 통신하며,
     * LIST 로 응답 받는다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param httpHeaders 헤더 정보
     * @param returnClass 응답받고자 하는 Class
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    @SuppressWarnings("unchecked")
    public static <T> List<T> getListTransfer(ExternalUrlInfo apiInfo, Object parameter, HttpHeaders httpHeaders, Class<T> returnClass) throws Exception {
        return (List<T>) getResponseObject(getTransfer(apiInfo, parameter, httpHeaders, createListType(returnClass)));
    }

    /**
     * 외부연동 - METHOD.GET (include. header, List / Feat. ResponseObject)
     * GET 방식으로 외부연동을 한다.
     * HEADER 정보를 입력받아서 해당 헤더정보로 통신하며,
     * ResponseObject<LIST> 로 응답 받는다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param httpHeaders 헤더 정보
     * @param returnClass 응답받고자 하는 Class
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    @SuppressWarnings("unchecked")
    public static <T> ResponseObject<List<T>> getResponseTransfer(ExternalUrlInfo apiInfo, Object parameter, HttpHeaders httpHeaders, Class<T> returnClass) throws Exception {
        return (ResponseObject<List<T>>) getResponseObject(getTransfer(apiInfo, parameter, httpHeaders, createResponseType(returnClass)));
    }

    /**
     * 외부연동 - METHOD.POST
     * POST 방식으로 외부연동을 한다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param returnClass 응답받고자 하는 Class
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    public static <T> T postTransfer(ExternalUrlInfo apiInfo, Object parameter, Class<T> returnClass) throws Exception {
        return getResponseObject(postTransfer(apiInfo, parameter, null, createType(returnClass)));
    }

    /**
     * 외부연동 - METHOD.POST (Feat. List)
     * POST 방식으로 외부연동을 한다.
     * LIST 로 응답 받는다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param returnClass 응답받고자 하는 Class
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    @SuppressWarnings("unchecked")
    public static <T> List<T> postListTransfer(ExternalUrlInfo apiInfo, Object parameter, Class<T> returnClass) throws Exception {
        return (List<T>) getResponseObject(postTransfer(apiInfo, parameter, null, createListType(returnClass)));
    }

    /**
     /**
     * 외부연동 - METHOD.POST (include. header, List / Feat. ResponseObject)
     * POST 방식으로 외부연동을 한다.
     * HEADER 정보를 입력받아서 해당 헤더정보로 통신하며,
     * ResponseObject<LIST> 로 응답 받는다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param returnClass 응답받고자 하는 Class
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    @SuppressWarnings("unchecked")
    public static <T> ResponseObject<List<T>> postResponseTransfer(ExternalUrlInfo apiInfo, Object parameter, Class<T> returnClass) throws Exception {
        return (ResponseObject<List<T>>) getResponseObject(postTransfer(apiInfo, parameter, null, createResponseType(returnClass)));
    }

    /**
     * 외부연동 - METHOD.POST (include. header)
     * POST 방식으로 외부연동을 한다.
     * HEADER 정보를 입력받아서 해당 헤더정보로 통신한다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param httpHeaders 헤더 정보
     * @param returnClass 응답받고자 하는 Class
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    public static <T> T postTransfer(ExternalUrlInfo apiInfo, Object parameter, HttpHeaders httpHeaders, Class<T> returnClass) throws Exception {
        return getResponseObject(postTransfer(apiInfo, parameter, httpHeaders, createType(returnClass)));
    }

    /**
     * 외부연동 - METHOD.POST (include. header / feat. List)
     * POST 방식으로 외부연동을 한다.
     * HEADER 정보를 입력받아서 해당 헤더정보로 통신하며,
     * LIST 로 응답 받는다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param httpHeaders 헤더 정보
     * @param returnClass 응답받고자 하는 Class
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    @SuppressWarnings("unchecked")
    public static <T> List<T> postListTransfer(ExternalUrlInfo apiInfo, Object parameter, HttpHeaders httpHeaders, Class<T> returnClass) throws Exception {
        return (List<T>) getResponseObject(postTransfer(apiInfo, parameter, httpHeaders, createListType(returnClass)));
    }

    /**
     * 외부연동 - METHOD.POST (include. header, List / Feat. ResponseObject)
     * POST 방식으로 외부연동을 한다.
     * HEADER 정보를 입력받아서 해당 헤더정보로 통신하며,
     * ResponseObject<LIST> 로 응답 받는다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param httpHeaders 헤더 정보
     * @param returnClass 응답받고자 하는 Class
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    @SuppressWarnings("unchecked")
    public static <T> ResponseObject<List<T>> postResponseTransfer(ExternalUrlInfo apiInfo, Object parameter, HttpHeaders httpHeaders, Class<T> returnClass) throws Exception {
        return (ResponseObject<List<T>>) getResponseObject(postTransfer(apiInfo, parameter, httpHeaders, createResponseType(returnClass)));
    }

    /**
     * Get 방식 통신 모듈
     * 헤더 정보를 입력받아 해당 헤더 정보로 통신한다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param httpHeaders 헤더정보
     * @param typeReference 응답받고자 하는 클레스의 ParameterizedTypeReference
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    @SuppressWarnings("unchecked")
    private static <T> ResponseObject<T> getTransfer(ExternalUrlInfo apiInfo, Object parameter, HttpHeaders httpHeaders, ParameterizedTypeReference<ResponseObject<T>> typeReference) throws Exception {
        if(httpHeaders == null){
            httpHeaders = RequestUtils.createHttpHeaders();
        }
        ResponseObject<T> responseObject = new ResponseObject<>();
        try{
            if(parameter == null) {
                responseObject = resourceClient.getForResponseObject( apiInfo.getApiId(), apiInfo.getUri(), httpHeaders, typeReference );
            } else if(parameter instanceof List){
                responseObject = resourceClient.getForResponseObject( apiInfo.getApiId(), apiInfo.getUri() + RequestUtils.getParameter((List<SetParameter>) parameter), httpHeaders, typeReference );
            } else {
                responseObject = resourceClient.getForResponseObject( apiInfo.getApiId(), apiInfo.getUri().concat("/").concat(ObjectUtils.toString(parameter)), httpHeaders, typeReference );
            }

        } catch (ResourceClientException rce) {
            log.error("getTransfer Module Error ::: {}", rce.getMessage());
            if (responseObject.getReturnStatus() == 0) {
                responseObject.setReturnStatus(rce.getHttpStatus());
            }
            return (ResponseObject<T>) getResponseObject(responseObject);
        }
        return responseObject;
//        throw new LogicException(ExceptionCode.SYS_ERROR_CODE_9001);
    }

    /**
     * POST 방식 통신 모듈
     * 헤더 정보를 입력받아 해당 헤더 정보로 통신한다.
     *
     * @param apiInfo 외부서버정보(apiId, uri)
     * @param parameter RequestData
     * @param httpHeaders 헤더정보
     * @param typeReference 응답받고자 하는 클레스의 ParameterizedTypeReference
     * @return 외부연동 응답 값
     * @throws Exception 오류시 오류에 대한 코드, 메시지 리턴.
     */
    @SuppressWarnings("unchecked")
    private static <T> ResponseObject<T> postTransfer(ExternalUrlInfo apiInfo, Object parameter, HttpHeaders httpHeaders, ParameterizedTypeReference<ResponseObject<T>> typeReference) throws Exception {
        try{
            if(httpHeaders == null){
                httpHeaders = RequestUtils.createHttpHeaders();
            }
            if(parameter instanceof List){
                return resourceClient.postForResponseObject( apiInfo.getApiId(), null, apiInfo.getUri() + RequestUtils.getParameter((List<SetParameter>) parameter), httpHeaders, typeReference );
            }
            return resourceClient.postForResponseObject( apiInfo.getApiId(), parameter, apiInfo.getUri(), httpHeaders, typeReference );
        } catch (ResourceClientException rce) {
            rce.printStackTrace();
        }
        throw new Exception("시스템 에러");
    }

    private static <T> ParameterizedTypeReference<ResponseObject<T>> createType(Class<T> returnClass){
        ResolvableType resolvableType = ResolvableType.forClassWithGenerics(ResponseObject.class, returnClass);
        return ParameterizedTypeReference.forType(resolvableType.getType());
    }

    private static <T> ParameterizedTypeReference<ResponseObject<T>> createListType(Class<T> returnClass){
        ResolvableType resolvableListType = ResolvableType.forClassWithGenerics(List.class, returnClass);
        ResolvableType resolvableType = ResolvableType.forClassWithGenerics(ResponseObject.class, resolvableListType);
        return ParameterizedTypeReference.forType(resolvableType.getType());
    }

    private static <T> ParameterizedTypeReference<ResponseObject<T>> createResponseType(Class<T> returnClass){
        ResolvableType resolvableListType = ResolvableType.forClassWithGenerics(List.class, returnClass);
        ResolvableType resolvableResponseType = ResolvableType.forClassWithGenerics(ResponseObject.class, resolvableListType);
        ResolvableType resolvableType = ResolvableType.forClassWithGenerics(ResponseObject.class, resolvableResponseType);
        return ParameterizedTypeReference.forType(resolvableType.getType());
    }

    private static <T> T getResponseObject(ResponseObject<T> responseObject) throws Exception {
        // responseObject Null Check
        assert responseObject != null;
        log.debug("{} :: {} :: {} :: {}", responseObject.getReturnCode(), responseObject.getReturnMessage(), responseObject.getData(), responseObject.getReturnStatus());
        // httpStatus 가 200이 아닌 경우
        if(responseObject.getReturnStatus() != 200){
            throw new Exception("시스템 에러");
        }
        // Return Code Check
        if(responseObject.getReturnCode().equalsIgnoreCase("0000") || responseObject.getReturnCode().equalsIgnoreCase("SUCCESS")) {
            return responseObject.getData();
        }
        // 정상이 아닐 경우 오류 처리.
        throw new Exception(MessageFormat.format("요청실패 [ReturnCode : {0} ::: ReturnMessage : {1}]", responseObject.getReturnCode(), responseObject.getReturnMessage()));
    }

    private static List<SetParameter> convertDtoToSetParameter(Object dto) throws Exception {
        List<SetParameter> resultList = new ArrayList<>();
        SqlUtils.getConvertMap(dto).forEach((key, value) -> resultList.add(SetParameter.create(key, value)));
        return resultList;
    }

}
