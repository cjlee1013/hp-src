package kr.co.homeplus.mileagebatch.management.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import kr.co.homeplus.mileagebatch.enums.ExternalUrlInfo;
import kr.co.homeplus.mileagebatch.enums.MileageCommonCode;
import kr.co.homeplus.mileagebatch.management.model.common.PaymentResultDto;
import kr.co.homeplus.mileagebatch.management.model.payment.MileageSaveDto;
import kr.co.homeplus.mileagebatch.management.model.payment.MileageUseDto;
import kr.co.homeplus.mileagebatch.management.model.payment.ReturnMileageSetDto;
import kr.co.homeplus.mileagebatch.message.model.UserInfoDto;
import kr.co.homeplus.mileagebatch.utils.ExternalUtil;
import kr.co.homeplus.mileagebatch.utils.MessageSendUtil;
import kr.co.homeplus.mileagebatch.utils.ObjectUtils;
import kr.co.homeplus.mileagebatch.utils.SetParameter;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ProcessManagementService {
    // Mileage Save/Return/Expire Process Service
    private final ProcessService processService;
    // Mapper Management Service
    private final ManagementMapperService mapperService;

    /**
     * 마일리지 적립 프로세스
     * 마일리지 적립대상 조회 후
     * 해당 정보를 For 문으로 반복하면서 마일리지 적립을 수행.
     *
     * @param mileageReqNo 마일리지 적립대상 마일리지 지급/회수 관리번호.
     * @return CompletableFuture<PaymentResultDto> Async 로 수행 결과를 리턴.
     * @throws Exception 오류 발생시 오류 처리.
     */
    @Async
    public CompletableFuture<PaymentResultDto> mileageSaveProcessing(long mileageReqNo) throws Exception {
        PaymentResultDto paymentResult = new PaymentResultDto();

        // 업데이트를 수행할 정보 Map
        HashMap<String, Object> updateMap;

        // 마일리지 적립 대상 조회 수만큼 반복.
        for(MileageSaveDto saveDto : mapperService.getMileageSaveInfoList(mileageReqNo)){
            // 마일리지 지급/회수 요청 회원정보 업데이트 맵.
            updateMap = new HashMap<>();
            // 마일리지 적립결과 코드
            String returnCode;
            try {
                // 마일리지 적립 수행.
                returnCode = processService.setSaveMileage(saveDto);

                // 마일리지 적립 결과에 따른 분기 처리.
                // 결과가 "00" 이면 성공으로 처리.
                if(returnCode.equals("00")){
                    // 성공에 대한 결과 DTO 내의 successDto 에 건 수와 금액 추가.
                    paymentResult.successDto.totalAddByData((int) saveDto.getMileageAmt());
                    // 마일리지 지급/회수 대상회원 상태를 성공으로 변경.
                    updateMap.put("mileageDetailStatus", MileageCommonCode.MILEAGE_REQUEST_DETAIL_STATUS_COMPLETE.getResponseCode());
                }else{
                    // 실패에 대한 결과 DTO 내의 failDto 에 건 수와 금액 추가.
                    paymentResult.failDto.totalAddByData((int) saveDto.getMileageAmt());
                    // 마일리지 지급/회수 대상회원 상태를 실패으로 변경.
                    updateMap.put("mileageDetailStatus", MileageCommonCode.MILEAGE_REQUEST_DETAIL_STATUS_FAIL.getResponseCode());
                }
            } catch (Exception e) {
                // 오류 발생시 해당 건은 실패로 처리.
                paymentResult.failDto.totalAddByData((int) saveDto.getMileageAmt());
                throw new Exception(e.getMessage());
            }

            // 마일리지 지급/회수 요청 회원정보 업데이트 Key
            updateMap.put("mileageReqDetailNo", saveDto.getMileageReqDetailNo());
            // 마일리지 지급/회수 요청 회원정보 수정아이디
            updateMap.put("chgId", "BATCH");
            // 적립결과 코드.
            updateMap.put("detailCode", returnCode);

            //마일리지 상세 정보 업데이트
            log.info("마일리지 상세 정보 업데이트 결과 :: {}", mapperService.modifyMileageReqUserInfo(updateMap));

            //MAP 초기화
            updateMap.clear();
        }

        return CompletableFuture.completedFuture(paymentResult);
    }

    /**
     * 마일리지 회수 프로세스
     * 마일리지 회수대상 조회 후
     * 해당 정보를 For 문으로 반복하면서 마일리지 회수 수행.
     *
     * @param mileageReqNo 마일리지 회수대상 마일리지 지급/회수 관리번호.
     * @return CompletableFuture<PaymentResultDto> Async 로 수행 결과를 리턴.
     * @throws Exception 오류 발생시 오류 처리.
     */
    @Async
    public CompletableFuture<PaymentResultDto> mileageReturnProcessing(long mileageReqNo) throws Exception{
        PaymentResultDto paymentResult = new PaymentResultDto();

        //상세요청 업데이트를 위한 HashMap
        HashMap<String, Object> updateMap = new HashMap<>();

        for(ReturnMileageSetDto returnDto : mapperService.getMileageReturnInfo(mileageReqNo)){
            String returnCode;
            try {
                // 마일리지 적립 수행.
                returnCode = processService.setMileageReturn(returnDto);

                // 마일리지 적립 결과에 따른 분기 처리.
                // 결과가 "00" 이면 성공으로 처리.
                if(returnCode.equals("00")){
                    // 성공에 대한 결과 DTO 내의 successDto 에 건 수와 금액 추가.
                    paymentResult.successDto.totalAddByData((int) returnDto.getRequestReturnAmt());
                    // 마일리지 지급/회수 대상회원 상태를 성공으로 변경.
                    updateMap.put("mileageDetailStatus", MileageCommonCode.MILEAGE_REQUEST_DETAIL_STATUS_COMPLETE.getResponseCode());
                }else{
                    // 실패에 대한 결과 DTO 내의 failDto 에 건 수와 금액 추가.
                    paymentResult.failDto.totalAddByData((int) returnDto.getRequestReturnAmt());
                    // 마일리지 지급/회수 대상회원 상태를 실패으로 변경.
                    updateMap.put("mileageDetailStatus", MileageCommonCode.MILEAGE_REQUEST_DETAIL_STATUS_FAIL.getResponseCode());
                }
            } catch (Exception e) {
                // 오류 발생시 해당 건은 실패로 처리.
                paymentResult.failDto.totalAddByData((int) returnDto.getRequestReturnAmt());
                throw new Exception(e.getMessage());
            }

            //mileage_req_detail update Key
            updateMap.put("mileageReqDetailNo", returnDto.getMileageReqDetailNo());
            //업데이트
            updateMap.put("chgId", "BATCH");
            //상세코드 업데이트
            updateMap.put("detailCode", returnCode);

            //마일리지 상세 정보 업데이트
            log.info("마일리지 상세 정보 업데이트 결과 :: {}", mapperService.modifyMileageReqUserInfo(updateMap));
            //MAP 초기화
            updateMap.clear();
        }

        return CompletableFuture.completedFuture(paymentResult);
    }

    @Async
    public CompletableFuture<Boolean> mileageExpireProcessing(String executeDay) throws Exception{
        Boolean result = Boolean.FALSE;
        try {
            List<MileageUseDto> mileageUseDtoList = mapperService.getMileageExpireInfoList(executeDay);
            log.info("마일리지 회수 ({})일 기준 ({})건 처리", executeDay, mileageUseDtoList.size());
            for(MileageUseDto mileageUseDto : mileageUseDtoList){
                log.info("마일리지 회수 정보 :: mileageUseDto :: ({})", mileageUseDto);
                result = processService.setMileageExpire(mileageUseDto);
            }
        } catch (Exception e){
            log.error("마일리지 회수 오류 :: {}", e.getMessage());
        }

        return CompletableFuture.completedFuture(result);
    }

    public boolean mileageExpireExpected(LinkedHashMap<String, Object> paramMap) throws Exception {
        Boolean result = Boolean.FALSE;
        try {
            return MessageSendUtil.sendMileageExpireMessage(paramMap);
        } catch (Exception e){
            log.error("2주소멸 알림톡 발송 오류 :: {}", e.getMessage());
            return Boolean.FALSE;
        }
    }

    public UserInfoDto getUserMobileInfo(long userNo) throws Exception {
        try {
            return ExternalUtil.getTransfer(ExternalUrlInfo.USER_INFO, new ArrayList<SetParameter>(){{ add(SetParameter.create("userNo", userNo)); }}, UserInfoDto.class);
        } catch (Exception e) {
            log.info("getUserMobileInfo ::: userNo: {} error : {}", userNo, e.getMessage());
            throw new Exception("사용자 휴대폰번호 취득에 실패하였습니다.");
        }
    }

}
