package kr.co.homeplus.mileagebatch.management.mapper;

import java.util.HashMap;
import java.util.List;
import kr.co.homeplus.mileagebatch.core.db.annotation.MasterConnection;
import kr.co.homeplus.mileagebatch.management.model.payment.MileageManageDto;
import kr.co.homeplus.mileagebatch.management.model.payment.MileageSaveDto;
import kr.co.homeplus.mileagebatch.management.model.common.MileageHistoryManageSetDto;
import kr.co.homeplus.mileagebatch.management.model.payment.MileageUseDto;
import kr.co.homeplus.mileagebatch.management.model.payment.ReturnMileageSetDto;
import kr.co.homeplus.mileagebatch.management.sql.MileageHistorySql;
import kr.co.homeplus.mileagebatch.management.sql.MileageMstSql;
import kr.co.homeplus.mileagebatch.management.sql.MileagePaymentSql;
import kr.co.homeplus.mileagebatch.management.sql.MileageSaveSql;
import kr.co.homeplus.mileagebatch.management.sql.MileageUseSql;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Options.FlushCachePolicy;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;

@MasterConnection
public interface ManagementWriteMapper {

    //============================================================================================================
    // 마일리지 지급/회수 관련 Method
    //============================================================================================================
    // 마일리지 요청 상태 업데이트
    @UpdateProvider(type = MileagePaymentSql.class, method = "updateMileageReqStatus")
    int updateMileageReqStatus(HashMap<String, Object> parameterMap);

    // 마일리지 지급/회수 요청 사용자 정보 업데이트
    @UpdateProvider(type = MileagePaymentSql.class, method = "updateMileageReqUserStatus")
    int updateMileageReqUserStatus(HashMap<String, Object> parameterMap);
    //============================================================================================================

    //============================================================================================================
    // 마일리지 적립 관련 Method
    //============================================================================================================
    // 마일리지 적립 대상자 조회
    @ResultType(MileageSaveDto.class)
    @SelectProvider(type = MileagePaymentSql.class, method = "selectMakeMileageSaveInfo")
    List<MileageSaveDto> selectMakeMileageSaveInfo(@Param("mileageReqNo") long mileageReqNo);

    // 마일리지 적립 테이블 저장.
    @Options(useGeneratedKeys = true, keyProperty = "mileageSaveNo")
    @InsertProvider(type = MileageSaveSql.class, method = "insertUserMileageSave")
    int insertMileageSaveInfo(MileageSaveDto mileageSaveDto);

    // 마일리지 마스터 원장 업데이트
    @UpdateProvider(type = MileageMstSql.class, method = "updateUserMileageMst")
    int updateUserMileageMst(@Param("userNo") String userNo, @Param("mileageAmt") long mileageAmt);

    //마일리지 적립 내용 되살리기
    @UpdateProvider(type = MileageSaveSql.class, method = "updateUserMileageSaveUsed")
    int updateUserMileageSaveUsed(MileageSaveDto mileageSaveDto);
    //============================================================================================================

    //============================================================================================================
    // 마일리지 회수/소멸 관련 Method
    //============================================================================================================
    // 마일리지 회수 대상자 조회
    @SelectProvider(type = MileagePaymentSql.class, method = "selectMakeMileageReturnInfo")
    List<ReturnMileageSetDto> selectMakeMileageReturnInfo(@Param("mileageReqNo") long mileageReqNo);

    // 마일리지 사용정보 저장.
    @Options(useGeneratedKeys = true, keyProperty = "mileageUseNo")
    @InsertProvider(type = MileageUseSql.class, method = "insertUserMileageUse")
    int insertUserMileageUse(MileageUseDto mileageUseDto);

    // 마일리지 관리 정보 저장
    @Options(useCache = false, flushCache = FlushCachePolicy.TRUE)
    @InsertProvider(type = MileageUseSql.class, method = "insertMileageManageInfo")
    int insertMileageManageInfo(MileageManageDto mileageManageDto);
    //============================================================================================================

    //============================================================================================================
    // 마일리지 히스토리 관련 Method
    //============================================================================================================
    //마일리지 사용/적립 히스토리.
    @Options(useGeneratedKeys = false)
    @InsertProvider(type = MileageHistorySql.class, method = "insertUserMileageHistory")
    int insertUserMileageHistory(MileageHistoryManageSetDto mileageHistoryManageSetDto);
    //============================================================================================================

}
