package kr.co.homeplus.mileagebatch.management.model.coupon;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "마일리지 쿠폰 등록 요청 DTO")
public class MileageCouponRequestDto {
    @ApiModelProperty(value = "제목(쿠폰명)", position = 1)
    private String couponNm;
    @ApiModelProperty(value = "사용여부", position = 2)
    private String useYn;
    @ApiModelProperty(value = "발급시작일", position = 3)
    private String issueStartDt;
    @ApiModelProperty(value = "발급종료일", position = 4)
    private String issueEndDt;
    @ApiModelProperty(value = "난수종류(ONE:일회성,MUL:다회성)", position = 5)
    private String couponType;
    @ApiModelProperty(value = "발급수량", position = 6)
    private long issueQty;
    @ApiModelProperty(value = "지급마일리지금액", position = 7)
    private long mileageAmt;
    @ApiModelProperty(value = "인당발급타입(D:일별,P:기간별)", position = 8)
    private String personalIssueType;
    @ApiModelProperty(value = "인당발급수량", position = 9)
    private long personalIssueQty;
    @ApiModelProperty(value = "마일리지타입번호", position = 10)
    private String mileageTypeNo;
    @ApiModelProperty(value = "고객노출문구", position = 11)
    private String displayMessage;
    @ApiModelProperty(value = "요청ID", position = 12)
    private String requestId;
    @ApiModelProperty(value = "쿠폰번호", position = 13)
    private String couponNo;
    @ApiModelProperty(value = "쿠폰관리번호", hidden = true)
    private long couponManageNo;

    public void setCouponNo(String couponNo) {
        if(couponNo == null){
            this.couponNo = null;
        } else if(!couponNo.startsWith("#")){
            this.couponNo = "#".concat(couponNo.toUpperCase());
        } else {
            this.couponNo = couponNo;
        }
    }
}
