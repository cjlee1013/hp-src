package kr.co.homeplus.mileagebatch.constants;

import java.text.MessageFormat;
import kr.co.homeplus.mileagebatch.utils.EntityFactory;
import kr.co.homeplus.mileagebatch.utils.SqlUtils;

public class SqlPatternConstants {

    public static final String SUM = "SUM({0})";
    public static final String MAX = "MAX({0})";
    public static final String IFNULL = "IFNULL({0}, {1})";
    public static final String DATE_FULL_24 = "DATE_FORMAT({0}, ''%Y-%m-%d HH:mi:ss'')";
    public static final String DATE_FULL_12 = "DATE_FORMAT({0}, ''%Y-%m-%d hh:mi:ss'')";
    public static final String DATE_FULL_ZERO = "DATE_FORMAT({0}, ''%Y-%m-%d 00:00:00'')";
    public static final String DATE_FULL_LAST = "DATE_FORMAT({0}, ''%Y-%m-%d 23:59:59'')";
    public static final String DATE_INPUT = "DATE_FORMAT({0}, ''{1}'')";
    public static final String DATE_NOW_24 = MessageFormat.format(DATE_FULL_24, "NOW()");
    public static final String DATE_NOW_12 = MessageFormat.format(DATE_FULL_12, "NOW()");
    public static final String DATE_NOW_INPUT = "DATE_FORMAT(NOW(), ''{0}'')";
    public static final String ADD_DATE = "ADDDATE({0}, {1})";
    public static final String ADD_DATE_MIN = "ADDDATE({0}, INTERVAL {1} MINUTE)";
    public static final String ADD_DATE_SEC = "ADDDATE({0}, INTERVAL {1} SECOND)";
    public static final String ADD_DATE_YEAR = "ADDDATE({0}, INTERVAL {1} YEAR)";
    public static final String COUNT = "COUNT({0})";
    public static final String AND = " AND {0}";
    public static final String ON = "{0} ON {1}";
    public static final String OR = " OR {0}";
    public static final String CASE = "CASE {0}";
    public static final String CASE_WHEN = "CASE WHEN ({0}) THEN ({1}) ELSE ({2}) END";
    public static final String WHEN = " WHEN {0} THEN {1}";
    public static final String ELSE = " ELSE {0} END";
    public static final String LIKE = " {0} LIKE ''%{1}%''";
    public static final String NOW = "(NOW())";


    /**
     * 입력받은 값을 Pattern 으로 맵핑.
     *  입력받은 object 들을 입력받은 pattern Type 에 맵핑.
     *
     * @param type Pattern 유형
     * @param objects Pattern 에 맵핑할 정보배열
     * @return String Pattern 에 정보를 맵핑한 결과.
     */
    public static String getConvertPattern(String type, Object... objects){
        // MessageFormat 을 사용하여 정보 맵핑.
        return MessageFormat.format(type, objects);
    }

    /**
     * 유형 선택으로 쿼리 생성.
     *
     * @param type 사용하고자하는 유형
     * @param objects 맵핑정보 배열
     * @return String 선택한 유형의 쿼리
     */
    public static String getConditionByPattern(String type, Object... objects){
        return getConditionByPatternWithAlias(type, null, objects);
    }

    /**
     * 유형 선택으로 쿼리 생성.(With alias)
     *
     * @param type 사용하고자하는 유형
     * @param alias 추가할 Alias.
     * @param objects 맵핑정보 배열
     * @return String 선택한 유형의 쿼리
     */
    public static String getConditionByPatternWithAlias(String type, String alias, Object... objects){
        // 회신할 쿼리 정보를 담을 StringBuilder
        StringBuilder returnCondition = new StringBuilder();
        // 현재 Class 의 Pattern 정보를 수집하기 위한 instance 생성.
        Object instance = EntityFactory.newInstance(SqlPatternConstants.class);
        // 선택한 Pattern 정보를 담을 변수
        String selectedPattern = "";
        try {
            // 생성 instance 정보를 활용하여
            // Class 내의 Field 정보 중 사용하고자 하는 타입을 찾아서
            // 해당 Field 의 Value 를 가지고 온다.
            selectedPattern = (String) SqlPatternConstants.class.getDeclaredField(type.toUpperCase()).get(instance);
        } catch (Exception e){
            e.printStackTrace();
            selectedPattern = null;
        }

        // 선택한 유형의 pattern 정보가 있으면
        // 해당 pattern 에 정보를 주입한다.
        if(selectedPattern != null){
            //MessageFormat 으로 정보 주입.
            returnCondition.append(getConvertPattern(selectedPattern, objects));

            // Alias 가 Null 아니면
            // 맵핑된 Pattern 정보 뒤에 Alias 추가.
            if(alias != null){
                returnCondition.append(SqlUtils.wrapWithSpace("AS"));
                returnCondition.append(alias);
            }
        }

        return returnCondition.toString();
    }
}
