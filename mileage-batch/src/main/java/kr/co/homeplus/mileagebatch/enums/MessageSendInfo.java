package kr.co.homeplus.mileagebatch.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum MessageSendInfo {

    MILEAGE_EXPIRE_MESSAGE("T11118", "S11118", "마일리지 소멸 예정 안내", "소멸마일리지|소멸일 YYYY-MM-DD", "mileage_amt|expire_dt"),
    ;

    @Getter
    private final String templateCode;

    @Getter
    private final String switchingTemplateCode;

    @Getter
    private final String sendTitle;

    @Getter
    private final String sendParams;

    @Getter
    private final String mappingParams;
}
