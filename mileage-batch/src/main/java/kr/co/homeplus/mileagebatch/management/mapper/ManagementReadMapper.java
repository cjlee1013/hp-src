package kr.co.homeplus.mileagebatch.management.mapper;

import java.util.LinkedHashMap;
import java.util.List;
import kr.co.homeplus.mileagebatch.core.db.annotation.SlaveConnection;
import kr.co.homeplus.mileagebatch.management.model.payment.MileageSaveDto;
import kr.co.homeplus.mileagebatch.management.model.payment.MileageUseDto;
import kr.co.homeplus.mileagebatch.management.model.user.MileageMstDto;
import kr.co.homeplus.mileagebatch.management.sql.MileageMstSql;
import kr.co.homeplus.mileagebatch.management.sql.MileagePaymentSql;
import kr.co.homeplus.mileagebatch.management.sql.MileageSaveSql;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.mapping.StatementType;

@SlaveConnection
public interface ManagementReadMapper {

    //============================================================================================================
    // 마일리지 지급/회수 관련 Method
    //============================================================================================================
    // 마일리지 지급/회수 대상 건 조회.
    @SelectProvider(type = MileagePaymentSql.class, method = "selectMileageReqNoList")
    List<Long> selectMileageReqNoList(@Param("executeDay") String executeDay, @Param("requestType") String requestType);

    // 마일리지 회원 여부 확인 조회.
    @SelectProvider(type = MileageMstSql.class, method = "selectUserMileageMstCheck")
    int selectUserMileageMstCheck(String userNo);

    //mileage_mst 회원정보 데이터
    @ResultType(value = MileageMstDto.class)
    @SelectProvider(type = MileageMstSql.class, method = "selectUserMileageMstInfo")
    MileageMstDto selectUserMileageMstInfo(String userNo);

    @Options(statementType = StatementType.CALLABLE)
    @SelectProvider(type = MileageSaveSql.class, method = "selectUserMileageUsableList")
    List<MileageSaveDto> selectUserMileageUsableList(@Param("userNo") String userNo, @Param("requestAmt") long requestAmt);
    //============================================================================================================

    //============================================================================================================
    // 마일리지 소멸 관련 Method
    //============================================================================================================
    @SelectProvider(type = MileageSaveSql.class, method = "selectMakeMileageExpireInfo")
    List<MileageUseDto> selectMakeMileageExpireInfo(@Param("executeDay") String executeDay);

    @SelectProvider(type = MileageSaveSql.class, method = "selectMileageExpirePossibleList")
    List<MileageSaveDto> selectMileageExpirePossibleList(@Param("userNo") String userNo, @Param("tradeNo") long tradeNo);

    @SelectProvider(type = MileageSaveSql.class, method = "selectMileageExpireUserInfo")
    List<LinkedHashMap<String, Object>> selectMileageExpireUserInfo(@Param("executeDay") String executeDay);
    //============================================================================================================
}
