package kr.co.homeplus.mileagebatch.management.service;

import java.util.List;
import kr.co.homeplus.mileagebatch.enums.MileageCommonCode;
import kr.co.homeplus.mileagebatch.management.model.common.MileageHistoryManageSetDto;
import kr.co.homeplus.mileagebatch.management.model.payment.MileageManageDto;
import kr.co.homeplus.mileagebatch.management.model.payment.MileageSaveDto;
import kr.co.homeplus.mileagebatch.management.model.payment.MileageUseDto;
import kr.co.homeplus.mileagebatch.management.model.payment.ReturnMileageSetDto;
import kr.co.homeplus.mileagebatch.management.model.user.MileageMstDto;
import kr.co.homeplus.mileagebatch.utils.MileageCommonUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class ProcessService {
    // Mapper Management Service
    private final ManagementMapperService mapperService;

    /**
     * 마일리지 적립 로직.
     * 마일리지 적립 프로세스 안에 수행되지만
     * 로직 정리를 위해 별도로 method 처리.
     *
     * @param mileageSaveDto 마일리지 적립 Dto
     * @return Boolean 적립 성공/실패
     * @throws Exception 예외가 발생할 경우 던짐
     */
    @Transactional(propagation = Propagation.NESTED)
    public String setSaveMileage(MileageSaveDto mileageSaveDto) throws Exception{

        //1. 마일리지 적립 대상자가 Master에 있는지 확인
        //mileagebatch 마스터DB에 데이터가 없는 경우 오류.
        //회원가입시 반드시 마일리지 DB에 해당 유저의 마일리지 정보가 있어야 함.
        if (!mapperService.checkUserMileageMst(mileageSaveDto.getUserNo())) {
            return "02";
        }

        //2. 마일리지 적립 대상자 정보로 mileage_save 저장
        //update실패시 오류
        if (!mapperService.addMileageSaveInfo(mileageSaveDto)){
            log.error("마일리지 적립 저장 실패 [{}]", mileageSaveDto);
            throw new Exception("마일리지 적립실패");
        }

        //마일리지 마스터 DB업데이트
        if (!mapperService.modifyMileageMstInfo(mileageSaveDto.getUserNo(), mileageSaveDto.getMileageAmt())) {
            log.info("마일리지 적립 후 마스터 원장 업데이트 실패 :: [{} : {}]", mileageSaveDto.getUserNo(), mileageSaveDto.getMileageAmt());
            throw new Exception("마일리지 적립실패");
        }

        // 마일리지 적립 내역을 마일리지 히스토리에 저장하기 위한 DTO 생성.
        MileageHistoryManageSetDto historySetDto = MileageHistoryManageSetDto.builder()
            .userNo(mileageSaveDto.getUserNo())
            .mileageCategory(mileageSaveDto.getMileageCategory())
            .mileageKind(mileageSaveDto.getMileageKind())
            .mileageNo(Long.parseLong(mileageSaveDto.getMileageSaveNo()))
            .mileageAmt(mileageSaveDto.getMileageAmt())
            .build();

        // 마일리지 히스토리에 적립 내역 저장.
        if (!mapperService.addMileageHistoryInfo(historySetDto)) {
            log.error("마일리지 히스토리 저장 실패 {}", historySetDto);
            throw new Exception("마일리지 적립실패");
        }

        return "00";
    }

    /**
     * 마일리지 회수
     * @param returnMileageSetDto 마일리지 최수 DTO
     * @return ResponseObject<MileageMstDto>
     * @throws Exception 예외가 발생할 경우 던짐
     */
    @Transactional(propagation = Propagation.NESTED)
    public String setMileageReturn(ReturnMileageSetDto returnMileageSetDto) throws Exception{

        String returnCode = "00";

        // point_mst 에 회원 정보가 있는지 확인
        if(!mapperService.checkUserMileageMst(returnMileageSetDto.getUserNo())){
            //회원없음으로 리턴.
            return "02";
        }

        //사용요청금액 확인을 위한 마일리지 마스터 정보 습득(회원번호)
        MileageMstDto mileageMstDto = mapperService.getUserMileageMstInfo(returnMileageSetDto.getUserNo());

        // 사용자의 최종마일리지 금액 >= 사용요청금액 여부 확인
        // 회수는 고객의 마일리지에 대한 차감을 목적이므로, 사용자의 금액이 요청금액보다 작을 경우에는 회수요청에 오류업데이트
        // 로직상에 오류없이 고객의 현재 최종금액을 사용처리 금액으로 전환하여 프로세스를 진행한다.
        if(mileageMstDto.getMileageAmt() < returnMileageSetDto.getRequestReturnAmt()){
            // 금액 치환.
            returnMileageSetDto.setRequestReturnAmt(mileageMstDto.getMileageAmt());
            // 회수금액부족
            if(mileageMstDto.getMileageAmt() == 0){
                //금액없음으로 리턴.
                return "03";
            }
            returnCode = "06";
        }

        //mileage_use 에 회수로 사용기록.
        //마일리지 사용 Entity 생성.
        MileageUseDto mileageUseDto = MileageCommonUtil.makeUseMileageEntity(returnMileageSetDto);
        //마일리지 사용처리.
        this.mileageUsableProcess(mileageUseDto);

        // 마일리지 마스터 DB에 마일리지 원복
        // 사용처리를 위해 최종금액만 -1 처리
        if (!mapperService.modifyMileageMstInfo(mileageUseDto.getUserNo(), (mileageUseDto.getMileageAmt() * -1))) {
            log.info("마일리지 적립 후 마스터 원장 업데이트 실패 :: [{} : {}]", mileageUseDto.getUserNo(), mileageUseDto.getMileageAmt());
            throw new Exception("마일리지 회수 실패");
        }

        // 마일리지 적립 내역을 마일리지 히스토리에 저장하기 위한 DTO 생성.
        MileageHistoryManageSetDto historySetDto = MileageHistoryManageSetDto.builder()
            .userNo(mileageUseDto.getUserNo())
            .mileageCategory(mileageUseDto.getMileageCategory())
            .mileageKind(mileageUseDto.getMileageKind())
            .mileageNo(Long.parseLong(mileageUseDto.getMileageUseNo()))
            .mileageAmt(mileageUseDto.getMileageAmt())
            .build();

        // mileage_history에 해당 기록 적재.
        if(!mapperService.addMileageHistoryInfo(historySetDto)){
            log.error("마일리지 히스토리 저장 실패 {}", historySetDto);
            throw new Exception("마일리지 회수실패");
        }

        return returnCode;
    }

    @Transactional(propagation = Propagation.NESTED)
    public Boolean setMileageExpire(MileageUseDto mileageUseDto) throws Exception{
        //마일리지 사용처리.
        this.mileageUsableProcess(mileageUseDto);
        // point_mst 에 회원 정보가 있는지 확인
        if(!mapperService.checkUserMileageMst(mileageUseDto.getUserNo())){
            //회원없음으로 리턴.
            // 마일리지 마스터 DB에 마일리지 원복
            // 사용처리를 위해 최종금액만 -1 처리
            throw new Exception("마일리지 회수 실패");
        }

        if (!mapperService.modifyMileageMstInfo(mileageUseDto.getUserNo(), (mileageUseDto.getMileageAmt() * -1))) {
            log.info("마일리지 적립 후 마스터 원장 업데이트 실패 :: [{} : {}]", mileageUseDto.getUserNo(), mileageUseDto.getMileageAmt());
            throw new Exception("마일리지 회수 실패");
        }

        // 마일리지 적립 내역을 마일리지 히스토리에 저장하기 위한 DTO 생성.
        MileageHistoryManageSetDto historySetDto = MileageHistoryManageSetDto.builder()
            .userNo(mileageUseDto.getUserNo())
            .mileageCategory(mileageUseDto.getMileageCategory())
            .mileageKind(mileageUseDto.getMileageKind())
            .mileageNo(Long.parseLong(mileageUseDto.getMileageUseNo()))
            .mileageAmt(mileageUseDto.getMileageAmt())
            .build();

        // mileage_history 에 해당 기록 적재.
        if(!mapperService.addMileageHistoryInfo(historySetDto)){
            log.error("마일리지 히스토리 저장 실패 {}", historySetDto);
            throw new Exception("마일리지 회수실패");
        }

        return Boolean.TRUE;
    }

    /**
     * 마일리지 사용처리.
     * @param mileageUseDto 마일리지 사용 Entity
     * @throws Exception 오류 발생시 오류 처리. Exception
     */
    private void mileageUsableProcess(MileageUseDto mileageUseDto) throws Exception{
        // 사용요청처리한 금액에 해당하는 금액 건수만큼 적립내역 취득
        List<MileageSaveDto> pointUsableList;

        if(mileageUseDto.getMileageCategory().equals(MileageCommonCode.MILEAGE_CATEGORY_EXPIRE.getResponseCode())){
            // 소멸처리
            pointUsableList = mapperService.getMileageExpirePossibleInfo(mileageUseDto.getUserNo(), Long.parseLong(mileageUseDto.getTradeNo()));
        } else {
            pointUsableList = mapperService.getUserMileageUsableList(mileageUseDto.getUserNo(), mileageUseDto.getMileageAmt());
        }

        if(pointUsableList.size() == 0){
            log.error("사용가능한 마일리지가 없습니다. [ {} :: {} ]", mileageUseDto.getUserNo(), mileageUseDto.getMileageAmt());
            throw new Exception("사용가능한 마일리지가 없습니다.");
        }

        //마일리지 사용 처리 만들기.
        if(!mapperService.addUserMileageUse(mileageUseDto)){
            log.error("마일리지 사용처리 실패. [ {} :: {} ]", mileageUseDto.getUserNo(), mileageUseDto.getMileageAmt());
            throw new Exception("마일리지 회수 처리 실패.");
        }

        log.info("mileageUsableProcess pointUsableList [{}]", pointUsableList);
        log.info("mileageUsableProcess [{}]", mileageUseDto);

        //사용할 금액 처리 변수
        long recalculationReqAmt = mileageUseDto.getMileageAmt();

        // 마일리지 적립 mileage_remain_amt 사용처리(loop)
        for (MileageSaveDto subtractInfoDto : pointUsableList) {
            //변경아이디 설정
            subtractInfoDto.setChgId(mileageUseDto.getChgId());

            //적립테이블 마일리지 사용처리 업데이트
            if(recalculationReqAmt < subtractInfoDto.getMileageRemainAmt()){
                subtractInfoDto.setMileageRemainAmt(recalculationReqAmt);
            }

            MileageManageDto manageDto =  MileageManageDto.builder()
                .mileageTypeNo(subtractInfoDto.getMileageTypeNo())
                .userNo(mileageUseDto.getUserNo())
                .mileageUseNo(NumberUtils.toLong(mileageUseDto.getMileageUseNo()))
                .mileageSaveNo(NumberUtils.toLong(subtractInfoDto.getMileageSaveNo()))
                .mileageAmt(subtractInfoDto.getMileageRemainAmt())
                .cancelRemainAmt(0)
                .expireDt(subtractInfoDto.getExpireDt())
                .cancelYn("Y")
                .regId(mileageUseDto.getRegId())
                .chgId(mileageUseDto.getChgId())
                .build();

            //마일리지 관리 정보 저장.
            if(!mapperService.addMileageManageInfo(manageDto)){
                log.error("마일리지 관리 정보 저장 실패 :: [{}]", manageDto);
                throw new Exception("마일리지 관리에 정보를 저장하지 못하였습니다..");
            }

            // 금액 사용 처리
            if (!mapperService.modifyMileageSaveInfo(subtractInfoDto)) {
                log.error("마일리지 적립 사용처리 실패 :: [{}]", subtractInfoDto);
                throw new Exception("마일리지 적립 사용처리 실패");
            }
            // 사용해야할 금액
            recalculationReqAmt -= subtractInfoDto.getMileageRemainAmt();
        }
    }
}
