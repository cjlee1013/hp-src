package kr.co.homeplus.mileagebatch.entity;

public class MileageUseEntity {

    // 마일리지적립번호
    public String mileageUseNo;

    // 회원관리번호
    public String userNo;

    // 거래번호
    public String tradeNo;

    // 마일리지구분
    public String mileageCategory;

    // 마일리지종류
    public String mileageKind;

    // 마일리지금액
    public String mileageAmt;

    // 사용메시지
    public String useMessage;

    // 등록일
    public String regDt;

    // 등록아이디
    public String regId;

    // 수정일
    public String chgDt;

    // 수정아이디
    public String chgId;

}
