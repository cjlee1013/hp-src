package kr.co.homeplus.mileagebatch.utils;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import kr.co.homeplus.mileagebatch.constants.Constants;
import kr.co.homeplus.mileagebatch.constants.SqlPatternConstants;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

@Slf4j
public class MileageSqlUtils {

    public static String getDataFormat(String dateColumn){
        return getDataFormat(dateColumn, null, "%Y-%m-%d 00:00:00", Boolean.TRUE, Boolean.FALSE);
    }

    public static String getDataFormat(String dateColumn, String subDateColumn, String format, boolean nullCheckable, boolean subUsable){
        String[] conditions = new String[]{dateColumn, format};
        if(nullCheckable){
            if(subUsable){
                conditions[0] = getIfNull(dateColumn, subDateColumn);
            }else{
                conditions[0] = getIfNull(dateColumn, "NOW()");
            }
        }

        if(format == null){
            conditions[1] = "%Y-%m-%d HH:mi:ss";
        }

        return MessageFormat.format(
                SqlPatternConstants.DATE_INPUT,
                conditions[0],
                conditions[1]
           );
    }

    /**
     * SQL 생성용!
     * IFNULL 쿼리를 만든다.
     * @param src Null Check 기본값
     * @param nullable Null 일 경우 사용할 값
     * @return String IFNULL 쿼리.
     */
    public static String getIfNull(String src, String nullable){
        return MessageFormat.format(SqlPatternConstants.IFNULL, src, nullable);
    }

    public static String getCaseWhenCondition(String condition, String trueCondition, String elseCondition){
        return MessageFormat.format(SqlPatternConstants.CASE_WHEN, condition, trueCondition, elseCondition);
    }

    /**
     * SQL 생성용.
     * String[]의 underscore 문자열을 camelCase로 변경
     *
     * @param parameters 컬럼배열 (String[])
     * @return String[]
     */
    public static String[] covertParamToCamelCase(String... parameters){
        List<String> resultList = new ArrayList<>();
        for(String param : parameters){
            resultList.add(convertUnderscoreToCamelCase(param));
        }
        return resultList.toArray(String[]::new);
    }

    /**
     * SQL 생성용!
     * String[]의 underscore 문자열을 camelCase 변환 후
     * insert 에 맞게 수정.(dateType -> now());
     *
     * @param parameters 컬럼배열 (String[])
     * @return String[]
     */
    public static String[] convertInsertParamToCamelCase(String... parameters){
        List<String> insertParamList = new ArrayList<>();
        for(String param : covertParamToCamelCase(parameters)){
            if(param.contains("regDt") || param.contains("chgDt")){
                insertParamList.add("now()");
            } else {
                insertParamList.add(param);
            }
        }
        return insertParamList.toArray(String[]::new);
    }

    public static String[] convertInsertIntoValue(Object... values){
        List<String> insertParamList = new ArrayList<>();
        for(Object object : values){
            if(object instanceof String){
                if(!String.valueOf(object).startsWith("(")){
                    insertParamList.add(appendSingleQuoteFromValue(object));
                } else {
                    insertParamList.add(String.valueOf(object));
                }
            } else {
                insertParamList.add(String.valueOf(object));
            }
        }
        return insertParamList.toArray(String[]::new);
    }

    /**
     * SQL 생성용!
     * 단일컬럼에 대한 WHERE or And or OR 에 필요한 쿼리 생성.
     * @param column 변환하고자하는 컬럼명
     * @return String 쿼리문(예 : user_id -> user_id = ${userId}
     */
    public static String equalColumnMapping(String column){
        StringBuilder condition = new StringBuilder();
        condition.append(column);
        condition.append(wrapWithSpace(Constants.EQUAL));
        if(isSnakeCase(column)){
            condition.append(convertUnderscoreToCamelCase(column));
        }else{
            condition.append(convertColumnToMapper(column));
        }
        return condition.toString();
    }

    /**
     * SQL 생성용
     * 입력받은 Value 의 타입에 따라 Quote 여부를 결정하여 리턴.
     * @param column 조건 컬럼
     * @param value 입력값
     * @return String 입력값에 따른 quote 조건식( column = value or column = 'value' )
     */
    public static String equalConditionByInputValue(String column, Object value){
        if(value instanceof String){
            return equalColumnByInput(column, value);
        } else {
            return conditionByInputValueNonSingleQuote(column, value);
        }
    }

    /**
     * SQL 생성용!
     * 직접입력용 쿼리 생성.
     *
     * @param column 사용하고자하는 DB 컬럼
     * @param value 사용하고자하는 데이터 값
     * @return String
     */
    public static String conditionByInputValueNonSingleQuote(String column, Object value){
        return column
            + wrapWithSpace(Constants.EQUAL)
            + value;
    }

    /**
     * SQL 생성용!
     * 직접입력용 쿼리 생성.(Single Quote)
     *
     * @param column 사용하고자하는 DB 컬럼
     * @param value 사용하고자하는 데이터 값
     * @return String
     */
    public static String equalColumnByInput(String column, Object value){
        return column
            + wrapWithSpace(Constants.EQUAL)
            + appendSingleQuoteFromValue(value);
    }

    /**
     * SQL 생성용.
     * 특정 컬럼과 instance 컬럼의 동일 값을 찾아
     * Equal Condition 을 생성함.
     *
     * @param baseColumn 찾고자하는 컬럼.
     * @param instance 매치하고자하는 컬럼의 instance
     * @return String baseColumn = instance.baseColumn
     */
    public static String equalConditionClassCompare(String baseColumn, Object instance){
        return baseColumn
            + wrapWithSpace(Constants.EQUAL)
            + (String) EntityFactory.getFieldValue(baseColumn, instance);
    }

    /**
     * SQL 생성용.
     * Value 에 SingleQuote 를 Append.
     * @param value 입력값
     * @return String SingleQuote 가 추가된 Value.
     */
    public static String appendSingleQuoteFromValue(Object value){
        return Constants.SINGLE_QUOTE
            + value
            + Constants.SINGLE_QUOTE;
    }

    /**
     * SQL 생성용!
     * 특정 데이터를 지정해서 검색해야하는 경우 사용한다.
     *
     * @param value 입력 데이터
     * @param alias Alias 명.
     * @return String '입력 데이터' AS alias
     */
    public static String appendSingleQuoteWithAlias(Object value, String alias){
        return appendAliasToRowColumn(appendSingleQuoteFromValue(value), aliasCheckFromColumn(alias));
    }

    /**
     * SQL 생성용
     * underscore 로 이뤄진 Str 를
     * CamelCase 로 변환한다.
     *
     * @param column 사용하고자하는 컬럼명
     * @return String
     */
    public static String convertUnderscoreToCamelCase(String column){
        //컬럼명에 Alias 가 있는지 확인.
        //있으면 Alias 제거 후 컬럼명 리턴.
        //없으면 기존 컬럼명 리턴
        return convertColumnToMapper(snakeCaseToCamelCase(aliasCheckFromColumn(column)));
    }

    public static String convertColumnToMapper(String column){
        StringBuilder whereCondition = new StringBuilder();
        whereCondition.append(aliasCheckFromColumn(column));
        whereCondition.append(Constants.CURLY_BRACKET_END);
        whereCondition.insert(0, "#".concat(Constants.CURLY_BRACKET_START));
        return whereCondition.toString();
    }

    /**
     * SQL 생성용!
     * Snake Case 를 Camel Case 표기법으로 변경.
     *
     * @param snakeCase CamelCase 으로 변경할
     * @return String CamelCase.
     */
    public static String snakeCaseToCamelCase(String snakeCase){
        StringBuilder camelCase = new StringBuilder();
        for(String param : snakeCase.split("_")){
            if(camelCase.length() == 0){
                camelCase.append(param);
            }else{
                camelCase.append(StringUtils.capitalize(param));
            }
        }
        return camelCase.toString();
    }

    /**
     * SQL 생성용!
     * camelCase 를 snakeCase 로 변경한다.
     * @param camelCase camelCase 문자열
     * @return String snakeCase 문자열
     */
    public static String convertCamelCaseToSnakeCase(String camelCase){
        return convertCamelCaseToSnakeCase(camelCase, null);
    }

    /**
     * SQL 생성용!
     * camelCase 를 snakeCase 로 변경하면서
     * 입력받은 tableName 의 alias 를 추가한다.
     *
     * @param camelCase camelCase 문자열
     * @param tableName alias 생성용 tableName
     * @return String alias.snakeCase 문자열
     */
    public static String convertCamelCaseToSnakeCase(String camelCase, String tableName){
        StringBuilder convertCondition = new StringBuilder();
        // alias check
        camelCase = aliasCheckFromColumn(camelCase);

        for(String snakeCase : StringUtils.splitByCharacterTypeCamelCase(camelCase)){
            if(convertCondition.length() != 0){
                convertCondition.append(Constants.UNDERLINE);
            }
            convertCondition.append(snakeCase.toLowerCase());
        }
        if(!StringUtils.isEmpty(tableName)){
            convertCondition.insert(0, makeAliasFromTableName(tableName).concat(Constants.DOT));
        }
        return convertCondition.toString();
    }

    /**
     * SQL 생성용!
     * 업데이트 SET 조건문에 연산이 필요한경우.
     *
     * @param column 사용하고자하는 DB 컬럼
     * @param operator 사용하고자하는 연산자
     * @return String 업데이트 연산자 쿼리문.
     */
    public static String conditionForUpdateCalculation(String column, String operator){
        return column
            + wrapWithSpace(Constants.EQUAL)
            + column
            + wrapWithSpace(operator)
            + convertUnderscoreToCamelCase(column);
    }

    /**
     * SQL 생성용!
     * update 쿼리문 생성시 연산자를 추가하여 생성.
     *
     * @param column 기준 컬럼
     * @param value 연산할 Value
     * @param operator 연산자.
     * @return String 업데이트 쿼리문
     */
    public static String conditionValueForUpdateCalculation(String column, Object value, String operator){
        StringBuilder condition = new StringBuilder();
        condition.append(column);
        condition.append(Constants.EQUAL);
        condition.append(column);
        condition.append(wrapWithSpace(operator));
        if(value instanceof String){
            condition.append(appendSingleQuoteFromValue(value));
        } else {
            condition.append(value);
        }
        return condition.toString();
    }

    /**
     * SQL 생성용!
     * tableName 으로부터 Alias 를 생성한 뒤
     * 입력받은 TableName 에 Alias 를 추가하여 리턴.
     * @param tableName 테이블명.
     * @return String 입력받은 테이블명 + Alias
     */
    public static String getAppendAliasToTableName(String tableName){
        return tableName + Constants.SPACE + makeAliasFromTableName(tableName);
    }

    /**
     * SQL 생성용!
     * 입력받은 Alias 를 입력받은 TableName 에 추가하여 리턴.
     * @param tableName 테이블명.
     * @param alias 별칭.
     * @return String 입력받은 Alias + 입력받은 테이블 명 (예: tableName alias)
     */
    public static String getAppendAliasToTableName(String tableName, String alias){
        return tableName + Constants.SPACE + alias;
    }

    /**
     * SQL 생성용!
     * tableName 으로부터 Alias 를 생성한다.
     * @param tableName 테이블명
     * @return String 생성한 Alias
     */
    public static String makeAliasFromTableName(String tableName){
        StringBuilder tableNameAliasing = new StringBuilder();

        for(String param : tableName.split("_")){
            tableNameAliasing.append(param.toCharArray()[0]);
        }

        return tableNameAliasing.toString();
    }

    /**
     * SQL 생성용!
     * 컬럼명 앞에 alias 를 추가한다.
     *
     * @param columnArray alias 를 추가할 컬럼 배열
     * @param alias 추가할 alias
     * @return String[] alias 가 추가된 컬럼의 배열
     */
    public static String[] appendingAliasToColumnArray(String[] columnArray, String alias){
        List<String> returnColumns = new ArrayList<>();
        for(String column : columnArray){
            returnColumns.add(appendAliasToColumn(column, alias));
        }
        return returnColumns.toArray(String[]::new);
    }

    /**
     * SQL 생성용!
     * 컬럼배열 내 컬럼 앞에
     * tableName 으로 생성한 alias 를 추가한다.
     *
     * @param columnArray Alias 를 추가할 컬럼 배열
     * @param tableName Alias 를 생성할 tableName
     * @return String[] Alias 가 추가된 컬럼 배열
     */
    public static String[] appendingAliasToColumnArrayWithTableName(String[] columnArray, String tableName){
        List<String> returnColumns = new ArrayList<>();
        String alias = makeAliasFromTableName(tableName);
        for(String column : columnArray){
            returnColumns.add(appendAliasToColumn(column, alias));
        }
        return returnColumns.toArray(String[]::new);
    }

    /**
     * SQL 생성용!
     * 입력받은 alias 를 입력받은 Column 앞에 추가한다.
     * @param column 입력받은 컬럼
     * @param alias 입력받은 별칭
     * @return String 입력받은 별칭 + 입력받은 컬럼 (예: alias.column)
     */
    public static String appendAliasToColumn(String column, String alias){
        return alias + Constants.DOT + column;
    }

    /**
     * SQL 생성용!
     * 입력받은 TableName 으로부터 Alias 를 생성하여 컬럼앞에 추가한다.
     * @param column alias 를 추가할 컬럼명
     * @param tableName alias 을 생성할 테이블명.
     * @return String 테이블명으로 생성된 Alias + column
     */
    public static String appendAliasToColumnWithTableName(String column, String tableName){
        return makeAliasFromTableName(tableName) + Constants.DOT + column;
    }

    /**
     * SQL 생성용!
     * BETWEEN 문을 생성한다.
     * @param standard 기준값
     * @param condition1 조건1
     * @param condition2 조건2
     * @return String 기준값 BETWEEN 조건1 AND 조건2
     */
    public static String conditionBetween(String standard, String condition1, String condition2){
        return standard
            + wrapWithSpace("BETWEEN")
            + condition1
            + wrapWithSpace("AND")
            + condition2;
    }

    /**
     * SQL 생성용!
     * 특정 operator 연산용.
     *
     * @param operator 사용하고자하는 연산자
     * @param standard 기준값
     * @param condition 조건
     * @return 기준값 Operator 조건
     */
    public static String customOperatorCondition(String operator, String standard, String condition){
        return standard
            + wrapWithSpace(operator)
            + condition;
    }

    /**
     * SQL 생성용!
     * Procedure Call 조건문을 생성한.
     * @param procedureName Procedure 명
     * @param parameter Procedure 에 입력한 조건등.
     * @return String Procedure 호출가능한 조건문.
     */
    public static String getConditionForProcedure(String procedureName, Object... parameter){
        return Constants.CURLY_BRACKET_START
            + wrapWithSpace("CALL")
            + procedureName
            + wrapWithSpace(Constants.ROUND_BRACKET_START)
            + String.join(",", convertInsertIntoValue(parameter))
            + wrapWithSpace(Constants.ROUND_BRACKET_END)
            + wrapWithSpace(Constants.CURLY_BRACKET_END);
    }

    //연산자 포멧(SPACE)
    public static String wrapWithSpace(String operator) {
        return Constants.SPACE + operator + Constants.SPACE;
    }

    /**
     * 컬럼에 Alias 가 제거.
     * 컬럼에 alias 가 있는 경우 alias 제거 후 컬럼명만 리턴.
     *
     * @param column 확인할 컬럼
     * @return String alias 가 제거된 컬럼.
     */
    private static String aliasCheckFromColumn(String column){
        return column.contains(Constants.DOT) ? column.split(Constants.SPLIT_DOT)[1] : column;
    }

    /**
     * SQL 생성용!
     * Row 단위 쿼리에 대한 Alias 부여
     * @param column alias 를 부여할 컬럼
     * @param alias 컬럼에 부여한 alias
     * @return String alias 가 부여된 컬럼정보.
     */
    public static String appendAliasToRowColumn(String column, String alias){
        return column + wrapWithSpace("AS") + aliasCheckFromColumn(alias);
    }

    /**
     * SQL 생성용!
     * INNER JOIN 을 생성한다.
     *
     * @param tableName INNER JOIN 할 테이블.
     * @param conditionForEqual 동일조건으로 비교할 컬럼정보
     * @param conditionForAnd 특정입력값 조건식.
     * @return String INNER JOIN 쿼리를 생성하다.
     */
    public static String createJoinCondition(String tableName, List<Object> conditionForEqual,  Object[] conditionForAnd){
        StringBuilder innerJoinCondition = new StringBuilder();
        // tableName + on + 기본조건.
        if(conditionForEqual != null){
            for(Object equalObject : conditionForEqual){
                if(innerJoinCondition.length() == 0){
                    innerJoinCondition.append(equalObject.toString());
                } else {
                    innerJoinCondition.append(MessageFormat.format(SqlPatternConstants.AND, equalObject));
                }
            }
        }

        if(conditionForAnd != null){
            for(Object andObject : conditionForAnd){
                if(innerJoinCondition.length() == 0){
                    innerJoinCondition.append(andObject.toString());
                } else {
                    innerJoinCondition.append(MessageFormat.format(SqlPatternConstants.AND, andObject));
                }
            }
        }

        innerJoinCondition.insert(0, tableName + wrapWithSpace("ON"));

        return innerJoinCondition.toString();
    }

    /**
     * Class instance 를 활용하여
     * columns 의 column 명과 instance 내의 column 을 비교하여
     * 같은 값이 있으면 리턴.
     *
     * @param columns 찾고자 하는 컬럼 집합
     * @param instance 비교 하고자 하는 Class instance
     * @return Object[] 비교하여 생성한 Condition 의 집함.
     */
    public static Object[] onClause(Object[] columns, Object instance){
        List<Object> returnObject = new ArrayList<>();
        for(Object column : columns){
            returnObject.add(equalConditionClassCompare(String.valueOf(column), instance));
        }
        return returnObject.toArray();
    }

    /**
     * snakeCase 인지 확인.
     * 문자열에 UnderScore 가 존재하는 경우
     * SnakeCase 로 판단하여 리턴.
     *
     * @param value 확인하고자하는 값
     * @return Boolean snakeCase 여부.
     */
    private static Boolean isSnakeCase(String value){
        return value.contains("_");
    }

    /**
     * camelCase 인지 확인.
     * StringUtils 에서 camelCase 로 split 하게하여
     * length 가 0보다 크면 CamelCase 로 판단하여 리턴.
     *
     * @param value 확인하고자 하는 값
     * @return Boolean camelCase 여부
     */
    private static Boolean isCamelCase(String value){
        return StringUtils.splitByCharacterTypeCamelCase(value).length > 0 ? Boolean.TRUE : Boolean.FALSE;
    }



}
