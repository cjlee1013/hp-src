package kr.co.homeplus.mileagebatch.management.sql;

import kr.co.homeplus.mileagebatch.constants.Constants;
import kr.co.homeplus.mileagebatch.constants.CustomConstants;
import kr.co.homeplus.mileagebatch.constants.SqlPatternConstants;
import kr.co.homeplus.mileagebatch.enums.MysqlFunction;
import kr.co.homeplus.mileagebatch.utils.EntityFactory;
import kr.co.homeplus.mileagebatch.entity.MileageSaveEntity;
import kr.co.homeplus.mileagebatch.management.model.payment.MileageSaveDto;
import kr.co.homeplus.mileagebatch.utils.ObjectUtils;
import kr.co.homeplus.mileagebatch.utils.SqlUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

@Slf4j
public class MileageSaveSql {

    private static final String saveTable = EntityFactory.getTableName(MileageSaveEntity.class);
    private static final MileageSaveEntity saveColumns = EntityFactory.createEntityIntoValue(MileageSaveEntity.class);

    /**
     * 마일리지 적립 내역 저장 쿼리.
     *
     * @param mileageSaveDto 마일리지 적립 DTO
     * @return String 마일리지 적립 Insert Query
     */
    public static String insertUserMileageSave(MileageSaveDto mileageSaveDto){

        SQL query = new SQL()
            // INSERT 할 테이블 정보.
            .INSERT_INTO(saveTable)
            // INSERT 할 컬럼 정보
            .INTO_COLUMNS(EntityFactory.getColumnInfo(MileageSaveEntity.class))
            // INSERT 할 VALUE 정보.
            .INTO_VALUES(SqlUtils.convertInsertParamToCamelCase(EntityFactory.getColumnInfo(MileageSaveEntity.class)));
        return query.toString();
    }

    //CALLABLE
    public static String selectUserMileageUsableList(@Param("userNo") String userNo, @Param("requestAmt") long requestAmt) {
        return SqlUtils.getConditionForProcedure("UP_NT_MILEAGE_USABLE_CHECK", userNo, requestAmt);
    }

    /**
     * 마일리지 적립취소시 적립 마일리지 사용처리
     *
     * @param mileageSaveDto 마일리지 적립 정보 DTO
     * @return String 마일리지 적립 사용처리 쿼리
     */
    public static String updateUserMileageSaveUsed(MileageSaveDto mileageSaveDto){
        SQL query = new SQL()
            .UPDATE(saveTable)
            .SET(
                SqlUtils.conditionValueForUpdateCalculation(saveColumns.mileageRemainAmt, mileageSaveDto.getMileageRemainAmt(), Constants.HYPHEN)
            )
            .SET(SqlUtils.equalColumn(saveColumns.chgDt, "NOW()"))
            .SET(SqlUtils.equalColumnMapping(saveColumns.chgId))
            .WHERE(
                SqlUtils.equalColumnMapping(saveColumns.mileageSaveNo)
            )
            .WHERE(
                SqlUtils.equalColumnMapping(saveColumns.userNo)
            );
        return query.toString();
    }

    /**
     * 마일리지 소멸 가능 리스트 조회.
     *
     * @param executeDay 실행일
     * @return String 마일리지 소멸 리스트 조회 Query
     */
    public static String selectMakeMileageExpireInfo(@Param("executeDay") String executeDay){
        SQL query = new SQL()
            .SELECT(
                saveColumns.userNo,
                SqlUtils.aliasToRow(saveColumns.mileageSaveNo, "trade_no"),
                SqlUtils.appendSingleQuoteWithAlias("05", "mileage_category"),
                SqlUtils.appendSingleQuoteWithAlias("01", "mileage_kind"),
                SqlUtils.aliasToRow(saveColumns.mileageRemainAmt, "mileage_amt"),
                SqlUtils.appendSingleQuoteWithAlias("마일리지의 사용기간 만료에 인한 소멸처리", "use_message"),
                SqlUtils.appendSingleQuoteWithAlias("BATCH", "reg_id"),
                SqlUtils.appendSingleQuoteWithAlias("BATCH", "chg_id")
            )
            .FROM(saveTable)
            .WHERE(
                SqlUtils.customOperator(
                    Constants.ANGLE_BRACKET_LEFT,
                    saveColumns.expireDt,
                    SqlPatternConstants.getConditionByPattern("DATE_FULL_ZERO", SqlUtils.appendSingleQuote(executeDay))
                )
            )
            .WHERE(
                SqlUtils.customOperator(Constants.ANGLE_BRACKET_RIGHT, saveColumns.mileageRemainAmt, "0" )
            );
        log.info("selectMakeMileageExpireInfo \n[{}]", query.toString());
        return query.toString();
    }

    /**
     * 마일리지 소멸 조회
     *
     * @param userNo 고객관리번호
     * @param tradeNo 마일리지 적립 관리번호
     * @return String 마일리지 적립 소멸 Query.
     */
    public static String selectMileageExpirePossibleList(@Param("userNo") String userNo, @Param("tradeNo") long tradeNo) {
        SQL query = new SQL()
            .SELECT(
                saveColumns.mileageSaveNo,
                saveColumns.mileageReqDetailNo,
                saveColumns.mileageTypeNo,
                saveColumns.userNo,
                saveColumns.mileageCategory,
                saveColumns.mileageKind,
                SqlUtils.equalColumn(saveColumns.mileageRemainAmt, "mileage_amt"),
                saveColumns.mileageRemainAmt,
                saveColumns.useStartDt,
                saveColumns.expireDt
            )
            .FROM(saveTable)
            .WHERE(
                SqlUtils.equalColumnMapping(saveColumns.userNo)
            )
            .WHERE(
                SqlUtils.equalColumnByInput(saveColumns.mileageSaveNo, tradeNo)
            )
            .WHERE(
                SqlUtils.customOperator(
                    Constants.ANGLE_BRACKET_RIGHT,
                    saveColumns.mileageRemainAmt,
                    "0"
                )
            );
        return query.toString();
    }

    /**
     * 마일리지 소멸 대상 조회
     *
     * @param executeDay 실행일
     * @return
     */
    public static String selectMileageExpireUserInfo(@Param("executeDay") String executeDay){
        MileageSaveEntity mileageSave = EntityFactory.createEntityIntoValue(MileageSaveEntity.class, Boolean.TRUE);
        SQL query = new SQL()
            .SELECT(
                mileageSave.userNo,
                SqlUtils.sqlFunction(MysqlFunction.SUM, mileageSave.mileageRemainAmt, mileageSave.mileageAmt),
                SqlUtils.sqlFunction(MysqlFunction.DATE_FORMAT, ObjectUtils.toArray(mileageSave.expireDt, CustomConstants.YYYYMMDD), mileageSave.expireDt)
            )
            .FROM(EntityFactory.getTableNameWithAlias(mileageSave))
            .WHERE(
                SqlUtils.equalColumn(
                    mileageSave.expireDt,
                    SqlUtils.sqlFunction(
                        MysqlFunction.DATE_FULL_LAST,
                        SqlUtils.sqlFunction(MysqlFunction.DATEADD_WEEK, ObjectUtils.toArray(SqlUtils.appendSingleQuote(executeDay), "2"))
                    )
                ),
                SqlUtils.customOperator(CustomConstants.ANGLE_BRACKET_RIGHT, mileageSave.mileageRemainAmt, "0")
            )
            .GROUP_BY(mileageSave.userNo)
            ;
        log.debug("selectMileageExpireUserInfo : \n[{}]", query.toString());
        return query.toString();
    }

}
