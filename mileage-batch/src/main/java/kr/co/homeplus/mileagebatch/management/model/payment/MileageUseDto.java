package kr.co.homeplus.mileagebatch.management.model.payment;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MileageUseDto {
    @ApiModelProperty(value = "마일리지 사용번호", position = 1)
    private String mileageUseNo;

    @ApiModelProperty(value = "고객번호", position = 2)
    private String userNo;

    @ApiModelProperty(value = "거래번호", position = 3)
    private String tradeNo;

    @ApiModelProperty(value = "마일리지유형", position = 4)
    private String mileageCategory;

    @ApiModelProperty(value = "마일리지종류", position = 5)
    private String mileageKind;

    @ApiModelProperty(value = "마일리지금액", position = 6)
    private long mileageAmt;

    @ApiModelProperty(value = "사용메시지", position = 7)
    private String useMessage;

    @ApiModelProperty(value = "등록일", position = 8)
    private String regDt;

    @ApiModelProperty(value = "등록아이디", position = 9)
    private String regId;

    @ApiModelProperty(value = "수정일", position = 10)
    private String chgDt;

    @ApiModelProperty(value = "수정아이디", position = 11)
    private String chgId;
}
