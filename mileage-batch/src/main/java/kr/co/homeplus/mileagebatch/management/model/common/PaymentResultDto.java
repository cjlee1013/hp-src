package kr.co.homeplus.mileagebatch.management.model.common;

import lombok.Data;

@Data
public class PaymentResultDto {

    public PaymentSuccessDto successDto;
    public PaymentFailDto failDto;

    public PaymentResultDto(){
        this.successDto = new PaymentSuccessDto();
        this.failDto = new PaymentFailDto();
    }

    @Data
    public static class PaymentSuccessDto{
        public int successCnt;
        public int successAmt;

        public PaymentSuccessDto(){
            this.successAmt = 0;
            this.successCnt = 0;
        }

        public void totalAddByData(int amt){
            this.successCnt++;
            this.successAmt += amt;
        }
    }

    @Data
    public static class PaymentFailDto{
        public int failCnt;
        public int failAmt;

        public PaymentFailDto(){
            this.failAmt = 0;
            this.failCnt = 0;
        }

        public void totalAddByData(int amt){
            this.failCnt++;
            this.failAmt += amt;
        }
    }

}
