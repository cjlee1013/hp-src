package kr.co.homeplus.mileagebatch.message.model;

import lombok.Data;

@Data
public class UserInfoDto {
    private long userNo;
    private String mobile;
}
