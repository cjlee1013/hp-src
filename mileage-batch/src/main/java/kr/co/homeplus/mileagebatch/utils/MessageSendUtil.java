package kr.co.homeplus.mileagebatch.utils;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Locale;
import javax.annotation.PostConstruct;
import kr.co.homeplus.mileagebatch.enums.MessageSendInfo;
import kr.co.homeplus.mileagebatch.message.model.MessageSendTalkGetDto;
import kr.co.homeplus.mileagebatch.message.model.MessageSendTalkSetDto;
import kr.co.homeplus.mileagebatch.message.model.MessageSendTalkSetDto.MessageSendBodyDto;
import kr.co.homeplus.plus.api.support.client.ResourceClient;
import kr.co.homeplus.plus.api.support.client.model.ResponseObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class MessageSendUtil {
    private final ResourceClient resourceClient;
    private static ResourceClient messageClient;

    @PostConstruct
    private void initialize() {
        messageClient = resourceClient;
    }

    public static boolean sendMileageExpireMessage(LinkedHashMap<String, Object> messageMap){
        return sendMessageTalk(MessageSendInfo.MILEAGE_EXPIRE_MESSAGE, messageMap);
    }

    private static boolean sendMessageTalk(MessageSendInfo messageInfo, LinkedHashMap<String, Object> messageMap){
        MessageSendTalkSetDto setDto = createMessageSendAlimTalkDto(messageInfo, messageMap);
        log.info("알림톡 발송 정보 :: {}", setDto);
        try{
            ResponseObject<MessageSendTalkGetDto> responseObject =
                messageClient.postForResponseObject(
                    "message",
                    setDto,
                    "/send/alimtalk/switch",
                    new ParameterizedTypeReference<ResponseObject<MessageSendTalkGetDto>>() {}
                );
            log.info("알림톡 발송결과 :: {} :: {} :: {}", responseObject.getReturnCode(), responseObject.getReturnMessage(), responseObject.getData());
        } catch (Exception e) {
            log.error("Send Message Error ::: {}", e.getMessage());
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    private static MessageSendTalkSetDto createMessageSendAlimTalkDto(MessageSendInfo messageInfo, LinkedHashMap<String, Object> messageMap) {
        return MessageSendTalkSetDto.builder()
            .workName(messageInfo.getSendTitle())
            .description(messageInfo.getSendTitle())
            .regId("SYSTEM")
            .templateCode(messageInfo.getTemplateCode())
            .switchTemplateCode(messageInfo.getSwitchingTemplateCode())
            .bodyArgument(
                new ArrayList<>(){{
                    add(MessageSendBodyDto.builder()
                        .identifier(ObjectUtils.toString(messageMap.get("user_no")))
                        .toToken(ObjectUtils.toString(messageMap.get("ship_mobile_no")))
                        .mappingData(getConvertParameterMap(messageInfo, messageMap))
                        .build());
//                    add(MessageSendBodyDto.builder().identifier(ObjectUtils.toString(messageMap.get("claim_no"))).toToken("010-6556-7978").mappingData(getConvertParameterMap(messageInfo, messageMap)).build());
                }}
            )
            .build();
    }

    private static LinkedHashMap<String, String> getConvertParameterMap(MessageSendInfo messageInfo, LinkedHashMap<String, Object> dataMap) {
        LinkedHashMap<String, String> returnMap = new LinkedHashMap<>();
        String[] sendParams = messageInfo.getSendParams().split("\\|");
        String[] mappingParams = messageInfo.getMappingParams().split("\\|");
        for(int idx = 0; idx < mappingParams.length; idx++){
            if(dataMap.get(mappingParams[idx]) != null){
                returnMap.put(sendParams[idx], getConvertParam(mappingParams[idx], dataMap.get(mappingParams[idx]).toString()));
            }
        }
        return returnMap;
    }

    private static String getConvertParam(String key, String value) {
        if(key.contains("amt")){
            return NumberFormat.getInstance(Locale.KOREA).format(Long.parseLong(value));
        } else if(key.contains("claim_type")){
            return value.equals("C") ? "취소" : value.equals("R") ? "반품" : "교환";
        } else if(key.contains("qty")) {
            return String.valueOf(value).concat("개");
        } else {
            return value;
        }
    }
}
