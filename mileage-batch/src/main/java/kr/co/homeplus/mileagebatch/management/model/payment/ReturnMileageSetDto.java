package kr.co.homeplus.mileagebatch.management.model.payment;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "마일리지 회수 요청 DTO", value = "ReturnMileageSetDto")
public class ReturnMileageSetDto {

    @ApiModelProperty(value = "고객번호", position = 1)
    private String userNo;

    @ApiModelProperty(value = "마일리지회수구분", position = 2)
    private String requestReturnType;

    @ApiModelProperty(value = "거래번호(회수)", position = 3)
    private String tradeNo;

    @ApiModelProperty(value = "회수요청금액", position = 4)
    private long requestReturnAmt;

    @ApiModelProperty(value = "회수내역 상세메시지", position = 5)
    private String requestReturnMessage;

    @ApiModelProperty(value = "등록아이디", position = 6)
    private String regId;

    @ApiModelProperty(value = "마일리지 지급/회수 상세관리번호", position = 7, hidden = true)
    private String mileageReqDetailNo;

}
