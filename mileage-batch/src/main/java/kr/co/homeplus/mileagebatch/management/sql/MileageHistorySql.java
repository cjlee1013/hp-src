package kr.co.homeplus.mileagebatch.management.sql;

import java.util.Arrays;
import kr.co.homeplus.mileagebatch.entity.MileageHistoryEntity;
import kr.co.homeplus.mileagebatch.utils.EntityFactory;
import kr.co.homeplus.mileagebatch.management.model.common.MileageHistoryManageSetDto;
import kr.co.homeplus.mileagebatch.utils.SqlUtils;
import org.apache.ibatis.jdbc.SQL;

public class MileageHistorySql {

    // 마일리지 히스토리 테이블 명.
    public static final String historyTable = EntityFactory.getTableNameWithAlias(MileageHistoryEntity.class);
    // 마일리지 히스토리 컬럼 정보.
    public static final MileageHistoryEntity historyColumns = EntityFactory.createEntityIntoValue(MileageHistoryEntity.class, Boolean.TRUE);

    /**
     * 마일리지 히스토리 INSERT 쿼리 생성.
     *
     * @param mileageHistoryManageSetDto 마일리지 히스토리 정보 DTO
     * @return 마일리지 히스토리 INSERT Query
     */
    public static String insertUserMileageHistory(MileageHistoryManageSetDto mileageHistoryManageSetDto){
        // 마일리지 히스토리 insert 에 사용할 컬럼정보.
        String[] historyColumn = EntityFactory.getColumnInfo(MileageHistoryEntity.class);

        SQL query = new SQL()
            // INSERT Table 설정.
            .INSERT_INTO(EntityFactory.getTableName(MileageHistoryEntity.class))
            // INSERT 할 컬럼 설정.
            // mileage_history_no 는 AUTO_INCREMENT 를 사용하여 처리하므로 INSERT 시 제외.
            .INTO_COLUMNS(Arrays.copyOfRange(historyColumn, 1, historyColumn.length))
            // INSERT 할 Value 설정.
            // INTO_COLUMN 에 대응할 수 있도록 mileage_history_no 제외.
            .INTO_VALUES(SqlUtils.convertInsertParamToCamelCase(Arrays.copyOfRange(historyColumn, 1, historyColumn.length)));
        return query.toString();
    }
}
