package kr.co.homeplus.mileagebatch.core.runners;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import kr.co.homeplus.mileagebatch.management.service.ManagementService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import picocli.CommandLine.ArgGroup;
import picocli.CommandLine.Command;
import picocli.CommandLine.ExitCode;
import picocli.CommandLine.IExitCodeExceptionMapper;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

import java.util.concurrent.Callable;

@Component
@RequiredArgsConstructor
@Slf4j
@Command(name = "java -jar mileage-batch.jar", mixinStandardHelpOptions = true,
        version = "0.0.1",
        description = "mileage-batch BATCH Execute Command")
public class AppCommand implements Callable<Integer>, IExitCodeExceptionMapper {

    // Mileage Batch Service
    private final ManagementService managementService;

    @ArgGroup(exclusive = true, multiplicity = "1")
    private Exclusive exclusive;

    static class Exclusive {
        @Option(names = {"-n", "--name"}, description = "service name")
        private boolean isName;
    }

    @Parameters(index = "0", paramLabel = "service_name", description = "positional params")
    private String serviceName;

    @Parameters(index = "1", paramLabel = "meta1", description = "add param1", defaultValue = "")
    private String meta1;

    @Parameters(index = "2", paramLabel = "meta2", description = "add param2", defaultValue = "")
    private String meta2;

    @Override
    public Integer call() throws Exception {
        log.info("exclusive.isName {}, service_name {}", exclusive.isName, serviceName);

        if(exclusive.isName) {
            log.info("실행 유형 선택 유무 :: [{}]", exclusive.isName);
            log.info("적립 실행 조건일 :: [{}]", meta1);
            // 실행 시간 확인.
            Date executionTime = new Date();
            log.info("마일리지 배치 수행 시작 :: 시간 [{}] :: 실행 유형 :: [{}]", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(executionTime), serviceName);

            switch (serviceName) {
                case "save":
                    managementService.actMileageSave(meta1);
                    break;
                case "return":
                    managementService.actMileageReturn(meta1);
                    break;
                case "expire":
                    managementService.actMileageExpire(meta1);
                    break;
                case "expected" :
                    managementService.actMileageExpireExpected(meta1);
                    break;
                default:
                    break;
            }
            log.info("마일리지 배치 수행 종료 :: 시간 [{}] :: 실행 유형 :: [{}]", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), serviceName);
            log.info("마일리지 배치 수행 시간 :: 총 [{}] Second", TimeUnit.SECONDS.convert(Math.abs(new Date().getTime() - executionTime.getTime()), TimeUnit.MILLISECONDS));
        }
        return ExitCode.OK;
    }

    @Override
    public int getExitCode(Throwable exception) {
        Throwable cause = exception.getCause();
        if (cause instanceof ArrayIndexOutOfBoundsException) {
            return 12;
        } else if (cause instanceof NumberFormatException) {
            return 13;
        }
        return 11;
    }

}