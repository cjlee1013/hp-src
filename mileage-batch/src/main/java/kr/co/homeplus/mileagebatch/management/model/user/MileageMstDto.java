package kr.co.homeplus.mileagebatch.management.model.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class MileageMstDto {

    @ApiModelProperty(value = "고객번호", position = 1)
    private String userNo;

    @ApiModelProperty(value = "마일리지금액", position = 2)
    private long mileageAmt;
}
