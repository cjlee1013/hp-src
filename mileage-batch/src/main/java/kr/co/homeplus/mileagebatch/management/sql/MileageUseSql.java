package kr.co.homeplus.mileagebatch.management.sql;

import kr.co.homeplus.mileagebatch.constants.SqlPatternConstants;
import kr.co.homeplus.mileagebatch.entity.MileageManageEntity;
import kr.co.homeplus.mileagebatch.entity.MileageUseEntity;
import kr.co.homeplus.mileagebatch.management.model.payment.MileageManageDto;
import kr.co.homeplus.mileagebatch.management.model.payment.MileageUseDto;
import kr.co.homeplus.mileagebatch.utils.EntityFactory;
import kr.co.homeplus.mileagebatch.utils.SqlUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.jdbc.SQL;

@Slf4j
public class MileageUseSql {

    // 마일리지 사용 테이블명
    public final static String useTable = EntityFactory.getTableName(MileageUseEntity.class);
    // 마일리지 관리 테이블명
    public final static String manageTable = EntityFactory.getTableName(MileageManageEntity.class);

    /**
     * 마일리지 사용 등록
     * 
     * @param mileageUseDto 마일리지 사용등록 DTO
     * @return String 마일리지 사용등록 Query
     */
    public static String insertUserMileageUse(MileageUseDto mileageUseDto){
        MileageUseEntity useColumns = EntityFactory.createEntityIntoValue(MileageUseEntity.class, Boolean.FALSE);
        SQL query = new SQL()
            .INSERT_INTO(useTable)
            .INTO_COLUMNS(EntityFactory.getColumnInfo(MileageUseEntity.class))
            .INTO_VALUES(
                SqlUtils.convertInsertIntoValue(
                    mileageUseDto.getMileageUseNo(),
                    mileageUseDto.getUserNo(),
                    mileageUseDto.getTradeNo(),
                    mileageUseDto.getMileageCategory(),
                    mileageUseDto.getMileageKind(),
                    mileageUseDto.getMileageAmt(),
                    mileageUseDto.getUseMessage(),
                    SqlPatternConstants.NOW,
                    mileageUseDto.getRegId(),
                    SqlPatternConstants.NOW,
                    mileageUseDto.getChgId()
                )
            );
        return query.toString();
    }

    /**
     * 마일리지 관리 등록
     *
     * @param mileageManageDto 마일리지 관리 등록 DTO
     * @return String 마일리지 관리 등록 Query.
     */
    public static String insertMileageManageInfo(MileageManageDto mileageManageDto){
        String[] manageColumnInfo = EntityFactory.getColumnInfo(MileageManageEntity.class);
        SQL query = new SQL()
            .INSERT_INTO(manageTable)
            .INTO_COLUMNS(manageColumnInfo)
            .INTO_VALUES(SqlUtils.convertInsertParamToCamelCase(manageColumnInfo));

        return query.toString();
    }
}
